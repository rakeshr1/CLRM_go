<?php
$data = getEndpoint();

$custIdPath =  $data['CUSTOMER_ID_PATH'];
$custIdPath = substr($custIdPath, 1,-1);
$custIdPath = explode(',',$custIdPath);
$custIdPathCount = count($custIdPath);

$custEmailPath =  $data['CUSTOMER_EMAIL_PATH'];
$custEmailPath = substr($custEmailPath, 1,-1);
$custEmailPath = explode(',',$custEmailPath);
$custEmailPathCount = count($custEmailPath);

$endpointURL =  $data['END_POINT'];
$endPointData = curlGetService($endpointURL);
$endPointData = json_decode($test,true);

foreach($endPointData as $eData) {

	$custId = $eData;
	for($i=1; $i<=$custIdPathCount; $i++) {
		$custId = $custId[substr($custIdPath[$i-1], 1,-1)];
	}	
	echo $custId;
	
	$custEmail = $eData;
	for($i=1; $i<=$custEmailPathCount; $i++) {
		$custEmail = $custEmail[substr($custEmailPath[$i-1], 1,-1)];
	}	
	echo $custEmail;
	
}

function getEndpoint() {
  try {
    $db = getConnection();
	$row  = '';
    $sql = 'SELECT * from ENDPOINT WHERE ID=57';
    if ($result = $db->query($sql)) {
      $row = $result->fetch_assoc();
    } else {
      echo '{"message":"No result"}';
    }
	  return $row;
  } catch(Exception $e) {
    echo '{"message":"Please Try Again Later"}'; 
  }
}

function getConnection() {
  $dbhost="192.168.0.222";
  $dbuser="touchpoint";
  $dbpass="touchpoint";
  $dbname="touchpoint_dev";
  $mysqli_conn = new mysqli($dbhost, $dbuser, $dbpass, $dbname);
  if ($mysqli_conn->connect_errno) {
    die("Failed to connect to MySQL: (" . $mysqli_conn->connect_errno . ") " . $mysqli_conn->connect_error);
  }
  return $mysqli_conn;
}

function curlGetService($url) {
    $ch = curl_init();   
    curl_setopt($ch,CURLOPT_URL,$url);
    curl_setopt($ch,CURLOPT_RETURNTRANSFER,true);
 
    $output=curl_exec($ch);
	if($output === false) {
        echo "Error Number:".curl_errno($ch)."<br>";
        echo "Error String:".curl_error($ch);
	}
    curl_close($ch);
    return $output;
}