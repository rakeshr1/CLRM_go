<?php 

/*
  Assigning customer with bank employee
  From Bank API
*/

require_once('lib/db-config.php');
class GetCustomer{

  private $sp_arr = array();
  protected $db   = false;
  
  function __construct($db){
     $this->db = $db;
  }
  
  public function CustomerInfo($serviceData){
  
    array_map( array($this, "split_here") , $serviceData);
	$this->mapping_customer2employee();
	//print_r($this->sp_arr);
	return $this->sp_arr;
	
  }
  
  private function mapping_customer2employee(){
	  
	 // echo "mani";
  
  	 foreach($this->sp_arr as $val_1)
	 {
		 foreach($val_1 as $val_2)
		 { 
			 if($val_2['customer']):
				$emp_list = $this->getEmplyeeList($val_2['bank_id'] , $val_2['branch'] );
				//echo $val_2['bank_id'];
				//echo $val_2['branch'];
				//print_r($emp_list);
				if($emp_list):
					$emp_count = count($emp_list);
				//echo $emp_count;
					$cus_count = count($val_2['customer']);
				//echo $cus_count;
					 $cus_per_emp  = 0;
					 $balance_cus  = 0;
					 
					 if($emp_count <= $cus_count ){
						  $cus_per_emp  =  (int)($cus_count / $emp_count);
						  $balance_cus  =  (int)($cus_count % $emp_count);
					 } 
					 else{
					     $balance_cus   = $cus_count;
					 }
					 $start 	= 0;
					  $sql 	    = "";
					 if($cus_per_emp):
					    
						 foreach($emp_list as $emp){
							$emp_id     = $emp;
							$customers  =   array_slice($val_2['customer'],$start, $cus_per_emp);
							$bid=$val_2['bank_id'];
							$brid=$val_2['branch'];
							//echo $bid;
							//echo $brid;
							$start     +=  $cus_per_emp;	
							
							foreach($customers as $cus){
							//echo $cus;
							   $sql = "INSERT INTO CUSTOMER_EMPLOYEE_MAPPING set EMPLOYEE_ID = $emp ,CUSTOMER_ID = $cus, BRANCH_CODE=$brid ,BANK_CODE=$bid";
							//echo $sql;
							   $result = $this->db->query($sql);
							 // echo $result.'<br>';
						
							} 
							 
						 }
					  endif;
					 if($balance_cus):
					 
						    $emp_list = $this->getEmplyeeListByCount($val_2['bank_id'] , $val_2['branch'] , $balance_cus);
							$customers  =   array_slice($val_2['customer'],$start, $balance_cus);
							$com    =  array_combine($emp_list, $customers );
							foreach($com as $emp_ => $cus_){
							   $sql = "INSERT INTO CUSTOMER_EMPLOYEE_MAPPING set EMPLOYEE_ID = $emp_ ,CUSTOMER_ID = $cus_;";
							  $result = $this->db->query($sql);
							 //echo $result.'<br>';

							 
							} 
							
					 endif;
					
					/*	if ($this->db->multi_query($sql)) {
							echo "Success ".$this->db->affected_rows;
						}	
						else {
							echo $this->db->error;	
						}*/
				else:
					echo "No employee to map..";
				endif; 
			 endif;
			
		  }
	  }
  
  }
 
  private function  split_here( $item ){
     echo "<pre>";
	//print_r($this->sp_arr);
     echo "</pre>";
	 if(isset($this->sp_arr['bank_'.$item['BANK_ID']]['branch_'.$item['BRANCH_ID']])){
	   $this->sp_arr['bank_'.$item['BANK_ID']]['branch_'.$item['BRANCH_ID']]['customer'][] = $item['CUSTOMER_ID'];
	 //  $this->sp_arr['bank_'.$item['BANK_ID']]['branch_'.$item['BRANCH_ID']]['customerInfo'][] = $item;
	    
	  }
	  else{
  	      $this->sp_arr['bank_'.$item['BANK_ID']]['branch_'.$item['BRANCH_ID']]['bank_id'] = $item['BANK_ID'];
   	      $this->sp_arr['bank_'.$item['BANK_ID']]['branch_'.$item['BRANCH_ID']]['branch'] = $item['BRANCH_ID'];	  
  	      $this->sp_arr['bank_'.$item['BANK_ID']]['branch_'.$item['BRANCH_ID']]['customer'][] = $item['CUSTOMER_ID'];
	  }
  }
  
  
   function getEmplyeeList($bank, $branch){
	   
   
   			$sql = "select EMPLOYEE_ID from EMPLOYEE where bank_code = '$bank' and role_id= '4' and branch_code = '$branch' ";
		
		//echo $sql;
			
		
			$emp_id = array();
			if ($result = $this->db->query($sql)) {
			  	while($data = $result->fetch_assoc()) {
				  $emp_id[] = $data['EMPLOYEE_ID']; 
				}
			}	
			else {
				echo $this->db->error;	
			}
			
			return $emp_id;
   
   }
   
   function getEmplyeeListByCount($bank, $branch, $limit){
   
   
  			$sql =  "select e.EMPLOYEE_ID, count(1) as customer_count from EMPLOYEE e "
				   ." left outer join  CUSTOMER_EMPLOYEE_MAPPING cem on e.EMPLOYEE_ID = cem.EMPLOYEE_ID "
				   ."where bank_id = $bank and branch_id = $branch ORDER BY customer_count LIMIT 0, $limit;";
				  
				   
			$emp_id = array();
			if ($result = $this->db->query($sql)) {
			  	while($data = $result->fetch_assoc()) {
				  $emp_id[] = $data['EMPLOYEE_ID']; 
				  //echo $emp_id;
				}
			}	
			else {
				echo $this->db->error;	
			}
   return $emp_id;	
   
   }
  

}

require_once('campaign-functions.php');
   $db = getConnection();		
	$sql = "select * from ENDPOINT";
	
    if ($result = $db->query($sql)) 
	{
        while($data = $result->fetch_assoc()) 
		{
			$bankid = $data['BANK_CODE'];
			//echo $bankid;
			$endpoint  = $data['END_POINT'];
			
			$url = $endpoint ."/bank_prod_based/?no_of_days=12660&bankid=" .$bankid;
			//echo $url;
					
			$serviceData =curlGetService($url);
			//print_r($serviceData );
			 $serviceData = json_decode($serviceData , true);
			 $data = $serviceData->customerdetails;
			  $obj = new GetCustomer(getConnection());
			 $sp_arr = $obj->CustomerInfo($serviceData);
			echo "<pre>";
    print_r($sp_arr);
    echo "</pre>";

		}
		
}
 















   /* require_once('campaign-functions.php');
   $db = getConnection();		
	$sql = "select * from ENDPOINT";
	
	
    if ($result = $db->query($sql)) 
	{
	
        while($data = $result->fetch_assoc()) 
		{
			$bankid = $data['BANK_ID'];
		
			$endpoint  = $data['END_POINT'];
			
			$sql2 = "select BRANCH_CODE from BRANCHES where BANK_ID ='$bankid'";
			
			if ($result2 = $db->query($sql2)) 
			{
				 while($dat = $result2->fetch_assoc()) 
				{
					$branchcode = $dat['BRANCH_CODE'];
					
					//$productid = $dat['PRODUCT_ID'];
					
					$url = $endpoint ."/branch_prod_based/?no_of_days=12660&branchid=" .$branchcode;
					
					$serviceData =curlGetService($url);
					print_r($serviceData );
					 $serviceData = json_decode($serviceData , true);
					  $obj = new GetCustomer(getConnection());
   					 $sp_arr = $obj->CustomerInfo($serviceData);
				}
			}
			
				
			//$serviceData = curlGetService($endpoint);

	 $serviceData =curlGetService("http://123.201.60.252/project/tp/demo_new/bankApi/getdetail.php/branch_prod_based/?no_of_days=12660&bankid=139");
	
	
			//print_r($serviceData);
   
    }
   

   
		}*/
		
	
	
    //while($data = $result->fetch_assoc()) {
    // $end_point[] = $data['END_POINT']; 

    //print_r($end_point);
	
 ?>   