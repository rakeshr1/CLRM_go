<!DOCTYPE html>
<html>
<script src="jquery.min.js"></script>
<body>
Service Type: <br/>
<select onchange="changeView(this.value);" id="serviceType"><option value="REST">REST</option><option value="SOAP">SOAP</option></select><br/>
Endpoint URL : <br/>
<input type="text" id="endpointURL"/><br/>
<div class="soapView">
WSDL URL : <br/>
<input type="text" id="wsdlURL"/><br/>
SOAP Method : <br/>
<input type="text" id="soapMethod"/><br/>
SOAP Parameters : <br/>
<textarea id="soapParams"></textarea><br/>
SOAP Values : <br/>
<textarea id="soapParamValues"></textarea><br/>
Sample XML : 
</div>
<div class="restView">
Sample JSON : 
</div>
<textarea id="pathJSON" onchange="updatedPathJSON();"></textarea><br/>
Customer Id : <div id="custIdNodes"></div><br/>
Customer Email : <div id="custEmailNodes"></div><br/>
Bank Id : <div id="bankIdNodes"></div><br/>
Branch Id : <div id="branchIdNodes"></div><br/>
First Name : <div id="firstNameNodes"></div><br/>
Last Name : <div id="lastNameNodes"></div><br/>
DOJ : <div id="DOJNodes"></div><br/>
Employee Id : <div id="employeeIdNodes"></div><br/>
Customer Mobile No : <div id="mobNoNodes"></div><br/>
Product Id : <div id="productIdNodes"></div><br/>
<script>
var testData;
changeView('REST');
function changeView(x) {
	console.log(x);
	if(x == 'REST') {
		$('.soapView').css({display:'none'});
		$('.restView').css({display:'block'});
	} else {
		$('.soapView').css({display:'block'});
		$('.restView').css({display:'none'});
	}
}
function updatedPathJSON() {
	$('#custIdNodes').html('');
	$('#custEmailNodes').html('');
	$('#bankIdNodes').html('');
	$('#branchIdNodes').html('');
	$('#firstNameNodes').html('');
	$('#lastNameNodes').html('');
	$('#DOJNodes').html('');
	$('#employeeIdNodes').html('');
	$('#mobNoNodes').html('');
	$('#productIdNodes').html('');
	
	serviceType = $('#serviceType').val();
	testData = $('#pathJSON').val();
	if(serviceType == 'SOAP') {
		$.ajax({url: "xmlTOjson.php", data:{xml:testData}, type:'POST', success: function(result){
			testData = JSON.parse(result);
			createSelectBox('', 0, 'custIdNodes');
			createSelectBox('', 0, 'custEmailNodes');
			createSelectBox('', 0, 'bankIdNodes');
			createSelectBox('', 0, 'branchIdNodes');
			createSelectBox('', 0, 'firstNameNodes');
			createSelectBox('', 0, 'lastNameNodes');
			createSelectBox('', 0, 'DOJNodes');
			createSelectBox('', 0, 'employeeIdNodes');
			createSelectBox('', 0, 'mobNoNodes');
			createSelectBox('', 0, 'productIdNodes');
		}});
	} else {
		testData = JSON.parse(testData);
		createSelectBox('', 0, 'custIdNodes');
		createSelectBox('', 0, 'custEmailNodes');
		createSelectBox('', 0, 'bankIdNodes');
		createSelectBox('', 0, 'branchIdNodes');
		createSelectBox('', 0, 'firstNameNodes');
		createSelectBox('', 0, 'lastNameNodes');
		createSelectBox('', 0, 'DOJNodes');
		createSelectBox('', 0, 'employeeIdNodes');
		createSelectBox('', 0, 'mobNoNodes');
		createSelectBox('', 0, 'productIdNodes');
	}
	
	
	
	
}
function createSelectBox(x,y,z) {
		iterateId = $('#'+z+' select[name="endpoint[]"]').length;
		for(i=y+1; i<=iterateId; i++) {
			$('#'+z+' select[data-iterationPropId="'+i+'"]').remove();
		}
		iterateId = $('#'+z+' select[name="endpoint[]"]').length;
		$('#'+z).append('<select name="endpoint[]" data-iterationProp="'+x+'" data-iterationPropId="'+iterateId+'"><option value="">Select Node</option></select>');
		appendOptions(iterateId,z);
}

function appendOptions(y,z) {
	testDataCom = testData;
	if(y == '' || y == 0) {
		for(var propt in testData){
			$('#'+z+' select[data-iterationPropId="'+y+'"]').append('<option value="'+propt+'" onclick=createSelectBox("'+propt+'",'+y+',"'+z+'")>'+propt+'</option>');
		}
	} else {
		for(i=1; i<=y; i++) {
			testDataCom = testDataCom[$('#'+z+' select[data-iterationPropId="'+i+'"]').attr('data-iterationProp')];
		}
		
		if(typeof testDataCom == 'object') {
			for(var propt in testDataCom){
				$('#'+z+' select[data-iterationPropId="'+y+'"]').append('<option value="'+propt+'" onclick=createSelectBox("'+propt+'",'+y+',"'+z+'")>'+propt+'</option>');
			}
		} else {
			$('#'+z+' select[data-iterationPropId="'+y+'"]').remove();
			alert('Node Selected...');
		}
		
	}
}

function submitCust(x) {
	var custPath = [];
	$('#'+x+' select[name="endpoint[]"]').each(function(){
       custPath.push($(this).val());
    });
	custPath = JSON.stringify(custPath);
	return custPath;
}
function getCustPath() {
	custId = submitCust('custIdNodes');
	custEmail = submitCust('custEmailNodes');
	bankId = submitCust('bankIdNodes');
	branchId = submitCust('branchIdNodes');
	fname = submitCust('firstNameNodes');
	lname = submitCust('lastNameNodes');
	doj = submitCust('DOJNodes');
	employeeId = submitCust('employeeIdNodes');
	mobNo = submitCust('mobNoNodes');
	productId = submitCust('productIdNodes');	
	endpointURL = $('#endpointURL').val();
	serviceType = $('#serviceType').val();
	wsdlURL = $('#wsdlURL').val();
	soapMethod = $('#soapMethod').val();
	soapParams = $('#soapParams').val();
	soapParamValues = $('#soapParamValues').val();
	
	$.ajax({url: "http://192.168.0.17/touchpoint/tp/demo_new/api/index.php/addEndPointWithPath/", data:{custId:custId, custEmail:custEmail, bankId:bankId, branchId:branchId, fname:fname, lname:lname, doj:doj, employeeId:employeeId, mobNo:mobNo, productId:productId, endpointURL:endpointURL, serviceType:serviceType, wsdlURL:wsdlURL, soapMethod:soapMethod, soapParams:soapParams, soapParamValues:soapParamValues}, success: function(result){
        alert('Success...')
    }});
	
}
</script>
<button type="button" onclick="getCustPath()">Save</button>
</body>
</html>