<?php
require_once('lib/db-config.php');
if(isset($_REQUEST['r']) && isset($_REQUEST['u'])) {
	updateUserResponse($_REQUEST);
}
function updateUserResponse($data) {
	try {
			$db 			= getConnection();
			$communication = $data['c'];
			$id				=	$data['u'];
			$userResponse	=	$data['r'];		
			
			$sql = "SELECT USER_ACTIONS FROM COMMUNICATION WHERE COMMUNICATION_ID=$communication";
			$table_data = array();
			if ($result = $db->query($sql)) {
				$data = $result->fetch_object();
				$data->USER_ACTIONS = json_decode($data->USER_ACTIONS);
				$userResponseText =$data->USER_ACTIONS[$userResponse]->userActionTitle;
		$userActionEndpoint = $data->USER_ACTIONS[$userResponse]->userActionProtocol."".$data->USER_ACTIONS[$userResponse]->userActionEndpoint;
//echo $userActionEndpoint;
				$isLead = $data->USER_ACTIONS[$userResponse]->isLead;
				$sql = "UPDATE `CUSTOMER_RESPONSE` SET `CUSTOMER_RESPONSE`='$userResponseText', `IS_LEAD`='$isLead', `UPDATED_ON`=NOW() WHERE `UNIQUE_ID`='$id'";
				if($db->query($sql)) {
					echo '{"url":"'.$userActionEndpoint.'"}'; 
				} else {
					//echo $db->error;
					echo "Try Later";
				}
				die;	
			} else {
				echo "Try Later";
				//echo $db->error;
			}						
	} catch(Exception $e) {
		//echo '{"error":{"message":'. $e->getMessage() .'}}'; 
		echo "Try Later";
	}
}