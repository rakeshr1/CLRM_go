<?php
require('crm_url.php');
include ('../wbui/sndgrd.php');
//Authentication

$postfields = "------WebKitFormBoundary7MA4YWxkTrZu0gW\r\nContent-Disposition: form-data; name=\"input_type\"\r\n\r\nJSON\r\n------WebKitFormBoundary7MA4YWxkTrZu0gW\r\nContent-Disposition: form-data; name=\"response_type\"\r\n\r\nJSON\r\n------WebKitFormBoundary7MA4YWxkTrZu0gW\r\nContent-Disposition: form-data; name=\"method\"\r\n\r\nlogin\r\n------WebKitFormBoundary7MA4YWxkTrZu0gW\r\nContent-Disposition: form-data; name=\"rest_data\"\r\n\r\n{\"user_auth\":{\"user_name\":\"$user_name\",\"password\":\"$password\",\"encryption\":\"PLAIN\"},\"application\":\"MyRestAPI\"}\r\n------WebKitFormBoundary7MA4YWxkTrZu0gW--";

$res =curl_convertlead($base_url,$postfields);
$session_id = $res->id;

//Getting all leads which has staus=New from crm api

$postfields ="------WebKitFormBoundary7MA4YWxkTrZu0gW\r\nContent-Disposition: form-data; name=\"input_type\"\r\n\r\nJSON\r\n------WebKitFormBoundary7MA4YWxkTrZu0gW\r\nContent-Disposition: form-data; name=\"response_type\"\r\n\r\nJSON\r\n------WebKitFormBoundary7MA4YWxkTrZu0gW\r\nContent-Disposition: form-data; name=\"method\"\r\n\r\nget_entry_list\r\n------WebKitFormBoundary7MA4YWxkTrZu0gW\r\nContent-Disposition: form-data; name=\"rest_data\"\r\n\r\n{\"session\":\"$session_id\",\"module_name\":\"Leads\",\"query\":\"leads.status = 'New'\",\"order_by\":\"\",\"offset\":\"0\",\"select_fields\":[\"id\",\"name\",\"email1\",\"status\",\"date_entered\",\"phone_mobile\"],\"link_name_to_fields_array\":[],\"max_results\":\"1000\",\"deleted\":\"0\",\"Favorites\":false}\r\n------WebKitFormBoundary7MA4YWxkTrZu0gW--";
$res =curl_convertlead($base_url,$postfields);

$cdate=date('Y-m-d');

foreach ($res->entry_list as $key => $value) {

$lead_id=$value->name_value_list->id->value;
$name=ucfirst($value->name_value_list->name->value);
$email= $value->name_value_list->email1->value;
$status= $value->name_value_list->status->value;
$date_entered= $value->name_value_list->date_entered->value;
$phone_mobile=$value->name_value_list->phone_mobile->value;

// $url = "$host/get_underwriter_email.php?brokeremail=$email";
// $underwriter_email = file_get_contents($url);
// $underwriter_email = json_decode($underwriter_email);
$to=$email;
$toname=$name;
$from = "john.goodloe@ategrity.com";
$fromname = "John E. Goodloe";
$replyto="john.goodloe@ategrity.com";
$subject="Ategrity Specialty Insurance Company";
 $bodyhtml=$name.",
 <p>On behalf of Ategrity Specialty Insurance Company, we are honored and excited to extend to you a brokerage appointment contract.  As you know from our initial conversation, we have purposely limited the number of brokers we would be entertaining business from in order to provide better service and partnership.  Your past support of Ategrity individuals was a major factor in us determining the appointment process.  We are looking forward to not only reestablishing that relationship but also to grow it.  If you succeed, we succeed.  Isn’t that what partnership is all about??</p>
 <p>Please read over the contract and sign at your earliest.  I want to call special attention to the wording you will see on the first page.  We have placed a contingency comment on the contract due to our present position with licensing through the state of Delaware.  As of this date, we have not received the approval and issuance from Delaware on our license.  We do however feel it will be in the very near future.  As you are aware, we will be unable to entertain business until this approval comes through.  It is our full intention to keep you updated on the progress of our licensing and when we can start writing business together.</p>
 <p>When you are ready to sign the appointment contract, please follow the below link and create an account in our Broker Workbench.  Once you have created your account and successfully logged in, you will see a Navigation Pane on the left-hand side of the screen where you can select “Sign Broker Agreement,” and from there our system will walk you through the process.  A fully executed copy of the contract will automatically be generated and stored in your Broker Workbench once you have signed.</p>
 <p><a href='https://ategrity.esinsurancecloud.com/'>Create account to e-sign the appointment contract</a></p>
 <p>If you would like to review a specimen copy of the appointment contract prior to digitally signing the document in your broker workbench, a copy can be downloaded via the following link.</p>
 <p><a href='https://ategrity.esinsurancecloud.com/wbui/BROKER%20AGREEMENT%2006212018%20FOR%20REVIEW.pdf' target='_blank'>Specimen appointment contract</a></p>
<p>Again, many thanks for all you have done for us in the past and what we will be doing in the future.  We cannot wait to get started with you.</p>
<p>Kind regards,</p>
<img src ='https://ategrity-dev.esinsurancecloud.com/wbui/img/ategrity_logo_final.png' width='120px' height='40px'><br/>
<strong>John E. Goodloe</strong><br/>
Chief Underwriting Officer<br/><br/>
<font color='red'>o 480.448.0849  | c 312.450.5500</font><br/><br/>
<strong>Ategrity Specialty Insurance Company</strong><br/>
15990 N. Greenway Hayden Loop, Suite D-160<br/>
Scottsdale, AZ 85260</p>";
$bodytext = $bodyhtml;

  $valcode = sendgridmail ($to, $toname, $from,$fromname,$replyto,$subject, $bodytext, $bodyhtml);
    
  if ($valcode == 0) {
  
 
    $postfields = "------WebKitFormBoundary7MA4YWxkTrZu0gW\r\nContent-Disposition: form-data; name=\"input_type\"\r\n\r\nJSON\r\n------WebKitFormBoundary7MA4YWxkTrZu0gW\r\nContent-Disposition: form-data; name=\"response_type\"\r\n\r\nJSON\r\n------WebKitFormBoundary7MA4YWxkTrZu0gW\r\nContent-Disposition: form-data; name=\"method\"\r\n\r\nset_entry\r\n------WebKitFormBoundary7MA4YWxkTrZu0gW\r\nContent-Disposition: form-data; name=\"rest_data\"\r\n\r\n{\"session\":\"$session_id\",\"module_name\":\"Leads\",\n\"name_value_list\":\n[{\"name\":\"id\",\"value\":\"$lead_id\"},{\"name\":\"status\",\"value\":\"Invited\"},{\"name\":\"invited_dt_c\",\"value\":\"$cdate\"}]\n}\r\n------WebKitFormBoundary7MA4YWxkTrZu0gW--";
    $res =curl_convertlead($base_url,$postfields);

  

  echo "Mail Send...";




}

/*
//sms function twilio
if($phone_mobile!=''){
  //echo $phone_mobile."==>".$email."==>".$date_entered;
  $msgBody ="Hi $name,this is a reminder msg to sign the brokerage aggrement.";
  require('twilio/twilio/Services/Twilio.php'); 
    $account_sid = 'ACd5dbc6bd718ff3b09ab0bb63fd77abce'; 
    $auth_token = '43c519e4cec7007133f1e81aa31c23af'; 
    $client = new Services_Twilio($account_sid, $auth_token); 

    // $client->account->messages->create(array(
    //                      "Body" => $msgBody,
    //                      "From" => "+13126265059",
    //                      "To" => "$phone_mobile",
    //                 ));

}
*/


}


//print_r($arr); 



function curl_convertlead($base_url,$postfields)
{
$curl = curl_init();	
curl_setopt_array($curl, array(
  CURLOPT_URL => $base_url."/service/v4_1/rest.php",
  CURLOPT_RETURNTRANSFER => true,
  CURLOPT_ENCODING => "",
  CURLOPT_MAXREDIRS => 10,
  CURLOPT_TIMEOUT => 30,
  CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
  CURLOPT_CUSTOMREQUEST => "POST",
  CURLOPT_POSTFIELDS => $postfields,
  CURLOPT_HTTPHEADER => array(
    "Cache-Control: no-cache",
    "Postman-Token: c4321aca-3305-4d55-9e7f-5d4f231decc3",
    "content-type: multipart/form-data; boundary=----WebKitFormBoundary7MA4YWxkTrZu0gW"
  ),
));

$response = curl_exec($curl);
$err = curl_error($curl);

curl_close($curl);

if ($err) {
  echo "cURL Error #:" . $err;
} else {
  return json_decode($response);
}
}
