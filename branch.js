var method, id;
id = getParamValeuByName('id');
var app = angular.module('touchpoint', []);

app.controller('tpCtrl', function ($scope, $http, $timeout) {
  $('#loader').css({
    'display': 'none'
  });
  
  $http({
    method: 'GET',
    url: apiURL + '/bankList/',
    params: {
      "type": 'json'
    },
  }).then(function mySucces(response) {
    $scope.banklistoptions = response.data.BANKLIST;
    // console.log(response.data.BANKLIST);
    angular.forEach($scope.banklistoptions, function (task, index) {
      if (task.BANK_CODE == localStorage.bankCode && task.BANK_CODE != 0) {
        $scope.banklistObj = $scope.banklistoptions[index];
        // console.log($scope.banklistObj.BANK_NAME);
        $scope.banklist = $scope.banklistObj.BANK_NAME;
        // console.log($scope.banklist);
      }
    });
  }, function myError(response) {
    $scope.errormess = response.statusText;
  });
  $http({
    method: 'GET',
    url: apiURL + '/countryList/',
    params: {
      "type": 'json'
    },
  }).then(function mySucces(response) {
    $scope.countrylistoptions = response.data.COUNTRYLIST;
  }, function myError(response) {
    $scope.errormess = response.statusText;
  });

  if (id == '') {
    $scope.title = 'Add Branch';
    $scope.alertMessage = 'Branch Added Successfullly';
  } else {
    $scope.title = 'Edit Branch';
    $scope.alertMessage = 'Branch Updated Successfully';
    $http({
      method: 'GET',
      url: apiURL + '/branch/',
      params: {
        "id": id
      },
    }).then(function mySucces(response) {
      console.log(response.data);
      angular.forEach($scope.banklistoptions, function (task, index) {
        if (task.BANK_CODE == response.data.BRANCH_DATA.BANK_CODE && task.BANK_CODE !=0) {
          $scope.banklistObj = $scope.banklistoptions[index];
          // console.log($scope.banklistObj.BANK_NAME);
          $scope.banklist = $scope.banklistObj.BANK_NAME;
          // console.log($scope.banklist);
        }
      });
      angular.forEach($scope.countrylistoptions, function (task, index) {
        if (task.COUNTRY_CODE == response.data.BRANCH_DATA.COUNTRY) {
          $scope.country = $scope.countrylistoptions[index];
        }
      });
      $scope.statetemp = response.data.BRANCH_DATA.STATE;
      $scope.branch_name = response.data.BRANCH_DATA.BRANCH_NAME;
      $scope.branch_code = response.data.BRANCH_DATA.BRANCH_CODE;
      $scope.city = response.data.BRANCH_DATA.CITY;
      $scope.address = response.data.BRANCH_DATA.ADDRESS;
      // console.log(response.data);
      $scope.getStates();
    }, function myError(response) {
      $scope.errormess = response.statusText;
    });
  }
  $scope.getStates = function () {
    $http({
      method: 'GET',
      url: apiURL + '/stateList/',
      params: {
        "country_code": $scope.country.COUNTRY_CODE
      },
    }).then(function mySucces(response) {
      $scope.statelistoptions = response.data.STATELIST;
      angular.forEach($scope.statelistoptions, function (task, index) {
        if (task.STATE_CODE == $scope.statetemp) {
          $scope.state = $scope.statelistoptions[index];
        }
      });
    }, function myError(response) {
      $scope.errormess = response.statusText;
    });

  }
  $scope.validateForm = function () {
    var bankname = $scope.banklist;
    if (bankname == '' || bankname == undefined || bankname == 'null' || bankname == false) {
      $scope.errormess = 'Please Select Bank';
      return false;
    } else {
      if (id == '') {
        method = 'PUT';
      } else {
        method = 'POST';
        // console.log($scope.address);
      }
      
      $http({
        method: method,
        url: apiURL + '/branch/',
        params: {
          "msg": "handshake",
          "bankcode": localStorage.bankCode,
          "id": id,
          "branchname": $scope.branch_name,
          "branchcode": $scope.branch_code,
          "country": $scope.country.COUNTRY_CODE,
          "state": $scope.state.STATE_CODE,
          "city": $scope.city,
          "address": $scope.address
        },
      }).then(function mySucces(response) {
        console.log(response.data);
        if (response.data.message == 'success' || response.data.message == 'Branch Data Updated') {
          $('#alertModelBoxBtn').trigger('click');
          $timeout(function () {
            $scope.manageBranch();
          }, 3000);
        }
      }, function myError(response) {
        //$scope.errormess = response.statusText;
      });
    }
  }
  $scope.manageBranch = function () {
    window.location.href = manage_branch;
  }
});