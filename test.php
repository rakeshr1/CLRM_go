<?php

/* returns the shortened url */
function get_bitly_short_url($url,$login,$appkey,$format='txt') {
	$connectURL = 'http://api.bit.ly/v3/shorten?login='.$login.'&apiKey='.$appkey.'&uri='.urlencode($url).'&format='.$format;
	return curl_get_result($connectURL);
}

/* returns expanded url */
function get_bitly_long_url($url,$login,$appkey,$format='txt') {
	$connectURL = 'http://api.bit.ly/v3/expand?login='.$login.'&apiKey='.$appkey.'&shortUrl='.urlencode($url).'&format='.$format;
	return curl_get_result($connectURL);
}

/* returns a result form url */
function curl_get_result($url) {
	$ch = curl_init();
	$timeout = 5;
	curl_setopt($ch,CURLOPT_URL,$url);
	curl_setopt($ch,CURLOPT_RETURNTRANSFER,1);
	curl_setopt($ch,CURLOPT_CONNECTTIMEOUT,$timeout);
	$data = curl_exec($ch);
	curl_close($ch);
	return $data;
}

/* get the short url */
$short_url = get_bitly_short_url('http://localhost:8080/tpv3/demo_new/thanking.html?c=981&r=0&u=5cb2a8dadea1e6f238674023902abcde2','purush24','R_b124639e14514befa63c19d160884cfa');

/* get the long url from the short one */
$long_url = get_bitly_long_url($short_url,'purush24','R_b124639e14514befa63c19d160884cfa');


echo $short_url."<br>";

echo $long_url;

?>