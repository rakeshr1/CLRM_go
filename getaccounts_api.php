<?php
require('crm_url.php');
//The comments written will be helpfull for understanding suiteCRM API for getting data and insert data.. THe end point will be common for all get and all set.. We will changing the module name for getting Accounts we will use Module_name Accounts for leads we use Module_name Leads for submission we use Opportunities
//Authentication
$postfields = "------WebKitFormBoundary7MA4YWxkTrZu0gW\r\nContent-Disposition: form-data; name=\"input_type\"\r\n\r\nJSON\r\n------WebKitFormBoundary7MA4YWxkTrZu0gW\r\nContent-Disposition: form-data; name=\"response_type\"\r\n\r\nJSON\r\n------WebKitFormBoundary7MA4YWxkTrZu0gW\r\nContent-Disposition: form-data; name=\"method\"\r\n\r\nlogin\r\n------WebKitFormBoundary7MA4YWxkTrZu0gW\r\nContent-Disposition: form-data; name=\"rest_data\"\r\n\r\n{\"user_auth\":{\"user_name\":\"$user_name\",\"password\":\"$password\",\"encryption\":\"PLAIN\"},\"application\":\"MyRestAPI\"}\r\n------WebKitFormBoundary7MA4YWxkTrZu0gW--";
$res =curl_convertlead($base_url,$postfields);
$session_id = $res->id;
//Get leads FOr getting data use method get_entry_list( In Restdata i have give module_name Leads if you want accounts data give it as Accounts For Submission use Opportunities)
$postfields ="------WebKitFormBoundary7MA4YWxkTrZu0gW\r\nContent-Disposition: form-data; name=\"input_type\"\r\n\r\nJSON\r\n------WebKitFormBoundary7MA4YWxkTrZu0gW\r\nContent-Disposition: form-data; name=\"response_type\"\r\n\r\nJSON\r\n------WebKitFormBoundary7MA4YWxkTrZu0gW\r\nContent-Disposition: form-data; name=\"method\"\r\n\r\nget_entry_list\r\n------WebKitFormBoundary7MA4YWxkTrZu0gW\r\nContent-Disposition: form-data; name=\"rest_data\"\r\n\r\n{\"session\":\"$session_id\",\"module_name\":\"Accounts\",\"query\":\"\",\"order_by\":\"\",\"offset\":\"0\",\"select_fields\":[\"id\",\"name\",\"email1\",\"date_entered\",\"phone_office\"],\"link_name_to_fields_array\":[],\"max_results\":\"1000\",\"deleted\":\"0\",\"Favorites\":false}\r\n------WebKitFormBoundary7MA4YWxkTrZu0gW--";
$res =curl_convertlead($base_url,$postfields);
print_r(json_encode($res)); 

function curl_convertlead($base_url,$postfields)
{
$curl = curl_init();    
curl_setopt_array($curl, array(
  CURLOPT_URL => $base_url."/service/v4_1/rest.php",
  CURLOPT_RETURNTRANSFER => true,
  CURLOPT_ENCODING => "",
  CURLOPT_MAXREDIRS => 10,
  CURLOPT_TIMEOUT => 30,
  CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
  CURLOPT_CUSTOMREQUEST => "POST",
  CURLOPT_POSTFIELDS => $postfields,
  CURLOPT_HTTPHEADER => array(
    "Cache-Control: no-cache",
    "Postman-Token: c4321aca-3305-4d55-9e7f-5d4f231decc3",
    "content-type: multipart/form-data; boundary=----WebKitFormBoundary7MA4YWxkTrZu0gW"
  ),
));
$response = curl_exec($curl);
$err = curl_error($curl);
curl_close($curl);
if ($err) {
  echo "cURL Error #:" . $err;
} else {
  return json_decode($response);
}
}

