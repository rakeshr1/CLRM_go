var method,id;
id = getParamValeuByName('id');
var app = angular.module('touchpoint', []);
app.controller('tpCtrl', function($scope, $http, $timeout) {	
$('#loader').css({'display':'none'});
	/*$http({
				method : 'GET',
				url : apiURL+'/bankList/',
				params: {"type": 'json'},
			}).then(function mySucces(response) {
				$scope.banklistoptions = response.data.BANKLIST;
			}, function myError(response) {
				$scope.errormess = response.statusText;
			});*/
	if(id == '') {
		$scope.title = 'Add Role';
	} else {
		$scope.title = 'Edit Role';
		$http({
			method : 'GET',
			url : apiURL+'/role/',
			params: {"id": id},
				}).then(function mySucces(response) {
					$scope.role_name = response.data.ROLE_DATA.ROLE_NAME;
				}, function myError(response) {
					$scope.errormess = response.statusText;
		});
	}
	$scope.removeRole = function() {
		console.log("Sdf");
	}
	$scope.manageRole = function() {
		window.location.href = manage_role;
	}
	$scope.validateForm = function() {
		//var bankname = $scope.banklist;
		var bankname = '1';
		//scope.banklist.BANK_CODE = 1;
		if(bankname == '' || bankname == undefined || bankname == 'null' || bankname == false) {
			$scope.errormess = 'Please Select Bank';
			return false;
		} else {
			if(id == '') {
				method = 'PUT';
				$scope.alertMessage = 'Role Data Added Successfullly';
			} else {
				method = 'POST';
				$scope.alertMessage = 'Role Data Updated Successfully';
			}
			$http({
				method : method,
				url : apiURL+'/role/',
				params: {"bankname": 1, "id": id,"role_name": $scope.role_name},
			}).then(function mySucces(response) {
				$scope.errormess = response.data.message;
				if(response.data.message == 'Role Data Updated' || response.data.message == 'success'){
					$('#alertModelBoxBtn').trigger('click');
					$timeout(function() {
						$scope.manageRole();
					}, 3000);
				}
			}, function myError(response) {
				$scope.errormess = response.statusText;
			});
		}
	}
	
});