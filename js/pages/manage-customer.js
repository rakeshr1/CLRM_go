var app = angular.module('touchpoint', []);
app.controller('tpCtrl', function($scope, $http) {
$('#loader').css({'display':'none'});
  $http.get(apiURL+'/customerList/').then(function (response) {
	 $('#customerList').html(response.data.BANKLIST);
	 $scope.dataTable();
	  });
		$scope.addCustomer = function() {
		window.location.href = add_customer;
	}
	$scope.dataTable = function() {
	
	var oTable = $('#example').dataTable({
                    "oLanguage": {
                        "sSearch": "Search all columns"
                    },
                    "aoColumnDefs": [
                        {
                            'bSortable': false,
                            'aTargets': [0,6]
                        } //disables sorting for column one
            ],
                    'iDisplayLength': 10,
                    "sPaginationType": "full_numbers",
                    "dom": 'T<"clear">lfrtip'
                });
	}

});