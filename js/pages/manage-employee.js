var app = angular.module('touchpoint', []);
app.controller('tpCtrl', function($scope, $http) {
$('#loader').css({'display':'none'});
  console.log(localStorage.roleId);
  
  $http({
    method: 'GET',
    url: apiURL + '/employeeList/',
    params: {
      "currentRoleId": localStorage.roleId,
      "currentBankId": localStorage.bankcode
    },
  }).then(function mySucces(response) {
	  //alert(localStorage.bankcode);
    $('#employeeList').html(response.data.BANKLIST);
    $scope.dataTable();
    $scope.errormess = response.statusText;
  }, function myError(response) {
	  //alert(localStorage.roleId);
	  //alert(localStorage.bankcode);
    
  });
  
//  $http.get(apiURL+'/employeeList/').then(function (response) {
//	 $('#employeeList').html(response.data.BANKLIST);
//	 $scope.dataTable();
//  });
  $scope.addEmployee = function() {
		window.location.href = add_employee;
	}
	$scope.dataTable = function() {
	
	var oTable = $('#example').dataTable({
                    "oLanguage": {
                        "sSearch": "Search all columns"
                    },
                    "aoColumnDefs": [
                        {
                            'bSortable': false,
                            'aTargets': [0,6]
                        } //disables sorting for column one
            ],
                    'iDisplayLength': 10,
                    "sPaginationType": "full_numbers",
                    "dom": 'T<"clear">lfrtip'
                });
	}
});