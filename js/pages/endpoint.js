var method, id;
id = getParamValeuByName('id');
var app = angular.module('touchpoint', []);

app.controller('tpCtrl', function ($scope, $http, $timeout) {
  $('#loader').css({
    'display': 'none'
  });
  $scope.$on('json-updated', function(msg, value) {
	
});
  $scope.isAdded = '';
  $scope.data = '';
  $scope.selectedRoot = '';
  $scope.selectedRootEmp = '';
  if ($scope.data == '') {
    $scope.showCusSelect = true;
    $scope.showEmpSelect = true;
  }
  if ($scope.isAdded == '') {
    id = localStorage.bankcode;
    $scope.title = 'Add Endpoint';
    $scope.alertMessage = 'Endpoint Added Successfullly';
  } else {
    $scope.title = 'Edit Endpoint';
    $scope.alertMessage = 'Endpoint Updated Successfully';
  }
  $http({
    method: 'GET',
    url: apiURL + '/endpoint/',
    params: {
      "id": localStorage.bankcode
    },
  }).then(function mySucces(response) {

    console.log(response.data.ENDPOINT_DETAILS);
    if (response.data.ENDPOINT_DETAILS != null) {
      $scope.vEndpoint = response.data.ENDPOINT_DETAILS.END_POINT;
      $scope.isAdded = true;
      console.log($scope.vEndpoint);
    } else
      $scope.vEndpoint = '';
  }, function myError(response) {
    $scope.errormess = response.statusText;
  });

  $scope.validateForm = function () {

    var endpoint = $scope.vEndpoint;
    $http({
      method: 'GET',
      url: apiURL + '/endpointbank/',
      params: { "endpoint": $scope.vEndpoint, "id": localStorage.bankcode },
    }).then(function mySucces(response) {

    })

    if (endpoint == '' || endpoint == undefined || endpoint == 'null' || endpoint == false) {
      $scope.errormess = 'Please Enter Endpoint';
      return false;
    } else {
      if ($scope.isAdded != true) {
        method = 'PUT';
      } else {
        method = 'POST';
      }
      $http({
        method: method,
        url: apiURL + '/endpoint/',
        params: {
          "endpoint": $scope.vEndpoint,
          "id": localStorage.bankcode
        },
      }).then(function mySucces(response) {
        if (response.data.message == 'success' || response.data.message == 'Endpoint Updated Successfully') {
          $('#alertModelBoxBtn').trigger('click');
          $timeout(function () {
            // $scope.clearField();
          }, 3000);
        }
      }, function myError(response) {
        $scope.errormess = response.statusText;
      });
    }
  }

  $scope.clearField = function () {
    $scope.vEndpoint = '';
    // $http({
    //   method: 'GET',
    //   url: apiURL + '/endpoint/',
    //   params: {
    //     "id": localStorage.bankCode
    //   },
    // }).then(function mySucces(response) {

    //   //    console.log(response.data.ENDPOINT_DETAILS);
    //   if (response.data.ENDPOINT_DETAILS != null) {
    //     //      console.log("no data foudn");
    //     $scope.vEndpoint = response.data.ENDPOINT_DETAILS.ENDPOINT;
    //     $scope.isAdded = true;
    //   } else
    //     $scope.vEndpoint = '';
    // }, function myError(response) {
    //   $scope.errormess = response.statusText;
    // });

  }
  $scope.getDataFromUrl = function (vEndpoint) {
    $scope.dataPath = "https://data.gov.in/api/datastore/resource.json?resource_id=863d77b2-a35c-44c4-aaaf-8b3a19fc8e0e&api-key=ada54775475904a5249c23a058153aac";
	data = {"BRANCH_DETAILS":[{"Bank_name":"croatiabank","Bank_code":"456","Branch_name":"croatiabranch1","Branch_code":"123","Branch_addr":"address","city":"city","state":"state","country":"country","CreatedBy":null},{"Bank_name":"croatiabank","Bank_code":"456","Branch_name":"croatiabranch2","Branch_code":"321","Branch_addr":"address","city":"city","state":"state","country":"country","CreatedBy":null}], "CUSTOMER_DETAILS":[{"FIRST_NAME":"croatia","LAST_NAME":"cus1","EMAIL":"pshankaran@elitecorp.in","mobile":"1230457896","BRANCH_ID":"123","PRODUCT_ID":"14","JOINING_DATE":"2016-05-31","BANK_ID":"456","EMPLOYEE_ID":"","CUSTOMER_ID":"394"},{"FIRST_NAME":"croatia","LAST_NAME":"cus2","EMAIL":"phankaran@elitecorp.in","mobile":"7896541320","BRANCH_ID":"123","PRODUCT_ID":"14","JOINING_DATE":"2016-05-31","BANK_ID":"456","EMPLOYEE_ID":"","CUSTOMER_ID":"395"},{"FIRST_NAME":"croatia","LAST_NAME":"cus3","EMAIL":"pshankaran@elitecorp.in","mobile":"7412589630","BRANCH_ID":"123","PRODUCT_ID":"14","JOINING_DATE":"2016-05-31","BANK_ID":"456","EMPLOYEE_ID":"","CUSTOMER_ID":"396"},{"FIRST_NAME":"croatia","LAST_NAME":"cus4","EMAIL":"pshanakaran@elitecorp.in","mobile":"1236547890","BRANCH_ID":"123","PRODUCT_ID":"14","JOINING_DATE":"2016-05-31","BANK_ID":"456","EMPLOYEE_ID":"","CUSTOMER_ID":"397"},{"FIRST_NAME":"croatia","LAST_NAME":"cus5","EMAIL":"pshankaran@elitecorp.in","mobile":"9176140286","BRANCH_ID":"123","PRODUCT_ID":"14","JOINING_DATE":"2016-05-31","BANK_ID":"456","EMPLOYEE_ID":"","CUSTOMER_ID":"398"},{"FIRST_NAME":"croatia","LAST_NAME":"cus6","EMAIL":"msaravanan@elitecorp.in","mobile":"7410258963","BRANCH_ID":"321","PRODUCT_ID":"14","JOINING_DATE":"2016-05-31","BANK_ID":"456","EMPLOYEE_ID":"","CUSTOMER_ID":"399"},{"FIRST_NAME":"croatia","LAST_NAME":"cus9","EMAIL":"msaravanan@elitecorp.in","mobile":"1204578963","BRANCH_ID":"321","PRODUCT_ID":"14","JOINING_DATE":"2016-05-31","BANK_ID":"456","EMPLOYEE_ID":"","CUSTOMER_ID":"403"},{"FIRST_NAME":"croatia","LAST_NAME":"cus7","EMAIL":"msaravanan@elitecorp.in","mobile":"7896541230","BRANCH_ID":"321","PRODUCT_ID":"14","JOINING_DATE":"2016-05-31","BANK_ID":"456","EMPLOYEE_ID":"","CUSTOMER_ID":"404"},{"FIRST_NAME":"croatia","LAST_NAME":"cus8","EMAIL":"msaravanan@elitecorp.in","mobile":"9566243320","BRANCH_ID":"321","PRODUCT_ID":"14","JOINING_DATE":"2016-05-31","BANK_ID":"456","EMPLOYEE_ID":"","CUSTOMER_ID":"405"},{"FIRST_NAME":"croatia","LAST_NAME":"cus10","EMAIL":"msaravanan@elitecorp.in","mobile":"9176140286","BRANCH_ID":"321","PRODUCT_ID":"14","JOINING_DATE":"2016-05-31","BANK_ID":"456","EMPLOYEE_ID":"","CUSTOMER_ID":"406"}]};
    $http.get($scope.dataPath)
      .success(function (response) {
        $scope.showCusSelect = false;
        $scope.showEmpSelect = false;
        $scope.dataCus = response;
        $scope.dataEmp = response;
        // console.log($scope.data);
        $scope.pathCus = "$scope.data";
        $scope.pathEmp = "$scope.data";
        $scope.dataItemsEmp = Object.keys($scope.dataEmp);
        $scope.dataItemsCus = Object.keys($scope.dataCus);
        $scope.iteration = 0;
        $scope.dataCopyEmp = $scope.dataEmp;
        $scope.dataCopyCus = $scope.dataCus;
      })
      .error(function (response) {
        console.log("Error occured");
      });
  }

  $scope.changeSourceData = function (dataItems, selectedRoot, iteration, serveFor) {
	  alert($scope.iteration);
    if (serveFor == 'employee') {
      if ($scope.iteration % 2 != 1) {
        if (typeof ($scope.dataEmp[selectedRoot]) != "string") {
          $scope.tempData = $scope.dataEmp[selectedRoot];
          $scope.dataEmp = $scope.tempData;
          $scope.tempData = '';
          if (isNaN(parseInt(selectedRoot)))
            $scope.pathEmp = $scope.pathEmp + "['" + selectedRoot + "']";
          else
            $scope.pathEmp = $scope.pathEmp + "[" + selectedRoot + "]";
        }
      }
      if (typeof ($scope.dataEmp[selectedRoot]) != "string") {
        $scope.dataItemsEmp = Object.keys($scope.dataEmp);
      }
      $scope.iteration++;
    } else {
      if ($scope.iteration % 2 != 1) {
        if (typeof ($scope.dataCus[selectedRoot]) != "string") {
          $scope.tempData = $scope.dataCus[selectedRoot];
          $scope.dataCus = $scope.tempData;
          $scope.tempData = '';
          if (isNaN(parseInt(selectedRoot)))
            $scope.pathCus = $scope.pathCus + "['" + selectedRoot + "']";
          else
            $scope.pathCus = $scope.pathCus + "[" + selectedRoot + "]";
        }
      }
      if (typeof ($scope.data[selectedRoot]) != "string") {
        $scope.dataItemsCus = Object.keys($scope.dataCus);
      }
      $scope.iteration++;
    }
	example($scope.pathCus);
  }
function example(datapath)
{
	//testData = $scope.pathCus;
	testData = $scope.data['records'][4];
	$('#custIdNodes').html('');
	$('#custEmailNodes').html('');
	$('#bankIdNodes').html('');
	$('#branchIdNodes').html('');
	$('#firstNameNodes').html('');
	$('#lastNameNodes').html('');
	$('#DOJNodes').html('');
	$('#employeeIdNodes').html('');
	$('#mobNoNodes').html('');
	$('#productIdNodes').html('');
	
	
		
		//JSON.parse(testData);
		createSelectBox('', 0, 'custIdNodes');
		createSelectBox('', 0, 'custEmailNodes');
		createSelectBox('', 0, 'bankIdNodes');
		createSelectBox('', 0, 'branchIdNodes');
		createSelectBox('', 0, 'firstNameNodes');
		createSelectBox('', 0, 'lastNameNodes');
		createSelectBox('', 0, 'DOJNodes');
		createSelectBox('', 0, 'employeeIdNodes');
		createSelectBox('', 0, 'mobNoNodes');
		createSelectBox('', 0, 'productIdNodes');
}
  $scope.clearArrayField = function () {
    $scope.dataCus = '';
    $scope.dataEmp = '';
    $scope.dataCus = $scope.dataCopyCus;
    $scope.dataEmp = $scope.dataCopyEmp;
    $scope.pathEmp = "$scope.data";
    $scope.pathCus = "$scope.data";
    $scope.dataItemsCus = Object.keys($scope.dataCus);
    $scope.dataItemsEmp = Object.keys($scope.dataEmp);
  }
  
  $scope.validateArrayDetailForm = function(){
    $http.get(apiURL+'/addArrayDetails/',{
      params:{"pathCus": $scope.pathCus, "pathEmp": $scope.pathEmp, "bankId":'321'}
    })
    .success(function(response){
      if (response.message == 'success') {
        $('#alertModelBoxBtn').trigger('click');
          $timeout(function () {
            $('#alertModelBoxBtn').trigger('click');
            $scope.clearArrayField();
          }, 3000);
        }
    })
    .error(function(response){
      
    })
  }
  $scope.previewData = function(serverFor) {
    serveFor = serverFor;
    if(serverFor == 'employee'){
      $scope.jsonObj = angular.toJson($scope.dataItemsEmp, true);
    } else{
      $scope.jsonObj = angular.toJson($scope.dataItemsCus, true);  
    }
    $scope.alertMessage = 'JSON objects of your selection';
    $('#alertModelBoxBtn').trigger('click');
  }
  
  
  

function updatedPathJSON() {
	
	$('#custIdNodes').html('');
	$('#custEmailNodes').html('');
	$('#bankIdNodes').html('');
	$('#branchIdNodes').html('');
	$('#firstNameNodes').html('');
	$('#lastNameNodes').html('');
	$('#DOJNodes').html('');
	$('#employeeIdNodes').html('');
	$('#mobNoNodes').html('');
	$('#productIdNodes').html('');
	
	serviceType = $('#serviceType').val();
	testData = $('#pathJSON').val();
	if(serviceType == 'SOAP') {
		$.ajax({url: "xmlTOjson.php", data:{xml:testData}, type:'POST', success: function(result){
			testData = JSON.parse(result);
			createSelectBox('', 0, 'custIdNodes');
			createSelectBox('', 0, 'custEmailNodes');
			createSelectBox('', 0, 'bankIdNodes');
			createSelectBox('', 0, 'branchIdNodes');
			createSelectBox('', 0, 'firstNameNodes');
			createSelectBox('', 0, 'lastNameNodes');
			createSelectBox('', 0, 'DOJNodes');
			createSelectBox('', 0, 'employeeIdNodes');
			createSelectBox('', 0, 'mobNoNodes');
			createSelectBox('', 0, 'productIdNodes');
		}});
	} else {
		testData = $scope.pathCus;
		//JSON.parse(testData);
		createSelectBox('', 0, 'custIdNodes');
		createSelectBox('', 0, 'custEmailNodes');
		createSelectBox('', 0, 'bankIdNodes');
		createSelectBox('', 0, 'branchIdNodes');
		createSelectBox('', 0, 'firstNameNodes');
		createSelectBox('', 0, 'lastNameNodes');
		createSelectBox('', 0, 'DOJNodes');
		createSelectBox('', 0, 'employeeIdNodes');
		createSelectBox('', 0, 'mobNoNodes');
		createSelectBox('', 0, 'productIdNodes');
	}
	
	
	
	
}
function createSelectBox(x,y,z) {
		iterateId = $('#'+z+' select[name="endpoint[]"]').length;
		for(i=y+1; i<=iterateId; i++) {
			$('#'+z+' select[data-iterationPropId="'+i+'"]').remove();
		}
		iterateId = $('#'+z+' select[name="endpoint[]"]').length;
		$('#'+z).append('<select name="endpoint[]" data-iterationProp="'+x+'" data-iterationPropId="'+iterateId+'"><option value="">Select Node</option></select>');
		appendOptions(iterateId,z);
}

function appendOptions(y,z) {
	testDataCom = testData;
	if(y == '' || y == 0) {
		for(var propt in testData){
			$('#'+z+' select[data-iterationPropId="'+y+'"]').append('<option value="'+propt+'" onclick=createSelectBox("'+propt+'",'+y+',"'+z+'")>'+propt+'</option>');
		}
	} else {
		for(i=1; i<=y; i++) {
			testDataCom = testDataCom[$('#'+z+' select[data-iterationPropId="'+i+'"]').attr('data-iterationProp')];
		}
		
		if(typeof testDataCom == 'object') {
			for(var propt in testDataCom){
				$('#'+z+' select[data-iterationPropId="'+y+'"]').append('<option value="'+propt+'" onclick=createSelectBox("'+propt+'",'+y+',"'+z+'")>'+propt+'</option>');
			}
		} else {
			$('#'+z+' select[data-iterationPropId="'+y+'"]').remove();
			alert('Node Selected...');
		}
		
	}
}

function submitCust(x) {
	var custPath = [];
	$('#'+x+' select[name="endpoint[]"]').each(function(){
       custPath.push($(this).val());
    });
	custPath = JSON.stringify(custPath);
	return custPath;
}

function getCustPath() {
	custId = submitCust('custIdNodes');
	custEmail = submitCust('custEmailNodes');
	bankId = submitCust('bankIdNodes');
	branchId = submitCust('branchIdNodes');
	fname = submitCust('firstNameNodes');
	lname = submitCust('lastNameNodes');
	doj = submitCust('DOJNodes');
	employeeId = submitCust('employeeIdNodes');
	mobNo = submitCust('mobNoNodes');
	productId = submitCust('productIdNodes');	
	endpointURL = $('#endpointURL').val();
	serviceType = $('#serviceType').val();
	wsdlURL = $('#wsdlURL').val();
	soapMethod = $('#soapMethod').val();
	soapParams = $('#soapParams').val();
	soapParamValues = $('#soapParamValues').val();
	
	$.ajax({url: "http://localhost:8080/tpv3/api/index.php/addEndPointWithPath/", data:{custId:custId, custEmail:custEmail, bankId:bankId, branchId:branchId, fname:fname, lname:lname, doj:doj, employeeId:employeeId, mobNo:mobNo, productId:productId, endpointURL:endpointURL, serviceType:serviceType, wsdlURL:wsdlURL, soapMethod:soapMethod, soapParams:soapParams, soapParamValues:soapParamValues}, success: function(result){
        alert('Success...')
    }});
	
}
});