document.createElement("chart");
document.createElement("responseChart");
var app = angular.module('touchpoint', []);
app.controller('tpCtrl', function ($scope, $http) {

	$('#loader').css({ 'display': 'none' });
	var todayDate = new Date();
	$scope.todayDate = todayDate.toDateString();
	var responseGroup = { "RESPONSE_GROUP": [{ "LABEL": "EMAIL", "VALUE": "email" }, { "LABEL": "SMS", "VALUE": "sms" }, { "LABEL": "VOICE", "VALUE": "voice" }] };
	var activityGroup = { "ACTIVITY_GROUP": [{ "LABEL": "WEEKLY", "VALUE": "w" }, { "LABEL": "MONTHLY", "VALUE": "m" }, { "LABEL": "YEARLY", "VALUE": "y" }] };
	$scope.responseGroup = responseGroup.RESPONSE_GROUP;
	$scope.activityGroup = activityGroup.ACTIVITY_GROUP;
	$scope.recentActivityValue = $scope.activityGroup[0];
	$scope.responseRateValue = $scope.responseGroup[0];

	$http({
		method: 'GET',
		url: 'api/employeeVolume/',
		params: { "id": localStorage.userId },
	}).then(function mySucces(response) {
		if (response.data.length == 0) {
			$('#empDayVolume').html('<p style="text-align:center;min-height:200px;">No One Activity Done For Today</p>');
			$('#empDayVolume').css({ "visibility": "visible" });
		} else {
			$scope.createPie(response.data, 'chart');
			$('#empDayVolume').css({ "visibility": "visible" });
		}

	}, function myError(response) {
		alert("error");
	});

	$scope.responseRate = function (nn) {
		$http({
			method: 'GET',
			url: 'api/responseRate/',
			params: { "id": localStorage.userId, "searchOn": $scope.responseRateValue.VALUE },
		}).then(function mySucces(response) {
			data = response.data;
			$('responseChart').html('');
			var margin = { top: 30, right: 20, bottom: 30, left: 50 },
				width = 600 - margin.left - margin.right,
				height = 270 - margin.top - margin.bottom;

			var parseDate = d3.time.format("%b %Y").parse;
			var x = d3.time.scale().range([0, width]);
			var y = d3.scale.linear().range([height, 0]);
			var xAxis = d3.svg.axis().scale(x).orient("bottom").ticks(5);
			var yAxis = d3.svg.axis().scale(y).orient("left").ticks(5);

			var priceline = d3.svg.line()
				.x(function (d) { return x(d.RESPONSE_MONTH); })
				.y(function (d) { return y(d.RESPONSE_COUNT); });

			var svg = d3.select("responseChart")
				.append("svg")
				.attr("width", width + margin.left + margin.right)
				.attr("height", height + margin.top + margin.bottom)
				.append("g")
				.attr("transform",
				"translate(" + margin.left + "," + margin.top + ")");

			data.forEach(function (d) {
				d.RESPONSE_MONTH = parseDate(d.RESPONSE_MONTH);
				d.RESPONSE_COUNT = +d.RESPONSE_COUNT;
			});

			x.domain(d3.extent(data, function (d) { return d.RESPONSE_MONTH; }));
			y.domain([0, d3.max(data, function (d) { return d.RESPONSE_COUNT; })]);

			var dataNest = d3.nest()
				.key(function (d) { return d.CUSTOMER_RESPONSE; })
				.entries(data);

			dataNest.forEach(function (d) {
				svg.append("path")
					.attr("class", "line")
					.attr("d", priceline(d.values)).
					style("stroke", function () {
						if (d.key == 'NOT_NOW') {
							return 'rgb(204, 204, 0)';
						} else if (d.key == 'INTERESTED') {
							return 'rgb(51, 102, 0)';
						} else if (d.key == 'NOT_INTERESTED') {
							return 'rgb(204, 51, 0)';
						} else {
							return 'rgb(204, 204, 204)';
						}

					});
			});

			svg.append("g")
				.attr("class", "x axis")
				.attr("transform", "translate(0," + height + ")")
				.call(xAxis);
			svg.append("g")
				.attr("class", "y axis")
				.call(yAxis);

		}, function myError(response) {
		});
	}
	$scope.responseRate();

	$scope.loadActivityList = function (groupBy) {
		$http({
			method: 'GET',
			url: 'api/employeeActivityList/',
			params: { "id": localStorage.userId, "groupBy": $scope.recentActivityValue.VALUE },
		}).then(function mySucces(response) {
			$scope.communicationList = response.data.COMMUNICATION_DATA;
		}, function myError(response) {
			//alert("error");
		});
	}

	$scope.createPie = function (pData, pElement) {
		var m = 10, r = 100, z = d3.scale.category20c();
		var pie = d3.layout.pie()
			.value(function (d) { return +d.CCOUNT; })
			.sort(function (a, b) { return b.CCOUNT - a.CCOUNT; });
		var arc = d3.svg.arc()
			.innerRadius(r / 2)
			.outerRadius(r);
		var jsonData = d3.nest()
			.key(function (d) { return d.EMPLOYEE_ID; })
			.entries(pData);
		var svg = d3.select(pElement).selectAll("div")
			.data(jsonData)
			.enter().append("div")
			.style("display", "inline-block")
			.style("width", (r + m) * 2 + "px")
			.style("height", (r + m) * 2 + "px")
			.append("svg")
			.attr("width", (r + m) * 2)
			.attr("height", (r + m) * 2)
			.append("g")
			.attr("transform", "translate(" + (r + m) + "," + (r + m) + ")");

		// Add a label 
		svg.append("text")
			.attr("dy", ".35em")
			.attr("text-anchor", "middle")
			.text(function (d) {
				if (d.values[0].CCOUNT == 0.01) {
					//return 0;
				} else {
					//return d.values[0].CCOUNT;
				}
				return '';
			});

		var g = svg.selectAll("g")
			.data(function (d) { return pie(d.values); })
			.enter().append("g");

		g.append("path")
			.attr("d", arc)
			.style("fill", function (d) {
				if (d.data.CAMPAIGN_TYPE == 'email') {
					return 'rgb(189, 195, 199)';
				} else if (d.data.CAMPAIGN_TYPE == 'sms') {
					return 'rgb(38, 185, 154)';
				} else if (d.data.CAMPAIGN_TYPE == 'voice') {
					return 'rgb(97, 79, 205)';
				} else if (d.data.CAMPAIGN_TYPE == 'customers') {
					return 'rgb(204, 102, 255)';
				} else {
					return 'rgb(0, 34, 102)';
				}
				//return z(d.data.CCOUNT); 

			})
			.append("title")
			.text(function (d) {
				return d.data.CAMPAIGN_TYPE + ": " + d.data.CCOUNT;
			});

		g.filter(function (d) { return d.endAngle - d.startAngle > .2; }).append("text")
			.attr("dy", ".35em")
			.attr("text-anchor", "middle")
			.attr("transform", function (d) { return "translate(" + arc.centroid(d) + ")rotate(" + $scope.angle(d) + ")"; })
			.text(function (d) {
				return d.data.CCOUNT;
				//return '';
			});

	}

	$scope.angle = function (d) {
		var a = (d.startAngle + d.endAngle) * 90 / Math.PI - 90;
		return a > 90 ? a - 180 : a;
	}
	$http.get(apiURL + '/myCustomer/?id=' + localStorage.userId, $.param({})).then(function (response) {
		$scope.customerList = response.data.MY_CUSTOMER;
	});


	$scope.loadActivityList(0);
});