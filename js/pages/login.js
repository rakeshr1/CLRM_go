//$('head').append('<link rel="icon" href="images/logo/fprints_title.png">');
localStorage.clear();
//var hostPath = "http://123.201.60.252/project/tp/demo_new/";
//var hostPath = "http://192.168.0.45/touchpoint/tp/production/";
//var hostPath = "http://192.168.0.69/touchpoint/tp/production/";
//var hostPath = "http://localhost:8080/tpv3/demo_new/";
var hostPath ="http://ategrity-dev.esinsurancecloud.com/clrm/";

var apiURL = hostPath+"api/index.php";
var app = angular.module('touchpoint', []);

app.controller('tpCtrl', function($scope, $http) 
{	
$('#loader').css({'display':'none'});
	$scope.SignVsForget = false; 
	$scope.title = 'Login';
	$scope.signVsForgetVis = function() {
		alert ("In signVsForgetVis");
		$scope.errormess = '';
		$scope.successmess = '';
		$scope.SignVsForget = !$scope.SignVsForget;
		if(!$scope.SignVsForget) {
			$scope.title = 'Login';
		} else {
			$scope.title = 'Forget Password';
		}
	}
	
	$scope.authenticate = function() {
		var username = $scope.username;
		var password = $scope.password;
		
		//alert ("In authenticate");
		
		if(username == '' || username == undefined || username == 'null' || username == false) {
			$scope.errormess = 'Please Enter Your Username';
			return false;
		} else if(password == '' || password == undefined || password == 'null' || password == false) {
			$scope.errormess = 'Please Enter Your Password';
			return false;
		} else {
			$http({
				method : "POST",
				url : apiURL+'/authenticate/',
				params: {"username": $scope.username, "password": $scope.password},
			}).then(function mySucces(response) {
				
				var res = response.data.USER;
				if(res.message == 'success') {

					localStorage.setItem("userId", res.EMPLOYEE_ID);
					localStorage.setItem("bankcode", res.BANK_CODE);
					localStorage.setItem("branchId", res.BRANCH_CODE);
					localStorage.setItem("roleId", res.ROLE_ID);
					localStorage.setItem("firstName", res.FIRST_NAME);
					localStorage.setItem("lastName", res.LAST_NAME);
					localStorage.setItem("email", res.EMAIL);
					localStorage.setItem("topMenu", res.TOPMENU);
					localStorage.setItem("sideBar", res.SIDEBAR);
					localStorage.setItem("accessibleURLs", res.accessibleURLs);
					localStorage.setItem("dashBoard", res.dashboard);
					
					//alert (localStorage.dashBoard);
				//console.log(localStorage.userId);
					if(res.ROLE_ID <= 5) {
						
						window.location.href = localStorage.dashBoard;
					} else {
						$scope.errormess = 'Role Not Defined';
					}
				} else {
					$scope.errormess = response.data.USER.message;
				}
			}, function myError(response) {
				$scope.errormess = response.statusText;
			});
		}
	}
	$scope.forgetPassword = function() {
		
		var username = $scope.username;
		if(username == '' || username == undefined || username == 'null' || username == false) {
			$scope.errormess = 'Please Enter Your Username';
			return false;
		} else {
			jQuery('#overlay').fadeIn();
			$http({
				method : "POST",
				url : apiURL+'/forgetPassword/',
				params: {"username": $scope.username},
			}).then(function mySucces(response) {
				var res = response.data.USER;
				console.log(res);
				jQuery('#overlay').fadeOut();
				if(res.message == 'success') {					
					$('.errormesssp').css({'display':'none'});
					$scope.successmess = 'Your Password Sent Your Mail';
				} else {
					$scope.errormess = response.data.USER.message;
				}
			}, function myError(response) {
				$scope.errormess = response.statusText;
			});
		}
		
		
	}
	
	

});

