var app = angular.module('touchpoint', []);

app.controller('tpCtrl', function($scope, $http) {
$('#loader').css({'display':'none'});
  $http.get(apiURL+'/branchList/?bankId='+localStorage.bankcode).then(function (response) {
	 // alert(response.data.BRANCHLIST);;
     // $scope.bankList = response.data.BANKLIST;
	 $('#branchList').html(response.data.BRANCHLIST);
	 $scope.dataTable();
  });
  $scope.addBranch = function() {
		window.location.href = add_branch;
	}
	
	
	$scope.synch = function()
	{
		$http({
		  method:'GET',
		  url: apiURL + '/getendpoint/',
		  params:{"id": localStorage.bankcode,"email": localStorage.email},
	  }).then(function mySucces(response) {
		 // alert(response.data); 
	
		  
		  window.location.href ="manage-branch.html";
	 
	  
	  })
	}
	$scope.dataTable = function() {
	
	var oTable = $('#example').dataTable({
                    "oLanguage": {
                        "sSearch": "Search all columns"
                    },
                    "aoColumnDefs": [
                        {
                            'bSortable': false,
                            'aTargets': [0,4]
                        } //disables sorting for column one
            ],
                    'iDisplayLength': 10,
                    "sPaginationType": "full_numbers",
                    "dom": 'T<"clear">lfrtip'
                });
	}
});