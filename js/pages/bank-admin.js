var method,id;
id = getParamValeuByName('id');
var app = angular.module('touchpoint', [
  'ngRoute',
  '720kb.datepicker'
]);

app.directive("limitTo", [function() {
  return {
    restrict: "A",
    link: function(scope, elem, attrs) {
      var limit = parseInt(attrs.limitTo);
      angular.element(elem).on("keypress", function(e) {
        if (this.value.length == limit) e.preventDefault();
      });
    }
  }
}]);

app.controller('tpCtrl', function($scope, $http, $timeout) {
  $('#loader').css({'display':'none'});	
  $scope.email_pattern = /^[a-z]+[a-z0-9._]+@[a-z]+\.[a-z.]{2,5}$/;
  var status_data = {"STATUS_LIST": [{"STATUS_LABEL":"ACTIVE","STATUS_CODE":"1"},{"STATUS_LABEL":"INACTIVE","STATUS_CODE":"0"}]};
  var fd_data = {"FLOWDESIGN_LIST": [{"LABEL":"NO","VALUE":"0"},{"LABEL":"YES","VALUE":"1"}]};
  var gender_data = {"GENDER_LIST": [{"GENDER_LABEL":"MALE","GENDER_NAME":"MALE"},{"GENDER_LABEL":"FEMALE","GENDER_NAME":"FEMALE"}]};
  $http({
    method : 'GET',
    url : apiURL+'/bankList/',
    params: {"type": 'json'},
  }).then(function mySucces(response) {
    $scope.banklistoptions = response.data.BANKLIST;
    //    console.log($scope.banklistoptions);
  }, function myError(response) {
    $scope.errormess = response.statusText;
  });
  $http({
    method : 'GET',
    url : apiURL+'/countryList/',
    params: {"type": 'json'},
  }).then(function mySucces(response) {
    $scope.countrylistoptions = response.data.COUNTRYLIST;
  }, function myError(response) {
    $scope.errormess = response.statusText;
  });
  
  $scope.statuslistoptions = status_data.STATUS_LIST;
  $scope.flowdesignlistoptions = fd_data.FLOWDESIGN_LIST;
  $scope.genderlistoptions = gender_data.GENDER_LIST;
  
  if(id == '') {
    $scope.title = 'Add Employee';
    $scope.branchtemp = '';
    $scope.roletemp = '';
    $scope.alertMessage = 'Employee Data Added Successfullly';
    
    $http({
      method : 'GET',
      url : apiURL+'/bank/',
      params: {"id": localStorage.bankCode},
    }).then(function mySucces(response) {
      angular.forEach($scope.banklistoptions, function (task, index) {
        if (task.BANK_NAME == response.data.BANK_DATA.BANK_NAME) {
          $scope.banklist = $scope.banklistoptions[index];
        }
      });
      $scope.getBranchList();

    }, function myError(response) {
      $scope.errormess = response.statusText;
    });


  } else {
    $scope.title = 'Edit Employee Profile';
    $scope.alertMessage = 'Employee Data Updated Successfully';
    $http({
      method : 'GET',
      url : apiURL+'/employee/',
      params: {"id": id},
    }).then(function mySucces(response) {
      angular.forEach($scope.banklistoptions, function (task, index) {
        if (task.BANK_CODE == response.data.EMPLOYEE_DATA.BANK_CODE) {
          $scope.banklist = $scope.banklistoptions[index];
        }
      });
      angular.forEach($scope.countrylistoptions, function (task, index) {
        console.log("loop for find country activated");
        if (task.COUNTRY_CODE == response.data.EMPLOYEE_DATA.COUNTY) {
          $scope.country = $scope.countrylistoptions[index];
          console.log($scope.country);
        }
      });
      angular.forEach($scope.statuslistoptions, function (task, index) {
        if (task.STATUS_CODE == response.data.EMPLOYEE_DATA.STATUS) {
          $scope.status = $scope.statuslistoptions[index];
        }
      });
      angular.forEach($scope.flowdesignlistoptions, function (task, index) {
        if (task.VALUE == response.data.EMPLOYEE_DATA.ISFLOWDESIGNER) {
          $scope.flowdesign = $scope.flowdesignlistoptions[index];
        }
      });
      angular.forEach($scope.genderlistoptions, function (task, index) {
        if (task.GENDER_NAME == response.data.EMPLOYEE_DATA.GENDER) {
          $scope.gender = $scope.genderlistoptions[index];
        }
      });
      $scope.statetemp = response.data.EMPLOYEE_DATA.STATE;
      $scope.branchtemp = response.data.EMPLOYEE_DATA.BRANCH_CODE;
      $scope.roletemp = response.data.EMPLOYEE_DATA.ROLE_ID;
      $scope.email = response.data.EMPLOYEE_DATA.EMAIL;
      $scope.password = response.data.EMPLOYEE_DATA.PASSWORD;
      $scope.first_name = response.data.EMPLOYEE_DATA.FIRST_NAME;
      $scope.last_name = response.data.EMPLOYEE_DATA.LAST_NAME;
      $scope.dob = response.data.EMPLOYEE_DATA.DATE_OF_BIRTH;
      $scope.phno = parseInt(response.data.EMPLOYEE_DATA.PHONE_NO.substr(3));
      $scope.phone_prefix = parseInt(response.data.EMPLOYEE_DATA.PHONE_NO.substr(0,3));
      $scope.mono = parseInt(response.data.EMPLOYEE_DATA.MOBILE_NO);
      $scope.gendertemp = response.data.EMPLOYEE_DATA.GENDER;
      $scope.zip = parseInt(response.data.EMPLOYEE_DATA.PIN_CODE);
      $scope.city = response.data.EMPLOYEE_DATA.CITY;
      $scope.address = response.data.EMPLOYEE_DATA.ADDRESS;
      $scope.getBranchList();
      $scope.getStates();
      console.log("temp state"+$scope.statetemp);

    }, function myError(response) {
      $scope.errormess = response.statusText;
    });
  }
  $scope.getBranchList = function () {
    console.log('getbranchlist() called');
    console.log($scope.banklist);
    $http({
      method: 'GET',
      url: apiURL + '/bankBranchList/',
      params: {
        "id": localStorage.bankCode
      },
    }).then(function mySucces(response) {
      $scope.branchlistoptions = response.data.BRANCH_LIST;
      $scope.branchlist = $scope.branchlistoptions[0];
      console.log($scope.branchlist);
      console.log($scope.branchlistoptions);
      $http({
        method: 'GET',
        url: apiURL + '/branchRoleList/',
        params: {
          "id": $scope.banklist.BANK_CODE
        },
      }).then(function mySucces(response) {
        console.log(response.data);
        $scope.rolelistoptions = response.data.ROLE_LIST;
        //$scope.banklist = $scope.banklistoptions[1];

        /*angular.forEach($scope.branchlistoptions, function (task, index) {
        		if (task.BRANCH_CODE == $scope.branchtemp) {
        			//$scope.branchlist = $scope.branchlistoptions[index];
        		}
        });*/
        angular.forEach($scope.rolelistoptions, function (task, index) {
        		if (task.ROLE_ID == $scope.roletemp) {
        			$scope.rolelist = $scope.rolelistoptions[index];
        		}
        });
        
      }, function myError(response) {
        $scope.errormess = response.statusText;
      });
    }, function myError(response) {
      $scope.errormess = response.statusText;
    });
  }
//  $scope.getBranchList = function() {
//    //    console.log("get branch list called");
//    $http({
//      method : 'GET',
//      url : apiURL+'/bankBranchList/',
//      params: {"id":localStorage.bankCode},
//    }).then(function mySucces(response) {
//      //      console.log("This call get succeded");
//      $scope.branchlistoptions = response.data;
//      //      console.log(response.data);
//      angular.forEach($scope.branchlistoptions, function (task, index) {
//        //        console.log("forEach called");
//        if (task.BRANCH_CODE == $scope.branchtemp) {
//          $scope.branchlist = $scope.branchlistoptions[index];
//        }
//      });
//      $http({
//        method : 'GET',
//        url : apiURL+'/branchRoleList/',
//        params: {"id":$scope.banklist.BANK_CODE},
//      }).then(function mySucces(response) {
//        $scope.rolelistoptions = response.data.ROLE_LIST;
//        angular.forEach($scope.branchlistoptions, function (task, index) {
//          if (task.BRANCH_CODE == $scope.branchtemp) {
//            $scope.branchlist = $scope.branchlistoptions[index];
//          }
//        });
//        angular.forEach($scope.rolelistoptions, function (task, index) {
//          if (task.ROLE_ID == $scope.roletemp) {
//            $scope.rolelist = $scope.rolelistoptions[index];
//          }
//        });
//
//      }, function myError(response) {
//        $scope.errormess = response.statusText;
//      });
//    }, function myError(response) {
//      $scope.errormess = response.statusText;
//    });
//  }
  $scope.getStates = function() {
    console.log("getStates() called");
    $http({
      method : 'GET',
      url : apiURL+'/stateList/',
      params: {"country_code": $scope.country.COUNTRY_CODE},
    }).then(function mySucces(response) {
      $scope.statelistoptions = response.data.STATELIST;
      //      console.log("State list"+response.data.STATELIST);

      angular.forEach($scope.statelistoptions, function (task, index) {
        if (task.STATE_NAME == $scope.statetemp) {
          $scope.state = $scope.statelistoptions[index];
        }
      });
      //      console.log("E"+$scope.state);
    }, function myError(response) {
      $scope.errormess = response.statusText;
    });

  }
  $scope.validateForm = function() {
    var bankname = $scope.banklist;
    if(bankname == '' || bankname == undefined || bankname == 'null' || bankname == false) {
      $scope.errormess = 'Please Select Bank';
      return false;
    } else {
      if(id == '') {
        method = 'PUT';
      } else {
        method = 'POST';
      }
      //      console.log($scope.phone_prefix.toString()+$scope.phno.toString());
      $http({
        method : method,
        url : apiURL+'/employee/',
        params: {"bankname": $scope.banklist.BANK_CODE, "id": id,"branchname": $scope.branchlist.BRANCH_CODE,"role":$scope.rolelist.ROLE_ID,"email":$scope.email,"password":$scope.password,"firstname":$scope.first_name,"lastname":$scope.last_name,"dob":$scope.dob,"phno":$scope.phone_prefix.toString()+$scope.phno.toString(),"mono":$scope.mono,"gender":$scope.gender.GENDER_NAME,"country": $scope.country.COUNTRY_CODE,"state": $scope.state.STATE_NAME,"city": $scope.city,"address": $scope.address,"zip":$scope.zip,"status":$scope.status.STATUS_CODE,"isfd":$scope.flowdesign.VALUE},
      }).then(function mySucces(response) {
        //$scope.errormess = response.data.message;
        if(response.data.message == 'success' || response.data.message == 'Employee Data Updated'){
          $('#alertModelBoxBtn').trigger('click');
          $timeout(function() {
            $scope.manageEmployee();
          }, 3000);
        }

      }, function myError(response) {
        $scope.errormess = response.statusText;
      });
    }
  }
  $scope.manageEmployee = function() {
    window.location.href = manage_bank_admin;
  }
});