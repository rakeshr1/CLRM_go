var method, id;
id = getParamValeuByName('id');
var app = angular.module('touchpoint', [
    'ngRoute',
    '720kb.datepicker'
]);
app.controller('tpCtrl', function ($scope, $http, $timeout) {
	$('#loader').css({ 'display': 'none' });
	var gender_data = { "GENDER_LIST": [{ "GENDER_LABEL": "MALE", "GENDER_NAME": "MALE" }, { "GENDER_LABEL": "FEMALE", "GENDER_NAME": "FEMALE" }] };
	var account_data = { "ACCTYPE_LIST": [{ "ACCOUNT_LABEL": "TYPE 1", "ACCOUNT_NAME": "TYPE1" }, { "ACCOUNT_LABEL": "TYPE 2", "ACCOUNT_NAME": "TYPE2" }] };
	$http({
		method: 'GET',
		url: apiURL + '/bankList/',
		params: { "type": 'json' },
	}).then(function mySucces(response) {
		$scope.banklistoptions = response.data.BANKLIST;
	}, function myError(response) {
		$scope.errormess = response.statusText;
	});
	$http({
		method: 'GET',
		url: apiURL + '/countryList/',
		params: { "type": 'json' },
	}).then(function mySucces(response) {
		$scope.countrylistoptions = response.data.COUNTRYLIST;
	}, function myError(response) {
		$scope.errormess = response.statusText;
	});
	$scope.genderlistoptions = gender_data.GENDER_LIST;
	$scope.acclistoptions = account_data.ACCTYPE_LIST;

	if (id == '') {
		$scope.title = 'Add Customer';
		$scope.branchtemp = '';
		$scope.employeetemp = '';
		$scope.alertMessage = 'Customer Data Added Successfullly';
	} else {
		$scope.title = 'Edit Customer Profile';
		$scope.alertMessage = 'Customer Data Updated Successfully';
		$http({
			method: 'GET',
			url: apiURL + '/customer/',
			params: { "id": id },
		}).then(function mySucces(response) {
			angular.forEach($scope.banklistoptions, function (task, index) {
				if (task.BANK_CODE == response.data.CUSTOMER_DATA.BANK_CODE) {
					$scope.banklist = $scope.banklistoptions[index];
				}
			});
			angular.forEach($scope.countrylistoptions, function (task, index) {
				if (task.COUNTRY_CODE == response.data.CUSTOMER_DATA.COUNTRY) {
					$scope.country = $scope.countrylistoptions[index];
				}
			});
			angular.forEach($scope.acclistoptions, function (task, index) {
				if (task.ACCOUNT_NAME == response.data.CUSTOMER_DATA.ACCOUNT_TYPE) {
					$scope.account = $scope.acclistoptions[index];
				}
			});
			angular.forEach($scope.genderlistoptions, function (task, index) {
				if (task.GENDER_NAME == response.data.CUSTOMER_DATA.GENDER) {
					$scope.gender = $scope.genderlistoptions[index];
				}
			});
			$scope.statetemp = response.data.CUSTOMER_DATA.STATE;
			$scope.branchtemp = response.data.CUSTOMER_DATA.BRANCH_CODE;
			$scope.employeetemp = response.data.CUSTOMER_DATA.EMPLOYEE_ID;
			$scope.email = response.data.CUSTOMER_DATA.EMAIL;
			$scope.password = response.data.CUSTOMER_DATA.PASSWORD;
			$scope.first_name = response.data.CUSTOMER_DATA.FIRST_NAME;
			$scope.last_name = response.data.CUSTOMER_DATA.LAST_NAME;
			$scope.dob = response.data.CUSTOMER_DATA.DATE_OF_BIRTH;
			$scope.doj = response.data.CUSTOMER_DATA.DATE_OF_JOIN;
			$scope.occupation = response.data.CUSTOMER_DATA.OCCUPATION;
			//$scope.dob = response.data.CUSTOMER_DATA.DATE_OF_BIRTH;
			$scope.phno = response.data.CUSTOMER_DATA.PHONE_NO;
			$scope.mono = response.data.CUSTOMER_DATA.MOBILE_NO;
			$scope.gendertemp = response.data.CUSTOMER_DATA.GENDER;
			$scope.zip = response.data.CUSTOMER_DATA.PINCODE;
			$scope.city = response.data.CUSTOMER_DATA.CITY;
			$scope.address = response.data.CUSTOMER_DATA.ADDRESS;
			$scope.getBranchList();
			$scope.getStates();
		}, function myError(response) {
			$scope.errormess = response.statusText;
		});
	}
	$scope.getBranchList = function () {
		$http({
			method: 'GET',
			url: apiURL + '/bankBranchList/',
			params: { "id": $scope.banklist.BANK_CODE },
		}).then(function mySucces(response) {
			$scope.branchlistoptions = response.data.BRANCH_LIST;

			angular.forEach($scope.branchlistoptions, function (task, index) {
				if (task.BRANCH_CODE == $scope.branchtemp) {
					$scope.branchlist = $scope.branchlistoptions[index];
					$scope.getEmployeeList();
				}
			});
		}, function myError(response) {
			$scope.errormess = response.statusText;
		});
	}
	$scope.getEmployeeList = function () {
		$http({
			method: 'GET',
			url: apiURL + '/branchEmployeeList/',
			params: { "id": $scope.branchlist.BRANCH_CODE },
		}).then(function mySucces(response) {
			$scope.employeelistoptions = response.data.EMPLOYEE_LIST;
			angular.forEach($scope.employeelistoptions, function (task, index) {
				if (task.EMPLOYEE_ID == $scope.employeetemp) {
					$scope.employeelist = $scope.employeelistoptions[index];
				}
			});
		}, function myError(response) {
			$scope.errormess = response.statusText;
		});

	}
	$scope.getStates = function () {
		$http({
			method: 'GET',
			url: apiURL + '/stateList/',
			params: { "country_code": $scope.country.COUNTRY_CODE },
		}).then(function mySucces(response) {
			$scope.statelistoptions = response.data.STATELIST;
			angular.forEach($scope.statelistoptions, function (task, index) {
				if (task.STATE_CODE == $scope.statetemp) {
					$scope.state = $scope.statelistoptions[index];
				}
			});
		}, function myError(response) {
			$scope.errormess = response.statusText;
		});

	}
	$scope.validateForm = function () {
		var bankname = $scope.banklist;
		if (bankname == '' || bankname == undefined || bankname == 'null' || bankname == false) {
			$scope.errormess = 'Please Select Bank';
			return false;
		} else {
			if (id == '') {
				method = 'PUT';
			} else {
				method = 'POST';
			}
			$http({
				method: method,
				url: apiURL + '/customer/',
				params: { "bankname": $scope.banklist.BANK_CODE, "id": id, "branchname": $scope.branchlist.BRANCH_CODE, "employee": $scope.employeelist.EMPLOYEE_ID, "account": $scope.account.ACCOUNT_NAME, "email": $scope.email, "firstname": $scope.first_name, "lastname": $scope.last_name, "occupation": $scope.occupation, "doj": $scope.doj, "phno": $scope.phno, "mono": $scope.mono, "gender": $scope.gender.GENDER_NAME, "country": $scope.country.COUNTRY_CODE, "state": $scope.state.STATE_CODE, "city": $scope.city, "address": $scope.address, "zip": $scope.zip, "dob": $scope.dob },
			}).then(function mySucces(response) {
				//$scope.errormess = response.data.message;
				if (response.data.message == 'success' || response.data.message == 'Customer Data Updated') {
					$('#alertModelBoxBtn').trigger('click');
					$timeout(function () {
						$scope.manageCustomer();
					}, 3000);
				}
			}, function myError(response) {
				$scope.errormess = response.statusText;
			});
		}
	}
	$scope.manageCustomer = function () {
		window.location.href = manage_customer;
	}
});