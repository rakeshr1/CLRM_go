var app = angular.module('touchpoint', []);
app.controller('tpCtrl', function($scope, $http) {
$('#loader').css({'display':'none'});
  $http.get(apiURL+'/roleList/').then(function (response) {
	 $('#roleList').html(response.data.ROLELIST);
	  $scope.dataTable();
  });
  $scope.addRole = function() {
		window.location.href = add_role;
	}
	$scope.dataTable = function() {
	
	var oTable = $('#example').dataTable({
                    "oLanguage": {
                        "sSearch": "Search all columns"
                    },
                    "aoColumnDefs": [
                        {
                            'bSortable': false,
                            'aTargets': [0,3]
                        } //disables sorting for column one
            ],
                    'iDisplayLength': 10,
                    "sPaginationType": "full_numbers",
                    "dom": 'T<"clear">lfrtip'
                });
	}
});