var app = angular.module('touchpoint', []);
app.controller('tpCtrl', function($scope, $http) {
$('#loader').css({'display':'none'});
 /* $http.get(apiURL+'/targetList/').then(function (response) 
  {
	 
	 //alert(response.data.TARGETLIST);
	 $('.targetList').html(response.data.TARGETLIST);
    console.log(response.data);
	  $scope.dataTable();
  });*/
$http({
		method:"GET",
		url:apiURL+'/targetList/',
		params:{"bankid":localStorage.bankcode, "roleid":localStorage.roleId, "branchid":localStorage.branchId},
	}).then(function mySuccess(response)
	{	
	//alert(response.data);
	$('.targetList').html(response.data.TARGETLIST);
    console.log(response.data);
	  $scope.dataTable();
	});
 $scope.addTarget = function() {
	 
		window.location.href = add_target;
	}
	$scope.dataTable = function() {
	
	var oTable = $('#example').dataTable({
                    "oLanguage": {
                        "sSearch": "Search all columns"
                    },
                    "aoColumnDefs": [
                        {
                            'bSortable': false,
                            'aTargets': [0,4]
                        } //disables sorting for column one
            ],
                    'iDisplayLength': 10,
                    "sPaginationType": "full_numbers",
                    "dom": 'T<"clear">lfrtip'
                });
	}
});