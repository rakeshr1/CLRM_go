var app = angular.module('touchpoint', []);
app.controller('tpCtrl', function($scope, $http) {
$('#loader').css({'display':'none'});
  $http.get(apiURL+'/manageBanklist/').then(function (response) {
	  console.log(response.data.BANKLIST);
	 $('#employeeList').html(response.data.BANKLIST);
	 $scope.dataTable();
  });
  $scope.addEmployee = function() {
		window.location.href = add_bank;
	}
	$scope.dataTable = function() {
	
	var oTable = $('#example').dataTable({
                    "oLanguage": {
                        "sSearch": "Search all columns"
                    },
                    "aoColumnDefs": [
                        {
                            'bSortable': false,
                            'aTargets': [0,4]
                        } //disables sorting for column one
            ],
                    'iDisplayLength': 10,
                    "sPaginationType": "full_numbers",
                    "dom": 'T<"clear">lfrtip'
                });
	}
});