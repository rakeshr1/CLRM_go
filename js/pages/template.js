$(document).ready(function() {
	$("#txtEditor").Editor();		
});

var method,id;
id = getParamValeuByName('id');
var app = angular.module('touchpoint', []);
app.controller('tpCtrl', function($scope, $http, $timeout) {	
$('#loader').css({'display':'none'});
	$scope.tmptypelistoptions = [{"TEMPLATE_TYPE":"email"},{"TEMPLATE_TYPE":"sms"}];
	$http({
		method : 'GET',
		url :apiURL+'/bank/',
		params: {"id": 1},
	}).then(function mySucces(response) {
		
		$scope.banklist=response.data.BANK_DATA.BANK_NAME;
		//$scope.banklistoptions = response.data.BANKLIST;
    angular.forEach($scope.banklistoptions, function (task, index) {
      if (task.BANK_CODE == localStorage.bankCode) {
        $scope.banklist = $scope.banklistoptions[index];
      }
    });
	},
	function myError(response) {
		$scope.errormess = response.statusText;
	});
				
	if(id == '') {
		$scope.title = 'Add Template';
		$scope.screnShot = '';
		$scope.alertMessage = 'Template Added Successfullly';
	} else {
		$scope.title = 'Edit Template';
		$scope.alertMessage = 'Template Updated Successfully';
		$http({
				method : 'GET',
				url : apiURL+'/templateData/',
				params: {"id": id},
		}).then(function mySucces(response) {
				//console.log(response.data.TEMPLATE_DATA);
				angular.forEach($scope.banklistoptions, function (task, index) {
					if (task.BANK_CODE == response.data.TEMPLATE_DATA.BANK_CODE) {
						$scope.banklist = $scope.banklistoptions[index];
					}
					
				});
				angular.forEach($scope.tmptypelistoptions, function (task, index) {
					if (task.TEMPLATE_TYPE == response.data.TEMPLATE_DATA.TEMPLATE_TYPE) {
						$scope.tmp_type = $scope.tmptypelistoptions[index];
					}
					
				});
				$scope.tmp_name = response.data.TEMPLATE_DATA.TEMPLATE_NAME;
				$scope.screnShot = response.data.TEMPLATE_DATA.SCREENSHOT;
				$("#txtEditor").Editor("setText", response.data.TEMPLATE_DATA.TEMPLATE_CONTENT);
		}, function myError(response) {
				$scope.errormess = response.statusText;
		});
	
	}
	$scope.validateForm = function() {
		var tmp_content=$("#txtEditor").Editor('getText');
		/* var bankname = $scope.banklist;
		if(bankname == '' || bankname == undefined || bankname == 'null' || bankname == false) {
			$scope.errormess = 'Please Select Bank';
			return false;
		} else { */
			$('#screenshot').html(tmp_content);
			html2canvas([document.getElementById('screenshot')], {   
			onrendered: function(canvas)  {
						//$('#screenshot').css("display","none");
						var img = canvas.toDataURL();
						$.post("screenshot/save.php", {data: img,sShot:$scope.screnShot}, function (file) {
						$scope.screnShot = file;
						
						var data = $.param({
										id:id,
										bankname: localStorage.bankcode, 
										tmp_type:$scope.tmp_type.TEMPLATE_TYPE,
										tmp_name:$scope.tmp_name,
										tmp_content:tmp_content,screenShot:$scope.screnShot})

            console.log(data);
						var config = {headers :{'Content-Type': 'application/x-www-form-urlencoded;charset=utf-8;'}};
		
						if(id == '') {
							
							$http.post(apiURL+'/template/', data, config)
							.success(function (data, status, headers, config) {
								//alert(localStorage.bankcode);
								$('#alertModelBoxBtn').trigger('click');
								$timeout(function() {
									
									$scope.manageTemplate();
								}, 3000); 
							  
							})
							.error(function (data, status, header, config) {
								//
							});
						} else {
							$http.post(apiURL+'/updatetemplate/', data, config)
							.success(function (data, status, headers, config){
								$('#alertModelBoxBtn').trigger('click');
								$timeout(function() {
									$scope.manageTemplate();
								}, 3000);
							})
							.error(function (data, status, header, config) {
								//
							});
						}
						
						});   
			},
			width:125,
			height:125,
			});
		
	}
	$scope.manageTemplate = function() {
		 window.location.href = manage_template;
	}	
});