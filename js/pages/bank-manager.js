var app = angular.module('touchpoint', [
    'ngRoute',
  'ngBootstrap'
  ]);
app.controller('tpCtrl', function ($scope, $http, $filter) {
  $('#loader').css({
    'display': 'none'
  });
  //  $scope.LoadPieJson(1);
  $scope.date = {
    startDate: moment(),
    endDate: moment()
  };
  $scope.thisMonth = moment().month() + 1;
  $scope.thisQuarter = '';
  $scope.startMonth = '';
  $scope.endMonth = '';

  if ($scope.thisMonth <= 3){
    $scope.startMonth = 0;
    $scope.endMonth = 9;
  }
  else if ($scope.thisMonth <= 6) {
    $scope.thisQuarter = 2;
    $scope.startMonth = 3;
    $scope.endMonth = 6;
  } else if ($scope.thisMonth <= 9){
    $scope.thisQuarter = 3;
    $scope.startMonth = 6;
    $scope.endMonth = 3;
  }
  else{
    $scope.thisQuarter = 4;
    $scope.startMonth = 9;
    $scope.endMonth = 0;
  }
//  console.log($scope.date.startDate['_d']);
//  console.log($scope.date.startDate['_d'].getMonth());
  $scope.ranges = {
    'Today': [moment(), moment()],
    'Last 7 days': [moment().subtract('days', 7), moment()],
    'Last 30 days': [moment().subtract('days', 30), moment()],
    'This month': [moment().startOf('month'), moment().endOf('month')],
    'This Quarter': [moment().startOf('year').add('month', $scope.startMonth), moment()]
  };
  $scope.startDateInitial = $scope.ranges["Today"][0]["_d"];
  $scope.endDateInitial = $scope.ranges["Today"][0]["_d"];

  $scope.changed = function () {
    console.log($scope.date["startDate"].getDate());
    console.log($scope.date["endDate"].getDate());

  }
  $scope.listing = 'officer';
  $scope.LoadPieJson = function (x) {
    if ($scope.date["startDate"]["_d"] != null && $scope.date["endDate"]["_d"] != null) {
      var fromMonthAlone = $scope.date["startDate"]['_d'].getMonth() + 1;
      fromMonthAlone = "0"+fromMonthAlone.toString().slice(-2);
      fromDate = fromMonthAlone + "/" + ('0' + $scope.date["startDate"]['_d'].getDate()).slice(-2) + "/" + $scope.date["startDate"]['_d'].getFullYear();

      var endMonthAlone = $scope.date["endDate"]['_d'].getMonth() + 1;
      endMonthAlone = "0"+endMonthAlone.toString().slice(-2);
      toDate = endMonthAlone + "/" + ('0' + $scope.date["endDate"]['_d'].getDate()).slice(-2) + "/" + $scope.date["endDate"]['_d'].getFullYear();
     
    } else {
      var fromMonthAlone = $scope.date["startDate"].getMonth() + 1;
      fromMonthAlone = "0"+fromMonthAlone.toString();
      fromDate = fromMonthAlone + "/" + ('0' + $scope.date["startDate"].getDate()).slice(-2) + "/" + $scope.date["startDate"].getFullYear();

      var endMonthAlone = $scope.date["endDate"].getMonth() + 1;
      endMonthAlone = "0"+endMonthAlone.toString();
      toDate = endMonthAlone + "/" + ('0' + $scope.date["endDate"].getDate()).slice(-2) + "/" + $scope.date["endDate"].getFullYear();

    }

    console.log("from date "+fromDate);
    console.log("to date "+toDate);
	
    if ($scope.listing == 'product') {
	  	var pr_count;
		pr_count=document.getElementById("product_count").value;
		if(pr_count==1){
		$scope.date = {
		startDate: moment(),
		endDate: moment()
		};
	   fromDate = getdatefrom();
       toDate = getdateto();	   		
		
		pr_count=pr_count+1;
		document.getElementById("product_count").value=pr_count;
		}	
      console.log("productFromDat");
      loadClass = 'productChartList';
	  
    } else if ($scope.listing == 'date') {
		 	var c_count;
		c_count=document.getElementById("data_count").value;
		if(c_count==1){
		$scope.date = {
		startDate: moment(),
		endDate: moment()
		};
		 fromDate = getdatefrom();
       toDate = getdateto();
	  		
		
		c_count=c_count+1;
		document.getElementById("data_count").value=c_count;
		}
      console.log("date");
      loadClass = 'dateChartList';
	  
    } else {
		
      console.log("officer");
      loadClass = 'officerChartList';
    }
    if (fromDate == '' && toDate == '') {} else if (x == 1 && (fromDate > toDate || fromDate.length != 10 || toDate.length != 10)) {
      console.log("Bigger");
      return false;
    }
    $http({
      method: 'GET',
      url: apiURL + '/customerResponseBm/',
      params: {
        bankId: localStorage.bankcode,
        branchId: localStorage.branchId,
        fromDate: fromDate,
        toDate: toDate,
        listing: $scope.listing
      },
    }).then(function mySucces(response) {
		//alert(localStorage.bankcode);
		//alert(localStorage.branchId)
      $('.' + loadClass).html('');
      angular.forEach(response.data, function (task, index) {
        console.log(response.data);
        for (property in response.data[index]) {
          emailCount = Math.floor(response.data[index][property][0]["SEND_COUNT"]);
          smsCount = Math.floor(response.data[index][property][1]["SEND_COUNT"]);
          voiceCount = Math.floor(response.data[index][property][1]["SEND_COUNT"]);
          leadCount = Math.floor(response.data[index][property][3]["SEND_COUNT"]);
          loanCount = Math.floor(response.data[index][property][4]["SEND_COUNT"]);
          depositCount = Math.floor(response.data[index][property][5]["SEND_COUNT"]);

          $('.' + loadClass).append("<div id='readOnlyInputDiv' class='x_content circle_slider_p collapsibleDiv'><p class='cust_name_append'>" + property + "<span style='float:right'><a class='collapseChild' data-state='expanded'><i class='fa fa-chevron-circle-down' aria-hidden='true'></i></a></span></p><div class='col-xs-12 col-sm-12 col-md-12 chartDiv'><div class='col-xs-6 col-md-2 col-sm-4' style='margin-bottom:15px;'><div style='display:inline;width:100px;height:120px;'><input data-max='"+Math.pow(10, emailCount.toString().length)+"' readonly value='" + Math.floor(response.data[index][property][0]["SEND_COUNT"]) + "' data-fgcolor='#26B99A' data-angleoffset='90' data-height='120' data-width='100' class='knob' style='width: 54px; height: 33px; position: absolute; vertical-align: middle; margin-top: 33px; margin-left: -77px; border: 0px none; background: transparent none repeat scroll 0% 0%; font: bold 20px Arial; text-align: center; color: rgb(38, 185, 154); padding: 0px;'></div><p>Email sent</p></div><div class='col-xs-6 col-md-2 col-sm-4' style='margin-bottom:15px;'><div style='display:inline;width:100px;height:120px;'><input data-max='"+Math.pow(10, smsCount.toString().length)+"' readonly value='" + Math.floor(response.data[index][property][1]["SEND_COUNT"]) + "' data-rotation='anticlockwise' data-fgcolor='#34495E' data-anglearc='250' data-angleoffset='-125' data-height='120' data-width='100' class='knob' style='width: 54px; height: 33px; position: absolute; vertical-align: middle; margin-top: 33px; margin-left: -77px; border: 0px none; background: transparent none repeat scroll 0% 0%; font: bold 20px Arial; text-align: center; color: rgb(52, 73, 94); padding: 0px;'></div><p>SMS sent</p></div><div class='col-xs-6 col-md-2 col-sm-4' style='margin-bottom:15px;'><div style='display:inline;width:100px;height:120px;'><input data-max='"+Math.pow(10, voiceCount.toString().length)+"' readonly value='" + Math.floor(response.data[index][property][2]["SEND_COUNT"]) + "' data-fgcolor='#3498DB' data-angleoffset='90' data-height='120' data-width='100' class='knob' style='width: 54px; height: 33px; position: absolute; vertical-align: middle; margin-top: 33px; margin-left: -77px; border: 0px none; background: transparent none repeat scroll 0% 0%; font: bold 20px Arial; text-align: center; color: rgb(38, 185, 154); padding: 0px;'></div><p>Voice sent</p></div><div class='col-xs-6 col-md-2 col-sm-4' style='margin-bottom:15px;'><div style='display:inline;width:100px;height:120px;'><input data-max='"+Math.pow(10, leadCount.toString().length)+"' readonly value='" + Math.floor(response.data[index][property][3]["SEND_COUNT"]) + "' data-rotation='anticlockwise' data-fgcolor='#DE7571' data-anglearc='250' data-angleoffset='-125' data-height='120' data-width='100' class='knob' style='width: 54px; height: 33px; position: absolute; vertical-align: middle; margin-top: 33px; margin-left: -77px; border: 0px none; background: transparent none repeat scroll 0% 0%; font: bold 20px Arial; text-align: center; color: rgb(52, 73, 94); padding: 0px;'></div><p>Leads</p></div><div class='col-xs-6 col-md-2 col-sm-4' style='margin-bottom:15px;'><div style='display:inline;width:110px;height:120px;'><input data-max='"+Math.pow(10, loanCount.toString().length)+"' readonly value='" + Math.floor(response.data[index][property][4]["SEND_COUNT"]) + "' data-thickness='.2' data-skin='tron' data-fgcolor='#00bfff' data-displayprevious='true' data-height='120' data-width='110' class='knob' style='width: 59px; height: 36px; position: absolute; vertical-align: middle; margin-top: 36px; margin-left: -84px; border: 0px none; background: transparent none repeat scroll 0% 0%; font: bold 22px Arial; text-align: center; color: rgb(38, 185, 154); padding: 0px;'></div><p>Loans $ "+ Math.floor(response.data[index][property][6]["SEND_COUNT"]).toString().replace(/\B(?=(\d{3})+(?!\d))/g, ',') +"</p></div><div class='col-xs-6 col-md-2 col-sm-4' style='margin-bottom:15px;'><div style='display:inline;width:100px;height:120px;'><input data-max='"+Math.pow(10, depositCount.toString().length)+"' readonly value='" + Math.floor(response.data[index][property][5]["SEND_COUNT"]) + "' data-fgcolor='#26B99A' data-angleoffset='90' data-height='120' data-width='100' class='knob' style='width: 54px; height: 33px; position: absolute; vertical-align: middle; margin-top: 33px; margin-left: -77px; border: 0px none; background: transparent none repeat scroll 0% 0%; font: bold 20px Arial; text-align: center; color: rgb(38, 185, 154); padding: 0px;'></div><p>Deposits</p></div></div></div>");
       }
        $scope.changeListng = function(u,v) {
          $scope.listing = u;
          if($('.'+v).html() == ''){
            $scope.LoadPieJson();
          }
        }
        $(function ($) {

          $(".knob").knob({
            change: function (value) {
              //console.log("change : " + value);
            },
            release: function (value) {
              //console.log(this.$.attr('value'));
              console.log("release : " + value);
            },
            cancel: function () {
              console.log("cancel : ", this);
            },
            /*format : function (value) {
                 return value + '%';
                 },*/
            draw: function () {

              // "tron" case
              if (this.$.data('skin') == 'tron') {

                this.cursorExt = 0.3;

                var a = this.arc(this.cv) // Arc
                ,
                    pa // Previous arc
                , r = 1;

                this.g.lineWidth = this.lineWidth;

                if (this.o.displayPrevious) {
                  pa = this.arc(this.v);
                  this.g.beginPath();
                  this.g.strokeStyle = this.pColor;
                  this.g.arc(this.xy, this.xy, this.radius - this.lineWidth, pa.s, pa.e, pa.d);
                  this.g.stroke();
                }

                this.g.beginPath();
                this.g.strokeStyle = r ? this.o.fgColor : this.fgColor;
                this.g.arc(this.xy, this.xy, this.radius - this.lineWidth, a.s, a.e, a.d);
                this.g.stroke();

                this.g.lineWidth = 2;
                this.g.beginPath();
                this.g.strokeStyle = this.o.fgColor;
                this.g.arc(this.xy, this.xy, this.radius - this.lineWidth + 1 + this.lineWidth * 2 / 3, 0, 2 * Math.PI, false);
                this.g.stroke();

                return false;
              }
            }
          });

          // Example of infinite knob, iPod click wheel
          var v, up = 0,
              down = 0,
              i = 0,
              $idir = $("div.idir"),
              $ival = $("div.ival"),
              incr = function () {
                i++;
                $idir.show().html("+").fadeOut();
                $ival.html(i);
              },
              decr = function () {
                i--;
                $idir.show().html("-").fadeOut();
                $ival.html(i);
              };
          $("input.infinite").knob({
            min: 0,
            max: 20,
            stopper: false,
            change: function () {
              if (v > this.cv) {
                if (up) {
                  decr();
                  up = 0;
                } else {
                  up = 1;
                  down = 0;
                }
              } else {
                if (v < this.cv) {
                  if (down) {
                    incr();
                    down = 0;
                  } else {
                    down = 1;
                    up = 0;
                  }
                }
              }
              v = this.cv;
            }
          });
        });

      });
    }, function myError(response) {
      $scope.errormess = response.statusText;
    });
  }
  $scope.LoadPieJson();
    function getdatefrom(){
	  if ($scope.date["startDate"]["_d"] != null && $scope.date["endDate"]["_d"] != null) {
      var fromMonthAlone = $scope.date["startDate"]['_d'].getMonth() + 1;
      fromMonthAlone = "0" + fromMonthAlone.toString().slice(-2);
      fromDate = fromMonthAlone + "/" + ('0' + $scope.date["startDate"]['_d'].getDate()).slice(-2) + "/" + $scope.date["startDate"]['_d'].getFullYear();
      //		toDate = '';
      var endMonthAlone = $scope.date["endDate"]['_d'].getMonth() + 1;
      endMonthAlone = "0" + endMonthAlone.toString().slice(-2);
      toDate = endMonthAlone + "/" + ('0' + $scope.date["endDate"]['_d'].getDate()).slice(-2) + "/" + $scope.date["endDate"]['_d'].getFullYear();
     
	   
    } else {
	
      var fromMonthAlone = $scope.date["startDate"].getMonth() + 1;
      fromMonthAlone = "0" + fromMonthAlone.toString();
      fromDate = fromMonthAlone + "/" + ('0' + $scope.date["startDate"].getDate()).slice(-2) + "/" + $scope.date["startDate"].getFullYear();
     
      var endMonthAlone = $scope.date["endDate"].getMonth() + 1;
      endMonthAlone = "0" + endMonthAlone.toString();
      toDate = endMonthAlone + "/" + ('0' + $scope.date["endDate"].getDate()).slice(-2) + "/" + $scope.date["endDate"].getFullYear();
    
    }  
   return fromDate;
  }

  function getdateto(){
	  
	  
	   if ($scope.date["startDate"]["_d"] != null && $scope.date["endDate"]["_d"] != null) {
      var fromMonthAlone = $scope.date["startDate"]['_d'].getMonth() + 1;
      fromMonthAlone = "0" + fromMonthAlone.toString().slice(-2);
      fromDate = fromMonthAlone + "/" + ('0' + $scope.date["startDate"]['_d'].getDate()).slice(-2) + "/" + $scope.date["startDate"]['_d'].getFullYear();
      //		toDate = '';
      var endMonthAlone = $scope.date["endDate"]['_d'].getMonth() + 1;
      endMonthAlone = "0" + endMonthAlone.toString().slice(-2);
      toDate = endMonthAlone + "/" + ('0' + $scope.date["endDate"]['_d'].getDate()).slice(-2) + "/" + $scope.date["endDate"]['_d'].getFullYear();
     
	   
    } else {
	
      var fromMonthAlone = $scope.date["startDate"].getMonth() + 1;
      fromMonthAlone = "0" + fromMonthAlone.toString();
      fromDate = fromMonthAlone + "/" + ('0' + $scope.date["startDate"].getDate()).slice(-2) + "/" + $scope.date["startDate"].getFullYear();
     
      var endMonthAlone = $scope.date["endDate"].getMonth() + 1;
      endMonthAlone = "0" + endMonthAlone.toString();
      toDate = endMonthAlone + "/" + ('0' + $scope.date["endDate"].getDate()).slice(-2) + "/" + $scope.date["endDate"].getFullYear();
      
    }  
    return toDate;
  }
});