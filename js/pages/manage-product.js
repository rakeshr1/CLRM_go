var app = angular.module('touchpoint', []);
app.controller('tpCtrl', function($scope, $http) {
$('#loader').css({'display':'none'});
  $http.get(apiURL+'/productList/?bankcode='+localStorage.bankcode).then(function (response) {
	 $('#productList').html(response.data.BANKLIST);
	 $scope.dataTable();
  });
   $scope.addProduct = function() {
		window.location.href = add_product;
	}
	$scope.dataTable = function() {
	
	var oTable = $('#example').dataTable({
                    "oLanguage": {
                        "sSearch": "Search all columns"
                    },
                    "aoColumnDefs": [
                        {
                            'bSortable': false,
                            'aTargets': [0,2]
                        } //disables sorting for column one
            ],
                    'iDisplayLength': 10,
                    "sPaginationType": "full_numbers",
                    "dom": 'T<"clear">lfrtip'
                });
	}
});