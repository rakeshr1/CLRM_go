var app = angular.module('touchpoint', [
    'ngRoute',
    '720kb.datepicker'
  ]);
app.controller('tpCtrl', function($scope, $http, $window) {
$('#loader').css({'display':'none'});
//  var windowScreen = angular.element($window);
//  $scope.groupItems = 3;
//  windowScreen.bind('resize', function(){
//    if($window.innerWidth < 768){
//            $scope.groupItems = 3;
////      console.log($window.innerWidth);
//      console.log(groupItems);
//    }
////      $scope.groupItems = 3;
//    else
//      $scope.groupItems = 6;
//  })
	$scope.listing = 'branch';
	$scope.productFromDate = '';
	$scope.productToDate = '';
	$scope.dateFromDate = '';
	$scope.dateToDate = '';
	$scope.branchFromDate = '';
	$scope.branchToDate = '';
	$scope.customerFromDate = '';
	$scope.customerToDate = '';
	$scope.LoadPieJson = function(x) {
		fromDate = '';
		toDate = '';
		if($scope.listing == 'product') {
			fromDate = $scope.productFromDate;
			toDate = $scope.productToDate;
			loadClass = 'productChartList';
		} else if($scope.listing == 'date') {
			fromDate = $scope.dateFromDate;
			toDate = $scope.dateToDate;
			loadClass = 'dateChartList';
		} else if($scope.listing == 'customer') {
			fromDate = $scope.customerFromDate;
			toDate = $scope.customerToDate;
			loadClass = 'customerChartList';
		} else {
			fromDate = $scope.branchFromDate;
			toDate = $scope.branchToDate;
			loadClass = 'branchChartList';
		}
		if(fromDate == '' && toDate == '') {
		}else if(x == 1 && (fromDate > toDate || fromDate.length != 10 || toDate.length !=10)) {
			console.log("Bigger");
			return false;
		}    
		$http({
			method : 'GET',
			url : apiURL+'/customerResponse/',
			params: {bankId:localStorage.bankId,fromDate:fromDate,toDate:toDate,listing:$scope.listing},
		}).then(function mySucces(response) {
			$('.'+loadClass).html('');
			angular.forEach(response.data, function (task, index) {	
				for(property in response.data[index]) {
					$('.'+loadClass).append("<div class='row progress_row' ><div onclick='hideShowC(this)' class='right_minmax joeclkdiv joehidediv' operation='hidecir'><i class='fa joecoll fa-minus'></i></div><div class='col-md-2 col-xs-6 col-sm-4 cus_na'><span class='custmr_name'>"+property.replace("**", " to ")+"</span></div><div class='col-md-12 col-xs-12 col-sm-12 joediv' style='text-align: center;'><div id='"+loadClass+"chart"+index+"' style='height:200px;text-align:center;'></div></div></div>");
					//console.log(response.data[index][property]);
					$scope.createPie(response.data[index][property], "#"+loadClass+"chart"+index);
				}
				
			});
		}, function myError(response) {
			$scope.errormess = response.statusText;
		});
	}
	
	$scope.changeListng = function(u,v) {
		$scope.listing = u;
		if($('.'+v).html() == ''){
			$scope.LoadPieJson();
		}
	}
	
	$scope.createPie = function(pData, pElement) {
		var m = 10, r = 40, z = d3.scale.category20c();
		var pie = d3.layout.pie()
			.value(function(d) { return +d.SEND_COUNT; })
			.sort(function(a, b) { return b.SEND_COUNT - a.SEND_COUNT; });
		var arc = d3.svg.arc()
			.innerRadius(r / 1.5)
			.outerRadius(r);
		var jsonData = d3.nest()
			  .key(function(d) { return d.CUSTOMER_RESPONSE; })
			  .entries(pData);
		var svg = d3.select(pElement).selectAll("div")
			  .data(jsonData)
			.enter().append("div")
			  .style("display", "inline-block")
			  .style("width", (r + m) * 2 + "px")
			  .style("height", (r + m) * 2 + "px")
			.append("svg")
			  .attr("width", (r + m) * 2)
			  .attr("height", (r + m) * 2)
			.append("g")
			  .attr("transform", "translate(" + (r + m) + "," + (r + m) + ")");

		  // Add a label 
		  svg.append("text")
			  .attr("dy", ".35em")
			  .attr("text-anchor", "middle")
			  .text(function(d) { 
					if(d.values[0].SEND_COUNT == 0.01) {
						return 0;
					} else {
						return d.values[0].SEND_COUNT;
					}
			 });

		  var g = svg.selectAll("g")
			  .data(function(d) { return pie(d.values); })
			.enter().append("g");

		  g.append("path")
			  .attr("d", arc)
			  .style("fill", function(d) { 
								if(d.data.SEND_COUNT == 0.01) {
									return 'rgb(204, 204, 204)';
								} else {
									return 'rgb(97, 79, 205)';
								}
								//return z(d.data.SEND_COUNT); 
							
							})
			.append("title")
			  .text(function(d) { 
							//return d.data.CUSTOMER_RESPONSE + ": " + d.data.SEND_COUNT; 
							return '';
						});

		  g.filter(function(d) { return d.endAngle - d.startAngle > .2; }).append("text")
			  .attr("dy", ".35em")
			  .attr("text-anchor", "middle")
			  .attr("transform", function(d) { return "translate(" + arc.centroid(d) + ")rotate(" + $scope.angle(d) + ")"; })
			  .text(function(d) { 
						//return d.data.SEND_COUNT; 
						return '';
					});
			$(pElement+' div').eq(0).append('<span style="text-align:center;display:block;width:100%;">Email Sent<span>');
			$(pElement+' div').eq(1).append('<span style="text-align:center;display:block;width:100%;">SMS Sent<span>');
			$(pElement+' div').eq(2).append('<span style="text-align:center;display:block;width:100%;">Voice Sent<span>');
			$(pElement+' div').eq(3).append('<span style="text-align:center;display:block;width:100%;">Leads<span>');
			var amt = $(pElement+' div').eq(6).children().eq(0).children().eq(0).children().eq(0).text();
    $(pElement+' div').eq(4).append('<span style="text-align:center;display:block;width:100%;">Loans $ ' +amt.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",")+'<span>');
			$(pElement+' div').eq(5).append('<span style="text-align:center;display:block;width:100%;">Deposits<span>');
			$(pElement+' div').eq(6).html('');
			console.log();
				$(pElement).sliderInit({
					'navigation':   'always' 
					, 'indicator':  'always' 
					, 'speed':       1000
					, 'delay':       0
					, 'transition': 'slide' 
					, 'loop':        false     
					, 'group':      6
				});
					
	}
	
	$scope.angle = function(d) {
			var a = (d.startAngle + d.endAngle) * 90 / Math.PI - 90;
			return a > 90 ? a - 180 : a;
	}
	$scope.LoadPieJson();
});