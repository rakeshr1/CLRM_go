var method, id;
id = getParamValeuByName('id');
var app = angular.module('touchpoint', [
  'ngRoute',
  '720kb.datepicker'
]);

app.directive("limitTo", [function () {
  return {
    restrict: "A",
    link: function (scope, elem, attrs) {
      var limit = parseInt(attrs.limitTo);
      angular.element(elem).on("keypress", function (e) {
        if (e.keyCode != 8 && this.value.length == limit) e.preventDefault();
      });
    }
  }
}]);

app.controller('tpCtrl', function ($scope, $http, $timeout) {
	  alert (apiURL);
  $('#loader').css({ 'display': 'none' });
  $scope.email_pattern = /^[a-z]+[a-z0-9._]+@[a-z]+\.[a-z.]{2,5}$/;
  var status_data = { "STATUS_LIST": [{ "STATUS_LABEL": "Active", "STATUS_CODE": "1" }, { "STATUS_LABEL": "Inactive", "STATUS_CODE": "0" }] };
  var fd_data = { "FLOWDESIGN_LIST": [{ "LABEL": "No", "VALUE": "0" }, { "LABEL": "Yes", "VALUE": "1" }] };
  var gender_data = { "GENDER_LIST": [{ "GENDER_LABEL": "Male", "GENDER_NAME": "Male" }, { "GENDER_LABEL": "Female", "GENDER_NAME": "Female" }] };

  if (id == '') {
    $scope.title = 'Add Bank Admin';
    $scope.branchtemp = '';
    $scope.roletemp = '';
    $scope.alertMessage = 'Admin Data Added Successfullly';
  } else {
    $scope.title = 'Edit Bank Admin';
    $scope.alertMessage = 'Bank Admin Data Updated Successfully';
    $http({
      method: 'GET',
      url: apiURL + '/getadminemployee/',
      params: { "id": id },
    }).then(function mySucces(response) {
      
      $scope.banklist = response.data.EMPLOYEE_DATA.BANK_CODE;
      $scope.email = response.data.EMPLOYEE_DATA.EMAIL;
      $scope.password = response.data.EMPLOYEE_DATA.PASSWORD;
      $scope.first_name = response.data.EMPLOYEE_DATA.FIRST_NAME;
      $scope.last_name = response.data.EMPLOYEE_DATA.LAST_NAME;
      $http({
        method: 'GET',
        url: apiURL + '/bank/',
        params: { "id": $scope.banklist },
      }).then(function mySucces(response) {
        console.log(response.data);
        $scope.banklist = response.data.BANK_DATA.BANK_NAME;
        $scope.bankcode = response.data.BANK_DATA.BANK_CODE;
      })



    }, function myError(response) {
      $scope.errormess = response.statusText;
    });
  }

  $scope.validateForm = function () {
     console.log($scope.bankcode);
	  alert (apiURL);
    var bankname = $scope.banklist;
    if (bankname == '' || bankname == undefined || bankname == 'null' || bankname == false) {
      $scope.errormess = 'Please Select Bank';
      return false;
    } else {
      if (id == '') {
        method = 'PUT';
      } else {
        method = 'POST';
      }
      $http({
        method: method,
        url: apiURL + '/admin/',
        params: { "bankname": $scope.banklist, "bankcode":$scope.bankcode, "id": id, "email": $scope.email, "password": $scope.password, "firstname": $scope.first_name, "lastname": $scope.last_name },
      }).then(function mySucces(response) {
		  //alert($scope.banklist);
		  //alert($scope.bankcode);
		  
        if (response.data.message == 'success' || response.data.message == 'Employee Data Added') {
          $('#alertModelBoxBtn').trigger('click');
          $timeout(function () {
            $scope.manageEmployee();
          }, 3000);
        }

      }, function myError(response) { 
	  
	 
        $scope.errormess = response.statusText;
      });


      $http({
        method: 'POST',
        url: apiURL + '/updateAdmin/',
        params: { "bankname": $scope.banklist,"bankcode":$scope.bankcode, "id": id, "email": $scope.email, "password": $scope.password, "firstname": $scope.first_name, "lastname": $scope.last_name },
      }).then(function mySucces(response) {
        if (response.data.message == 'success' || response.data.message == 'Employee Data Updated') {
          $('#alertModelBoxBtn').trigger('click');
          $timeout(function () {
            $scope.manageEmployee();
          }, 3000);
        }

      }, function myError(response) {
        $scope.errormess = response.statusText;
      });
    }
  }
  $scope.getBankList = function () {

	   http({
      method: 'POST',
      url: apiURL + '/getadminbank/',
      params: { "id": id },
    }).then(function mySucces(response) {
      // $scope.banklist = "mani";
      if (response.data.message == 'success' || response.data.message == 'Employee Data Updated') {
        $('#alertModelBoxBtn').trigger('click');
        $timeout(function () {
          $scope.manageEmployee();
        }, 3000);
      }
    }, function myError(response) {
      $scope.errormess = response.statusText;
    });
  }
  $scope.manageEmployee = function () {
    window.location.href = manage_bank;
  }
});