//var method, id;
//id = getParamValeuByName('id');
//var app = angular.module('touchpoint', []);
//app.controller('tpCtrl', function ($scope, $http, $timeout) {
//  //  console.log("Bank id"+localStorage.bankCode);
//  $('#loader').css({
//    'display': 'none'
//  });
//  
//  $http({
//    method: 'GET',
//    url: apiURL + '/targetBank/',
//    params: {
//      "type": 'json',
//      "id": localStorage.bankCode
//    },
//  }).then(function mySucces(response) {
//    $scope.bankname = response.data.BANK_NAME;
//  }, function myError(response) {
//    $scope.errormess = response.data.message;
//  });
//
//  $http({
//    method: 'GET',
//    url: apiURL + '/targetBankBranches/',
//    params: {
//      "type": 'json',
//      "id": localStorage.bankCode
//    },
//  }).then(function mySucces(response) {
//    $scope.branchList = response.data.BRANCH_LIST;
//  }, function myError(response) {
//    $scope.errormess = response.data.message;
//  });
//  
//  if(id != ''){
//    $http({
//      method: 'GET',
//      url: apiURL + '/targetDetails/',
//      params: {
//        "type": 'json',
//        "id": id
//      },
//    }).then(function mySucces(response) {
//      console.log(response.data.TARGET_DETAILS);
//      console.log($scope.branchList);
//      angular.forEach($scope.branchList, function (task, index) {
//        if (task.BRANCH_CODE == response.data.TARGET_DETAILS.BRANCH_CODE) {
//          $scope.branchname = $scope.branchList[index].BRANCH_NAME;
//                    console.log( $scope.branchList[index].BRANCH_NAME);
////          $scope.banklistObj = $scope.banklistoptions[index];
////          console.log($scope.banklistObj.BANK_NAME);
////          $scope.banklist = $scope.banklistObj.BANK_NAME;
////          console.log($scope.banklist);
//        }
//      });
//    }, function myError(response) {
//      $scope.errormess = response.data.message;
//    });
//  }
//  $scope.validateForm = function () {
//    if ($scope.branchname == '' || $scope.branchname == undefined || $scope.branchname == 'null' || $scope.branchname == false) {
//      $scope.errormess = 'Please Select Branch';
//      return false;
//    } else {
//      if (id == '') {
//        console.log("adding");
//        method = 'PUT';
//      } else {
//        console.log("updating");
//
//        method = 'POST';
//      }
//      $http({
//        method: method,
//        url: apiURL + '/setTarget/',
//        params: {
//          "bankid": localStorage.bankCode,
//          "branchid": $scope.branchname,
//          "targetamount": $scope.targetamount,
//          "id": id
//        },
//      }).then(function mySucces(response) {
//        //$scope.errormess = response.data.message;
//        console.log(response.data.message);
//        if (response.data.message == 'Target Updated Successfully' || response.data.message == 'Target Added Successfully') {
//          $scope.alertMessage = response.data.message;
//          $('#alertModelBoxBtn').trigger('click');
//          $timeout(function () {
//            $scope.manageTarget();
//          }, 3000);
//        }
//      }, function myError(response) {
//        $scope.errormess = response.statusText;
//      });
//    }
//  }
//    $scope.manageTarget = function()
//    {
//      window.location.href = manage_target;
//    }
//});


var method, id;
id = getParamValeuByName('id');
var app = angular.module('touchpoint', []);
app.controller('tpCtrl', function ($scope, $http, $timeout) {
  $('#loader').css({
    'display': 'none'
  });
  $http({
    method: 'GET',
    url: apiURL + '/bankList/',
    params: {
      "type": 'json'
    },
  }).then(function mySucces(response) {
    $scope.banklistoptions = response.data.BANKLIST;
//    console.log($scope.banklistoptions);
    angular.forEach($scope.banklistoptions, function (task, index) {
      if (task.BANK_CODE == localStorage.bankcode) {
        $scope.banklistObj = $scope.banklistoptions[index];
//        console.log($scope.banklistObj.BANK_NAME);
        $scope.banklist = $scope.banklistObj.BANK_NAME;
//        console.log($scope.banklist);
      }
    });
  }, function myError(response) {
    $scope.errormess = response.statusText;
  });



  if (id == '') {
	   //alert('add');
    $scope.showHideBranchInput = true;
    $scope.title = 'Add Target';
    $http({
      method: 'GET',
      url: apiURL + '/targetBankBranches/',
      params: {
        "type": 'json',
        "id": localStorage.bankcode,
        "process": "add",
        "horid": localStorage.userId
      },
    }).then(function mySucces(response) {
		//alert(response.data);  
      $scope.branchlistoptions = response.data.BRANCH_LIST;
    }, function myError(response) {
      $scope.errormess = response.data.message;
    });
    $scope.alertMessage = 'Target Added Successfullly';
  } else {
	//alert('Edit');
    $scope.title = 'Edit Target';
    $scope.alertMessage = 'Target Updated Successfully';
    $http({
      method: 'GET',
      url: apiURL + '/targetDetails/',
      params: {
        "id": id
		
      },
    }).then(function mySucces(response) {
		//alert(response);  
      $scope.branchcode = response.data.TARGET_DETAILS.BRANCH_CODE;
      var targetamount = response.data.TARGET_DETAILS.TARGET_AMOUNT;
      $scope.targetamount = parseInt(targetamount);
 console.log($scope.branchcode); 
 
//edit target get the branch list  
      $http({
        method: 'GET',
        url: apiURL + '/targetBankBranchesEdit/',
        params: {
          "type": 'json',
          "id": localStorage.bankcode,
		  "horid": localStorage.userId

        },
      }).then(function mySucces(response) {		 
		//alert(response.data); 
        console.log(response.data); 		
        $scope.showHideInput = true;
        $scope.branchlistoptions = response.data.BRANCH_LIST;
        console.log($scope.branchlistoptions);
        angular.forEach($scope.branchlistoptions, function (task, index) {
          if (task.BRANCH_CODE == $scope.branchcode) {
            $scope.branchlistObj = $scope.branchlistoptions[index];
            $scope.branchlist = $scope.branchlistObj.BRANCH_NAME;
            console.log($scope.branchlistObj.BRANCH_CODE);
          }
        });
      }, function myError(response) {
        $scope.errormess = response.statusText;
      });

      //        angular.forEach($scope.banklistoptions, function (task, index) {
      //          if (task.BANK_CODE == response.data.BRANCH_DATA.BANK_CODE) {
      //            $scope.banklistObj = $scope.banklistoptions[index];
      //            console.log($scope.banklistObj.BANK_NAME);
      //            $scope.banklist = $scope.banklistObj.BANK_NAME;
      //            console.log($scope.banklist);
      //          }
      //        });

      //      $scope.branchname = response.data.BRANCH_DATA.BRANCH_NAME;
      //      $scope.branchcode = response.data.BRANCH_DATA.BRANCH_CODE;

    }, function myError(response) {
      $scope.errormess = response.statusText;
    });
  }
  
  
  
  
  // 
  $scope.validateForm = function () {
    $scope.branchname = $scope.branchlist;
//    console.log("branch name "+$scope.branchname);
//    console.log("branch name" + $scope.branchlist);
    if ($scope.branchname == '' || $scope.branchname == undefined || $scope.branchname == 'null' || $scope.branchname == false) {
      $scope.errormess = 'Please Select Branch';
      return false;
    } else {
      if (id == '') {
        console.log("adding");
        method = 'PUT';
      } else {
        console.log("updating");
        method = 'POST';
      }
      if($scope.branchname.BRANCH_CODE != undefined)
        $scope.currentBranchId = $scope.branchname.BRANCH_CODE;
      else
        $scope.currentBranchId = $scope.branchlistObj.BRANCH_CODE;
      
      $http({
        method: method,
        url: apiURL + '/setTarget/',
        params: {
          "bankid": localStorage.bankcode,
          "branchid": $scope.currentBranchId,
          "targetamount": $scope.targetamount,
          "id": id
        },
      }).then(function mySucces(response) {
        //$scope.errormess = response.data.message;
        console.log(response.data.message);
        if (response.data.message == 'Target Updated Successfully' || response.data.message == 'Target Added Successfully') {
          $scope.alertMessage = response.data.message;
          console.log(response.data.message);
          $('#alertModelBoxBtn').trigger('click');
          $timeout(function () {
            $scope.manageTarget();
          }, 3000);
        }
      }, function myError(response) {
        console.log(response.data.message);
        $scope.errormess = response.statusText;
      });
    }
  }
  $scope.manageTarget = function () {
    window.location.href = manage_target;
  }
});