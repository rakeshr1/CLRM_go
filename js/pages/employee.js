var method, id;
id = getParamValeuByName('id');
var app = angular.module('touchpoint', [
  'ngRoute',
  '720kb.datepicker',
  'checklist-model'
]);

//Directive to limit input value 
app.directive("limitTo", [function () {
  if (id != '') {
    document.getElementById('rolelist').disabled = true;
  }
  return {
    restrict: "A",
    link: function (scope, elem, attrs) {
      var limit = parseInt(attrs.limitTo);
      angular.element(elem).on("keypress", function (e) {
        if (e.keyCode != 8 && this.value.length == limit) e.preventDefault();
      });
    }
  }
}]);

//Main Controller begins
app.controller('tpCtrl', function ($scope, $http, $timeout) {



   

  // toggle selection for a given fruit by name

  //Identifies which role id is accessing this page and hides some fields in page
  if (localStorage.roleId == 5) {
    //      Bank Admin
    $scope.hideForBankAdmin = false;
  } else {
    //      or Other Admin
    $scope.hideForBankAdmin = true;
  }


  $scope.showForHOR = function () {
    if ($scope.hideForBankAdmin == false && (id == null || id == "")) {
      if ($scope.rolelist.ROLE_NAME == "Head Of Retail") {
        if (id == '' || id == null) {
          $http({
            method: 'GET',
            url: apiURL + '/unassignedBranches/',
            params: {
              "type": 'json',
              "bankId": localStorage.bankcode,
              "currentRole": $scope.rolelist.ROLE_NAME
            },
          }).then(function mySucces(response) {
            $scope.unassignedlistHOR = true;
            $scope.unassignedlist = response.data.UNASSIGNED_BRANCHES;
            console.log($scope.unassignedlist);
            // angular.forEach($scope.unassignedlist, function(object, index){
            //   console.log(index);
            // })
          }, function myError(response) {
            $scope.errormess = response.statusText;
          });
        }
        $scope.showSelectForBM = false;
        $scope.showSelectForBO = false;
        $scope.showSelectForHOR = true;
        $scope.selectedDropDown = "HOR";
        $scope.getBranchList();
      } else if ($scope.rolelist.ROLE_NAME == "Bank Officer") {
        $scope.showSelectForBM = false;
        $scope.showSelectForBO = true;
        $scope.showSelectForHOR = false;
        $scope.selectedDropDown = "BO";
        $scope.getBranchList();

      } else if ($scope.rolelist.ROLE_NAME == "Bank Manager") {
         if (id == '' || id == null) {
           $http({
             method: 'GET',
             url: apiURL + '/unassignedBranches/',
             params: {
               "type": 'json',
               "bankId": localStorage.bankcode,
               "currentRole": $scope.rolelist.ROLE_NAME
             },
           }).then(function mySucces(response) {
             $scope.unassignedlist = response.data.UNASSIGNED_BRANCHES;
             $scope.unassignedlistBM = true;
             console.log($scope.unassignedlist);
           }, function myError(response) {
             $scope.errormess = response.statusText;
           });
         }
        $scope.showSelectForBM = true;
        $scope.showSelectForBO = false;
        $scope.showSelectForHOR = false;
        $scope.selectedDropDown = "BM";
        $scope.getBranchList();
      }
      $scope.selectedRole = $scope.rolelist.ROLE_NAME;
    }
  }
  //End of showForHOR()

  //    Variable to handle checkbox group selection
  $scope.branches = {
    selected: []
  }

  $('#loader').css({
    'display': 'none'
  });
  $scope.email_pattern = /^[a-z]+[a-z0-9._]+@[a-z]+\.[a-z.]{2,5}$/;
  var status_data = {
    "STATUS_LIST": [{
      "STATUS_LABEL": "Active",
      "STATUS_CODE": "1"
    }, {
        "STATUS_LABEL": "Inactive",
        "STATUS_CODE": "0"
      }]
  };

  var fd_data = {
    "FLOWDESIGN_LIST": [{
      "LABEL": "No",
      "VALUE": "0"
    }, {
        "LABEL": "Yes",
        "VALUE": "1"
      }]
  };

  var gender_data = {
    "GENDER_LIST": [{
      "GENDER_LABEL": "Male",
      "GENDER_NAME": "Male"
    }, {
        "GENDER_LABEL": "Female",
        "GENDER_NAME": "Female"
      }]
  };

  //  First request made to get list of banks
  $http({
    method: 'GET',
    url: apiURL + '/bankList/',
    params: {
      "type": 'json'
    },
  }).then(function mySucces(response) {
    $scope.banklistoptions = response.data.BANKLIST;
    console.log(response.data);
    angular.forEach($scope.banklistoptions, function (task, index) {
      if (task.BANK_CODE == localStorage.bankcode) {
        $scope.banklistObj = $scope.banklistoptions[index];
        $scope.banklist = $scope.banklistObj.BANK_NAME;
        console.log($scope.banklist);
        console.log($scope.banklistObj);
        $scope.getBranchList();
      }
    });
  }, function myError(response) {
    $scope.errormess = response.statusText;
  });

  //  End of Banklist request

  //Request to get country list
  $http({
    method: 'GET',
    url: apiURL + '/countryList/',
    params: {
      "type": 'json'
    },
  }).then(function mySucces(response) {
    $scope.countrylistoptions = response.data.COUNTRYLIST;
  }, function myError(response) {
    $scope.errormess = response.statusText;
  });

  //  End of Country list request

  $scope.statuslistoptions = status_data.STATUS_LIST;
  $scope.flowdesignlistoptions = fd_data.FLOWDESIGN_LIST;
  $scope.genderlistoptions = gender_data.GENDER_LIST;

  if (id == '') {
	    $(".add-role").show()
    $scope.title = 'Add Employee';
    $scope.branchtemp = '';
    $scope.roletemp = '';
    $scope.alertMessage = 'Employee Data Added Successfullly';
    $http({
      method: 'GET',
      url: apiURL + '/bank/',
      params: {
        "id": localStorage.bankcode
      },
    }).then(function mySucces(response) {
      angular.forEach($scope.banklistoptions, function (task, index) {
        if (task.BANK_NAME == response.data.BANK_DATA.BANK_NAME) {
          $scope.banklistObj = $scope.banklistoptions[index];
          $scope.banklist = $scope.banklistObj.BANK_NAME;
        }
      });
    }, function myError(response) {
      $scope.errormess = response.statusText;
    });
  } else {
    $scope.title = 'Edit Employee';
    $scope.alertMessage = 'Employee Data Updated Successfully';
    $http({
      method: 'GET',
      url: apiURL + '/employee/',
      params: {
        "id": id
      },
    }).then(function mySucces(response) {
      angular.forEach($scope.banklistoptions, function (task, index) {
        if (task.BANK_CODE == response.data.EMPLOYEE_DATA.BANK_CODE) {
          $scope.banklistObj = $scope.banklistoptions[index];
          $scope.banklist = $scope.banklistObj.BANK_NAME;

        }
      });
      angular.forEach($scope.countrylistoptions, function (task, index) {
        if (task.COUNTRY_CODE == response.data.EMPLOYEE_DATA.COUNTY) {
          $scope.country = $scope.countrylistoptions[index];
        }
      });
      angular.forEach($scope.statuslistoptions, function (task, index) {
        if (task.STATUS_CODE == response.data.EMPLOYEE_DATA.STATUS) {
          $scope.status = $scope.statuslistoptions[index];
        }
      });
      angular.forEach($scope.flowdesignlistoptions, function (task, index) {
        if (task.VALUE == response.data.EMPLOYEE_DATA.ISFLOWDESIGNER) {
          $scope.flowdesign = $scope.flowdesignlistoptions[index];
        }
      });
      angular.forEach($scope.genderlistoptions, function (task, index) {
        if (task.GENDER_NAME == response.data.EMPLOYEE_DATA.GENDER) {
          $scope.gender = $scope.genderlistoptions[index];
        }
      });
	   $(".edit-role").show()
	 
	 if (response.data.EMPLOYEE_DATA.ROLE_ID == 3) {
          $scope.roleliste = "Bank Manager";
          $scope.showSelectForBM = true;
          $scope.showSelectForBO = false;
          $scope.showSelectForHOR = false;
          $scope.unassignedlistBM = false;		 
     } else if (response.data.EMPLOYEE_DATA.ROLE_ID == 4) {
          $scope.showSelectForBO = true;
          $scope.showSelectForBM = false;
          $scope.showSelectForHOR = false;
		   $scope.roleliste = "Bank Officer";		  
     } else if (response.data.EMPLOYEE_DATA.ROLE_ID == 2) {
          $scope.showSelectForHOR = true;
          $scope.showSelectForBO = false;
          $scope.showSelectForBM = false;
          $scope.unassignedlistHOR = false;
		   $scope.roleliste = "Head Of Retail";		  	
      }
      $scope.statetemp = response.data.EMPLOYEE_DATA.STATE;
      $scope.branchtemp = response.data.EMPLOYEE_DATA.BRANCH_CODE;
      $scope.roletemp = response.data.EMPLOYEE_DATA.ROLE_ID;
      $scope.email = response.data.EMPLOYEE_DATA.EMAIL;
      $scope.password = response.data.EMPLOYEE_DATA.PASSWORD;
      $scope.first_name = response.data.EMPLOYEE_DATA.FIRST_NAME;
      $scope.last_name = response.data.EMPLOYEE_DATA.LAST_NAME;
      $scope.dob = response.data.EMPLOYEE_DATA.DATE_OF_BIRTH;
      $scope.phno = parseInt(response.data.EMPLOYEE_DATA.PHONE_NO.substr(3));
      $scope.phone_prefix = parseInt(response.data.EMPLOYEE_DATA.PHONE_NO.substr(0, 3));
      $scope.mono = parseInt(response.data.EMPLOYEE_DATA.MOBILE_NO);
      $scope.gendertemp = response.data.EMPLOYEE_DATA.GENDER;
      $scope.zip = parseInt(response.data.EMPLOYEE_DATA.PIN_CODE);
      $scope.city = response.data.EMPLOYEE_DATA.CITY;
      $scope.address = response.data.EMPLOYEE_DATA.ADDRESS;
      $scope.getBranchList();
      $scope.getStates();
    }, function myError(response) {
      $scope.errormess = response.statusText;
    });
  }


  $scope.getBranchList = function () {
    $http({
      method: 'GET',
      url: apiURL + '/bankBranchList/',
      params: {
        "id": localStorage.bankcode,
        "currentUserId": id,
        "currentRoleId": localStorage.roleId
      },
    }).then(function mySucces(response) {
		//alert(response.data);
		//alert(response.data.BRANCH_SEL_LIST);
	
      $scope.branchlistoptions = response.data.BRANCH_LIST;
      $scope.branchlistselectedhor = response.data.BRANCH_SEL_LIST;
      $scope.branchlist = $scope.branchlistoptions;
      console.log($scope.branchlist);	  
      angular.forEach($scope.branchlistoptions, function (task, index) {
        if (task.BRANCH_CODE == $scope.branchtemp) {
          console.log($scope.branchtemp);
          $scope.branches.selected = task.BRANCH_CODE;
          $scope.branchlist = $scope.branchlistoptions[index];
          $scope.selectedBrachId = $scope.branchlist.BRANCH_CODE;
        }
      });
    },
      function myError(response) {
        $scope.errormess = response.statusText;
      });
	  
  }

  $http({
    method: 'GET',
    url: apiURL + '/branchRoleList/',
    params: {
      "id": localStorage.bankcode,
      "currentRoleId": localStorage.roleId,
      "selectedDropDown": $scope.selectedDropDown
    },
  }).then(function mySucces(response) {

    $scope.rolelistoptions = response.data.ROLE_LIST;
    angular.forEach($scope.rolelistoptions, function (task, index) {
      if (task.ROLE_ID == $scope.roletemp) {
        $scope.rolelist = $scope.rolelistoptions[index];
        if ($scope.rolelist.ROLE_NAME == "Bank Manager") {
          $scope.showSelectForBM = true;
          $scope.showSelectForBO = false;
          $scope.showSelectForHOR = false;
          $scope.unassignedlistBM = false;
        } else if ($scope.rolelist.ROLE_NAME == "Bank Officer") {
          $scope.showSelectForBO = true;
          $scope.showSelectForBM = false;
          $scope.showSelectForHOR = false;
        } else if ($scope.rolelist.ROLE_NAME == "Head Of Retail") {
          $scope.showSelectForHOR = true;
          $scope.showSelectForBO = false;
          $scope.showSelectForBM = false;
          $scope.unassignedlistHOR = false;
        }
      }
    });

  }, function myError(response) {
    $scope.errormess = response.statusText;
  },
    function myError(response) {
      $scope.errormess = response.statusText;
    });

  $scope.getStates = function () {

    $http({
      method: 'GET',
      url: apiURL + '/stateList/',
      params: {
        "country_code": $scope.country.COUNTRY_CODE
      },
    }).then(function mySucces(response) {
      $scope.statelistoptions = response.data.STATELIST;


      angular.forEach($scope.statelistoptions, function (task, index) {
        if (task.STATE_NAME == $scope.statetemp) {
          $scope.state = $scope.statelistoptions[index];
        }
      });

    }, function myError(response) {
      $scope.errormess = response.statusText;
    });

  }
  $scope.validateForm = function () {

    var bankname = $scope.banklist;
    if (bankname == '' || bankname == undefined || bankname == 'null' || bankname == false) {
      $scope.errormess = 'Please Select Bank';
      return false;
    } else {
      if (id == '') {
        method = 'PUT';
      } else {
        method = 'POST';
      }
	 
      if (!$scope.hideForBankAdmin) {
        $scope.customBranchId = $scope.branches.selected;
        $scope.customPhno = "";
        $scope.customDob = "00/00/0000";
        $scope.customMono = "";
        $scope.customGender = "";
        $scope.customCountry = "";
        $scope.customState = "";
        $scope.customCity = "";
        $scope.customAddress = " ";
        $scope.customZip = 0;
        $scope.customStatus = 1;
        $scope.customFlow = 0;
      } else {
        $scope.customBranchId = $scope.branchlist.BRANCH_CODE;
        $scope.customPhno = $scope.phone_prefix + "" + $scope.phno;
        $scope.customDob = $scope.dob;
        $scope.customMono = $scope.mono;
        $scope.customGender = $scope.gender.GENDER_NAME;
        $scope.customCountry = $scope.country.COUNTRY_CODE;
        $scope.customState = $scope.state.STATE_NAME;
        $scope.customCity = $scope.city;
        $scope.customAddress = $scope.address;
        $scope.customZip = $scope.zip;
        $scope.customStatus = $scope.status.STATUS_CODE;
        $scope.customFlow = $scope.flowdesign.VALUE;
      }
	
      if ($scope.rolelist.ROLE_NAME == "Head Of Retail") {
        $scope.customFlow = 1;
        $scope.customBranchId = 0;
      }
	  if ($scope.rolelist.ROLE_NAME == "Bank Manager") {
        $scope.customFlow = 1;       
      }
	  
      $http({
        method: method,
        url: apiURL + '/employee/',
        params: {
          "bankname": localStorage.bankcode,
          "id": id,
          "branchname": $scope.customBranchId,
          "role": $scope.rolelist.ROLE_ID,
          "email": $scope.email,
          "password": $scope.password,
          "firstname": $scope.first_name,
          "lastname": $scope.last_name,
          "dob": $scope.customDob,
          "phno": $scope.customPhno,
          "mono": $scope.customMono,
          "gender": $scope.customGender,
          "country": $scope.customCountry,
          "state": $scope.customState,
          "city": $scope.customCity,
          "address": $scope.customAddress,
          "zip": $scope.customZip,
          "status": $scope.customStatus,
          "isfd": $scope.customFlow,
          "selectedRole": $scope.rolelist.ROLE_NAME
        },
      }).then(function mySucces(response) {
        if (response.data.message == 'success' || response.data.message == 'Employee Data Updated') {
          $scope.last_insert_id = response.data.last_insert_id;
          if ($scope.showSelectForHOR) {
            if ($scope.last_insert_id == undefined || $scope.last_insert_id == 'undefined')
              $scope.last_insert_id = id;
            angular.forEach($scope.branches.selected, function (value) {
              $http({
                method: 'PUT',
                url: apiURL + '/setHeadOfRetail/',
                params: {
                  "id": localStorage.bankcode,
                  "horId": $scope.last_insert_id,
                  "selectedBranch": value
                },
              }).then(function mySucces(response) {
				  
                console.log(response.data.message);
              }, function myError(response) {
                $scope.errormess = response.statusText;
              });
            });
          }
          if ($scope.showSelectForBM) {
            if ($scope.last_insert_id == undefined || $scope.last_insert_id == 'undefined')
              $scope.last_insert_id = id;
            $http({
              method: 'PUT',
              url: apiURL + '/setBranchManager/',
              params: {
                "id": localStorage.bankcode,
                "bmId": $scope.last_insert_id,
                "selectedBranch": $scope.branches.selected
              },
            }).then(function mySucces(response) {
              console.log(response.data.message);
            }, function myError(response) {
              $scope.errormess = response.statusText;
            });
          }
        }
        $('#alertModelBoxBtn').trigger('click');
        $timeout(function () {
          $scope.manageEmployee();
        }, 3000);
      }, function myError(response) {
        $scope.errormess = response.statusText;
      });
    }
  }
  $scope.manageEmployee = function () {
    window.location.href = manage_employee;
  }
  
});
