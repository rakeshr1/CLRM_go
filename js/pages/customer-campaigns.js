var app = angular.module('touchpoint', [
  'ngRoute',
  'ngBootstrap'
  ]);
app.filter('customDateFilter', function ($filter) {
  return function (input) {
	  /* console.log(input.substr(0,2)>=12); */
    var meridiem, hour, modifiedInput;
	
    if (input.substr(0, 2) >= 12) {
      hour = input.substr(0, 2);
      hour = parseInt(hour) - 12;
      hour = ("0" + hour).slice(-2);

      modifiedInput = input.replace(input.substr(0, 2), hour);
      meridiem = "PM";
    } else{
		modifiedInput = input;
      meridiem = "AM";
	}
    return modifiedInput + ' ' + meridiem;
  }
})
app.controller('tpCtrl', function ($scope, $http, $timeout) {

  $('#loader').css({
    'display': 'none'
  });
  $scope.date = {
    startDate: moment(),
    endDate: moment()
  };
  $scope.thisMonth = moment().month() + 1;
  $scope.thisQuarter = '';
  $scope.startMonth = '';
  $scope.endMonth = '';

  if ($scope.thisMonth <= 3) {
    $scope.startMonth = 0;
    $scope.endMonth = 9;
  } else if ($scope.thisMonth <= 6) {
    $scope.thisQuarter = 2;
    $scope.startMonth = 3;
    $scope.endMonth = 6;
  } else if ($scope.thisMonth <= 9) {
    $scope.thisQuarter = 3;
    $scope.startMonth = 6;
    $scope.endMonth = 3;
  } else {
    $scope.thisQuarter = 4;
    $scope.startMonth = 9;
    $scope.endMonth = 0;
  }
  $scope.ranges = {
    'Today': [moment(), moment()],
    'Last 7 days': [moment().subtract('days', 7), moment()],
    'Last 30 days': [moment().subtract('days', 30), moment()],
    'This month': [moment().startOf('month'), moment().endOf('month')],
    'This Quarter': [moment().startOf('year').add('month', $scope.startMonth), moment()]
  };
  $scope.thisDate = new Date();
  $scope.customerName = localStorage.firstName;
  $scope.customerlName = localStorage.lastName;
  $scope.campaignFromDate = '';
  $scope.campaignToDate = '';

  $scope.addLead = function (x) {
    $http({
      method: 'PUT',
      url: apiURL + '/lead/',
      params: {
        bankId: localStorage.bankCode,
        branchId: localStorage.branchCode,
        employeeId: localStorage.userId,
        customerId: $scope.custlist.CUSTOMER_EMAIL,
        productId: $scope.productlist.PRODUCT_ID,
        campaignId: $scope.campaignlist.CAMPAIGN_ID,
        touchpointId: $scope.tplist.TOUCHPOINT_ID,
        commId: $scope.tpclist.COMMUNICATION_ID,
        leadText: $scope.leadText
      },
    }).then(function mySucces(response) {
      if (response.data.message == 'success') {
        $scope.getTouchPoints();
      } else {

      }
    }, function myError(response) {
      $scope.errormess = response.statusText;
    });
  }
  $scope.showHideTouchpoint = function (x) {
    console.log(x);
    $('.tpLists_' + x).toggle();
    $('.tpPlusMinus_' + x).toggleClass('fa-minus');
    $('.tpPlusMinus_' + x).toggleClass('fa-plus');
  }
  $scope.getTpCommun = function (x) {
    angular.forEach($scope.touchpointList, function (task, index) {
      if (task.TOUCHPOINT_ID == $scope.tplist.TOUCHPOINT_ID) {
        $scope.commList = $scope.touchpointList[index].COMMUNICATIN_DATA;
      }
    });
  }
  $http({
    method: 'GET',
    url: apiURL + '/campaignListDrop/',
    params: {
      "userId": localStorage.userId,
      "bankId": localStorage.bankcode,
      "branchId": localStorage.branchId,
      "for": "bankOfficerDashboard"
    },
  }).then(function mySucces(response) {
	  //alert(response.data);
	
    $scope.campaignlistoptions = response.data.CAMPAIGN_DATA;
    $scope.campaignlist = $scope.campaignlistoptions[0];
    $scope.getTouchPoints();
  }, function myError(response) {
    $scope.errormess = response.statusText;
  });

  $scope.getTouchPoints = function () {
    if ($scope.date["startDate"]["_d"] != null && $scope.date["endDate"]["_d"] != null) {
      var fromMonthAlone = $scope.date["startDate"]['_d'].getMonth() + 1;
      fromMonthAlone = "0" + fromMonthAlone.toString().slice(-2);
      fromDate = fromMonthAlone + "/" + ('0' + $scope.date["startDate"]['_d'].getDate()).slice(-2) + "/" + $scope.date["startDate"]['_d'].getFullYear();
      var endMonthAlone = $scope.date["endDate"]['_d'].getMonth() + 1;
      endMonthAlone = "0" + endMonthAlone.toString().slice(-2);
      toDate = endMonthAlone + "/" + ('0' + $scope.date["endDate"]['_d'].getDate()).slice(-2) + "/" + $scope.date["endDate"]['_d'].getFullYear();
      
      if(fromDate == toDate){
        console.log($('#ng-bs-date').val().substr(0,10));
        $('#ng-bs-date').html($('#ng-bs-date').val().substr(0,10));
      }

    } else {
      var fromMonthAlone = $scope.date["startDate"].getMonth() + 1;
      fromMonthAlone = "0" + fromMonthAlone.toString();
      fromDate = fromMonthAlone + "/" + ('0' + $scope.date["startDate"].getDate()).slice(-2) + "/" + $scope.date["startDate"].getFullYear();
      var endMonthAlone = $scope.date["endDate"].getMonth() + 1;
      endMonthAlone = "0" + endMonthAlone.toString();
      toDate = endMonthAlone + "/" + ('0' + $scope.date["endDate"].getDate()).slice(-2) + "/" + $scope.date["endDate"].getFullYear();
      
      if(fromDate == toDate){
        console.log($('#ng-bs-date').val().substr(0,10));
        $('#ng-bs-date').val($('#ng-bs-date').val().substr(0,10));
      }
    }
	
	/*
    if(fromDate == toDate){
      console.log($('#ng-bs-date').val().substr(0,10));
      $('#ng-bs-date').val($('#ng-bs-date').val().substr(0,10));
    }
	*/
    $http({
      method: 'GET',
      url: apiURL + '/touchpointListWithComm/',
      params: {
        "userId": localStorage.userId,
        "campaignId": $scope.campaignlist.CAMPAIGN_ID,
        "bankId": localStorage.bankcode,
        "branchId": localStorage.branchId,
        fromDate: fromDate,
        toDate: toDate
      },
    }).then(function mySucces(response) {
		//alert(response.data);
      if (response.data != '') {
        $scope.touchpointList = response.data.TOUCHPOINT_DATA;
        var counts = response.data.TOUCHPOINT_COUNT;
        if (counts >= 2) {
          console.log("Log 1" + response.data.TOUCHPOINT_DATA.length);
        } else {
          console.log("log 2" + response.data.TOUCHPOINT_DATA.length);
        }
      }
      $timeout(function () {
        angular.forEach($scope.touchpointList, function (task, index) {
          if ($('.tpLists_' + task.TOUCHPOINT_ID).text().trim().length == 0) {
            $('.tpLists_' + task.TOUCHPOINT_ID).html('Not yet communicated');
          } else {
            angular.forEach(task.COMMUNICATIN_DATA, function (task2, index2) {
              if ($('.comSend_' + task2.COMMUNICATION_ID).text().trim().length == 0) {
                $('.comSend_' + task2.COMMUNICATION_ID).html('<div> Not yet communicated </div>');
              }
              if ($('.comLoans_' + task2.COMMUNICATION_ID).text().trim().length == 0) {
                $('.comLoans_' + task2.COMMUNICATION_ID).html('<div>No product(s) selected </div>');
              }
              if ($('.comLeads_' + task2.COMMUNICATION_ID).text().trim().length == 0) {
                $('.comLeads_' + task2.COMMUNICATION_ID).html('<div> No <span style="text-transform:lowercase">response yet</span></div>');
              }
            });
          }
        });
      }, 100);
    }, function myError(response) {
      $scope.errormess = response.statusText;
    });
  }
  $http({
    method: 'GET',
    url: apiURL + '/productList/',
    params: {
      "type": "json"
    },
  }).then(function mySucces(response) {
    $scope.productlistoptions = response.data.PRODUCT_LIST;
  }, function myError(response) {
    $scope.errormess = response.statusText;
  });
  
  //MApping 
  
  /* $http({
    method: 'GET',
    url: apiURL + '/customermapping/',
    params: {
      "userId": localStorage.userId,
      "bankId": localStorage.bankCode,
      "branchId": localStorage.branchCode
     
    },
  }).then(function mySucces(response) {
    //$scope.productlistoptions = response.data.PRODUCT_LIST;
  }, function myError(response) {
    $scope.errormess = response.statusText;
  }); */


});