function req(name) {
	name = name.replace(/[\[]/, "\\[").replace(/[\]]/, "\\]");
    var regex = new RegExp("[\\?&]" + name + "=([^&#]*)"),
        results = regex.exec(location.search);
    return results === null ? "" : decodeURIComponent(results[1].replace(/\+/g, " "));
}
var r =req('r');
var u =req('u');
var c =req('c');
var app = angular.module('touchpoint', []);
app.controller('tpCtrl', function($scope, $http, $timeout) {	
		$scope.thankingMsg = 'Thank You';
		$http({
			method : 'POST',
			url : 'update-response.php',
			params: {c:c,r:r,u:u},
		}).then(function mySucces(response) {
			$('#loader').css({'display':'none'});
			if(response.data.url != '' && response.data.url != undefined) {
				window.location.href = response.data.url;
			}
		}, function myError(response) {
			//$scope.errormess = response.statusText;
		});
});
