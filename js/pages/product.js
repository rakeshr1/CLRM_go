var method,id;
id = getParamValeuByName('id');
var app = angular.module('touchpoint', []);
app.controller('tpCtrl', function($scope, $http, $timeout) {	
$('#loader').css({'display':'none'});
	if(id == '') {
		$scope.title = 'Add Product';
		$scope.alertMessage = 'Product Data Added Successfullly';
	} else {
		$scope.title = 'Edit Product';
		$scope.alertMessage = 'Product Data Updated Successfully';
		$http({
				method : 'GET',
				url : apiURL+'/product/',
				params: {"id": id},
			}).then(function mySucces(response) {
				$scope.productname = response.data.PRODUCT_DATA.PRODUCT_NAME;
			}, function myError(response) {
				$scope.errormess = response.statusText;
			});
	}
	$scope.validateForm = function() {
		var productname = $scope.productname;
		if(productname == '' || productname == undefined || productname == 'null' || productname == false) {
			$scope.errormess = 'Please Enter Product Name';
			return false;
		} else {
			if(id == '') {
				method = 'PUT';
			} else {
				method = 'POST';
			}
			$http({
				method : method,
				url : apiURL+'/product/',
				params: {"productname": $scope.productname, "id": id,bankcode:localStorage.bankcode},
			}).then(function mySucces(response) {
				if(response.data.message == 'success' || response.data.message == 'Product Data Updated'){
					$('#alertModelBoxBtn').trigger('click');
					$timeout(function() {
						$scope.manageProduct();
					}, 3000);
				}
			}, function myError(response) {
				$scope.errormess = response.statusText;
			});
		}
	}
	$scope.manageProduct = function() {
		window.location.href = manage_product;
	}
});