
var method,id;
id = getParamValeuByName('id');
if(id == '') { window.location.href='customer-campaigns.html'; }

var app = angular.module('touchpoint', []);

app.directive('numbersOnly', function () {
    return {
        require: 'ngModel',
        link: function (scope, element, attr, ngModelCtrl) {
            function fromUser(amount) {
                if (amount) {
                    var transformedInput = amount.replace(/[^0-9]/g, '');

                    if (transformedInput !== amount) {
                        ngModelCtrl.$setViewValue(transformedInput);
                        ngModelCtrl.$render();
                    }
                    return transformedInput;
                }
                return undefined;
            }            
            ngModelCtrl.$parsers.push(fromUser);
        }
    };
  }); 
  
 
  
app.controller('tpCtrl', function($scope, $http, $timeout) {	
$('#loader').css({'display':'none'});	
	$scope.responseList=[];
	$http({
		method : 'GET',
		url : apiURL+'/getCustomerByEmail/',
		params: {"id":id},
	}).then(function mySucces(response) {
		$scope.customerFName = response.data.CUSTOMER_DATA.CUSTOMER_FNAME;
		$scope.customerLName = response.data.CUSTOMER_DATA.CUSTOMER_LNAME;
		$scope.customerId = response.data.CUSTOMER_DATA.CUSTOMER_ID;
		$scope.customerDOJ = response.data.CUSTOMER_DATA.CUSTOMER_JOINING_DATE;
	}, function myError(response) {
		//$scope.errormess = response.statusText;
	});
	
	
	/*Slider controller start*/
	//$('#loader').css({'display':'block'});
	$scope.touchpoints = function() {
		$http({
			method : 'GET',
			url : apiURL+'/customerTouchpoints/',
			params: {"cid":id,eid:localStorage.userId,"bankId":localStorage.bankcode,"branchId":localStorage.branchId,},
		}).then(function mySucces(response) {
			$scope.touchpoint = response.data.TOUCHPOINTS;
			//$scope.count=count(response.data.TOUCHPOINTS);
			var counts=response.data.TOUCHPOINT_COUNT;
			//alert(counts);
			if(counts < 6){
				//alert('less than six');
				$scope.loadTouchpoint(0);
				  $timeout(function() {
					$('.bxslider').bxSlider({
					  minSlides: 2,
					  maxSlides: 6,
					  slideWidth:130,
					  slideMargin:0,
					  infiniteLoop : false,
					  controls: false
					  
					});
				  }, 100);
			} else {
				//alert('greter than six');
				$scope.loadTouchpoint(0);
				  $timeout(function() {
					$('.bxslider').bxSlider({
					  minSlides: 3,
					  maxSlides: 6,
					  slideWidth:130,
					  slideMargin: 0,
					  infiniteLoop : false,
					  controls: true
					});
				  }, 100);
			}
				
		}, function myError(response) {
			//$scope.errormess = response.statusText;
		});
	}
	/*Slider controller end*/
	
	
	
	$scope.loadTouchpoint = function(x) {
		//alert('load');
		$scope.responseList=[];
		/*if($(".ul_timeline li").eq(x).children().eq(0).hasClass('timeline_active')) { return; }
		for(i=0;i<$scope.touchpoint.length;i++) {
			$(".ul_timeline li").eq(i).children().eq(0).removeClass('timeline_active');
		}*/
		$http({
			method : 'GET',
			url : apiURL+'/customerTouchpointCommunications/',
			params: {"tid":$scope.touchpoint[x].TOUCHPOINT_ID,"cid":id,eid:localStorage.userId},
		}).then(function mySucces(response) {
			//alert(response.data);
			//alert($scope.touchpoint[x].TOUCHPOINT_ID);					
			$scope.responseList = response.data;
			$(".ul_timeline li").eq(x).children().eq(0).addClass('timeline_active');
		}, function myError(response) {
			//alert($scope.touchpoint[x].TOUCHPOINT_ID);
			//$scope.errormess = response.statusText;
		});
	}	
	
	$scope.loadTouchpoint1 = function(x) {
		//alert(x);
		$scope.responseList=[];
		if($(".ul_timeline li").eq(x).children().eq(0).hasClass('timeline_active')) { 
		//return; 		
		} 
		
		for(i=0;i<$scope.touchpoint.length;i++) {
			$(".ul_timeline li").eq(i).children().eq(0).removeClass('timeline_active');
		}

		//exit;
		$http({
			method : 'GET',
			url : apiURL+'/customerTouchpointCommunications/',
			params: {"tid":$scope.touchpoint[x].TOUCHPOINT_ID,"cid":id,eid:localStorage.userId},
		}).then(function mySucces(response) {
			//alert($scope.touchpoint[x].TOUCHPOINT_ID);		
			$scope.responseList = response.data;		
			$(".ul_timeline li").eq(x).children().eq(0).addClass('timeline_active');
		}, function myError(response) {
			//alert($scope.touchpoint[x].TOUCHPOINT_ID);
			//$scope.errormess = response.statusText;
		});
	}
	
	$scope.touchpoints();
	$scope.prList = function() {
	$http({
					method : 'GET',
					url : apiURL+'/productList/',
					params: {"type": "json","bankcode":localStorage.bankcode,"bankId":localStorage.bankcode,"branchId":localStorage.branchId},
				}).then(function mySucces(response) {
					$scope.productlistoptions = response.data.PRODUCT_LIST;
				}, function myError(response) {
					$scope.errormess = response.statusText;
				});
	}
$scope.prList();	
	$scope.addLead = function(a,b,c,d,e,f,g) {
		$http({
			method : 'PUT',
			url : apiURL+'/lead/',
			params: {bankId:localStorage.bankcode, branchId:localStorage.branchId, employeeId:localStorage.userId,customerId:id,productId:d,campaignId:'',touchpointId:'',commId:a,leadText:f},
		}).then(function mySucces(response) {
			if(response.data.message == 'success'){
					angular.forEach($scope.responseList, function (task, index) {
								
								if (task.COMMUNICATION_ID == a) {
									task.leadText = '';
									if(task.LEADS_DATA == '') {
									task.LEADS_DATA = [];
									}
									task.LEADS_DATA.push({ 
										LEAD_TEXT: f,
										PRODUCT_NAME: g,
										CUSTOMER_ID: id
									 });
								}
					});
			} else {
			
			}
		}, function myError(response) {
			$scope.errormess = response.statusText;
		});
	
	}
	 
	$scope.addNewProduct = function() {
		$http({
				method : "PUT",
				url : apiURL+'/product/',
				params: {"productname": $scope.productname, "id": ""},
			}).then(function mySucces(response) {
				if(response.data.message == 'success'){
						$scope.prList();
						$scope.productname = '';
						$('#closeProdutAdd').trigger('click');
				}
			}, function myError(response) {
				//
			});
	}
});