<?php
if(!isset($_REQUEST['bankCode'])) {
	die;
}
$bankCode = $_REQUEST['bankCode'];
$sql = "select * from ENDPOINT where BANK_CODE = '$bankCode' ORDER BY ID DESC LIMIT 0,1"; 
$db = getConnection();
$endpoint = '';
$custIdPath = '';
$custEmailPath = '';
$bankIdPath = '';
$branchIdPath = '';
$fnamePath = '';
$lnamePath = '';
$dojPath = '';
$employeeIdPath = '';
$mobNoPath = '';
$productIdPath = '';
$serviceType = '';
$wsdlURL = '';
$soapMethod = '';
$soapParams = '';
$soapParamValues = '';
$mapArray = '';
if ($result = $db->query($sql)) {
	$nonArrayParams = array('SERVICE_TYPE', 'MAP_ARRAY', 'ID', 'BANK_ID', 'BANK_CODE');
	$data = $result->fetch_assoc();
	//echo '<pre/>'; print_r($data);
	//die;
	
	
	$customData = array();
	foreach($data as $pKey=>$pValue){
		$customData[$pKey] = $pValue;
		if (!in_array($pKey,$nonArrayParams)) {
			$customData[$pKey] = str_replace('[','', $customData[$pKey]);
			$customData[$pKey] = str_replace(']','', $customData[$pKey]);
			$customData[$pKey] = str_replace('"','', $customData[$pKey]);
			$customData[$pKey] = explode(',', $customData[$pKey]);
		}
		if($pKey == 'MAP_ARRAY'){
			$customData[$pKey] = explode('***',$customData[$pKey]);
			$mapArrayCount = count($customData[$pKey]);
			for($i=0; $i<$mapArrayCount; $i++) {
				$customData[$pKey][$i] = explode(',', $customData[$pKey][$i]);
			}
		}
	}

}
//echo '<pre/>';print_r($customData);
//die;
$configInArrayCount = count($customData['END_POINT']);
for($i=0; $i<$configInArrayCount; $i++) {
	$bankAPIData = '';
	if($customData['SERVICE_TYPE'] == 'REST') {
		$bankAPIData = curlGetService($customData['END_POINT'][$i]);
	} else {
		$bankAPIData = soapUrlService($customData['END_POINT'][$i], $customData['WSDL_URL'][$i], $customData['SOAP_METHOD'][$i], $customData['SOAP_PARAMS'][$i], $customData['SOAP_PARAM_VALUES'][$i]);
	}
	$customData['END_POINT_DATA'][$i] = json_decode($bankAPIData, true);
}
//echo '<pre/>';print_r($customData['END_POINT_DATA']);
//die;

$submitInArray = array('CUSTOMER_ID_PATH', 
						'CUSTOMER_EMAIL_PATH', 
						'CUSTOMER_MOB_NO_PATH', 
						'CUSTOMER_FNAME_PATH', 
						'CUSTOMER_LNAME_PATH', 
						'CUSTOMER_DOJ', 
						'BANK_ID_PATH', 
						'BRANCH_ID_PATH', 
						'CUSTOMER_PRODUCT_ID_PATH', 
						'EMPLOYEE_ID_PATH'
					);
$submiInArrayName = array('CUSTOMER_ID', 
						'CUSTOMER_EMAIL', 
						'CUSTOMER_MOB_NO', 
						'CUSTOMER_FNAME', 
						'CUSTOMER_LNAME', 
						'CUSTOMER_DOJ', 
						'BANK_ID', 
						'BRANCH_ID', 
						'PRODUCT_ID', 
						'EMPLOYEE_ID'
					);
$submitInArrayMap = array();
foreach($submitInArray as $submitInData) {
	$endPointMap = '';
	foreach($customData['MAP_ARRAY'] as $mArrayKey=>$mArrayValue) {
		if(in_array($submitInData, $mArrayValue)) {
			$endPointMap = $mArrayKey;
		}
	}
	$submitInArrayMap[] = $endPointMap;
}
//echo '<pre/>';print_r($submitInArrayMap);
//die;

$endPointDataCount = count($customData['END_POINT_DATA'][0]);
$finalData = array();
$submitInArrayCount = count($submitInArray);
for($i=0; $i<$endPointDataCount; $i++) {
	for($j=0; $j<$submitInArrayCount; $j++) {
		$dataName = $submitInArray[$j];
		$dataNameAlias = $submiInArrayName[$j];
		$dataNameEndPointDataMap = $submitInArrayMap[$j];
		$finalData[$i][$dataNameAlias] = $customData['END_POINT_DATA'][$dataNameEndPointDataMap][$i];
		$dataMapArrayCount = count($customData[$dataName]);
		for($k=0; $k<$dataMapArrayCount; $k++) {
			$finalData[$i][$dataNameAlias] = $finalData[$i][$dataNameAlias][$customData[$dataName][$k]];
		}
	}
}
//echo '<pre/>';print_r($finalData);
//die;

$finalData = json_encode($finalData);
echo $finalData;


function soapUrlService($url, $wsdl, $soapMethod, $params, $paramValues) {
	$paramConst = '';
	$soapclient = new SoapClient($url);
	
	if($params != '') {
		$params = explode(',',$params);
		$paramValues = explode(',',$paramValues);
		$paramCount = count($params);
		$paramConst = array();
		for($i=0; $i<=$paramCount-1; $i++) {
			$paramConst[$params[$i]] = $paramValues[$i];
		}
	}
	
	$response = $soapclient->$soapMethod($paramConst);
	while(!is_string($response)) {
		$resp = '';
		foreach($response as $res) {
			$resp = $res;
		}
		$response = $resp;
	}
	$response = explode('?>',$response);
	$response = $response[1];
	$xml = simplexml_load_string($response);
	$json = json_encode($xml);
	return $json;
}

function curlGetService($url) {
    $ch = curl_init();   
    curl_setopt($ch,CURLOPT_URL,$url);
    curl_setopt($ch,CURLOPT_RETURNTRANSFER,true);
 
    $output=curl_exec($ch);
	if($output === false) {
        echo "Error Number:".curl_errno($ch)."<br>";
        echo "Error String:".curl_error($ch);
	}
    curl_close($ch);
    return $output;
}

function getConnection() {
	$dbhost="192.168.0.222";
	$dbuser="touchpoint";
	$dbpass="touchpoint";
	$dbname="touchpoint";
	$mysqli_conn = new mysqli($dbhost, $dbuser, $dbpass, $dbname);
	if ($mysqli_conn->connect_errno) {
	die("Failed to connect to MySQL: (" . $mysqli_conn->connect_errno . ") " . $mysqli_conn->connect_error);
	}
	return $mysqli_conn;
}