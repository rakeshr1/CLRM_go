<?php
//define("USERRESPONSEURL","http://192.168.0.17/touchpoint/tp/production/thanking.html");
define("USERRESPONSEURL","http://localhost/clrm/thanking.html");

function getConnection() {
	$dbhost="127.0.0.1:3306";
    $dbuser = "sedev";
    $dbpass = 'p@$$w0rdSEDONA';
	$dbname="asic_antelope_dev";
	$mysqli_conn = new mysqli($dbhost, $dbuser, $dbpass, $dbname);
	if ($mysqli_conn->connect_errno) {
		die("Failed to connect to MySQL: (" . $mysqli_conn->connect_errno . ") " . $mysqli_conn->connect_error);
	}
	return $mysqli_conn;
}