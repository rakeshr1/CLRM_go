<!DOCTYPE html>
<html lang="en">
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<!-- Meta, title, CSS, favicons, etc. -->
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1">
<meta name="viewport" content="width=device-width, initial-scale=1.0">
<title>Touch Point | Campaigns</title>
<script src="js/Angular/angular.min.js"></script>
<script src="js/jquery.min.js"></script>
<link href="css/bootstrap.min.css" rel="stylesheet">
<link href="fonts/css/font-awesome.min.css" rel="stylesheet">
<link href="css/animate.min.css" rel="stylesheet">
<link href="css/custom.css" rel="stylesheet">
<link href="css/custom-2.css" rel="stylesheet">
<link href="css/rwd.css" rel="stylesheet">
<link href="css/validation.css" rel="stylesheet">
<!--[if lt IE 9]>
        <script src="../assets/js/ie8-responsive-file-warning.js"></script>
        <![endif]-->
<!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
<!--[if lt IE 9]>
          <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
          <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
<![endif]-->
<script src="js/angular-sanitize.js"></script>
<link href="css/editor.css" rel="stylesheet">
<!-- Template editor fontstyles -->
<link href="css/editor/font/fontstyle.css" rel="stylesheet">
<script src="js/pages/editor.js"></script>
<link rel="stylesheet" type="text/css" href="css/angular-datepicker.css">
</head>
<body class="nav-md" ng-app="touchpoint" ng-controller="tpCtrl">
<div id="loader"></div>
<div class="container body">
  <div class="main_container">
    <div class="col-md-3 left_col">
      <div id="sideMenu"></div>
    </div>
    
    <!-- top navigation -->
    <div id="topMenu"></div>
    <!-- /top navigation --> 
    <!-- page content -->
    <div class="right_col" role="main">
    <header class="flow_head">
      <div class="x_title" style="border-bottom:2px solid #e8e8e8;">    
        <p class="camp_stp f_l">Customer On-Boarding Campaign Setup <span class="user_name" ng-bind-html="campaignName"></span></p>   
        <div class="clearfix"></div>     
       </div>  
              
      <div class="col-md-6 col-sm-4" style="display:none;">
        <div class="btn_test_live">
          <ul>
            <li><a href="#">
              <button class="btn btn-primary">play</button>
              </a></li>
          </ul>
        </div>
      </div>
    </header>
    <div class="clearfix"></div>
    <!-- header colse-->
    <div class="row pad_top">
      <div class="hide_section_1">
        <div class="col-md-4 col-sm-12 col-xs-12">
          <div class="custmr_list">
            <ul class="cutm_ul">
              <label style="text-transform:capitalize !important;"><span class="click_down" ng-click="slideCampaigns()" id="slideCampaigns"><i class="fa fa-caret-up" aria-hidden="true"></i></span>List of Campaigns<span class="list_of_icon"><i class="fa fa-check" title="Active" aria-hidden="true" ng-click="filterCampaign('PRESENT')"></i> <i class="fa fa-times" title="Inactive" aria-hidden="true"  ng-click="filterCampaign('EXPIRED')"></i><i class="fa fa-refresh" title="Refresh" aria-hidden="true" ng-click="filterCampaign('')"></i></span></label>
              <li class="add_name" style="cursor:pointer;" ng-click="addCampaignModelShow();"> <a href="#"><i class="fa fa-plus"></i></a> </li>
              <li ng-show="slide" ng-repeat="x in campaignList" class="sortFilter sort_{{x.SORT_STATUS}} sortId_{{x.CAMPAIGN_ID}}" style="text-align:left;"> <span ng-click="loadCampaign($index,x.SORT_STATUS)">{{ x.CAMPAIGN_NAME }}</span> <a href="#" class="pull-right" ng-click="deleteCampaign($index)"><i class="fa fa-trash-o" title="Delete"></i></a> <a href="#" class="pull-right" ng-click="editCampaign($index)"><i class="fa fa-pencil-square-o" title="Edit"></i></a> <a href="#" class="pull-right tooltip_wrapper" style="background: none;display: table; height: auto;text-transform:capitalize;padding: 0 10px 0 0;position: relative;text-align: center;text-overflow: ellipsis;white-space: nowrap;margin:0px 0px;width: auto"><i class="fa fa-calendar" aria-hidden="true"></i><span class="tooltip_text">{{ x.DATE_ADDED.substr(0,10) | date: "MMM d, y" }}</span></a> </li>
            </ul>
            <div class="add_campgn" ng-show="addCampaignModel">
              <div  class="add_a_campgn_wrap reset_pop ui-draggable" id="add_campaign">
                <div class="inner_div">
                  <div class="modal-content date-min">
                    <div class="modal-header">
                      <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true"></span></button>
                      <h4 class="modal-title" id="cmpgnTitle">{{ campaignTitle }} Campaign</h4>
                    </div>
                    <div class="modal-body">
                      <div class="form-group"><!--<label>Touchpoint Name:</label>-->
                        <form name="addCampaignForm">
                          <input type="text" required placeholder="Campaign Name" id="type_name" class="form-control add_caps my-colorpicker1 colorpicker-element" style="margin: 15px 0px 0px;" id="cmpgnName" ng-model="campaignAddName" name="campaignAddName">
                          <br/>
                        
                          <datepicker
							  date-format="MM/dd/yyyy"
							  date-min-limit="1992/09/07"
							  button-prev='<i class="fa fa-arrow-circle-left"></i>'
							  button-next='<i class="fa fa-arrow-circle-right"></i>'>
                            <input ng-keyup='campaignFromDate = ""' type="text" style="margin-bottom:0;"placeholder="Start Date - mm/dd/yyyy" class="input-bg add_caps angular-datepicker-input form-control from-date"  ng-model="campaignFromDate" required>                                                     
                          </datepicker>
                          <br/>
                          <datepicker
							  date-format="MM/dd/yyyy"
							  date-min-limit="{{campaignFromDate}}"
							  button-prev='<i class="fa fa-arrow-circle-left"></i>'
							  button-next='<i class="fa fa-arrow-circle-right"></i>'>
                            <input  style="margin-bottom:0;"ng-keyup='campaignToDate = ""' type="text" placeholder="End Date - mm/dd/yyyy" class="input-bg add_caps angular-datepicker-input form-control to-date"  ng-model="campaignToDate" required>                            
                          </datepicker>                          
                        </form>
                      </div>
                    </div>
                    <div class="modal-footer move-center">
                      <button style="margin-left:10px;" type="button" ng-click="addCampaign()" id="add_cmgn" class="btn btn-success btn-top" ng-disabled="addCampaignForm.campaignAddName.$invalid">Submit</button>
                      <button type="button"  ng-click="addCampaignModelHide();" id="cancel_btn" class="btn btn-primary" data-dismiss="modal"> Cancel </button>
                    </div>
                  </div>
                  <!-- /.modal-content -->
				  </div>
              </div>
            </div>
          </div>
        </div>
        <div class="col-md-8 col-sm-12 col-xs-12" id="setup_communcation_href">
          <div class="clearfix"></div>
          <div class="flow_adjust"  ng-show="addTouchpointPlus">
            <div class="set_up_com">
              <div class="plus_wrapper">
                <div class="play_pause">
                  <div class="play_pause_btn"> <span class="paly_btn" ng-click="runCampaign()"> <i class="fa fa-play-circle" aria-hidden="true"></i> </span> <span class="pause_btn" ng-click="pauseCampaign()"> <i class="fa fa-pause-circle" aria-hidden="true"></i> </span> </div>
                </div>
                <div class="cntr_plus" ng-click="addTouchpointModelShow();" style="cursor:pointer;"> <a href="#" class="plus_icon"><i class="fa fa-plus"></i></a> </div>
              </div>
            </div>
            <div class="clearfix"></div>
            <div class="flow_section" style="z-index:1;">
              <div class="adjust_div_one setup_commu" ng-show="alertCommunicationModel" style="position:absolute;">
                <div class="set_com" style="margin-left:0;">
                  <div class="set_plus">
                    <div  style="width:100%;padding:3px 0 0; margin-bottom:5px;"> <a href="#add_campaign_big" ng-click="loadCommunication();">Set Up Communication </a> </div>
                    <div ng-repeat="z in communicationList" class="flt-lft-rht" >
                      <div class="flt-lft">
                        <p><a href="#add_campaign_big">{{z.CAMMUNICATION_NAME}}</a></p>
                      </div>
                      <div class="right_icon"> <i ng-click="loadCommunication($index)" class="fa fa-pencil-square-o" title="Edit" ></i> <i ng-click="deleteCommunication($index)" class="fa fa-trash-o" title="Delete"></i> </div>
                    </div>
                    <!--<div ng-repeat="z in communicationList" style="text-align:left;width:100%;padding:5px 10px; border-top:1px solid;">
                     <a href="#"  class="pull-right"  style="margin-left:10px;font-size:14px;" ng-click="deleteCommunication($index)">
                     <i class="fa fa-trash-o"></i>
                     </a>
                     <a href="#add_campaign_big" ng-click="loadCommunication($index)" style="font-size:14px;">{{z.CAMMUNICATION_NAME}}
                     <span class="pull-right">
                     <i class="fa fa-pencil-square-o"></i>
                     </span>
                     </a>
                     </div>--> 
                  </div>
                </div>
              </div>
              <div class="clearfix"></div>
              <ul class="flow_ul touchpoint_list">
                <li ng-repeat="y in touchpointList"><a href="#" ng-click="loadTouchpoint($index)">{{ y.TOUCHPOINT_NAME }}</a> <span ng-click="editTouchpoint($index)"><i class="fa fa-pencil-square-o" title="Edit"></i></span> <span ng-click="deleteTouchpoint($index)"><i class="fa fa-trash-o"title="Delete"></i></span></li>
              </ul>
            </div>
            <div class="clearfix"></div>
          </div>
          <div class="clearfix"></div>
        </div>
      </div>
    </div>
    <!-- tab view for responsive only show -->
    <div class="big_campaign">
    <div style="left: 309.141px;" class="add_a_campgn_wrap ui-draggable" id="add_campaign_big" ng-show="showTouchpointCommunication">
    <div class="hide_t_c">
    <div class="modal-content">
    <div class="modal-header">
      <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true"></span></button>
      <h4 id="tchpntCommTitle" class="modal-title">Add Touchpoint Communication</h4>
    </div>
    <div style="overflow-y: unset; height: 360px;" class="modal-body">
    <form name="addCommunicationForm">
      <div class="form-group">
        <label>Title</label>
        <input type="text" id="tchpntCommName" placeholder="Communication Name" class="form-control add_caps" ng-model="communicationName" required>
      </div>
      <div class="form-group">
        <label>Target Customers</label>
        <div class="col-md-12 col-xs-12 rm_pad">
          <div class="calender_section">
            <input id="noOfDays" placeholder="No of Day(s) old" class="form-control add_caps" name="noOfDays" ng-model="targetCustomers"  numbers-only required>
            
            <!-- <div class="col-md-6">
              <input id="noOfDays2" placeholder="No Of Day(s) Old Maximum" class="form-control" name="noOfDays2" ng-model="targetCustomers2"  ng-min={{targetCustomers}} numbers-only required>
            </div> --> 
          </div>
        </div>
      </div>
      <div class="form-group custom_check"><br/>
        <br/>
        <label style="width:100%;">Push Communication Channels</label>
        <div style="margin:0;" class="checkbox">
          <div class="col-md-4">
            <div class="e_s_v">
              <input type="radio" name="pushcommureq" ng-model="pushTempType" ng-click="loadTemplateData('email');" value="email">
              <label for="Email" style="color:#519A1B;">Email</label>
              <input type="radio" name="pushcommureq" ng-model="pushTempType" ng-click="loadTemplateData('sms');" value="sms">
              <label for="Text" style="color:#AD3674;">SMS</label>
              <input type="radio" name="pushcommureq" required ng-model="pushTempType" ng-click="loadTemplateData('voice');" value="voice">
              <label for="Voice" style="color:#7F00FF;">Voice</label>
            </div>
          </div>
        </div>
      </div>
      <div class="form-group" ng-show="filterTemplate">
        <label style="text-align:left; width:100%;">Template Filter</label>
        <div class="col-md-12 rm_pad">
          <div class="calender_section">
            <div class="col-md-4 col-xs-12">
              <input type="text" id="searchTemplateName" ng-change="loadTemplateData('s')" placeholder="Enter the template name" class="form-control add_caps" ng-model="searchTemplateName">
            </div>
            <div class="col-md-3 col-xs-12">
              <datepicker
							  date-format="MM/dd/yyyy"
							  date-min-limit="1992/09/07"
							  button-prev='<i class="fa fa-arrow-circle-left"></i>'
							  button-next='<i class="fa fa-arrow-circle-right"></i>'>
                <input style="margin-bottom:0;" ng-keyup='templateFromDate = ""' type="text" placeholder="mm/dd/yyyy" class="angular-datepicker-input form-control from-date"  ng-model="templateFromDate" ng-change="loadTemplateData()">
              </datepicker>
            </div>
            <div class="col-md-3 col-xs-12">
              <datepicker
							  date-format="MM/dd/yyyy"
							  date-min-limit="{{templateFromDate}}"
							  button-prev='<i class="fa fa-arrow-circle-left"></i>'
							  button-next='<i class="fa fa-arrow-circle-right"></i>'>
                <input  style="margin-bottom:0;" ng-keyup='templateToDate = ""' type="text" placeholder="mm/dd/yyyy" class="angular-datepicker-input form-control to-date"  ng-model="templateToDate" ng-change="loadTemplateData()">
              </datepicker>
            </div>
          </div>
        </div>
      </div>
      <div class="form-group" ng-show="loadedComTemplate" style="display:none;">
        <label>Template Settings</label>
        <div style="width: 290px;">
          <div ng-click="editTemplateForCommunication();" class="lng_btn_frm autoClickEditTemplate" data-toggle="modal" data-target="#editTemplateModal"><span class="f_r"><i class="fa fa-check"></i></span><span style="text-align:center;font-weight:bold;cursor:pointer">{{ selectedTemplateName }}</span>
            <input type="hidden" required ng-model="selectedTemplateName">
          </div>
        </div>
      </div>
      <div class="form-group col-md-12 col-xs-12">
        <div class="col-md-3 col-xs-4 set_width" ng-repeat="u in templateList" ng-click="setCommunicationTemplate($index, '')">
        <div class="tooltip_wrapper checkTemplateSelected setTS_{{u.TEMPLATE_ID}}">
        <!--<i class="fa fa-pencil-square-o showEditTemp"></i>-->
        <i class="fa fa-envelope"></i>
        <span class="show_text">{{ u.TEMPLATE_NAME }}</span>
        <p class="tooltip_text_flow">{{ u.TEMPLATE_NAME }}</p>        
        </div>        
      </div>      
      </div>
      <!--<div class="form-group col-md-12">
        <div class="set_width col-md-2" ng-repeat="u in templateList" class="col-sm-3" ng-click="setCommunicationTemplate($index, '')">
        <div class="tooltip_wrapper checkTemplateSelected setTS_{{u.TEMPLATE_ID}}"> <!--<i class="fa fa-pencil-square-o showEditTemp" style="position:absolute;top:0px;z-index:99;right:0px;display:none"></i><i class="fa fa-envelope" aria-hidden="true"></i> <span class="tooltip_text">{{ u.TEMPLATE_NAME }}</span> </div>
        <div class="setTS_{{u.TEMPLATE_ID}}">
          <p class="showEditTemp2" style="display:none;">{{ u.TEMPLATE_NAME }}</p>
        </div>
      </div>
      </div>-->
      <div class="form-group custom_check bor-non" id="leadOption">
        <label>User Actions</label>
        <div style="margin:0;" class="checkbox">
          <div class="col-md-12"> <span class="lead" style="margin-left:15px;" ng-click="addLeadOption();"><i class="fa fa-plus-circle"></i></span> </div>
          <div class="col-md-12">
            <div class="col-md-4">
              <label class="title_u_a">Title</label>
            </div>
            <div class="col-md-4">
              <label class="title_u_a">Endpoint</label>
            </div>
            <div class="col-md-3">
              <label class="title_u_a">Code</label>
            </div>
            <div class="col-md-1">
              <label class="title_u_a">Lead</label>
            </div>
          </div>
          <div class="col-md-12 leadOption" ng-repeat="actlist in userActionList">
            <div class="col-md-4">
              <input type="text" name="userActionTitle[]" ng-model="actlist.userActionTitle" class="form-control add_caps adjst_input" required placeholder="Link Title"/>
            </div>
            <div class="col-md-4">
              <select class="col-md-4" name="userActionProtocol[]" ng-model="actlist.userActionProtocol">
                <option type="text" class="endpoints" value="http://" required placeholder="Link Endpoint" >http://</option>
                <option type="text" class="endpoints" value="https://" required placeholder="Link Endpoint" >https://</option>
              </select>
              <input class="col-md-8" name="userActionEndpoint[]"  ng-model="actlist.userActionEndpoint" type="text" class="form-control" required placeholder="Link Endpoint"/>
            </div>
            <div class="col-md-3">
              <input type="text" value="**action1**" name="userActionShortCode[]" style="background:none;" ng-model="actlist.userActionShortCode" class="form-control" required placeholder="Link Short Code" disabled=disabled readonly=true/>
            </div>
            <div class="col-md-1">
              <input type="checkbox" name="isLead" ng-model="actlist.isLead" ng-checked="actlist.isLead == 'true'" style="margin:0 0 0 7px; text-align:center;">
              <span class="trash_icon leadActClear" style="cursor:pointer;"> <i class="fa fa-trash" aria-hidden="true" ng-click="removeLeadOption($index)"></i> </span> </div>
          </div>
        </div>
      </div>
      <div class="form-group">
        <div class="col-md-12 col-sm-12 align-right">
          <button class="btn btn-success" ng-click="addCommunication();" type="button" class="btn btn-success pull-right" ng-disabled="addCommunicationForm.communicationName.$invalid">
          Submit
          </button>
          <button ng-click="addCommunicationModelHide();" type="button" class="btn btn-primary">Cancel</button>
        </div>
       
      </div>
      </div>
      <div class="modal-footer"> </div>
      </div>
      <!-- /.modal-content -->
      </div>
      </div>
      </div>
      </form>
    <div class="adjust_div f_r add_touch_plus" ng-show="addTouchpointModel" align="center">
      <div class="add_campgn_1">
        <div id="add_campaign" class="add_a_campgn_wrap reset_pop ui-draggable">
          <div class="inner_div">
            <div id="template-editor" class="modal-content">
              <div class="modal-header">
                <button ng-click="addTouchpointModelHide();" aria-label="Close" data-dismiss="modal" class="close" type="button"><i  class="fa fa-close"></i></button>
                <h4 id="cmpgnTitle" class="modal-title">{{ touchpointTitle }} Touchpoint</h4>
              </div>
              <div class="modal-body">
                <div class="form-group"> 
                  <!--<label>Touchpoint Name:</label>-->
                  <form name="addTouchpointForm">
                    <input type="text" id="cmpgnName" style="margin: 15px 0px 0px;" class="form-control add_caps my-colorpicker1 colorpicker-element" placeholder="Touchpoint Name" required ng-model="touchpointAddName" name="touchpointAddName">
                  </form>
                </div>
    
              </div>
              <div class="modal-footer move-center">
                <button style="margin-left:10px;" type="button" id="add_cmgn" class="btn btn-success btn-top" ng-click="addTouchpoint()"  ng-disabled="addTouchpointForm.touchpointAddName.$invalid">Submit</button>
                <button ng-click="addTouchpointModelHide();" type="button" onClick="" class="cancel_btn_1 btn btn-primary "> Cancel </button>
              </div>
            </div>
          </div>
          
          <!-- /.modal-content --> 
        </div>
      </div>
    </div>
    
    <!--page content --> 
    <!-- second section--> 
    
  </div>
  <!-- /page content --> 
</div>
</div>
<!-- Modal -->
<div id="editTemplateModal" class="modal fade" role="dialog">
  <div class="modal-dialog" style="width: 100%;height: 100%;margin: 0;padding: 0;">
    <div class="modal-content" style="height: auto;width: 75%;margin-left:15%">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title">Edit Template : {{ selectedTemplateName }}</h4>
      </div>
      <div class="modal-body">
        <div class="form-group">
          <div class="col-md-12 col-sm-12 col-xs-12">
            <div class="x_content">
              <div class="container-fluid">
                <div class="row">
                  <div class="container">
                    <div class="row">
                      <div class="col-lg-12 nopadding">
                        <textarea id="txtEditor" class="form-control">{{communicationTemplateContent}}</textarea>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
      <div class="modal-footer">
        <button type="button" style="margin-left:10px;" class="btn btn-primary update-btn" ng-click="saveCurrentTemplateData()">Update</button>
        <button type="button" class="btn btn-default" data-dismiss="modal" id="closeTempEditor">Close</button>
      </div>
    </div>
  </div>
</div>
<!-- footer content -->
<!-- <footer class="manualCapitalize">
    <a class="foot-link" href="#">
      <img src="images/logo/logo-3.PNG" alt="footer_logo">
    </a>
    &copy;&nbsp; Copyright <span id="dynamicYear"></span> All rights reserved.
  </footer> -->
<!-- /footer content --> 

<script src="js/config.js" type="text/javascript"></script> 
<script src="js/bootstrap.min.js"></script> 
<script src="js/nicescroll/jquery.nicescroll.min.js"></script> 
<script src="js/custom.js"></script> 
<script src="js/pages/flow-page.js" type="text/javascript"></script> 
<script type="text/javascript" src="js/angular-route.min.js"></script> 
<script type="text/javascript" src="js/angular-datepicker.js"></script>
</body>
</html>
