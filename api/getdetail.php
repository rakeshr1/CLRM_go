<?php

session_start();
require 'Slim/Slim.php';
$app = new Slim();
$app->get('/','getalldetails');
$app->get('/zone_based/','zonebased');
$app->get('/bankid_based/','bankidbased');
$app->get('/branchid_based/','branchidbased');
$app->get('/productid_based/','prodcutidbased');
$app->get('/branch_prod_based/','branch_prod_based');
$app->get('/bank_prod_based/','bank_prod_based');
$app->get('/banknamebased/','banknamebased');
$app->run();
function getalldetails() //f
{
	try 
	{
		$db = getConnection();
		$sql = "select * from bank";
		$alldata = array();
		if($result = $db->query($sql))
		{
			while($row = $result->fetch_assoc())
			{
				$alldata[] = $row;
			}
		}
		print_r(json_encode($alldata));
	}
	catch(Exception $e) 
	{
		//echo '{"error":{"message":'. $e->getMessage() .'}}'; 
		echo '{"message":"Please Try Again Later"}'; 
	}
}
function zonebased() //f
{
	$zone = $_REQUEST['zone'];
	$db = getConnection();
	$sql = "select BANK_ID,BANK_NAME from BANK where BANK_ZONE = '$zone'";
	$data = array();
	if($result = $db->query($sql))
	{
		while($row = $result->fetch_assoc())
		{
			$data[] = $row;
		}
	}
	print_r(json_encode($data));
}
function bankidbased() //f
{
	$bankid = $_REQUEST['bank_id'];
	$db = getConnection();
	$sql = "select Bank_name,Bank_code,Branch_code,Branch_name,Branch_addr,city,state,country,CreatedBy from branch2 where Bank_code = '$bankid'";
	$data = array();
	if($result = $db->query($sql))
	{
		while($row = $result->fetch_assoc())
		{
			$data[] = $row;
		}
	}
	echo '{"DETAILS":'.json_encode($data).'}';
}

function banknamebased()
{
	$bankname = $_REQUEST['bank_name'];
	
	$data ='';
	$db = getConnection();
	$sql = "select BANK_ID from bank where BANK_NAME = '$bankname'";
	
	if($result = $db->query($sql))
	{
		while($row = $result->fetch_assoc())
		{
			$data = $row['BANK_ID'];
		}
	}
	print_r($data);
}
function branchidbased() //f
{
	$branchid = $_REQUEST['branch_id'];
	$no_of_days = $_REQUEST['no_of_days'];
	$db = getConnection();
	$sql = "select id,NAME,EMAIL,PRODUCT_ID from CUSTOMER where BRANCH_ID = '2' and DATEDIFF(CURDATE(),JOINING_DATE) < '$no_of_days'";
	$data = array();
	if($result = $db->query($sql))
	{
		while($row = $result->fetch_assoc())
		{
			$data[] = $row;
		}
	}
	print_r(json_encode($data));
}
function prodcutidbased()
{
	$productid = $_REQUEST['product_id'];
	$no_of_days = $_REQUEST['no_of_days'];
	$db = getConnection();
	$sql = "select id,NAME,EMAIL,BRANCH_ID from CUSTOMER where PRODUCT_ID = '$productid' and DATEDIFF(CURDATE(),JOINING_DATE) < '$no_of_days'";
	$data = array();
	if($result = $db->query($sql))
	{
		while($row = $result->fetch_assoc())
		{
			$data[] = $row;
		}
	}
	print_r(json_encode($data));
	
}
 function bank_prod_based()
{
	$bankid = $_REQUEST['bankid'];
	
	//$productid = $_REQUEST['product_id'];
	$no_of_days = $_REQUEST['no_of_days'];
	
	$db = getConnection();
	//$sql = "select id,NAME,EMAIL,BRANCH_ID from CUSTOMER where PRODUCT_ID = '$productid' and DATEDIFF(CURDATE(),JOINING_DATE) < '$no_of_days'";
	//$sql = "select * from customer where PRODUCT_ID = '$productid' and BRANCH_ID = '$branchid' and DATEDIFF(CURDATE(),JOINING_DATE) < '$no_of_days'";
	
	
	$sql = "select * from customer where  BANK_ID = '$bankid' and DATEDIFF(CURDATE(),JOINING_DATE) < '$no_of_days'";
	
	//echo $sql;
	$data = array();
	if($result = $db->query($sql))
	{
		while($row = $result->fetch_assoc())
		{
			$data[] = $row;
		}
	}
	print_r(json_encode($data));
} 

 function branch_prod_based()
{
	$branchid = $_REQUEST['branchid'];
	$bid = $_REQUEST['bankid'];
	//$productid = $_REQUEST['product_id'];
	$no_of_days = $_REQUEST['no_of_days'];
	$role = $_REQUEST['role'];
	$db = getConnection();
	//$sql = "select id,NAME,EMAIL,BRANCH_ID from CUSTOMER where PRODUCT_ID = '$productid' and DATEDIFF(CURDATE(),JOINING_DATE) < '$no_of_days'";
	//$sql = "select * from customer where PRODUCT_ID = '$productid' and BRANCH_ID = '$branchid' and DATEDIFF(CURDATE(),JOINING_DATE) < '$no_of_days'";
	if ($role == 2)
	{
		$sql = "select * from customer where  BRANCH_ID IN($branchid) and BANK_ID = '$bid' and DATEDIFF(CURDATE(),JOINING_DATE) < '$no_of_days'";
	}
	else
	{
		$sql = "select * from customer where BRANCH_ID = '$branchid' and BANK_ID = '$bid' and DATEDIFF(CURDATE(),JOINING_DATE) < '$no_of_days'"; 
	}
	
	$data = array();
	if($result = $db->query($sql))
	{
		while($row = $result->fetch_assoc())
		{
			$data[] = $row;
		}
	}
	print_r(json_encode($data));
} 
function getConnection() {
	$dbhost="127.0.0.1";
    $dbuser ='sedev';
    $dbpass = 'p@$$w0rdSEDONA';
	$dbname='asic_antelope';
	$mysqli_conn = new mysqli($dbhost, $dbuser, $dbpass, $dbname);
	if ($mysqli_conn->connect_errno) {
		die("Failed to connect to MySQL: (" . $mysqli_conn->connect_errno . ") " . $mysqli_conn->connect_error);
	}
	return $mysqli_conn;
}

?>