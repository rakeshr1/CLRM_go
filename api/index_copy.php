<?php
session_start();
require 'Slim/Slim.php';
$app = new Slim();
$app->get('/', 'checkConnection');
$app->post('/authenticate/', 'authenticate');
$app->post('/forgetPassword/', 'forgetPassword');

$app->get('/campaignList/', 'getCampaigns');
$app->put('/campaign/', 'addCampaign');
$app->post('/campaign/', 'updateCampaign');
$app->post('/pauseCampaign/', 'pauseCampaign');
$app->delete('/campaign/', 'deleteCampaign');

$app->get('/touchpointList/', 'getTouchpoints');
$app->get('/touchpointListWithComm/', 'touchpointListWithComm');
$app->post('/touchpoint/', 'updateTouchpoint');
$app->put('/touchpoint/', 'addTouchpoint');
$app->delete('/touchpoint/', 'deleteTouchpoint');

$app->get('/communicationList/', 'communicationList');
$app->post('/communications/', 'addCommunication');
$app->post('/communication/', 'updateCommunication');
$app->get('/communication/', 'getCommunication');
$app->delete('/communication/', 'deleteCommunication');

$app->get('/templateList/', 'templateList');
$app->get('/gettemplatetypeList/', 'GetTemplatetypeList');
$app->get('/gettemplatetype/', 'GetTemplateType');
$app->get('/gettemplatenameList/', 'GetTemplatenameList');
$app->get('/gettemplatecontentList/', 'GetTemplatecontentList');
$app->get('/templateData/', 'templateData');

$app->put('/bank/', 'addBank');
$app->get('/bank/', 'getBank');
$app->post('/bank/', 'updateBank');
$app->get('/bankList/', 'bankList');

$app->put('/branch/', 'addBranch');
$app->get('/branch/', 'getBranch');
$app->post('/branch/', 'updateBranch');
$app->get('/branchList/', 'branchList');

$app->put('/role/', 'addRole');
$app->get('/role/', 'getRole');
$app->post('/role/', 'updateRole');
$app->get('/roleList/', 'roleList');

$app->get('/employeeList/', 'employeeList');
$app->put('/employee/', 'addEmployee');
$app->get('/employee/', 'getEmployee');
$app->post('/employee/', 'updateEmployee');

$app->get('/customerList/', 'customerList');
$app->put('/customer/', 'addCustomer');
$app->get('/customer/', 'getCustomer');
$app->post('/customer/', 'updateCustomer');

$app->get('/productList/', 'productList');
$app->put('/product/', 'addProduct');
$app->get('/product/', 'getProduct');
$app->post('/product/', 'updateProduct');

$app->get('/countryList/', 'countryList');
$app->get('/stateList/', 'stateList');
$app->get('/bankBranchList/', 'bankBranchList');
$app->get('/branchRoleList/', 'branchRoleList');
$app->get('/branchEmployeeList/', 'branchEmployeeList');

$app->get('/edit_tmp_form', 'editTmpForm');
$app->post('/template/', 'addTemplate');
$app->get('/template/', 'getTemplateId');
$app->post('/updatetemplate/', 'updateTmpForm');

$app->get('/employeeActivityList/', 'employeeActivityList');
$app->get('/customerResponse/', 'customerResponse');
$app->get('/responseRate/', 'responseRate');
$app->get('/employeeVolume/','employeeVolume');
$app->get('/myCustomer/','myCustomer');
$app->get('/customerTouchpoints/','customerTouchpoints');
$app->get('/getCustomerByEmail/','getCustomerByEmail');
$app->get('/customerTouchpointCommunications/','customerTouchpointCommunications');
$app->get('/customersResponseList/','customersResponseList');
$app->put('/lead/', 'addLead');
$app->run();

function checkConnection() {
	try {
		$db = getConnection();
		echo '{"TouchPoint": ' . json_encode($db) . '}';
	} catch(Exception $e) {
		echo '{"error":{"message":'. $e->getMessage() .'}}'; 
	}
}
function employeeVolume() {
	$id = addslashes($_REQUEST['id']);
	$db = getConnection();
	$fDate = date("Y-m-d 00:00:00");
	$tDate = date("Y-m-d 23:59:59");
	$sql="SELECT a.EMPLOYEE_ID, a.CAMPAIGN_TYPE, COUNT(a.CAMPAIGN_TYPE) AS CCOUNT,(SELECT COUNT(DISTINCT(CUSTOMER_ID)) FROM CUSTOMER_RESPONSE WHERE EMPLOYEE_ID='$id' AND EMPLOYEE_ID!='' AND CREATED_ON BETWEEN '$fDate'  AND '$tDate') AS NEW_CUSTOMERS FROM CUSTOMER_RESPONSE a WHERE a.EMPLOYEE_ID='$id' AND a.EMPLOYEE_ID!='' AND a.CREATED_ON BETWEEN '$fDate'  AND '$tDate' GROUP BY a.CAMPAIGN_TYPE";
	if ($result = $db->query($sql))	{
			
		$table_data = array();
		while($data = $result->fetch_assoc()) {
			$table_data[] = $data; 
		} 

		if(count($table_data) == 0) {
			$table_data = array();
		} else {
			$table_data[] = array('EMPLOYEE_ID'=>$table_data[0]['EMPLOYEE_ID'],'CAMPAIGN_TYPE'=>'customers','CCOUNT'=>$table_data[0]['NEW_CUSTOMERS'],'NEW_CUSTOMERS'=>$table_data[0]['NEW_CUSTOMERS']);
		}
		
		
		echo json_encode($table_data);
	}
}
function customerTouchpointCommunications() {
	$customerId = addslashes($_REQUEST['cid']);
	$employeeId = addslashes($_REQUEST['eid']);
	$touchpointId = addslashes($_REQUEST['tid']);
	$db = getConnection();
	$sql="SELECT a.*,b.CAMMUNICATION_NAME FROM CUSTOMER_RESPONSE a INNER JOIN COMMUNICATION b ON a.COMMUNICATION_ID=b.COMMUNICATION_ID WHERE a.EMPLOYEE_ID='$employeeId' AND a.EMPLOYEE_ID!='' AND a.CUSTOMER_EMAIL='$customerId' AND b.TOUCHPOINT_ID='$touchpointId'";
	
	if ($result = $db->query($sql))	{
		$table_data = '';		
		while($data = $result->fetch_assoc()) {
			$responseData = '';
			$responseData['COMMUNICATION_ID'] = $data['COMMUNICATION_ID'];
			$responseData['BANK_ID'] = $data['BANK_ID'];
			$responseData['BRANCH_ID'] = $data['BRANCH_ID'];
			$responseData['PRODUCT_ID'] = $data['PRODUCT_ID'];
			$responseData['EMPLOYEE_ID'] = $data['EMPLOYEE_ID'];
			$cid= $data['COMMUNICATION_ID'];
			$resp_data = '';
			$sql3 = "SELECT a.*,b.PRODUCT_NAME FROM LEADS a INNER JOIN PRODUCT b ON a.PRODUCT_ID=b.PRODUCT_ID WHERE a.COMMUNCIATION_ID=$cid AND a.CUSTOMER_ID='$customerId' AND a.EMPLOYEE_ID='$employeeId'";
								$result3 = $db->query($sql3);
								while($data3 = $result3->fetch_assoc()) {
									$resp_data[] = $data3;
								}
								$responseData['LEADS_DATA'] = $resp_data;
								
								
		
				$responseData['COMMUNICATION_TITLE'] = $data['CAMMUNICATION_NAME'];
				$responseData['STATUS_TEXT'] = 'Response : '.$data['CUSTOMER_RESPONSE'];
				$responseData['JOINING_DATE']=$data['CUSTOMER_JOINING_DATE'];

			$table_data[] = $responseData; 
		}
		echo json_encode($table_data);
	}
}

function myCustomer() {
	try {
		$db = getConnection();
		$id = addslashes($_REQUEST['id']);
		$sql = "SELECT DISTINCT(a.CUSTOMER_EMAIL), a.CUSTOMER_FNAME, a.CUSTOMER_LNAME FROM CUSTOMER_RESPONSE a WHERE a.EMPLOYEE_ID='$id' ORDER BY CUSTOMER_FNAME";
		 if ($result = $db->query($sql)) {
					$table_data = array();
					while($data = $result->fetch_assoc()) {
						$table_data[] = $data;
					}
					echo '{"MY_CUSTOMER": ' . json_encode($table_data) . '}';
		} else {
			//echo $db->error;
		}
	} catch(Exception $e) {
			//echo '{"error":{"message":'. $e->getMessage() .'}}'; 
			echo '{"message":"Please Try Again Later"}'; 
	}
}
function authenticate() {
	$username = addslashes($_REQUEST['username']);
	$password = addslashes($_REQUEST['password']);
	$sql = 'SELECT * FROM EMPLOYEE WHERE EMAIL=? AND PASSWORD=? AND STATUS="1"';	
	try {
		$db = getConnection();
		$stmt = $db->prepare($sql);  
		$stmt->bind_param("ss", $username,$password);
		$stmt->execute();
		$stmt->store_result();
		if($stmt->num_rows == '1') {
			$stmt->close();
			$sql = "SELECT a.*,b.ROLE_NAME FROM EMPLOYEE a INNER JOIN ROLE b ON a.ROLE_ID=b.ROLE_ID WHERE a.EMAIL='$username' AND a.PASSWORD='$password'";
			if ($result = $db->query($sql)) {
				while ($row = $result->fetch_assoc()) {
					$flowDesignerMenu = '';
					$manageBankMenu = '';
					$manageBranchMenu = '';
					$manageEmployeeMenu = '';
					$managetemplateMenu = '';
					$manageProductMenu = '';
					$manageRoleMenu = '';
					$row['accessibleURLs'] = '';
					if($row['ISFLOWDESIGNER'] == 1) {
						$flowDesignerMenu = '<li><a href="flow-page.html"><i class="fa fa-cogs"></i>Flow Design<span class="fa fa-chevron-down"></span></a> </li>';
						$row['accessibleURLs'] .= 'flow-page,';
					}
					if($row['MANAGE_BANK'] == 1) {
						$manageBankMenu = '<li><a href="manage-bank.html" data-pages="bank"><i class="fa fa-database"></i>Manage Banks <span class="fa fa-chevron-down"></span></a> </li>';
						$row['accessibleURLs'] .= ',manage-bank,bank,';
					}
					if($row['MANAGE_BRANCH'] == 1) {
						$manageBranchMenu = '<li><a href="manage-branch.html" data-pages="branch"><i class="fa fa-sitemap"></i>Manage Branches<span class="fa fa-chevron-down"></span></a> </li>';
						$row['accessibleURLs'] .= ',manage-branch,branch,';
					}
					if($row['MANAGE_EMPLOYEE'] == 1) {
						$manageEmployeeMenu = '<li><a href="manage-employee.html" data-pages="employee"><i class="fa fa-user"></i>Manage Bank Employees<span class="fa fa-chevron-down"></span></a> </li>';
						$row['accessibleURLs'] .= ',manage-employee,employee,';
					}
					if($row['MANAGE_TEMPLATE'] == 1) {
						$managetemplateMenu = '<li><a href="manage_template.html" data-pages="template"><i class="fa fa-balance-scale"></i>Manage Templates<span class="fa fa-chevron-down"></span></a> </li>';
						$row['accessibleURLs'] .= ',manage_template,template,';
					}
					if($row['MANAGE_PRODUCT'] == 1) {
						$manageProductMenu = '<li><a href="manage-product.html" data-pages="product"><i class="fa fa-shopping-cart"></i>Manage Products<span class="fa fa-chevron-down"></span></a> </li>';
						$row['accessibleURLs'] .= ',manage-product,product,';
					}
					if($row['MANAGE_ROLE'] == 1) {
						$manageRoleMenu = '<li><a href="manage-role.html" data-pages="role"><i class="fa fa-recycle"></i>Manage Roles<span class="fa fa-chevron-down"></span></a> </li>';
						$row['accessibleURLs'] .= ',manage-role,role,';
					}
					
					
					$row['message'] = 'success';
					$row['TOPMENU'] = '<div class="top_nav">
										  <div class="nav_menu">
											<nav class="" role="navigation">
											  <div class="nav toggle"> <a id="menu_toggle" class="alt_plus"><i class="fa fa-bars"></i></a> </div>
											  <ul class="nav navbar-nav navbar-right">
												<li class=""> <a href="javascript:;" class="user-profile dropdown-toggle" data-toggle="dropdown" aria-expanded="false"><span>'.$row['ROLE_NAME'].'</span><span>&nbsp;|&nbsp;</span><span> '.$row['FIRST_NAME'].'</span><span class=" fa fa-cog"></span> </a>
												  <ul class="dropdown-menu dropdown-usermenu animated fadeInDown pull-right">
													<li><a href=""> Refresh</a> </li>
													<li><a href="login.html"><i class="fa fa-sign-out pull-right"></i> Log Out</a> </li>
												  </ul>
												</li>
											  </ul>
											</nav>
										  </div>
										</div>';
if($row['ROLE_ID'] == '1') {
$row['accessibleURLs'] = $row['accessibleURLs'];
$row['dashboard'] = 'manage-bank.html';
$row['SIDEBAR'] = '<div class="left_col scroll-view nice-scroll-test">
        <div class="navbar nav_title cutm_nav_title" style="border: 0;"><a href="#" class="site_title"><img src="images/logo/logo-3.PNG" alt="Logo" /></a> </div>
        <div class="clearfix"></div>
        <br />
         <div id="sidebar-menu" class="main_menu_side hidden-print main_menu">
          <div class="menu_section">
            <ul class="nav side-menu">
			  '.$manageBankMenu.$manageBranchMenu.$manageEmployeeMenu.$managetemplateMenu.$manageProductMenu.$manageRoleMenu.$flowDesignerMenu.'
            </ul>
          </div> 
        </div>
	</div>';
} else if($row['ROLE_ID'] == '2') {
$row['accessibleURLs'] = $row['accessibleURLs'].'head-of-retail';
$row['dashboard'] = 'head-of-retail.html';
$row['SIDEBAR'] = '<div class="left_col scroll-view nice-scroll-test">
        <div class="navbar nav_title cutm_nav_title" style="border: 0;"><a href="#" class="site_title"><img src="images/logo/logo-3.PNG" alt="Logo" /></a> </div>
        <div class="clearfix"></div>
        <br />
        <br />
        <div id="sidebar-menu" class="main_menu_side hidden-print main_menu">
          <div class="menu_section">
            <ul class="nav side-menu">
              <li><a href="head-of-retail.html"><i class="fa fa-area-chart"></i>DashBoard<span class="fa fa-chevron-down"></span></a> </li>
              '.$manageBankMenu.$manageBranchMenu.$manageEmployeeMenu.$managetemplateMenu.$manageProductMenu.$manageRoleMenu.$flowDesignerMenu.'
            </ul>
          </div>
        </div>
      </div>';
	  
} else if($row['ROLE_ID'] == '3') {
$row['accessibleURLs'] = $row['accessibleURLs'].'bank-manager';
$row['dashboard'] = 'bank-manager.html';
$row['SIDEBAR'] = '<div class="left_col scroll-view nice-scroll-test">
        <div class="navbar nav_title cutm_nav_title" style="border: 0;"><a href="#" class="site_title"><img src="images/logo/logo-3.PNG" alt="Logo" /></a> </div>
        <div class="clearfix"></div>
        <br />
        <div id="sidebar-menu" class="main_menu_side hidden-print main_menu">
          <div class="menu_section">
            <ul class="nav side-menu">
              <li><a href="bank-manager.html"><i class="fa fa-area-chart"></i>DashBoard<span class="fa fa-chevron-down"></span></a> </li>
              '.$manageBankMenu.$manageBranchMenu.$manageEmployeeMenu.$managetemplateMenu.$manageProductMenu.$manageRoleMenu.$flowDesignerMenu.'
            </ul>
          </div>
        </div>
      </div>';
} else if($row['ROLE_ID'] == '4') {
$row['accessibleURLs'] = $row['accessibleURLs'].'bank-officer,customer-touchpoints,manage-response,customer-campaigns';
$row['dashboard'] = 'customer-campaigns.html';
$row['SIDEBAR'] = '<div class="left_col scroll-view nice-scroll-test">
        <div class="navbar nav_title cutm_nav_title" style="border: 0;"><a href="#" class="site_title"><img src="images/logo/logo-3.PNG" alt="Logo" /></a> </div>
        <div class="clearfix"></div>
        <br />
        <div id="sidebar-menu" class="main_menu_side hidden-print main_menu">
          <div class="menu_section">
            <ul class="nav side-menu">
              <li><a href="customer-campaigns.html" data-pages="customer-touchpoints"><i class="fa fa-area-chart"></i>DashBoard<span class="fa fa-chevron-down"></span></a> </li>
			  <li><a href="manage-response.html"><i class="fa fa-paper-plane-o"></i>Customer Response<span class="fa fa-chevron-down"></span></a> </li>
			  '.$manageBankMenu.$manageBranchMenu.$manageEmployeeMenu.$managetemplateMenu.$manageProductMenu.$manageRoleMenu.$flowDesignerMenu.'
            </ul>
          </div>
        </div>
      </div>';
} else {
$row['accessibleURLs'] = $row['accessibleURLs'].'';
$row['dashboard'] = 'index.html';
$row['SIDEBAR'] = '<div class="left_col scroll-view nice-scroll-test">
        <div class="navbar nav_title cutm_nav_title" style="border: 0;"><a href="#" class="site_title"><img src="images/logo/logo-3.PNG" alt="Logo" /></a> </div>
        <div class="clearfix"></div>
        <br />
        <div id="sidebar-menu" class="main_menu_side hidden-print main_menu">
          <div class="menu_section">
            <ul class="nav side-menu">
            </ul>
          </div>
        </div>
      </div>';
}
					
					echo '{"USER": ' . json_encode($row) . '}';
					$db = null; die;
				}
			}
			$result->close();
			echo '{"USER": {"message":"You Entered Incorrect Username/Password"}}'; $db = null;	die;
		} else {
			echo '{"USER": {"message":"You Entered Incorrect Username/Password"}}'; 
		}
	} catch(Exception $e) {
		echo '{"error":{"message":'. $e->getMessage() .'}}'; 
		//echo '{"message":"Please Try Again Later"}'; 
	}
}
function forgetPassword() {
	$username = addslashes($_REQUEST['username']);
	$sql = 'SELECT * FROM EMPLOYEE WHERE EMAIL=? AND STATUS="1"';	
	try {
		$db = getConnection();
		$stmt = $db->prepare($sql);  
		$stmt->bind_param("s", $username);
		$stmt->execute();
		$stmt->store_result();
		if($stmt->num_rows == '1') {
			$stmt->close();
			$sql = "SELECT * FROM EMPLOYEE WHERE EMAIL='$username'";
			if ($result = $db->query($sql)) {
				while ($row = $result->fetch_assoc()) {
					require_once('../lib/class.phpmailer.php');
					$mailSubject	=	"Password Request";
					$mailBody		=	"Your Password: ".$row['PASSWORD'];
						$mail = new PHPMailer();
						$mail->IsSMTP();
						$mail->Subject   =	$mailSubject;
						$mail->MsgHTML($mailBody);
						$mail->AddAddress($row['EMAIL']);
						if(!$mail->Send()) {
							 $res['message'] = $mail->ErrorInfo;
						} else {
							$res['message'] = 'success';
						}	
					
					
					echo '{"USER": ' . json_encode($res) . '}';
					$db = null; die;
				}
			}
			$result->close();
			echo '{"USER": {"message":"Username Not Exist"}}'; $db = null;	die;
		} else {
			echo '{"USER": {"message":"Username Not Exist"}}'; 
		}
	} catch(Exception $e) {
		//echo '{"error":{"message":'. $e->getMessage() .'}}'; 
		echo '{"message":"Please Try Again Later"}'; 
	}
}
function stateList() {
	try {
			$db = getConnection();
			$code = $_REQUEST['country_code'];
			$sql = "SELECT * FROM STATES WHERE COUNTRY_CODE='$code'";
			if ($result = $db->query($sql)) {
				$row = array();
				$i = 1;
				$table_data = '';
				$table_data = '';
					while($data = $result->fetch_assoc()) {
						$table_data[] = $data; 
					}
					echo '{"STATELIST": ' . json_encode($table_data) . '}';
			}
			$result->close();
	} catch(Exception $e) {
		//echo '{"error":{"message":'. $e->getMessage() .'}}'; 
		echo '{"message":"Please Try Again Later"}'; 
	}
}

function countryList() {
	try {
			$db = getConnection();
			$sql = "SELECT * FROM COUNTRY";
			if ($result = $db->query($sql)) {
				$row = array();
				$i = 1;
				$table_data = '';
				$table_data = '';
					while($data = $result->fetch_assoc()) {
						$table_data[] = $data; 
					}
					echo '{"COUNTRYLIST": ' . json_encode($table_data) . '}';
			}
			$result->close();
	} catch(Exception $e) {
		//echo '{"error":{"message":'. $e->getMessage() .'}}'; 
		echo '{"message":"Please Try Again Later"}'; 
	}
}

function deleteCampaign() {
	$id = $_REQUEST['id'];
	try {
			$db = getConnection();
			$db->query("DELETE FROM COMMUNICATION WHERE COMPAIGN_ID=$id");
			$db->query("DELETE FROM TOUCHPOINT WHERE CAMPAIGN_ID=$id");
			$db->query("DELETE FROM CAMPAIGN WHERE CAMPAIGN_ID=$id");
			echo $db->error;
	} catch(Exception $e) {
		//echo '{"error":{"message":'. $e->getMessage() .'}}'; 
		echo '{"message":"Please Try Again Later"}'; 
	}
}

function deleteTouchpoint() {
	$id = $_REQUEST['id'];
	try {
			$db = getConnection();
			$db->query("DELETE FROM COMMUNICATION WHERE TOUCHPOINT_ID=$id");
			$db->query("DELETE FROM TOUCHPOINT WHERE TOUCHPOINT_ID=$id");
			echo $db->error;
	} catch(Exception $e) {
		//echo '{"error":{"message":'. $e->getMessage() .'}}'; 
		echo '{"message":"Please Try Again Later"}'; 
	}
}

function deleteCommunication() {
	$id = $_REQUEST['id'];
	try {
			$db = getConnection();
			$db->query("DELETE FROM COMMUNICATION WHERE COMMUNICATION_ID=$id");
			echo $db->error;
	} catch(Exception $e) {
		//echo '{"error":{"message":'. $e->getMessage() .'}}'; 
		echo '{"message":"Please Try Again Later"}'; 
	}
}

function getCampaigns() {
	$id = addslashes($_REQUEST['userId']);
	try {
		$db = getConnection();
			$sql = "SELECT * FROM CAMPAIGN WHERE CREATE_BY='$id' ORDER BY DATE_ADDED DESC";
			if(isset($_REQUEST['for'])) {
				if($_REQUEST['for'] == 'bankOfficerDashboard') {
					$bankId = $_REQUEST['bankId'];
					$branchId = $_REQUEST['branchId'];
					$sql = "SELECT a.* FROM CAMPAIGN a INNER JOIN EMPLOYEE b on a.CREATE_BY=b.EMPLOYEE_ID WHERE b.BRANCH_ID=$branchId ORDER BY a.DATE_ADDED DESC";
				}
			}
			if ($result = $db->query($sql)) {
				$table_data = '';
				while($data = $result->fetch_assoc()) {
						$timeStamp = date('Y-m-d H:i:s');
						if($data['RUN_FROM'] > $timeStamp && $data['RUN_TILL'] > $timeStamp) {
							//$data['SORT_STATUS'] = 'FEATURED';
							$data['SORT_STATUS'] = 'PRESENT';
						} else if($data['RUN_TILL'] <= $timeStamp) {
							$data['SORT_STATUS'] = 'EXPIRED';
						} else if($data['RUN_FROM'] <= $timeStamp && $data['RUN_TILL'] >= $timeStamp) {
							$data['SORT_STATUS'] = 'PRESENT';
						} else {
							$data['SORT_STATUS'] = 'UNKNOWN';
						}
						$datan = explode('-',$data['RUN_FROM']);
				$data['RUN_FROM'] = $datan[1].'/'.$datan[2].'/'.$datan[0];
						$datan = explode('-',$data['RUN_TILL']);
						$data['RUN_TILL'] = $datan[1].'/'.$datan[2].'/'.$datan[0];
						$table_data[] = $data; 
				}
				echo '{"CAMPAIGN_DATA": ' . json_encode($table_data) . '}';
			} else {
			//echo '{"message":"Campaign Data Not Available"}'; 
			echo $db->error;
		}
	} catch(Exception $e) {
		//echo '{"error":{"message":'. $e->getMessage() .'}}'; 
		echo '{"message":"Please Try Again Later"}'; 
	}
}
function getTouchpoints() {
	$id = addslashes($_REQUEST['campaignId']);
	$bid = addslashes($_REQUEST['bankId']);
	try {
		$db = getConnection();
			$sql = "SELECT * FROM TOUCHPOINT WHERE CAMPAIGN_ID=$id AND BANK_ID=$bid";
			if ($result = $db->query($sql)) {
				$table_data = '';
				while($data = $result->fetch_assoc()) {
						$table_data[] = $data; 
				}
				echo '{"TOUCHPOINT_DATA": ' . json_encode($table_data) . '}';
			} else {
			echo '{"message":"Touchpoint Data Not Available"}'; 
		}
	} catch(Exception $e) {
		//echo '{"error":{"message":'. $e->getMessage() .'}}'; 
		echo '{"message":"Please Try Again Later"}'; 
	}
}
function touchpointListWithComm() {
	$id = addslashes($_REQUEST['campaignId']);
	$bid = addslashes($_REQUEST['bankId']);
	$branchId = addslashes($_REQUEST['branchId']);
	$uid = addslashes($_REQUEST['userId']);
	$query1 = '';
	$query2 = '';
	$query3 = '';
	
	if(isset($_REQUEST['fromDate']) && isset($_REQUEST['toDate'])) {
		if($_REQUEST['fromDate'] != '' && $_REQUEST['toDate'] != '') {
			$fromDate = explode('/',$_REQUEST['fromDate']);
			$fromDate = $fromDate[2].'-'.$fromDate[0].'-'.$fromDate[1].' 00:00:00';
			$toDate = explode('/',$_REQUEST['toDate']);
			$toDate = $toDate[2].'-'.$toDate[0].'-'.$toDate[1].' 23:59:59';
			$query1 = "AND DATE_ADDED BETWEEN '$fromDate' AND '$toDate'";
			$query2 = "AND CREATED_ON BETWEEN '$fromDate' AND '$toDate'";
			$query3 = "AND a.CREATED_ON BETWEEN '$fromDate' AND '$toDate'";
		} else if($_REQUEST['fromDate'] == '' && $_REQUEST['toDate'] != '') {
				die;
		} else if($_REQUEST['fromDate'] != '' && $_REQUEST['toDate'] == '') {
				die;
		}
	}
	try {
		$db = getConnection();
			$sql = "SELECT * FROM TOUCHPOINT WHERE CAMPAIGN_ID=$id AND BANK_ID=$bid $query1";
			if ($result = $db->query($sql)) {
				$table_data = '';
				$commu_data = '';
				$resp_data = '';
				while($data = $result->fetch_assoc()) {
						$commu_data = '';
						$resp_data = '';
						$tid = $data['TOUCHPOINT_ID'];
						$sql2 = "SELECT a.COMMUNICATION_ID,a.TOUCHPOINT_ID,a.CAMMUNICATION_NAME,a.TEMPLATE_TYPE FROM COMMUNICATION a WHERE a.TOUCHPOINT_ID=$tid $query1";
						$result2 = $db->query($sql2);
						while($data2 = $result2->fetch_assoc()) {
							$resp_data = '';
							$cid = $data2['COMMUNICATION_ID'];
								$sql3 = "SELECT * FROM CUSTOMER_RESPONSE WHERE COMMUNICATION_ID=$cid $query2";
								$result3 = $db->query($sql3);
								while($data3 = $result3->fetch_assoc()) {
									$resp_data[] = $data3;
								}
								$data2['RESPONSE_DATA'] = $resp_data;
								//$commu_data[] = $data2;
								
								$leads_data = '';
								$sql3 = "SELECT a.*,b.PRODUCT_NAME FROM LEADS a INNER JOIN PRODUCT b ON a.PRODUCT_ID=b.PRODUCT_ID WHERE a.COMMUNCIATION_ID=$cid $query1";
								$result3 = $db->query($sql3);
								while($data3 = $result3->fetch_assoc()) {
									$data3['DATE_ADDED_FMTED'] = date("M d, Y h:i:s A",strtotime($data3['DATE_ADDED']));
									$leads_data[] = $data3;
								}
								$data2['LEADS_DATA'] = $leads_data;
								$commu_data[] = $data2;
								
								$resp_data = '';
								//$sql3 = "SELECT DISTINCT(a.CUSTOMER_EMAIL),a.CUSTOMER_FNAME FROM CUSTOMER_RESPONSE a INNER JOIN COMMUNICATION b ON a.COMMUNICATION_ID=b.COMMUNICATION_ID INNER JOIN CAMPAIGN c ON b.COMPAIGN_ID=c.CAMPAIGN_ID WHERE c.CAMPAIGN_ID=$id $query3";
								$sql3 = "SELECT DISTINCT(a.CUSTOMER_EMAIL),a.CUSTOMER_FNAME,DATEDIFF(NOW(), a.CUSTOMER_JOINING_DATE) AS CREATED_ON_DIFF FROM CUSTOMER_RESPONSE a WHERE a.BRANCH_ID='$branchId'";
								
								$result3 = $db->query($sql3);
								while($data3 = $result3->fetch_assoc()) {
									/* $datan = explode(' ',$data3['CREATED_ON']);
									$datan = explode('-',$datan[0]);
									$data3['CREATED_ON'] = $datan[1].'/'.$datan[2].'/'.$datan[0]; */
									$resp_data[] = $data3;
								}
								$data['MY_CUSTOMERS'] = $resp_data;

						}
						$data['COMMUNICATIN_DATA'] = $commu_data;
						$table_data[] = $data; 
				}
				echo '{"TOUCHPOINT_DATA": ' . json_encode($table_data) . '}';
			} else {
			echo '{"message":"Touchpoint Data Not Available"}'; 
		}
	} catch(Exception $e) {
		//echo '{"error":{"message":'. $e->getMessage() .'}}'; 
		echo '{"message":"Please Try Again Later"}'; 
	}
}
function customerTouchpoints() {
	$employeeId = addslashes($_REQUEST['eid']);
	$customerId = addslashes($_REQUEST['cid']);
	$bankId = addslashes($_REQUEST['bankId']);
	$branchId = addslashes($_REQUEST['branchId']);
	try {
		$db = getConnection();
			$sql = "SELECT DISTINCT(c.TOUCHPOINT_ID),c.TOUCHPOINT_NAME FROM CUSTOMER_RESPONSE a INNER JOIN COMMUNICATION b ON a.COMMUNICATION_ID=b.COMMUNICATION_ID INNER JOIN TOUCHPOINT c ON c.TOUCHPOINT_ID=b.TOUCHPOINT_ID WHERE a.CUSTOMER_EMAIL='$customerId' AND a.BRANCH_ID=$branchId AND a.BANK_ID='$bankId'";
		
			if ($result = $db->query($sql)) {
				$table_data = '';
				while($data = $result->fetch_assoc()) {
						$table_data[] = $data; 
				}
				echo '{"TOUCHPOINTS": ' . json_encode($table_data) . '}';
			} else {
			echo '{"TOUCHPOINTS":"Touchpoint Data Not Available"}'; 
		}
	} catch(Exception $e) {
		//echo '{"error":{"message":'. $e->getMessage() .'}}'; 
		echo '{"message":"Please Try Again Later"}'; 
	}
}

function getCommunication() {
	$cid = (int)addslashes($_REQUEST['cid']);
	try {
		$db = getConnection();
			$sql = "SELECT a.*,b.TEMPLATE_NAME FROM COMMUNICATION a INNER JOIN TEMPLATE b on a.TEMPLATE_ID=b.TEMPLATE_ID WHERE a.COMMUNICATION_ID=$cid";
			if ($result = $db->query($sql)) {
				$table_data = '';
				while($data = $result->fetch_assoc()) {
						$data['USER_ACTIONS'] = json_decode($data['USER_ACTIONS']);
						$table_data[] = $data; 
				}
				echo '{"COMMUNICATION_DATA": ' . json_encode($table_data) . '}';
			} else {
			echo '{"message":"Communication Data Not Available"}'; 
		}
	} catch(Exception $e) {
		//echo '{"error":{"message":'. $e->getMessage() .'}}'; 
		echo '{"message":"Please Try Again Later"}'; 
	}
}
function communicationList() {
	$cid = (int)addslashes($_REQUEST['campaignId']);
	$tid = (int)addslashes($_REQUEST['touchpointId']);
	try {
		$db = getConnection();
			$sql = "SELECT * FROM COMMUNICATION WHERE COMPAIGN_ID=$cid AND TOUCHPOINT_ID=$tid";
			if ($result = $db->query($sql)) {
				$table_data = '';
				while($data = $result->fetch_assoc()) {
						$table_data[] = $data; 
				}
				echo '{"COMMUNICATION_DATA": ' . json_encode($table_data) . '}';
			} else {
			echo '{"message":"Communication Data Not Available"}'; 
		}
	} catch(Exception $e) {
		//echo '{"error":{"message":'. $e->getMessage() .'}}'; 
		echo '{"message":"Please Try Again Later"}'; 
	}
}
function employeeActivityList() {
	$id = (int)addslashes($_REQUEST['id']);
	//$group = addslashes($_REQUEST['group']);
	try {
		$db = getConnection();
		$groupQuery = '';
		$date = date('Y-m-d');

		if($_REQUEST['groupBy'] == 'w') {
			$groupQuery = " AND WEEK(z.CREATED_ON) = WEEK('$date') AND YEAR(z.CREATED_ON) = YEAR('$date')";
		} else if($_REQUEST['groupBy'] == 'm') {
			$groupQuery = " AND MONTH(z.CREATED_ON) = MONTH('$date') AND YEAR(z.CREATED_ON) = YEAR('$date')";
		} else if($_REQUEST['groupBy'] == 'y') {
			$groupQuery = " AND YEAR(z.CREATED_ON) = YEAR('$date')";
		} 
			$sql = "SELECT DISTINCT(a.ACTIVITY_NAME) AS ACTIVITY_NAME, b.CAMMUNICATION_NAME, (SELECT COUNT(z.ACTIVITY_NAME)  FROM EMPLOYEE_ACTIVITIES z WHERE z.EMPLOYEE_ID='$id' AND z.ACTIVITY_NAME=a.ACTIVITY_NAME $groupQuery AND z.COMMUNICATION_ID=a.COMMUNICATION_ID) AS ACTIVITY_COUNT FROM EMPLOYEE_ACTIVITIES a INNER JOIN COMMUNICATION b ON a.COMMUNICATION_ID=b.COMMUNICATION_ID WHERE a.EMPLOYEE_ID='$id'";
			if ($result = $db->query($sql)) {
				$table_data = '';
				$cdata = '';
				while($data = $result->fetch_assoc()) {	
						$table_data[] = $data; 
				}
				echo '{"COMMUNICATION_DATA": ' . json_encode($table_data) . '}';
			} else {
			echo '{"message":"Communication Data Not Available"}'; 
			echo $db->error;
		}
	} catch(Exception $e) {
		echo '{"error":{"message":'. $e->getMessage() .'}}'; 
		//echo '{"message":"Please Try Again Later"}'; 
	}
}
function templateList() {
	$type = addslashes($_REQUEST['tempType']);
	$bankId = (int)$_REQUEST['bankId'];
	$inQuery = '';
	$searchQuery = '';
	if(isset($_REQUEST['fromDate']) && isset($_REQUEST['toDate'])) {
		if($_REQUEST['fromDate'] != '' && $_REQUEST['toDate'] != '') {
			$fromDate = addslashes($_REQUEST['fromDate']);
			$toDate = addslashes($_REQUEST['toDate']);
			$fromDate = explode('/',$fromDate);
			$fromDate = $fromDate[2].'-'.$fromDate[0].'-'.$fromDate[1].' 00:00:00';
			$toDate = explode('/',$toDate);
			$toDate = $toDate[2].'-'.$toDate[0].'-'.$toDate[1].' 23:59:59';	
			$inQuery = " AND DATE_ADDED BETWEEN '".$fromDate."' AND '".$toDate."'";
		}
	}
	if(isset($_REQUEST['searchText'])) {
		if($_REQUEST['searchText'] != '') {
			$searchText = addslashes($_REQUEST['searchText']);
			$searchQuery = " AND TEMPLATE_NAME LIKE '%".$searchText."%'";
	}
	}
	try {
		$db = getConnection();
			$sql = "SELECT * FROM TEMPLATE WHERE BANK_ID=$bankId AND TEMPLATE_TYPE='".$type."' $inQuery $searchQuery";
			
			if ($result = $db->query($sql)) {
				$table_data = '';
				while($data = $result->fetch_assoc()) {
						//$data['TEMPLATE_CONTENT'] = $data['TEMPLATE_CONTENT'];
						$table_data[] = $data; 
				}
				echo '{"TEMPLATE_DATA": ' . json_encode($table_data) . '}';
			} else {
			echo '{"message":"Template Data Not Available"}'; 
		}
	} catch(Exception $e) {
		//echo '{"error":{"message":'. $e->getMessage() .'}}'; 
		echo '{"message":"Please Try Again Later"}'; 
	}
}
function addCampaign() { 
	$userId = addslashes($_REQUEST['userId']);
	$bankId = addslashes($_REQUEST['bankId']);
	$campaignName = addslashes($_REQUEST['campaignName']);
	$fromDate = addslashes($_REQUEST['fromDate']);
	$toDate = addslashes($_REQUEST['toDate']);
	
	$fromDate = explode('/',$fromDate);
	$fromDate = $fromDate[2].'-'.$fromDate[0].'-'.$fromDate[1];
	$toDate = explode('/',$toDate);
	$toDate = $toDate[2].'-'.$toDate[0].'-'.$toDate[1];	
	
	$sql = 'SELECT * FROM CAMPAIGN WHERE CAMPAIGN_NAME=?';
	try {
		$db = getConnection();
		$stmt = $db->prepare($sql);  
		$stmt->bind_param("s", $bank_name);
		$stmt->execute();
		$stmt->store_result();
		if($stmt->num_rows == '0') {
			$stmt->close();
			$sql = "INSERT INTO CAMPAIGN (CAMPAIGN_NAME,BANK_ID,DATE_ADDED,DATE_UPDATED,CREATE_BY,UPDATED_BY,RUN_FROM,RUN_TILL) VALUES ('$campaignName',$bankId,NOW(),NOW(),$userId,$userId,'$fromDate','$toDate')";
			if ($db->query($sql)) {
					echo '{"message":"success"}';	$db = null; die;
			}
			echo '{"message":"Please Try Again Later.."}';  $db = null;	die;
		} else {
			echo '{"message":"Bank Name Already Exist"}'; 
		}
	} catch(Exception $e) {
		//echo '{"error":{"message":'. $e->getMessage() .'}}'; 
		echo '{"message":"Please Try Again Later"}'; 
	}
}
function addCommunication() {
	$userId = (int)addslashes($_REQUEST['userId']);
	$bankId = (int)addslashes($_REQUEST['bankId']);
	$campaingId = (int)addslashes($_REQUEST['campaingId']);
	$touchpointId = (int)addslashes($_REQUEST['touchpointId']);
	$communicationName = addslashes($_REQUEST['communicationName']);
	$targetCutomers = (int)addslashes($_REQUEST['targetCutomers']);
	$targetCutomers2 = (int)addslashes($_REQUEST['targetCutomers2']);
	$templateId = (int)addslashes($_REQUEST['templateId']);
	$templateType = addslashes($_REQUEST['templateType']);
	$userActions = json_encode($_REQUEST['userActionList']);
	$templateContent = html_entity_decode(addslashes($_REQUEST['templateContent']));
	//$templateContent = $_REQUEST['templateContent'];
	try {
			$db = getConnection();
			$sql = "INSERT INTO COMMUNICATION (COMPAIGN_ID,TOUCHPOINT_ID,BANK_ID,CAMMUNICATION_NAME,TARGET_CUSTOMERS,TEMPLATE_ID,TEMPLATE_TYPE,DATE_ADDED,DATE_UPDATED,CREATED_BY,UPDATED_BY,STATUS,PRODUCT_ID,TEMPLATE_CONTENT,USER_ACTIONS,TARGET_CUSTOMERS2) VALUES ($campaingId,$touchpointId,$bankId,'$communicationName',$targetCutomers,$templateId,'$templateType',NOW(),NOW(),$userId,$userId,'progress',14,'$templateContent','$userActions',$targetCutomers2)";
			if ($db->query($sql)) {
					echo '{"message":"success"}';	$db = null; die;
			} else {
				//echo '{"message":"Error"}'; 
				echo $db->error;
			}
	} catch(Exception $e) {
		//echo '{"error":{"message":'. $e->getMessage() .'}}'; 
		echo '{"message":"Please Try Again Later"}'; 
	}
}

function updateCommunication() {
	$userId = (int)addslashes($_REQUEST['userId']); 
	$communicationId = (int)addslashes($_REQUEST['communicationId']);
	$communicationName = addslashes($_REQUEST['communicationName']);
	$targetCutomers = (int)addslashes($_REQUEST['targetCutomers']);
	$targetCutomers2 = (int)addslashes($_REQUEST['targetCutomers2']);
	$templateId = (int)addslashes($_REQUEST['templateId']);
	$templateType = addslashes($_REQUEST['templateType']);
	$userActions = json_encode($_REQUEST['userActionList']);
	$templateContent = html_entity_decode(addslashes($_REQUEST['templateContent']));
	//$templateContent = $_REQUEST['templateContent'];
	try {
			$db = getConnection();
			$sql = "UPDATE COMMUNICATION SET CAMMUNICATION_NAME='$communicationName', TARGET_CUSTOMERS='$targetCutomers', TEMPLATE_ID='$templateId', TEMPLATE_TYPE='$templateType', DATE_UPDATED=NOW(), UPDATED_BY=$userId, TEMPLATE_CONTENT='$templateContent', USER_ACTIONS='$userActions', TARGET_CUSTOMERS2=$targetCutomers2 WHERE COMMUNICATION_ID=$communicationId";
			if ($db->query($sql)) {
					echo '{"message":"success"}';	$db = null; die;
			} else {
				echo '{"message":"Error"}'; 
			}
	} catch(Exception $e) {
		echo '{"error":{"message":'. $e->getMessage() .'}}'; 
		//echo '{"message":"Please Try Again Later"}'; 
	}
}

function addTouchpoint() {
	$userId = addslashes($_REQUEST['userId']);
	$bankId = addslashes($_REQUEST['bankId']);
	$campaignId = addslashes($_REQUEST['campaingId']);
	$touchpointName = addslashes($_REQUEST['touchpointName']);
	try {
		$db = getConnection();
			$sql = "INSERT INTO TOUCHPOINT (CAMPAIGN_ID,TOUCHPOINT_NAME,BANK_ID,DATE_ADDED,DATE_UPDATED,CREATED_BY,UPDATED_BY) VALUES ('$campaignId','$touchpointName',$bankId,NOW(),NOW(),$userId,$userId)";
			if ($db->query($sql)) {
					echo '{"message":"success"}';	$db = null; die;
			}
			echo '{"message":"Please Try Again Later.."}';  $db = null;	die;
	} catch(Exception $e) {
		//echo '{"error":{"message":'. $e->getMessage() .'}}'; 
		echo '{"message":"Please Try Again Later"}'; 
	}
}
function bankList() {
	try {
			$db = getConnection();
			$sql = "SELECT a.*,(SELECT COUNT(c.BRANCH_ID) FROM BRANCHES c WHERE a.BANK_ID=c.BANK_ID) AS branch_count FROM BANK a ORDER BY a.BANK_ID DESC";
			if ($result = $db->query($sql)) {
				$row = array();
				$i = 1;
				$table_data = '';
				if(isset($_REQUEST['type'])) { 
				$table_data = '';
					while($data = $result->fetch_assoc()) {
						$table_data[] = $data; 
					}
					echo '{"BANKLIST": ' . json_encode($table_data) . '}';
				} else {
					while($data = $result->fetch_assoc()) {
						$action_data = '<a href="bank.html?id='.$data['BANK_ID'].'"><i class="fa fa-pencil-square-o"></i></a>&nbsp;&nbsp;<a href="#"><i class="fa fa-trash-o"></i></a>';
						$table_data .= '<tr><td>'.$i.'</td><td>'.$data['BANK_ID'].'</td><td>'.$data['BANK_NAME'].'</td><td>'.$data['branch_count'].'</td><td>'.$action_data.'</td></tr>';
						$i = $i + 1;
					}
					echo '{"BANKLIST": ' . json_encode($table_data) . '}';
				}
			}
			$result->close();
	} catch(Exception $e) {
		//echo '{"error":{"message":'. $e->getMessage() .'}}'; 
		echo '{"message":"Please Try Again Later"}'; 
	}
}
function branchList() {
	try {
			$db = getConnection();
			$sql = "SELECT a.BRANCH_ID,a.BRANCH_NAME,a.BRANCH_CODE,b.BANK_NAME FROM BRANCHES a INNER JOIN BANK b on a.BANK_ID=b.BANK_ID ORDER BY a.BRANCH_ID DESC";
			if ($result = $db->query($sql)) {
				$row = array();
				$i = 1;
				$table_data = '';
				if(isset($_REQUEST['type'])) { 
				$table_data = '';
					while($data = $result->fetch_assoc()) {
						$table_data[] = $data; 
					}
					echo '{"BRANCHLIST": ' . json_encode($table_data) . '}';
				} else {
					while($data = $result->fetch_assoc()) {
						$action_data = '<a href="branch.html?id='.$data['BRANCH_ID'].'"><i class="fa fa-pencil-square-o"></i></a>&nbsp;&nbsp;<a href="#"><i class="fa fa-trash-o"></i></a>';
						$table_data .= '<tr><td>'.$i.'</td><td>'.$data['BRANCH_NAME'].'</td><td>'.$data['BRANCH_CODE'].'</td><td>'.$data['BANK_NAME'].'</td><td>'.$action_data.'</td></tr>';
						$i = $i + 1;
					}
					echo '{"BRANCHLIST": ' . json_encode($table_data) . '}';
				}
			}
			$result->close();
	} catch(Exception $e) {
		//echo '{"error":{"message":'. $e->getMessage() .'}}'; 
		echo '{"message":"Please Try Again Later"}'; 
	}
}

function bankBranchList() {
	$bank_id = (int)$_REQUEST['id'];
	try {
			$db = getConnection();
			$sql = "SELECT a.BRANCH_NAME,a.BRANCH_CODE,a.BRANCH_ID FROM BRANCHES a WHERE a.BANK_ID=$bank_id ORDER BY a.BRANCH_ID DESC";
			if ($result = $db->query($sql)) {
				$table_data = '';
					while($data = $result->fetch_assoc()) {
						$table_data[] = $data; 
					}
					echo '{"BRANCH_LIST": ' . json_encode($table_data) . '}';
			}
			$result->close();
	} catch(Exception $e) {
		//echo '{"error":{"message":'. $e->getMessage() .'}}'; 
		echo '{"message":"Please Try Again Later"}'; 
	}
}
function branchRoleList() {
	$bank_id = (int)$_REQUEST['id'];
	try {
			$db = getConnection();
			$sql = "SELECT a.ROLE_NAME,a.ROLE_ID FROM ROLE a WHERE a.ROLE_ID!=1 ORDER BY a.ROLE_ID DESC";
			if ($result = $db->query($sql)) {
				$table_data = '';
					while($data = $result->fetch_assoc()) {
						$table_data[] = $data; 
					}
					echo '{"ROLE_LIST": ' . json_encode($table_data) . '}';
			}
			$result->close();
	} catch(Exception $e) {
		//echo '{"error":{"message":'. $e->getMessage() .'}}'; 
		echo '{"message":"Please Try Again Later"}'; 
	}
}
function branchEmployeeList() {
	$branch_id = (int)$_REQUEST['id'];
	try {
			$db = getConnection();
			$sql = "SELECT a.FIRST_NAME AS EMPLOYEE_NAME,a.EMPLOYEE_ID FROM EMPLOYEE a WHERE a.BRANCH_ID=$branch_id ORDER BY a.EMPLOYEE_ID ASC";
			if ($result = $db->query($sql)) {
				$table_data = '';
					while($data = $result->fetch_assoc()) {
						$table_data[] = $data; 
					}
					echo '{"EMPLOYEE_LIST": ' . json_encode($table_data) . '}';
			}
			$result->close();
	} catch(Exception $e) {
		//echo '{"error":{"message":'. $e->getMessage() .'}}'; 
		echo '{"message":"Please Try Again Later"}'; 
	}
}

function roleList() {
	try {
			$db = getConnection();
			$sql = "SELECT a.ROLE_NAME,a.ROLE_ID,b.BANK_NAME FROM ROLE a INNER JOIN BANK b on a.BANK_ID=b.BANK_ID ORDER BY a.ROLE_ID DESC";
			if ($result = $db->query($sql)) {
				$row = array();
				$i = 1;
				$table_data = '';
				while($data = $result->fetch_assoc()) {
					$action_data = '<a href="role.html?id='.$data['ROLE_ID'].'"><i class="fa fa-pencil-square-o"></i></a>&nbsp;&nbsp;&nbsp;<span style="cursor:pointer;" type="button"><i class="fa fa-trash-o"></i></span>';
					$table_data .= '<tr><td>'.$i.'</td><td>'.$data['ROLE_NAME'].'</td><td>'.$data['BANK_NAME'].'</td><td>'.$action_data.'</td></tr>';
					$i = $i + 1;
				}
				echo '{"ROLELIST": ' . json_encode($table_data) . '}';
			}
			$result->close();
	} catch(Exception $e) {
		echo '{"error":{"message":'. $e->getMessage() .'}}'; 
		//echo '{"message":"Please Try Again Later"}'; 
	}
}

function employeeList() {
	try {
			$db = getConnection();
			$sql = "SELECT a.FIRST_NAME,a.LAST_NAME,a.EMAIL,a.EMPLOYEE_ID,b.BANK_NAME,c.BRANCH_NAME FROM EMPLOYEE a INNER JOIN BANK b on a.BANK_ID=b.BANK_ID INNER JOIN BRANCHES c ON a.BRANCH_ID=c.BRANCH_ID ORDER BY a.EMPLOYEE_ID DESC";
			if ($result = $db->query($sql)) {
				$row = array();
				$i = 1;
				$table_data = '';
				while($data = $result->fetch_assoc()) {
					$action_data = '<a href="employee.html?id='.$data['EMPLOYEE_ID'].'"><i class="fa fa-pencil-square-o"></i></a>&nbsp;&nbsp;<a href="#"><i class="fa fa-trash-o"></i></a>';
					$table_data .= '<tr><td>'.$i.'</td><td>'.$data['FIRST_NAME'].' '.$data['LAST_NAME'].'</td><td>'.$data['EMAIL'].'</td><td>'.$data['BANK_NAME'].'</td><td>'.$data['BRANCH_NAME'].'</td><td>'.$action_data.'</td></tr>';
					$i = $i + 1;
				}
				echo '{"BANKLIST": ' . json_encode($table_data) . '}';
			}
			$result->close();
	} catch(Exception $e) {
		//echo '{"error":{"message":'. $e->getMessage() .'}}'; 
		echo '{"message":"Please Try Again Later"}'; 
	}
}
function customerList() {
	try {
			$db = getConnection();
			$sql = "SELECT a.CUSTOMER_ID,a.FIRST_NAME,a.LAST_NAME,a.EMAIL,b.BANK_NAME,c.BRANCH_NAME,d.FIRST_NAME AS EMPLOYEE_NAME FROM CUSTOMER a INNER JOIN BANK b on a.BANK_ID=b.BANK_ID INNER JOIN BRANCHES c ON a.BRANCH_ID=c.BRANCH_ID INNER JOIN EMPLOYEE d ON a.EMPLOYEE_ID=d.EMPLOYEE_ID ORDER BY a.CUSTOMER_ID DESC";
			if ($result = $db->query($sql)) {
				$row = array();
				$i = 1;
				$table_data = '';
				if(isset($_REQUEST['type'])) { 
					while($data = $result->fetch_assoc()) {
						$table_data[] = $data; 
					}
					echo '{"CUSTOMER_LIST": ' . json_encode($table_data) . '}';die;
				} else {
				while($data = $result->fetch_assoc()) {
					$action_data = '<a href="customer.html?id='.$data['CUSTOMER_ID'].'"><i class="fa fa-pencil-square-o"></i></a>&nbsp;&nbsp;<a href="#"><i class="fa fa-trash-o"></i></a>';
					$table_data .= '<tr><td>'.$i.'</td><td>'.$data['FIRST_NAME'].' '.$data['LAST_NAME'].'</td><td>'.$data['EMAIL'].'</td><td>'.$data['EMPLOYEE_NAME'].'</td><td>'.$data['BRANCH_NAME'].'</td><td>'.$data['BANK_NAME'].'</td><td>'.$action_data.'</td></tr>';
					$i = $i + 1;
				}
				}
				echo '{"BANKLIST": ' . json_encode($table_data) . '}';
			} else {
				echo $db->error;
			}
	} catch(Exception $e) {
		//echo '{"error":{"message":'. $e->getMessage() .'}}'; 
		echo '{"message":"Please Try Again Later"}'; 
	}
}
function customersResponseList() {
	try {
			$db = getConnection();
			$sql = "SELECT a.*,b.CAMMUNICATION_NAME FROM CUSTOMER_RESPONSE a INNER JOIN COMMUNICATION b ON a.COMMUNICATION_ID=b.COMMUNICATION_ID";
			if ($result = $db->query($sql)) {
				$table_data = '';
				if(isset($_REQUEST['type'])) { 
					while($data = $result->fetch_assoc()) {
						$table_data[] = $data; 
					}
					echo '{"RESPONSE_LIST": ' . json_encode($table_data) . '}';die;
				} else {
				while($data = $result->fetch_assoc()) {
					$table_data .= '<tr><td>'.$data['CAMMUNICATION_NAME'].'</td><td>'.$data['CUSTOMER_ID'].'</td><td>'.$data['CUSTOMER_FNAME'].'</td><td>'.$data['CUSTOMER_EMAIL'].'</td><td>'.$data['CUSTOMER_MOBILE_NO'].'</td><td>'.$data['CAMPAIGN_TYPE'].'</td><td>'.$data['CUSTOMER_RESPONSE'].'</td></tr>';
				}
				}
				echo '{"RESPONSE_LIST": ' . json_encode($table_data) . '}';
			} else {
				echo $db->error;
			}
	} catch(Exception $e) {
		//echo '{"error":{"message":'. $e->getMessage() .'}}'; 
		echo '{"message":"Please Try Again Later"}'; 
	}
}
function productList() {
	try {
			$db = getConnection();
			$sql = "SELECT a.* FROM PRODUCT a ORDER BY a.PRODUCT_ID DESC";
			if ($result = $db->query($sql)) {
				$row = array();
				$i = 1;
				$table_data = '';
				if(isset($_REQUEST['type'])) { 
					while($data = $result->fetch_assoc()) {
						$table_data[] = $data; 
					}
					echo '{"PRODUCT_LIST": ' . json_encode($table_data) . '}';die;
				} else {
				while($data = $result->fetch_assoc()) {
					$action_data = '<a href="product.html?id='.$data['PRODUCT_ID'].'"><i class="fa fa-pencil-square-o"></i></a>&nbsp;&nbsp;<a href="#"><i class="fa fa-trash-o"></i></a>';
					$table_data .= '<tr><td>'.$i.'</td><td>'.$data['PRODUCT_ID'].'</td><td>'.$data['PRODUCT_NAME'].'</td><td>'.$action_data.'</td></tr>';
					$i = $i + 1;
				}
				echo '{"BANKLIST": ' . json_encode($table_data) . '}';
				}
			}
			$result->close();
	} catch(Exception $e) {
		//echo '{"error":{"message":'. $e->getMessage() .'}}'; 
		echo '{"message":"Please Try Again Later"}'; 
	}
}
function addBank() {
	$bank_name = addslashes($_REQUEST['bankname']);
	$sql = 'SELECT * FROM BANK WHERE BANK_NAME=?';
	try {
		$db = getConnection();
		$stmt = $db->prepare($sql);  
		$stmt->bind_param("s", $bank_name);
		$stmt->execute();
		$stmt->store_result();
		if($stmt->num_rows == '0') {
			$stmt->close();
			$sql = "INSERT INTO BANK (BANK_NAME) VALUES ('$bank_name')";
			if ($db->query($sql)) {
					echo '{"message":"success"}';	$db = null; die;
			}
			echo '{"message":"Please Try Again Later.."}';  $db = null;	die;
		} else {
			echo '{"message":"Bank Name Already Exist"}'; 
		}
	} catch(Exception $e) {
		//echo '{"error":{"message":'. $e->getMessage() .'}}'; 
		echo '{"message":"Please Try Again Later"}'; 
	}
}
function addProduct() {
	$product_name = addslashes($_REQUEST['productname']);
	$sql = 'SELECT * FROM PRODUCT WHERE PRODUCT_NAME=?';
	try {
		$db = getConnection();
		$stmt = $db->prepare($sql);  
		$stmt->bind_param("s", $product_name);
		$stmt->execute();
		$stmt->store_result();
		if($stmt->num_rows == '0') {
			$stmt->close();
			$sql = "INSERT INTO PRODUCT (PRODUCT_NAME) VALUES ('$product_name')";
			if ($db->query($sql)) {
					echo '{"message":"success"}';	$db = null; die;
			}
			echo '{"message":"Please Try Again Later.."}';  $db = null;	die;
		} else {
			echo '{"message":"Product Name Already Exist"}'; 
		}
	} catch(Exception $e) {
		//echo '{"error":{"message":'. $e->getMessage() .'}}'; 
		echo '{"message":"Please Try Again Later"}'; 
	}
}
function getBank() {
	$bank_id = addslashes($_REQUEST['id']);
	$sql = 'SELECT * FROM BANK WHERE BANK_ID=?';
	try {
		$db = getConnection();
		$stmt = $db->prepare($sql);  
		$stmt->bind_param("s", $bank_id);
		$stmt->execute();
		$stmt->store_result();
		if($stmt->num_rows == '1') {
			$sql = "SELECT * FROM BANK WHERE BANK_ID='$bank_id'";
			if ($result = $db->query($sql)) {
				$row = array();
				$data = $result->fetch_object();
				echo '{"BANK_DATA": ' . json_encode($data) . '}';
			} 
		} else {
			echo '{"message":"Bank Data Not Available"}'; 
		}
	} catch(Exception $e) {
		//echo '{"error":{"message":'. $e->getMessage() .'}}'; 
		echo '{"message":"Please Try Again Later"}'; 
	}
}

function getCampaign() {
	$id = (int)addslashes($_REQUEST['id']);
	$sql = 'SELECT * FROM CAMPAIGN WHERE CAMPAIGN_ID=?';
	try {
		$db = getConnection();
		$stmt = $db->prepare($sql);  
		$stmt->bind_param("i", $id);
		$stmt->execute();
		$stmt->store_result();
		if($stmt->num_rows == '1') {
			$sql = "SELECT * FROM CAMPAIGN WHERE CAMPAIGN_ID='$id'";
			if ($result = $db->query($sql)) {
				$row = array();
				$data = $result->fetch_object();
				echo '{"CAMPAIGN_DATA": ' . json_encode($data) . '}'; die;
			} 
		} else {
			echo '{"message":"Campaign Data Not Available"}'; 
		}
	} catch(Exception $e) {
		//echo '{"error":{"message":'. $e->getMessage() .'}}'; 
		echo '{"message":"Please Try Again Later"}'; 
	}
}
function getRole() {
	$role_id = (int)$_REQUEST['id'];
	if($role_id == '') { echo '{"ROLE_DATA": {"ROLE_NAME":"","BANK_ID":""}}'; die; }
	$sql = 'SELECT * FROM ROLE WHERE ROLE_ID=?';
	try {
		$db = getConnection();
		$stmt = $db->prepare($sql);  
		$stmt->bind_param("i", $role_id);
		$stmt->execute();
		$stmt->store_result();
		if($stmt->num_rows == '1') {
			$sql = "SELECT * FROM ROLE WHERE ROLE_ID='$role_id'";
			if ($result = $db->query($sql)) {
				$row = array();
				$data = $result->fetch_object();
				echo '{"ROLE_DATA": ' . json_encode($data) . '}';
			} 
		} else {
			echo '{"message":"Role Data Not Available"}'; 
		}
	} catch(Exception $e) {
		//echo '{"error":{"message":'. $e->getMessage() .'}}'; 
		echo '{"message":"Please Try Again Later"}'; 
	}
}

function getBranch() {
	$branch_id = (int)$_REQUEST['id'];
	if($branch_id == '') { echo '{"BRANCH_DATA": {"BRANCH_NAME":"","BRANCH_ID":""}}'; die; }
	$sql = 'SELECT * FROM BRANCHES WHERE BRANCH_ID=?';
	try {
		$db = getConnection();
		$stmt = $db->prepare($sql);  
		$stmt->bind_param("i", $branch_id);
		$stmt->execute();
		$stmt->store_result();
		if($stmt->num_rows == '1') {
			$sql = "SELECT * FROM BRANCHES WHERE BRANCH_ID='$branch_id'";
			if ($result = $db->query($sql)) {
				$row = array();
				$data = $result->fetch_object();
				echo '{"BRANCH_DATA": ' . json_encode($data) . '}';
			} 
		} else {
			echo '{"message":"Branch Data Not Available"}'; 
		}
	} catch(Exception $e) {
		//echo '{"error":{"message":'. $e->getMessage() .'}}'; 
		echo '{"message":"Please Try Again Later"}'; 
	}
}

function getEmployee() {
	$id = (int)$_REQUEST['id'];
	if($id == '') { echo '{"EMPLOYEE_DATA": {"EMPLOYEE_NAME":"","EMPLOYEE_ID":""}}'; die; }
	$sql = 'SELECT * FROM EMPLOYEE WHERE EMPLOYEE_ID=?';
	try {
		$db = getConnection();
		$stmt = $db->prepare($sql);  
		$stmt->bind_param("i", $id);
		$stmt->execute();
		$stmt->store_result();
		if($stmt->num_rows == '1') {
			$sql = "SELECT * FROM EMPLOYEE WHERE EMPLOYEE_ID='$id'";
			if ($result = $db->query($sql)) {
				$data = $result->fetch_object();
				$datan = explode('-',$data->DATE_OF_BIRTH);
				$data->DATE_OF_BIRTH = $datan[1].'/'.$datan[2].'/'.$datan[0];
				echo '{"EMPLOYEE_DATA": ' . json_encode($data) . '}';
			} 
		} else {
			echo '{"message":"Employee Data Not Available"}'; 
		}
	} catch(Exception $e) {
		//echo '{"error":{"message":'. $e->getMessage() .'}}'; 
		echo '{"message":"Please Try Again Later"}'; 
	}
}

function templateData() {
	$id = (int)$_REQUEST['id'];
	if($id == '') { echo '{"EMPLOYEE_DATA": {"EMPLOYEE_NAME":"","EMPLOYEE_ID":""}}'; die; }
	$sql = 'SELECT * FROM TEMPLATE WHERE TEMPLATE_ID=?';
	try {
		$db = getConnection();
		$stmt = $db->prepare($sql);  
		$stmt->bind_param("i", $id);
		$stmt->execute();
		$stmt->store_result();
		if($stmt->num_rows == '1') {
			$sql = "SELECT * FROM TEMPLATE WHERE TEMPLATE_ID='$id'";
			if ($result = $db->query($sql)) {
				$row = array();
				$data = $result->fetch_object();
				echo '{"TEMPLATE_DATA": ' . json_encode($data) . '}';
			} 
		} else {
			echo '{"message":"TEMPLATE Data Not Available"}'; 
		}
	} catch(Exception $e) {
		//echo '{"error":{"message":'. $e->getMessage() .'}}'; 
		echo '{"message":"Please Try Again Later"}'; 
	}
}

function getCustomer() {
	$id = (int)$_REQUEST['id'];
	if($id == '') { echo '{"CUSTOMER_DATA": {"CUSTOMER_NAME":"","CUSTOMER_ID":""}}'; die; }
	$sql = 'SELECT * FROM CUSTOMER WHERE CUSTOMER_ID=?';
	try {
		$db = getConnection();
		$stmt = $db->prepare($sql);  
		$stmt->bind_param("i", $id);
		$stmt->execute();
		$stmt->store_result();
		if($stmt->num_rows == '1') {
			$sql = "SELECT * FROM CUSTOMER WHERE CUSTOMER_ID='$id'";
			if ($result = $db->query($sql)) {
				$row = array();
				$data = $result->fetch_object();
				$datan = explode('-',$data->DATE_OF_BIRTH);
				$data->DATE_OF_BIRTH = $datan[1].'/'.$datan[2].'/'.$datan[0];
				$datan = explode('-',$data->DATE_OF_JOIN);
				$data->DATE_OF_JOIN = $datan[1].'/'.$datan[2].'/'.$datan[0];
				echo '{"CUSTOMER_DATA": ' . json_encode($data) . '}';
			} 
		} else {
			echo '{"message":"Customer Data Not Available"}'; 
		}
	} catch(Exception $e) {
		//echo '{"error":{"message":'. $e->getMessage() .'}}'; 
		echo '{"message":"Please Try Again Later"}'; 
	}
}

function getCustomerByEmail() {
	$id = $_REQUEST['id'];
	if($id == '') { echo '{"CUSTOMER_DATA": {"CUSTOMER_NAME":"","CUSTOMER_ID":""}}'; die; }
	$sql = 'SELECT * FROM CUSTOMER_RESPONSE WHERE CUSTOMER_EMAIL=?';
	try {
		$db = getConnection();
		$stmt = $db->prepare($sql);  
		$stmt->bind_param("s", $id);
		$stmt->execute();
		$stmt->store_result();
		if($stmt->num_rows >= '1') {
			$sql = "SELECT * FROM CUSTOMER_RESPONSE WHERE CUSTOMER_EMAIL='$id'";
			if ($result = $db->query($sql)) {
				
				$data = $result->fetch_object();
				$data->CUSTOMER_JOINING_DATE = date("M d, Y",strtotime($data->CUSTOMER_JOINING_DATE));
				echo '{"CUSTOMER_DATA": ' . json_encode($data) . '}';
			} 
		} else {
			echo '{"CUSTOMER_DATA":"Customer Data Not Available"}'; 
		}
	} catch(Exception $e) {
		//echo '{"error":{"message":'. $e->getMessage() .'}}'; 
		echo '{"CUSTOMER_DATA":"Please Try Again Later"}'; 
	}
}

function getProduct() {
	$id = addslashes($_REQUEST['id']);
	$sql = 'SELECT * FROM PRODUCT WHERE PRODUCT_ID=?';
	try {
		$db = getConnection();
		$stmt = $db->prepare($sql);  
		$stmt->bind_param("s", $id);
		$stmt->execute();
		$stmt->store_result();
		if($stmt->num_rows == '1') {
			$sql = "SELECT * FROM PRODUCT WHERE PRODUCT_ID='$id'";
			if ($result = $db->query($sql)) {
				$row = array();
				$data = $result->fetch_object();
				echo '{"PRODUCT_DATA": ' . json_encode($data) . '}';
			} 
		} else {
			echo '{"message":"Product Data Not Available"}'; 
		}
	} catch(Exception $e) {
		//echo '{"error":{"message":'. $e->getMessage() .'}}'; 
		echo '{"message":"Please Try Again Later"}'; 
	}
}
function updateBank() {
	$bank_id = (int)addslashes($_REQUEST['id']);
	$bank_name = addslashes($_REQUEST['bankname']);
	$sql = 'UPDATE BANK SET BANK_NAME=? WHERE BANK_ID=?';
	try {
		$db = getConnection();
		$stmt = $db->prepare($sql);  
		$stmt->bind_param("si", $bank_name,$bank_id);
		if($stmt->execute()) {
			echo '{"message":"Bank Data Updated"}'; 
		} else {
			echo '{"message":"error"}'; 
		}
	} catch(Exception $e) {
		//echo '{"error":{"message":'. $e->getMessage() .'}}'; 
		echo '{"message":"Please Try Again Later"}'; 
	}
}
function updateCampaign() {
	$cid = (int)addslashes($_REQUEST['cid']);
	$cname = addslashes($_REQUEST['campaignName']);
	$fromDate = addslashes($_REQUEST['fromDate']);
	$toDate = addslashes($_REQUEST['toDate']);
	$fromDate = explode('/',$fromDate);
	$fromDate = $fromDate[2].'-'.$fromDate[0].'-'.$fromDate[1];
	$toDate = explode('/',$toDate);
	$toDate = $toDate[2].'-'.$toDate[0].'-'.$toDate[1];
	$sql = 'UPDATE CAMPAIGN SET CAMPAIGN_NAME=?, RUN_FROM=?, RUN_TILL=? WHERE CAMPAIGN_ID=?';
	try {
		$db = getConnection();
		$stmt = $db->prepare($sql);  
		$stmt->bind_param("sssi", $cname,$fromDate,$toDate,$cid);
		if($stmt->execute()) {
			echo '{"message":"success"}'; 
		} else {
			echo '{"message":"error"}'; 
		}
	} catch(Exception $e) {
		//echo '{"error":{"message":'. $e->getMessage() .'}}'; 
		echo '{"message":"Please Try Again Later"}'; 
	}
}
function pauseCampaign() {
	$cid = (int)addslashes($_REQUEST['id']);
	$sql = 'UPDATE CAMPAIGN SET STATUS="INACTIVE" WHERE CAMPAIGN_ID=?';
	try {
		$db = getConnection();
		$stmt = $db->prepare($sql);  
		$stmt->bind_param("i", $cid);
		if($stmt->execute()) {
			echo '{"message":"success"}'; 
		} else {
			echo '{"message":"error"}'; 
		}
	} catch(Exception $e) {
		//echo '{"error":{"message":'. $e->getMessage() .'}}'; 
		echo '{"message":"Please Try Again Later"}'; 
	}
}
function updateTouchpoint() {
	$id = (int)addslashes($_REQUEST['touchpointId']);
	$name = addslashes($_REQUEST['touchpointName']);
	$sql = 'UPDATE TOUCHPOINT SET TOUCHPOINT_NAME=? WHERE TOUCHPOINT_ID=?';
	try {
		$db = getConnection();
		$stmt = $db->prepare($sql);  
		$stmt->bind_param("si", $name,$id);
		if($stmt->execute()) {
			echo '{"message":"success"}'; 
		} else {
			echo '{"message":"error"}'; 
		}
	} catch(Exception $e) {
		//echo '{"error":{"message":'. $e->getMessage() .'}}'; 
		echo '{"message":"Please Try Again Later"}'; 
	}
}

function updateRole() {
	$role_id = (int)addslashes($_REQUEST['id']);
	$bank_name = (int)$_REQUEST['bankname'];
	$role_name = addslashes($_REQUEST['role_name']);
	$sql = 'UPDATE ROLE SET ROLE_NAME=?, BANK_ID=? WHERE ROLE_ID=?';
	try {
		$db = getConnection();
		$stmt = $db->prepare($sql);  
		$stmt->bind_param("sii", $role_name, $bank_name, $role_id);
		if($stmt->execute()) {
			echo '{"message":"Role Data Updated"}'; 
		} else {
			echo '{"message":"error"}'; 
			//echo '{"message":"'.$db->error.'"}'; 
		}
	} catch(Exception $e) {
		//echo '{"error":{"message":'. $e->getMessage() .'}}'; 
		echo '{"message":"Please Try Again Later"}'; 
	}
}

function updateBranch() {
	$id		= (int)$_REQUEST['id'];
	$updated_by = (int)1;
	$bank_name		= (int)$_REQUEST['bankname'];
	$branch_name	= addslashes($_REQUEST['branchname']);
	$branch_code	= addslashes($_REQUEST['branchcode']);
	$country		= addslashes($_REQUEST['country']);
	$state			= addslashes($_REQUEST['state']);
	$city			= addslashes($_REQUEST['city']);
	$address		= addslashes($_REQUEST['address']);
	$sql = 'UPDATE BRANCHES SET BANK_ID=?, BRANCH_NAME=?, BRANCH_CODE=?, COUNTRY=?, STATE=?, CITY=?, ADDRESS=?, DATE_UPDATED=NOW(), UPDATED_BY=? WHERE BRANCH_ID=?';
	try {
		$db = getConnection();
		$stmt = $db->prepare($sql);  
		$stmt->bind_param("issssssii", $bank_name, $branch_name, $branch_code, $country, $state, $city, $address, $updated_by, $id);
		if($stmt->execute()) {
			echo '{"message":"Branch Data Updated"}'; 
		} else {
			echo '{"message":"error"}'; 
			//echo '{"message":"'.$db->error.'"}'; 
		}
	} catch(Exception $e) {
		//echo '{"error":{"message":'. $e->getMessage() .'}}'; 
		echo '{"message":"Please Try Again Later"}'; 
	}
}

function updateEmployee() {
	$id		= (int)$_REQUEST['id'];
	$updated_by = (int)1;
	$bank_name		= (int)$_REQUEST['bankname'];
	$branch_name	= (int)$_REQUEST['branchname'];
	$role_name		= (int)$_REQUEST['role'];
	$email			= addslashes($_REQUEST['email']);
	$password		= addslashes($_REQUEST['password']);
	$first_name		= addslashes($_REQUEST['firstname']);
	$last_name		= addslashes($_REQUEST['lastname']);
	$dob			= addslashes($_REQUEST['dob']);
	$phno			= addslashes($_REQUEST['phno']);
	$mono			= addslashes($_REQUEST['mono']);
	$gender			= addslashes($_REQUEST['gender']);
	$country		= addslashes($_REQUEST['country']);
	$state			= addslashes($_REQUEST['state']);
	$city			= addslashes($_REQUEST['city']);
	$address		= addslashes($_REQUEST['address']);
	$zip			= addslashes($_REQUEST['zip']);
	$status			= addslashes($_REQUEST['status']);
	$isfd			= (int)$_REQUEST['isfd'];
	$dob = explode('/',$dob);
	$dob = $dob[2].'-'.$dob[0].'-'.$dob[1];
	$sql = 'UPDATE EMPLOYEE SET BANK_ID=?, BRANCH_ID=?, ROLE_ID=?, FIRST_NAME=?, LAST_NAME=?, GENDER=?, DATE_OF_BIRTH=?, EMAIL=?, PASSWORD=?, PHONE_NO=?, MOBILE_NO=?, ADDRESS=?, CITY=?, STATE=?, COUNTY=?, PIN_CODE=?, STATUS=?, DATE_UPDATED=NOW(), ISFLOWDESIGNER=? WHERE EMPLOYEE_ID=?';
	try {
		$db = getConnection();
		$stmt = $db->prepare($sql);  
		$stmt->bind_param("iiissssssssssssssii", $bank_name, $branch_name, $role_name, $first_name, $last_name, $gender, $dob, $email, $password, $phno, $mono, $address, $city, $state, $country, $zip, $status, $isfd, $id);
		if($stmt->execute()) {
			echo '{"message":"Employee Data Updated"}'; 
		} else {
			echo '{"message":"error"}'; 
			//echo '{"message":"'.$db->error.'"}'; 
		}
	} catch(Exception $e) {
		//echo '{"error":{"message":'. $e->getMessage() .'}}'; 
		echo '{"message":"Please Try Again Later"}'; 
	}
}

function updateCustomer() {
	$id		= (int)$_REQUEST['id'];
	$updated_by = (int)1;
	$bank_name		= (int)$_REQUEST['bankname'];
	$branch_name	= (int)$_REQUEST['branchname'];
	$employee_name	= (int)$_REQUEST['employee'];
	$email			= addslashes($_REQUEST['email']);
	$first_name		= addslashes($_REQUEST['firstname']);
	$last_name		= addslashes($_REQUEST['lastname']);
	$doj			= addslashes($_REQUEST['doj']);
	$dob			= addslashes($_REQUEST['dob']);
	$phno			= addslashes($_REQUEST['phno']);
	$mono			= addslashes($_REQUEST['mono']);
	$gender			= addslashes($_REQUEST['gender']);
	$country		= addslashes($_REQUEST['country']);
	$state			= addslashes($_REQUEST['state']);
	$city			= addslashes($_REQUEST['city']);
	$address		= addslashes($_REQUEST['address']);
	$zip			= addslashes($_REQUEST['zip']);
	$account		= addslashes($_REQUEST['account']);
	$occupation		= addslashes($_REQUEST['occupation']);
	$dob = explode('/',$dob);
	$dob = $dob[2].'-'.$dob[0].'-'.$dob[1];
	$doj = explode('/',$doj);
	$doj = $doj[2].'-'.$doj[0].'-'.$doj[1];
	$sql = 'UPDATE CUSTOMER SET BANK_ID=?, BRANCH_ID=?, EMPLOYEE_ID=?, ACCOUNT_TYPE=?, EMAIL=?, FIRST_NAME=?, LAST_NAME=?, OCCUPATION=?, DATE_OF_JOIN=?, PHONE_NO=?, MOBILE_NO=?, GENDER=?, DATE_OF_BIRTH=?, ADDRESS=?, CITY=?, STATE=?, COUNTRY=?, PINCODE=?, DATE_UPDATED=NOW() WHERE CUSTOMER_ID=?';
	try {
		$db = getConnection();
		$stmt = $db->prepare($sql);  
		$stmt->bind_param("iiisssssssssssssssi", $bank_name, $branch_name, $employee_name, $account, $email, $first_name, $last_name, $occupation, $doj, $phno, $mono, $gender, $dob, $address, $city, $state, $country, $zip, $id);
		if($stmt->execute()) {
			echo '{"message":"Customer Data Updated"}'; 
		} else {
			echo '{"message":"error"}'; 
			//echo '{"message":"'.$db->error.'"}'; 
		}
	} catch(Exception $e) {
		//echo '{"error":{"message":'. $e->getMessage() .'}}'; 
		echo '{"message":"Please Try Again Later"}'; 
	}
}

function updateProduct() {
	$id = (int)$_REQUEST['id'];
	$name = addslashes($_REQUEST['productname']);
	$sql = 'UPDATE PRODUCT SET PRODUCT_NAME=? WHERE PRODUCT_ID=?';
	try {
		$db = getConnection();
		$stmt = $db->prepare($sql);  
		$stmt->bind_param("si", $name,$id);
		if($stmt->execute()) {
			echo '{"message":"Product Data Updated"}'; 
		} else {
			echo '{"message":"error"}'; 
		}
	} catch(Exception $e) {
		//echo '{"error":{"message":'. $e->getMessage() .'}}'; 
		echo '{"message":"Please Try Again Later"}'; 
	}
}
function addBranch() {
	$bank_name		= (int)$_REQUEST['bankname'];
	$branch_name	= addslashes($_REQUEST['branchname']);
	$branch_code	= addslashes($_REQUEST['branchcode']);
	$country		= addslashes($_REQUEST['country']);
	$state			= addslashes($_REQUEST['state']);
	$city			= addslashes($_REQUEST['city']);
	$address		= addslashes($_REQUEST['address']);
	$sql = 'SELECT * FROM BRANCHES WHERE BANK_ID=? AND BRANCH_CODE=?';
	try {
		$db = getConnection();
		$stmt = $db->prepare($sql);  
		$stmt->bind_param("ss", $bank_name,$branch_code);
		$stmt->execute();
		$stmt->store_result();
		if($stmt->num_rows == '0') {
			$stmt->close();
			$sql = "INSERT INTO BRANCHES (BANK_ID,BRANCH_NAME,BRANCH_CODE,COUNTRY,STATE,CITY,DATE_ADDED,DATE_UPDATED,CREATE_BY,UPDATED_BY) VALUES ($bank_name,'$branch_name','$branch_code','$country','$state','$city', NOW(),NOW(),'','')";
			if ($db->query($sql)) {
					echo '{"message":"success"}';	$db = null; die;
			} else {
				echo '{"message":"'.$db->error.'"}';  $db = null;	die;
			}
			echo '{"message":"Please Try Again Later.."}';  $db = null;	die;
		} else {
			echo '{"message":"Branch Code Already Exist in the Selected Bank"}'; 
		}
	} catch(Exception $e) {
		//echo '{"error":{"message":'. $e->getMessage() .'}}'; 
		echo '{"message":"Please Try Again Later"}'; 
	}
}
function addLead() {
	$employeeId		= addslashes($_REQUEST['employeeId']);
	$branchId	= addslashes($_REQUEST['branchId']);
	$bankId		= addslashes($_REQUEST['bankId']);
	$customerId	= addslashes($_REQUEST['customerId']);
	$productId	= addslashes($_REQUEST['productId']);
	$campaignId		= addslashes($_REQUEST['campaignId']);
	$touchpointId			= addslashes($_REQUEST['touchpointId']);
	$commId			= addslashes($_REQUEST['commId']);
	$leadText		= addslashes($_REQUEST['leadText']);
	try {
		$db = getConnection();
			$sql = "INSERT INTO LEADS (BANK_ID, BRANCH_ID, EMPLOYEE_ID,CUSTOMER_ID,PRODUCT_ID,TOUCHPOINT_ID,COMMUNCIATION_ID,CAMPAIGN_ID,DATE_ADDED,LEAD_TEXT) VALUES ('$bankId', '$branchId', '$employeeId','$customerId','$productId','$touchpointId','$commId','$campaignId', NOW(), '$leadText')";
			if ($db->query($sql)) {
					echo '{"message":"success"}';	$db = null; die;
			} else {
				echo '{"message":"'.$db->error.'"}';  $db = null;	die;
			}
	} catch(Exception $e) {
		//echo '{"error":{"message":'. $e->getMessage() .'}}'; 
		echo '{"message":"Please Try Again Later"}'; 
	}
}

function addEmployee() {
	$bank_name		= (int)$_REQUEST['bankname'];
	$branch_name	= (int)$_REQUEST['branchname'];
	$role_name		= (int)$_REQUEST['role'];
	$email			= addslashes($_REQUEST['email']);
	$password		= addslashes($_REQUEST['password']);
	$first_name		= addslashes($_REQUEST['firstname']);
	$last_name		= addslashes($_REQUEST['lastname']);
	$dob			= addslashes($_REQUEST['dob']);
	$phno			= addslashes($_REQUEST['phno']);
	$mono			= addslashes($_REQUEST['mono']);
	$gender			= addslashes($_REQUEST['gender']);
	$country		= addslashes($_REQUEST['country']);
	$state			= addslashes($_REQUEST['state']);
	$city			= addslashes($_REQUEST['city']);
	$address		= addslashes($_REQUEST['address']);
	$zip			= (int)addslashes($_REQUEST['zip']);
	$status			= (int)$_REQUEST['status'];
	$isfd			= (int)$_REQUEST['isfd'];
	$dob = explode('/',$dob);
	$dob = $dob[2].'-'.$dob[0].'-'.$dob[1];
	$sql = 'SELECT * FROM EMPLOYEE WHERE EMAIL=?';
	try {
		$db = getConnection();
		$stmt = $db->prepare($sql);  
		$stmt->bind_param("s", $email);
		$stmt->execute();
		$stmt->store_result();
		if($stmt->num_rows == '0') {
			$stmt->close();
			$sql = "INSERT INTO EMPLOYEE (BANK_ID,BRANCH_ID,ROLE_ID,FIRST_NAME,LAST_NAME,GENDER,DATE_OF_BIRTH,EMAIL,PASSWORD,PHONE_NO,MOBILE_NO,ADDRESS,CITY,STATE,COUNTY,PIN_CODE,STATUS,DATE_ADDED,DATE_UPDATED,ISFLOWDESIGNER) 
	VALUES ($bank_name,$branch_name,$role_name,'$first_name','$last_name','$gender','$dob','$email','$password','$phno','$mono','$address','$city','$state','$country','$zip','$status',NOW(),NOW(),$isfd)";
			if ($db->query($sql)) {
					echo '{"message":"success"}';	$db = null; die;
			} else {
				echo '{"message":"'.$db->error.'"}';  $db = null;	die;
			}
			echo '{"message":"Please Try Again Later.."}';  $db = null;	die;
		} else {
			echo '{"message":"Employee Email Already Exist"}'; 
		}
	} catch(Exception $e) {
		echo '{"error":{"message":'. $e->getMessage() .'}}'; 
		//echo '{"message":"Please Try Again Later"}'; 
	}
}

function addCustomer() {
	$bank_name		= (int)$_REQUEST['bankname'];
	$branch_name	= (int)$_REQUEST['branchname'];
	$employee_name	= (int)$_REQUEST['employee'];
	$email			= addslashes($_REQUEST['email']);
	$first_name		= addslashes($_REQUEST['firstname']);
	$last_name		= addslashes($_REQUEST['lastname']);
	$doj			= addslashes($_REQUEST['doj']);
	$dob			= addslashes($_REQUEST['dob']);
	$phno			= addslashes($_REQUEST['phno']);
	$mono			= addslashes($_REQUEST['mono']);
	$gender			= addslashes($_REQUEST['gender']);
	$country		= addslashes($_REQUEST['country']);
	$state			= addslashes($_REQUEST['state']);
	$city			= addslashes($_REQUEST['city']);
	$address		= addslashes($_REQUEST['address']);
	$zip			= (int)addslashes($_REQUEST['zip']);
	$account		= addslashes($_REQUEST['account']);
	$occupation		= addslashes($_REQUEST['occupation']);
	$dob = explode('/',$dob);
	$dob = $dob[2].'-'.$dob[0].'-'.$dob[1];
	$doj = explode('/',$doj);
	$doj = $doj[2].'-'.$doj[0].'-'.$doj[1];
	$sql = 'SELECT * FROM CUSTOMER WHERE EMAIL=?';
	try {
		$db = getConnection();
		$stmt = $db->prepare($sql);  
		$stmt->bind_param("s", $email);
		$stmt->execute();
		$stmt->store_result();
		if($stmt->num_rows == '0') {
			$stmt->close();
			$sql = "INSERT INTO CUSTOMER (BANK_ID,BRANCH_ID,EMPLOYEE_ID,ACCOUNT_TYPE,EMAIL,FIRST_NAME,LAST_NAME,OCCUPATION,DATE_OF_JOIN,PHONE_NO,MOBILE_NO,GENDER,DATE_OF_BIRTH,ADDRESS,CITY,STATE,COUNTRY,PINCODE,DATE_ADDED,DATE_UPDATED) 
	VALUES ($bank_name,$branch_name,$employee_name,'$account','$email','$first_name','$last_name','$occupation','$doj','$phno','$mono','$gender','$dob','$address','$city','$state','$country','$zip',NOW(),NOW())";
			if ($db->query($sql)) {
					echo '{"message":"success"}';	$db = null; die;
			} else {
				echo '{"message":"'.$db->error.'"}';  $db = null;	die;
			}
			echo '{"message":"Please Try Again Later.."}';  $db = null;	die;
		} else {
			echo '{"message":"Customer Email Already Exist"}'; 
		}
	} catch(Exception $e) {
		echo '{"error":{"message":'. $e->getMessage() .'}}'; 
		//echo '{"message":"Please Try Again Later"}'; 
	}
}
function addRole() {
	$bank_name = addslashes($_REQUEST['bankname']);
	$role_name = addslashes($_REQUEST['role_name']);
	$sql = 'SELECT * FROM ROLE WHERE BANK_ID=? AND ROLE_NAME=?';
	try {
		$db = getConnection();
		$stmt = $db->prepare($sql);  
		$stmt->bind_param("ss", $bank_name,$role_name);
		$stmt->execute();
		$stmt->store_result();
		if($stmt->num_rows == '0') {
			$stmt->close();
			$sql = "INSERT INTO ROLE (BANK_ID,ROLE_NAME) VALUES ('$bank_name','$role_name')";
			if ($db->query($sql)) {
					echo '{"message":"success"}';	$db = null; die;
			}
			echo '{"message":"Please Try Again Later.."}';  $db = null;	die;
		} else {
			echo '{"message":"Bank Name Already Exist"}'; 
		}
	} catch(Exception $e) {
		//echo '{"error":{"message":'. $e->getMessage() .'}}'; 
		echo '{"message":"Please Try Again Later"}'; 
	}
}

function addTemplate() {
	$tmp_bank_id=addslashes($_POST['bankname']);
	$tmp_name=addslashes($_POST['tmp_name']);
	$tmp_content=addslashes($_POST['tmp_content']);
	//$tmp_contents=htmlspecialchars($tmp_content);
	$tmp_contents=html_entity_decode($tmp_content);
	$tmp_type=addslashes($_POST['tmp_type']);
	$screenShot = addslashes($_POST['screenShot']);
	$db = getConnection();
	try {
			$db = getConnection();
			$sql = "INSERT INTO TEMPLATE (`BANK_ID`,`TEMPLATE_TYPE`,`TEMPLATE_NAME`,`TEMPLATE_CONTENT`,`SCREENSHOT`,`DATE_ADDED`,`DATE_UPDATED`) VALUES ('$tmp_bank_id','$tmp_type','$tmp_name','$tmp_contents','$screenShot',NOW(),NOW())";
			if ($db->query($sql)) {
					echo '{"message":"success"}';	
					$db = null; die;
			} else {
				echo '{"message":"Error"}'; 
			}
	} catch(Exception $e) {
		//echo '{"error":{"message":'. $e->getMessage() .'}}'; 
		echo '{"message":"Please Try Again Later"}'; 
	}	
		 
}


function editTmpForm() 
{
try 
{
	$db = getConnection();
	$sql = "SELECT e.TEMPLATE_TYPE,e.TEMPLATE_NAME,e.TEMPLATE_ID,r.BANK_ID,r.BANK_NAME FROM TEMPLATE e JOIN BANK r ON r.BANK_ID=e.BANK_ID";
	 if ($result = $db->query($sql)) 
		{
			$row = array();
				$i = 1;
				$table_data = '';
				while($data = $result->fetch_assoc())
				{
					$action_data = '<a href="template.html?id='.$data['TEMPLATE_ID'].'"><i class="fa fa-pencil-square-o"></i><i class="fa fa-trash-o"></i></a>';
					$table_data .= '<tr><td class="a-center">'.$i.'</td><td>'.$data['BANK_NAME'].'</td><td>'.$data['TEMPLATE_TYPE'].'</td><td>'.$data['TEMPLATE_NAME'].'</td><td class="last">'.$action_data.'</td></tr>';
					$i = $i + 1;
				}
				echo '{"TEMPLATE": ' . json_encode($table_data) . '}';
			} 
			else 
			{
				echo $db->error;
			}
	} 
	catch(Exception $e)
	{
		//echo '{"error":{"message":'. $e->getMessage() .'}}'; 
		echo '{"message":"Please Try Again Later"}'; 
	}
	
} 


function updateTmpForm() 
{
	$id=addslashes($_POST['id']);
	$tmp_bank_id=addslashes($_POST['bankname']);
	$tmp_name=addslashes($_POST['tmp_name']);
	$tmp_content=addslashes($_POST['tmp_content']);
	$tmp_type=addslashes($_POST['tmp_type']);
	//echo $tmp_bank_id;
$db = getConnection();
$sql = "UPDATE TEMPLATE SET BANK_ID='$tmp_bank_id',TEMPLATE_TYPE='$tmp_type',TEMPLATE_NAME='$tmp_name', TEMPLATE_CONTENT='$tmp_content' WHERE TEMPLATE_ID='$id'";


 if ($result = $db->query($sql)) 
	{
		echo "Updated";	
		//$db = null; 
	} 
   
}

function getTemplateId() 
{
	try 
{
	$id=addslashes($_REQUEST['id']);
	$db = getConnection();
	$sql="SELECT BANK_ID FROM TEMPLATE WHERE TEMPLATE_ID='$id'";
	$bid=$db->query($sql);
	$res1=mysqli_fetch_array($bid);
	$b_id=$res1['BANK_ID'];
	$sqls = "SELECT BANK_NAME FROM BANK WHERE BANK_ID='$b_id'";
	 if ($result = $db->query($sqls)) 
		{
			while($res2=mysqli_fetch_array($result))
				{
					$emparray = $res2['BANK_NAME'];
					//print_r($emparray);
					
					$json= json_encode($emparray);
	
				}    

				echo '{"BANK_NAME": ' . $json . '}';
			} 
			else 
			{
				echo $db->error;
			}
	} 
	catch(Exception $e)
	{
		//echo '{"error":{"message":'. $e->getMessage() .'}}'; 
		echo '{"message":"Please Try Again Later"}'; 
	}
   
}

function GetTemplatenameList() 
{
	
	try {
		$id=addslashes($_REQUEST['id']);
		$db = getConnection();
			$sql = "SELECT TEMPLATE_NAME FROM TEMPLATE WHERE TEMPLATE_ID='$id'";
			if ($result = $db->query($sql)) {
				$table_data = '';
				while($data = $result->fetch_assoc()) {
						$table_data = $data; 
				}
				echo '{"TEMPLATE_NAME": ' . json_encode($table_data) . '}';
			} else {
			echo '{"message":"Template Data Not Available"}'; 
		}
	} catch(Exception $e) {
		//echo '{"error":{"message":'. $e->getMessage() .'}}'; 
		echo '{"message":"Please Try Again Later"}'; 
	}
}


function GetTemplateType() 
{
	
	$id=addslashes($_REQUEST['id']);
	$db = getConnection();
	$sql = "SELECT TEMPLATE_TYPE FROM TEMPLATE WHERE TEMPLATE_ID='$id'";
	$result = $db->query($sql);	
	$data = mysqli_fetch_assoc($result);
	echo $data['TEMPLATE_TYPE'];
}

function GetTemplatecontentList() 
{
	
	$id=addslashes($_REQUEST['id']);
	$db = getConnection();
	$sql = "SELECT TEMPLATE_CONTENT FROM TEMPLATE WHERE TEMPLATE_ID='$id'";
	$result = $db->query($sql);	
	$data = mysqli_fetch_assoc($result);
	echo htmlspecialchars_decode($data['TEMPLATE_CONTENT']);

}

function GetTemplatetypeList() 
{
	
	$db = getConnection();
			$sql="SELECT DISTINCT TEMPLATE_TYPE FROM TEMPLATE";
			if ($result = $db->query($sql))
				{
				$row = array();
				
				$table_data = '';
				
					while($data = $result->fetch_assoc()) 
					{
						$table_data[] = $data; 
					}
					echo '{"TEMPLATE_TYPE_LIST": ' . json_encode($table_data) . '}';
				}
				
}



function customerResponse() {
			$bankId = '';
			$branchId = '';
			$productId = '';
			$employeeId = '';
			$customerId = '';
			$fDate = '';
			$tDate = '';
			if(isset($_REQUEST['bankId'])) {
				$bankId = $_REQUEST['bankId'];
			}
			if(isset($_REQUEST['branchId'])) {
				$branchId = $_REQUEST['branchId'];
			}
			if(isset($_REQUEST['productId'])) {
				$productId = $_REQUEST['productId'];
			}
			if(isset($_REQUEST['employeeId'])) {
				$employeeId = $_REQUEST['employeeId'];
			}
			
			if(isset($_REQUEST['fromDate']) && isset($_REQUEST['toDate'])) {
				if($_REQUEST['fromDate'] != '' && $_REQUEST['toDate'] != '') {
					$fDate = explode("/",$_REQUEST['fromDate']);
					$fDate = $fDate[2].'-'.$fDate[0].'-'.$fDate[1].' 00:00:00';
					$tDate = explode("/",$_REQUEST['toDate']);
					$tDate = $tDate[2].'-'.$tDate[0].'-'.$tDate[1].' 23:59:59';
				}
			}
			if(isset($_REQUEST['listing'])) {
				$listing = $_REQUEST['listing'];
				if($listing == 'product') {
					$db = getConnection();
					$sql = "SELECT a.* FROM PRODUCT a ORDER BY a.PRODUCT_ID ASC";
					if ($result = $db->query($sql)) {
					$table_data = array();
						while($data = $result->fetch_assoc()) {
							$table_data[] = array(str_replace(" ","_",$data['PRODUCT_NAME'])=>sendCountJson($bankId, $branchId, $data['PRODUCT_ID'], $employeeId, $customerId, $fDate, $tDate));
						}
					echo json_encode($table_data);
					}
					$result->close();
				} else if($listing == 'date' && ($fDate == '' || $tDate == '')) {
					$todayDate = date('m/d/Y');
					$table_data = array();
						for($i=10;$i>=0;$i--) {
							$userFmt = date('m/d/Y',strtotime($todayDate . "-".$i." days"));
							$plusNDate = date('Y-m-d',strtotime($todayDate . "-".$i." days"));
							$plusNDateStart = $plusNDate.' 00:00:00';
							$plusNDateEnd = $plusNDate.' 23:59:59';
							$table_data[] = array($userFmt=>sendCountJson($bankId, $branchId, $productId, $employeeId, $customerId, $plusNDateStart, $plusNDateEnd));
						}
					echo json_encode($table_data);
				}  else if($listing == 'date' && $fDate != '' && $tDate != '') {
						$table_data = array();
						$table_data[] = array($_REQUEST['fromDate'].'**'.$_REQUEST['toDate']=>sendCountJson($bankId, $branchId, $productId, $employeeId, $customerId, $fDate, $tDate));
						echo json_encode($table_data);
				} else if($listing == 'branch') {
					$db = getConnection();
					$sql = "SELECT a.* FROM BRANCHES a WHERE a.BANK_ID=$bankId ORDER BY a.BRANCH_ID ASC";
					if ($result = $db->query($sql)) {
					$table_data = array();
						while($data = $result->fetch_assoc()) {
							$table_data[] = array(str_replace(" ","_",$data['BRANCH_NAME'])=>sendCountJson($bankId, $data['BRANCH_ID'], $productId, $employeeId, $customerId, $fDate, $tDate));
						}
					echo json_encode($table_data);
					}
					$result->close();
				}  else if($listing == 'officer') {
					$db = getConnection();
					$sql = "SELECT a.* FROM EMPLOYEE a WHERE a.BANK_ID=$bankId AND a.BRANCH_ID=$branchId AND a.BRANCH_ID != '' AND a.ROLE_ID=4 ORDER BY a.FIRST_NAME ASC";
					if ($result = $db->query($sql)) {
					$table_data = array();
						while($data = $result->fetch_assoc()) {
							$table_data[] = array("".$data['LAST_NAME'].", ".$data['FIRST_NAME'].""=>sendCountJson($bankId, $branchId, $productId, $data['EMPLOYEE_ID'], $customerId, $fDate, $tDate));
						}
					echo json_encode($table_data);
					} else {
						//echo $db->error;
					}
				}  else if($listing == 'customer') {
					$db = getConnection();
					$newQuery = '';
					if($employeeId != '') {
						$newQuery = 'AND a.EMPLOYEE_ID ='.$employeeId;
					}
					$sql = "SELECT DISTINCT(a.CUSTOMER_EMAIL),a.CUSTOMER_FNAME,a.CUSTOMER_ID FROM CUSTOMER_RESPONSE a WHERE a.BANK_ID=$bankId $newQuery ORDER BY a.CUSTOMER_FNAME ASC";
					//echo $sql;
					if ($result = $db->query($sql)) {
					$table_data = array();
						while($data = $result->fetch_assoc()) {
							$table_data[] = array(str_replace(" ","_",$data['CUSTOMER_FNAME'])=>sendCountJson($bankId, $branchId, $productId, $employeeId, $data['CUSTOMER_EMAIL'], $fDate, $tDate));
						}
					echo json_encode($table_data);
					} else {
						//echo $db->error;
					}
				} else {
					echo json_encode(sendCountJson($bankId, $branchId, $productId, $employeeId, $customerId, $fDate, $tDate));
				}
			}
			
}

function sendCountJson($bankId, $branchId, $productId, $employeeId, $customerId, $fDate, $tDate) {
//echo '-<br/>'.$branchId.'-<br/>';
//echo $bankId.' - '.$branchId.' - '.$productId.' - '.$employeeId.' - '.$customerId.' - '.$fDate.' - '.$tDate;
	 try {
			$db = getConnection();
			$topDateQuery = '';
			$leadDateQuery =  "";
			$betweenDateQuery = '';
			$betweenTypeQuery = '';
			$topTypeQuery = "a.BANK_ID=".$bankId;
			$betweenTypeQuery = " AND z.BANK_ID=".$bankId;
			$betweenleadQuery = " AND zn.BANK_ID=".$bankId;
			if($bankId == '') {
				die;  
			}
			if($branchId != '') {
				$topTypeQuery .=  ' AND a.BRANCH_ID='.$branchId;
				$betweenTypeQuery .=  ' AND z.BRANCH_ID='.$branchId;
				$betweenleadQuery .=  ' AND zn.BRANCH_ID='.$branchId;
			}
			if($productId != '') {
				$topTypeQuery .=  " AND a.PRODUCT_ID=".$productId;
				$betweenTypeQuery .=  " AND z.PRODUCT_ID=".$productId;
				$betweenleadQuery .=  " AND zn.PRODUCT_ID=".$productId;
			}
			if($employeeId != '') {
				$topTypeQuery .=  " AND a.EMPLOYEE_ID=".$employeeId;
				$betweenTypeQuery .=  " AND z.EMPLOYEE_ID=".$employeeId;
				$betweenleadQuery .=  " AND zn.EMPLOYEE_ID=".$employeeId;
			}
			
			if($customerId != '') {
				$topTypeQuery .=  " AND a.CUSTOMER_EMAIL='".$customerId."'";
				$betweenTypeQuery .=  " AND z.CUSTOMER_EMAIL='".$customerId."'";
				$betweenleadQuery .=  " AND zn.CUSTOMER_ID='".$customerId."'";
			}
			if($fDate != '') {
				$topDateQuery =  " AND a.CREATED_ON BETWEEN '".$fDate." 00:00:00'  AND '".$tDate." 23:59:59'";
				$betweenDateQuery =  " AND z.CREATED_ON BETWEEN '".$fDate." 00:00:00'  AND '".$tDate." 		23:59:59'";
				$leadDateQuery =  " AND zn.DATE_ADDED BETWEEN '".$fDate." 00:00:00'  AND '".$tDate." 		23:59:59'";
			}
			
			$sql = "SELECT  
						(SELECT COUNT(z.BANK_ID) AS COUNT_LEADS FROM CUSTOMER_RESPONSE z WHERE z.IS_LEAD='true' $betweenTypeQuery $betweenDateQuery) AS COUNT_LEADS,
						(SELECT SUM(zn.LEAD_TEXT) FROM LEADS zn WHERE zn.BANK_ID!='' $betweenleadQuery $leadDateQuery) AS LEADS_AMOUNT,
						(SELECT COUNT(z.BANK_ID) FROM CUSTOMER_RESPONSE z WHERE z.CAMPAIGN_TYPE='email' $betweenTypeQuery $betweenDateQuery) AS SEND_COUNT_EMAIL ,
						(SELECT COUNT(z.BANK_ID) FROM CUSTOMER_RESPONSE z WHERE z.CAMPAIGN_TYPE='sms' $betweenTypeQuery $betweenDateQuery) AS SEND_COUNT_SMS ,
						(SELECT COUNT(z.BANK_ID) FROM CUSTOMER_RESPONSE z WHERE z.CAMPAIGN_TYPE='voice' $betweenTypeQuery $betweenDateQuery) AS SEND_COUNT_VOICE,
						(SELECT COUNT(zn.BANK_ID) FROM LEADS zn WHERE zn.BANK_ID!='' $betweenleadQuery $leadDateQuery) AS LOAN_COUNT
						FROM CUSTOMER_RESPONSE a LIMIT 0,1";
	

			if ($result = $db->query($sql)) {
				$row = array();
				$i = 1;
				$table_data = array(0=>array("CAMPAIGN_TYPE" => "EMAIL",
										"CUSTOMER_RESPONSE" => "EMAIL",
										"SEND_COUNT" => 0.01),
										1=>array("CAMPAIGN_TYPE" => "SMS",
										"CUSTOMER_RESPONSE" => "SMS",
										"SEND_COUNT" => 0.01),
										2=>array("CAMPAIGN_TYPE" => "VOICE",
										"CUSTOMER_RESPONSE" => "VOICE",
										"SEND_COUNT" => 0.01),
										3=>array("CAMPAIGN_TYPE" => "LEADS",
										"CUSTOMER_RESPONSE" => "LEADS",
										"SEND_COUNT" => 0.01),
										4=>array("CAMPAIGN_TYPE" => "LOAN",
										"CUSTOMER_RESPONSE" => "LOAN",
										"SEND_COUNT" => 0.01),
										5=>array("CAMPAIGN_TYPE" => "DEPOSITS",
										"CUSTOMER_RESPONSE" => "DEPOSITS",
										"SEND_COUNT" => 0.01),
										6=>array("CAMPAIGN_TYPE" => "LOANAMT",
										"CUSTOMER_RESPONSE" => "LOANAMT",
										"SEND_COUNT" => 0)); 
					while($data = $result->fetch_assoc()) {
					if($data['SEND_COUNT_EMAIL'] == 0) $data['SEND_COUNT_EMAIL'] = 0.01;
					if($data['SEND_COUNT_SMS'] == 0) $data['SEND_COUNT_SMS'] = 0.01;
					if($data['SEND_COUNT_VOICE'] == 0) $data['SEND_COUNT_VOICE'] = 0.01; 
					if($data['COUNT_LEADS'] == 0) $data['COUNT_LEADS'] = 0.01; 
					if($data['LOAN_COUNT'] == 0) $data['LOAN_COUNT'] = 0.01; 
						$table_data = array(0=>array("CAMPAIGN_TYPE" => "EMAIL",
										"CUSTOMER_RESPONSE" => "EMAIL",
										"SEND_COUNT" => $data['SEND_COUNT_EMAIL']),
										1=>array("CAMPAIGN_TYPE" => "SMS",
										"CUSTOMER_RESPONSE" => "SMS",
										"SEND_COUNT" => $data['SEND_COUNT_SMS']),
										2=>array("CAMPAIGN_TYPE" => "VOICE",
										"CUSTOMER_RESPONSE" => "VOICE",
										"SEND_COUNT" => $data['SEND_COUNT_VOICE']),
										3=>array("CAMPAIGN_TYPE" => "LEADS",
										"CUSTOMER_RESPONSE" => "LEADS",
										"SEND_COUNT" =>  $data['COUNT_LEADS']),
										4=>array("CAMPAIGN_TYPE" => "LOAN",
										"CUSTOMER_RESPONSE" => "LOAN",
										"SEND_COUNT" => $data['LOAN_COUNT']),
										5=>array("CAMPAIGN_TYPE" => "DEPOSITS",
										"CUSTOMER_RESPONSE" => "DEPOSITS",
										"SEND_COUNT" => 0.01),
										6=>array("CAMPAIGN_TYPE" => "LOANAMT",
										"CUSTOMER_RESPONSE" => "LOANAMT",
										"SEND_COUNT" => $data['LEADS_AMOUNT'])); 
					}
						return $table_data;
			
			} else {
				echo $db->error;
			}

	} catch(Exception $e) {
		echo '{"error":{"message":'. $e->getMessage() .'}}'; 
		echo '{"message":"Please Try Again Later"}'; 
	} 
}

function responseRate() {
	try {
			$db = getConnection();			
			$id = $_REQUEST['id'];
			$searchOn = $_REQUEST['searchOn'];
			$table_data = array();
			for($i=5;$i>=0;$i--) {
				$searchDate = date("Y-m-d 00:00:00", strtotime("-".$i." months"));
				//$searchMonth = date("M", strtotime("-".$i." months"));
				$searchMonth = date("M Y", strtotime("-".$i." months"));
				$sql = "SELECT DISTINCT(a.CUSTOMER_RESPONSE),(SELECT COUNT(z.CUSTOMER_RESPONSE) FROM CUSTOMER_RESPONSE z WHERE z.CUSTOMER_RESPONSE=a.CUSTOMER_RESPONSE AND MONTH(z.CREATED_ON)=MONTH('$searchDate') AND YEAR(z.CREATED_ON)=YEAR('$searchDate') AND z.EMPLOYEE_ID='$id' AND z.CAMPAIGN_TYPE='$searchOn') AS RESPONSE_COUNT FROM CUSTOMER_RESPONSE a WHERE a.CAMPAIGN_TYPE='$searchOn'";
				
				if ($result = $db->query($sql)) {
					while($data = $result->fetch_assoc()) {
						$data['RESPONSE_MONTH'] = $searchMonth;
						$data['RESPONSE_COUNT'] = (int)$data['RESPONSE_COUNT'];
						$table_data[] = $data;
					}
					
				} else {
					//echo $db->error;
				}
			}
			echo json_encode($table_data);
	} catch(Exception $e) {
		echo '{"error":{"message":'. $e->getMessage() .'}}'; 
		echo '{"message":"Please Try Again Later"}'; 
	}
}

function getConnection() {
	$dbhost="192.168.0.222";
	$dbuser="touchpoint";
	$dbpass="touchpoint";
	$dbname="touchpoint";
	$mysqli_conn = new mysqli($dbhost, $dbuser, $dbpass, $dbname);
	if ($mysqli_conn->connect_errno) {
		die("Failed to connect to MySQL: (" . $mysqli_conn->connect_errno . ") " . $mysqli_conn->connect_error);
	}
	return $mysqli_conn;
}

?>