<?php
ini_set('display_errors', 1);
ini_set('display_startup_errors', 1);
error_reporting(E_ALL & ~E_DEPRECATED);
session_start();
$rootdir = "https://ategrity-dev.esinsurancecloud.com/clrm";
$hostaddr = $rootdir;
require 'Slim/Slim.php';
$app = new Slim();

$app->get('/', 'checkConnection');
$app->post('/authenticate/', 'authenticate');
$app->post('/sso_authenticate/', 'sso_authenticate');

$app->post('/forgetPassword/', 'forgetPassword');

$app->get('/campaignListDrop/', 'getCampaignsdrop'); //only for drop down in bank officer
$app->get('/campaignList/', 'getCampaigns');
$app->get('/inBuildSplit/', 'inBuildSplit');
$app->put('/campaign/', 'addCampaign');
$app->post('/campaign/', 'updateCampaign');
$app->post('/pauseCampaign/', 'pauseCampaign');
$app->delete('/campaign/', 'deleteCampaign');

$app->get('/touchpointList/', 'getTouchpoints');
$app->get('/touchpointListWithComm/', 'touchpointListWithComm');
$app->post('/touchpoint/', 'updateTouchpoint');
$app->put('/touchpoint/', 'addTouchpoint');
$app->delete('/touchpoint/', 'deleteTouchpoint');

$app->get('/communicationList/', 'communicationList');
$app->post('/communications/', 'addCommunication');
$app->post('/communication/', 'updateCommunication');
$app->get('/communication/', 'getCommunication');
$app->delete('/communication/', 'deleteCommunication');

$app->get('/templateList/', 'templateList');
$app->get('/gettemplatetypeList/', 'GetTemplatetypeList');
$app->get('/gettemplatetype/', 'GetTemplateType');
$app->get('/gettemplatenameList/', 'GetTemplatenameList');
$app->get('/gettemplatecontentList/', 'GetTemplatecontentList');
$app->get('/templateData/', 'templateData');

$app->put('/bank/', 'addBank');
$app->get('/bank/', 'getBank');
$app->post('/bank/', 'updateBank');
$app->get('/bankList/', 'bankList');

$app->put('/branch/', 'addBranch');
$app->get('/branch/', 'getBranch');
$app->post('/branch/', 'updateBranch');
$app->get('/branchList/', 'branchList');

$app->put('/role/', 'addRole');
$app->get('/role/', 'getRole');
$app->post('/role/', 'updateRole');
$app->get('/roleList/', 'roleList');

$app->get('/employeeList/', 'employeeList');
$app->put('/employee/', 'addEmployee');
$app->get('/employee/', 'getEmployee');
$app->post('/employee/', 'updateEmployee');

$app->get('/customerList/', 'customerList');
$app->put('/customer/', 'addCustomer');
$app->get('/customer/', 'getCustomer');
$app->post('/customer/', 'updateCustomer');

$app->get('/productList/', 'productList');
$app->put('/product/', 'addProduct');
$app->get('/product/', 'getProduct');
$app->post('/product/', 'updateProduct');

$app->get('/countryList/', 'countryList');
$app->get('/stateList/', 'stateList');
$app->get('/bankBranchList/', 'bankBranchList');
$app->get('/branchRoleList/', 'branchRoleList');
$app->get('/branchEmployeeList/', 'branchEmployeeList');

$app->get('/edit_tmp_form', 'editTmpForm');
$app->post('/template/', 'addTemplate');
$app->get('/template/', 'getTemplateId');
$app->post('/updatetemplate/', 'updateTmpForm');

$app->post('/target/', 'addTarget');
$app->get('/targetList/', 'targetList');
$app->post('/updatetarget/', 'updatetarget');

$app->get('/targetBank/', 'getTargetBank');
$app->get('/targetBankBranches/', 'getTargetBankBranches');
$app->get('/targetBankBranchesEdit/', 'getTargetBankBranchesEdit');
$app->put('/setTarget/', 'setTargetForBranch');
$app->post('/setTarget/', 'resetTargetForBranch');
$app->get('/targetDetails/', 'getTargetDetails');

$app->get('/manageBanklist/', 'manageBanklist');
$app->put('/admin/', 'addAdmin');
$app->post('/updateAdmin/', 'updateAdmin');
$app->get('/getadminemployee/', 'Adminbanklist');

$app->get('/employeeActivityList/', 'employeeActivityList');
$app->get('/customerResponse/', 'customerResponse');
$app->get('/customerResponseBm/', 'customerResponseBm');
$app->get('/responseRate/', 'responseRate');
$app->get('/employeeVolume/','employeeVolume');
$app->get('/myCustomer/','myCustomer');
$app->get('/customerTouchpoints/','customerTouchpoints');
$app->get('/getCustomerByEmail/','getCustomerByEmail');
$app->get('/customerTouchpointCommunications/','customerTouchpointCommunications');
$app->get('/customersResponseList/','customersResponseList');
$app->put('/lead/', 'addLead');
$app->get('/currentBankName/','currentBankName');

$app->get('/endpoint/', 'getEndpoint');
$app->put('/endpoint/', 'setEndpoint');
$app->post('/endpoint/', 'resetEndpoint');

$app->put('/setHeadOfRetail/','setHeadOfRetail');
$app->put('/setBranchManager/','setBranchManager');

$app->get('/unassignedBranches/','unassignedBranches');
$app->get('/definedBranchList/','getDefinedBranchList');

$app->get('/getendpoint/','getendurl');
$app->get('/endpointbank/','endpointbank');
$app->get('/endpointbranch/','endpointbranch');

$app->get('/customermapping/','customermapping');

$app->get('/addArrayDetails/', 'addArrayDetails');

$app->get('/addEndPointWithPath/','addEndPointWithPath');

$app->get('/sampleapi/','sampleapi');
$app->run();




function addEndPointWithPath(){
	
  //$bank_id		= addslashes($_REQUEST['id']);
  $endpoint			= addslashes($_REQUEST['endpointURL']);
  $wsdl				= addslashes($_REQUEST['wsdlURL']);
  $serviceType		= addslashes($_REQUEST['serviceType']);
  $customerId		= addslashes($_REQUEST['custId']);
  $customerEmail	= addslashes($_REQUEST['custEmail']);
  $bankId	= addslashes($_REQUEST['bankId']);
  $branchId	= addslashes($_REQUEST['branchId']);
  $fname	= addslashes($_REQUEST['fname']);
  $lname	= addslashes($_REQUEST['lname']);
  $doj	= addslashes($_REQUEST['doj']);
  $employeeId	= addslashes($_REQUEST['employeeId']);
  $mobNo	= addslashes($_REQUEST['mobNo']);
  $productId	= addslashes($_REQUEST['productId']);
  $productName = addslashes($_REQUEST['productName']);
  $customerIdRoot		= addslashes($_REQUEST['custIdRoot']);
  $customerEmailRoot	= addslashes($_REQUEST['custEmailRoot']);
  $bankIdRoot	= addslashes($_REQUEST['bankIdRoot']);
  $branchIdRoot	= addslashes($_REQUEST['branchIdRoot']);
  $fnameRoot	= addslashes($_REQUEST['fnameRoot']);
  $lnameRoot	= addslashes($_REQUEST['lnameRoot']);
  $dojRoot	= addslashes($_REQUEST['dojRoot']);
  $employeeIdRoot	= addslashes($_REQUEST['employeeIdRoot']);
  $mobNoRoot	= addslashes($_REQUEST['mobNoRoot']);
  $productIdRoot	= addslashes($_REQUEST['productIdRoot']);
  $productNameRoot	= addslashes($_REQUEST['productNameRoot']);
  $soapMethod	= addslashes($_REQUEST['soapMethod']);
  $soapParams	= addslashes($_REQUEST['soapParams']);
  $soapParamValues	= addslashes($_REQUEST['soapParamValues']);
  $soapParamValues	= addslashes($_REQUEST['soapParamValues']);
  $nodeArray = addslashes($_REQUEST['configNode']);
  $nodeArray = stripslashes($nodeArray);
  $nodeArray = stripslashes($nodeArray);
  $nodeArray = str_replace('"{','{',$nodeArray);
	$nodeArray = str_replace('}"','}',$nodeArray);
	$nodeArray = str_replace('[','',$nodeArray);
	$nodeArray = str_replace(']','',$nodeArray);
	$nodeArray = str_replace('},{','***',$nodeArray);
	$nodeArray = str_replace('}','',$nodeArray);
	$nodeArray = str_replace('{','',$nodeArray);
	$nodeArray = str_replace('"','',$nodeArray);
  //var_dump($nodeArray);
  //echo $nodeArray;
  //die;
  //$bank_id 			= 144;
  $bankCode =  addslashes($_REQUEST['bankCode']);
  try {
    $db = getConnection();
    $sql = "INSERT INTO ENDPOINT (BANK_CODE, END_POINT, CUSTOMER_ID_PATH, CUSTOMER_EMAIL_PATH, BANK_ID_PATH, BRANCH_ID_PATH, CUSTOMER_MOB_NO_PATH, CUSTOMER_FNAME_PATH, CUSTOMER_LNAME_PATH, CUSTOMER_DOJ, EMPLOYEE_ID_PATH, CUSTOMER_PRODUCT_ID_PATH,CUSTOMER_PRODUCT_NAME_PATH, WSDL_URL, SERVICE_TYPE, SOAP_METHOD, SOAP_PARAMS, SOAP_PARAM_VALUES, MAP_ARRAY, CUSTOMER_ID_PATH_ROOT, CUSTOMER_EMAIL_PATH_ROOT, BANK_ID_PATH_ROOT, BRANCH_ID_PATH_ROOT, CUSTOMER_MOB_NO_PATH_ROOT, CUSTOMER_FNAME_PATH_ROOT, CUSTOMER_LNAME_PATH_ROOT, CUSTOMER_DOJ_ROOT, EMPLOYEE_ID_PATH_ROOT, CUSTOMER_PRODUCT_ID_PATH_ROOT,CUSTOMER_PRODUCT_NAME_PATH_ROOT) VALUES ('$bankCode','$endpoint', '$customerId', '$customerEmail', '$bankId', '$branchId', '$mobNo', '$fname', '$lname', '$doj', '$employeeId', '$productId','$productName', '$wsdl', '$serviceType', '$soapMethod', '$soapParams', '$soapParamValues', '$nodeArray', '$customerIdRoot', '$customerEmailRoot', '$bankIdRoot', '$branchIdRoot', '$mobNoRoot', '$fnameRoot', '$lnameRoot', '$dojRoot', '$employeeIdRoot', '$productIdRoot','$productNameRoot')";
	echo  $sql;
    if ($db->query($sql)) {
      echo '{"message":"success"}';	$db = null; die;
    } else {
      echo '{"message":"'.$db->error.'"}';  $db = null;	die;
    }
    echo '{"message":"Please Try Again Later.."}';  $db = null;	die;
  } catch(Exception $e) {
    echo '{"message":"Please Try Again Later"}'; 
  }
}



function addArrayDetails(){
  
  $pathCus = addslashes($_REQUEST['pathCus']);
  $pathEmp = addslashes($_REQUEST['pathEmp']);
  $bankid = $_REQUEST['bankId'];
  
  try{
    $db = getConnection();
    $qAddArrayDetails = "update ENDPOINT set CUSTOMER_ARRAY_NAME = '$pathCus', BRANCH_ARRAY_NAME = '$pathEmp' where BANK_CODE = $bankid";
    // echo $qAddArrayDetails; die;
    if($reslut = $db->query($qAddArrayDetails))
      echo '{"message":"success"}';
    else
      echo '{"message":"'.$db->error.'"}';
  }catch(Exception $sqlException){
    echo $sqlException->getMessage();
  }
}

function getDefinedBranchList(){
  $bank_id = $_REQUEST['id'];
  $branch_id = $_REQUEST['branchId'];
  $qGetDefinedBranch = "select BRANCH_CODE where BANK_CODE=$bank_id AND HEAD_OF_RETAIL=0";
  try{
    $db->getConnection();
    if ($result = $db->query($qGetDefinedBranch)) {
      $row = $result->fetch_assoc();
      echo '{"BANK_NAME":'.json_encode($row['BANK_CODE']).'}';
    } else {
      echo '{"message":"No result"}';
    }

  }catch(Exception $sqlException){

  }
}

function unassignedBranches(){
  $bank_id = $_REQUEST['bankId'];
  $current_role = $_REQUEST['currentRole'];
  // echo $bank_id;
  if($current_role == "Head Of Retail"){
    //$sql = "select BRANCH_CODE, BRANCH_NAME, BANK_CODE from BRANCHES where BRANCH_CODE not in (select BRANCH_CODE from BRANCH_INTEGRATION where head_of_retail = 0 and BANK_CODE='$bank_id') and BANK_CODE='$bank_id'";
	
	$sql = "SELECT b.BRANCH_CODE,b.BRANCH_NAME,b.BANK_CODE FROM BRANCHES b INNER JOIN BRANCH_INTEGRATION a ON a.BRANCH_CODE = b.BRANCH_CODE WHERE a.BANK_CODE='$bank_id' AND a.HEAD_OF_RETAIL=0";
  }
  else if($current_role == "Bank Manager"){
    //$sql = "select BRANCH_CODE, BRANCH_NAME, BANK_CODE from BRANCHES where BRANCH_CODE not in (select BRANCH_CODE from BRANCH_INTEGRATION where bank_manager = 0 and BANK_CODE='$bank_id') and BANK_CODE='$bank_id'";
	
	$sql = "SELECT b.BRANCH_CODE,b.BRANCH_NAME,b.BANK_CODE FROM BRANCHES b INNER JOIN BRANCH_INTEGRATION a ON a.BRANCH_CODE = b.BRANCH_CODE WHERE a.BANK_CODE='$bank_id' AND a.BANK_MANAGER=0";
  }
  try {
    $db = getConnection();
    if ($result = $db->query($sql)) {
      $table_data = '';
      while($data = $result->fetch_assoc()) {
        $table_data[] = $data; 
      }
      echo '{"UNASSIGNED_BRANCHES": ' . json_encode($table_data) . '}';
    }
    $result->close();
  } catch(Exception $e) {
    echo '{"message":"Please Try Again Later"}'; 
  }
}

function setHeadOfRetail(){
  $branch_id		= addslashes($_REQUEST['selectedBranch']);
  
  $hor_id	= addslashes($_REQUEST['horId']);
  $bank_id = addslashes($_REQUEST['id']);
  echo '{"message":"'.$branch_id.'"}';

  try {
    $db = getConnection();
    $isExistQuery = "select * from BRANCH_INTEGRATION where BANK_CODE='$bank_id' AND BRANCH_CODE='$branch_id'";
    $res = $db->query($isExistQuery);
    $num_of_rows = $res->num_rows;
    echo "Branch id".$branch_id."Bank id".$bank_id."HOR".$hor_id;
    if($num_of_rows <= 0){
      echo "No field exist";
      /* $sql = "insert into BRANCH_INTEGRATION (HEAD_OF_RETAIL, BANK_CODE, BRANCH_CODE) VALUES($hor_id, $bank_id, $branch_id)"; */
	  
	  $sql = "insert into BRANCH_INTEGRATION (BANK_ID,BRANCH_ID,BANK_CODE,BRANCH_CODE,HEAD_OF_RETAIL)VALUES('$bank_id','$branch_id','$bank_id','$branch_id','$hor_id')";
    }
    else{
      echo "A field exist";
      $sql = "update BRANCH_INTEGRATION set HEAD_OF_RETAIL='$hor_id' where BRANCH_CODE='$branch_id' and BANK_CODE='$bank_id'";
	  
      $sqlhor = "SELECT * FROM EMPLOYEE WHERE EMPLOYEE_ID='$hor_id'";
      if ($resulthor = $db->query($sqlhor)) {
        $data = $resulthor->fetch_object();
        $first_name = $data->FIRST_NAME;
        $last_name = $data->LAST_NAME;
        $email = $data->EMAIL;
        $password = $data->PASSWORD;   
       
      }
	  horemployEmailSend($first_name,$last_name,$email,$password);
	  
    }

    if ($db->query($sql)) {
      echo '{"message":"success"}';	$db = null; die;
    } else {
      echo '{"message":"'.$db->error.'"}';  $db = null;	die;
    }
    echo '{"message":"'.$db->error.'"}';  $db = null;	die;
    //    echo '{"message":"'.$db->error.'"}';  $db = null;	die;
  } catch(Exception $e) {
    echo '{"message":"'.$e->getMessage().'"}'; 
  }
}

function setBranchManager(){
  $branch_id		= addslashes($_REQUEST['selectedBranch']);
  $bm_id	= addslashes($_REQUEST['bmId']);
  $bank_id = addslashes($_REQUEST['id']);
  try {
    $db = getConnection();
    $isExistQuery = "select * from BRANCH_INTEGRATION where BANK_CODE='$bank_id' AND BRANCH_CODE='$branch_id'";
    $res = $db->query($isExistQuery);
    $num_of_rows = $res->num_rows;
    if($num_of_rows <= 0){
      echo "No field exist";
      /* $sql = "insert into BRANCH_INTEGRATION (BANK_MANAGER, BANK_CODE, BRANCH_CODE) VALUES($bm_id, $bank_id, $branch_id)"; */
	  $sql = "insert into BRANCH_INTEGRATION (BANK_ID,BRANCH_ID,BANK_MANAGER, BANK_CODE, BRANCH_CODE) VALUES('$bank_id', '$branch_id','$bm_id', '$bank_id', '$branch_id')";

    }
    else{
      echo "A field exist";
      $sql = "update BRANCH_INTEGRATION set BANK_MANAGER=$bm_id where BANK_CODE='$bank_id' AND BRANCH_CODE='$branch_id'";
    }

    if ($db->query($sql)) {
      echo '{"message":"success"}';	$db = null; die;
    } else {
      echo '{"message":"'.$db->error.'"}';  $db = null;	die;
    }
    echo '{"message":"'.$db->error.'"}';  $db = null;	die;
  } catch(Exception $e) {
    echo '{"message":"'.$e->getMessage().'"}'; 
  }
}

/*function endpointbank()
{
	
	$bank_id		= addslashes($_REQUEST['id']);
	$endpoint	= addslashes($_REQUEST['endpoint']);
  $db = getConnection();
	$sql = "select BANK_NAME from BANK where BANK_ID ='$bank_id'";
	 if ($result = $db->query($sql)) 
	 {
      while($data = $result->fetch_assoc()) 
	  {
			$table_data = $data['BANK_NAME'];
			$endpointurl = $endpoint."/banknamebased/?bank_name=".$table_data;
			
			
			
			
    $ch = curl_init();
    curl_setopt($ch,CURLOPT_URL,$endpointurl);
    curl_setopt($ch,CURLOPT_RETURNTRANSFER,true);
 
    $output=curl_exec($ch);
	
		$id = $output;
		echo $id;
	//print_r($var);
	//echo  $output['BANK_ID'];
	
	 $sql = "update BANK set BANK_CODE = '$id'  where BANK_ID ='$bank_id' and BANK_NAME ='$table_data' ";
	 
	 $sql2 = "update BRANCHES set BANK_CODE = '$id'  where BANK_ID ='$bank_id' ";
	 //echo $sql;
	if($result = $db->query($sql)) 
	 {
		 echo "success";
	 } 
	 
	 if($result = $db->query($sql2))
	 {
		 echo "sucess";
	 }
    curl_close($ch);
    return $output;
	

	
      }
	 }
}*/

function getEndpoint() {
	
  $id = addslashes($_REQUEST['id']);
  try {
    $db = getConnection();
    $sql = 'SELECT * from ENDPOINT WHERE BANK_CODE='.$id;
    if ($result = $db->query($sql)) {
      $row = $result->fetch_assoc();
      echo '{"ENDPOINT_DETAILS":'.json_encode($row).'}';

    } else {
      echo '{"message":"No result"}';
    }
    //        $result->close();
  } catch(Exception $e) {
    echo '{"message":"Please Try Again Later"}'; 
  }
}



function getendurl()
{
	
	$id = addslashes($_REQUEST['id']);
	$admin_email = addslashes($_REQUEST['email']);
  try {
    $db = getConnection();
    $sql = "SELECT * from ENDPOINT WHERE BANK_CODE='$id'";
	
    if ($result = $db->query($sql)) 
	{
      $row = $result->fetch_assoc();
     // echo $row['END_POINT'];
	  endpointbranch($admin_email,json_decode($row['END_POINT']),$id,$row['SERVICE_TYPE'],json_decode($row['SOAP_METHOD']),json_decode($row['SOAP_PARAMS']),json_decode($row['SOAP_PARAM_VALUES']),json_decode($row['WSDL_URL']));
	//  echo '{"ENDPOINT_DETAILS":'.$row['END_POINT'].'}';
	  
    } 
	else 
	{
      echo '{"message":"No result"}';
    }
    //        $result->close();
  } catch(Exception $e) {
    echo '{"message":"Please Try Again Later"}'; 
  }

}




function endpointbranch($admin_email,$endpo,$id,$service_type,$method,$param,$value,$wsdl)
{
	//echo $method; exit;
	//print_r($wsdl); exit;
	
	$bank_id		= $id;	
	$endpoint	= $endpo[0]; 
	$db = getConnection();
				
    if($service_type == 'REST') {
		$output=curlGetService($endpoint);
	} else {
		$wsdl	= $wsdl[0]; 
		$method	= $method[0]; 
		$param	= $param[0]; 
		$value	= $value[0]; 
		$output = soapUrlService($endpoint, $wsdl, $method, $param, $value);
		
	}
	
	$result = json_decode($output);
	/*first time synchronis flag getting*/
	$sqlflag = "SELECT BRANCH_CODE from BRANCHES where BANK_CODE = '$bank_id'";	
	$resultflage = $db->query($sqlflag);      
	$first_synchronise=mysqli_num_rows($resultflage);
	$newbranch=0;
	/*first time synchronis flag getting*/
	foreach($result->BRANCH_DETAILS as $data)
	{
		
		$bankcode = $data->Bank_code;
		$branchcode = $data->Branch_code;
		
		$branch_name = $data->Branch_name;
		$city = $data->city;
		$state = $data->state;
		$country = $data->country;
		$create = $data->CreatedBy;
		$addr = $data->Branch_addr;
		
		
		
		$sql = "SELECT BRANCH_CODE from BRANCHES where BRANCH_CODE = '$branchcode'";			
		$result = $db->query($sql);		
		
		if(mysqli_num_rows($result) == 0)
		{
			$newbranch=1;
			$sql2 = "insert into BRANCHES (BRANCH_ID,BANK_ID,BRANCH_NAME,BRANCH_CODE,COUNTRY,STATE,CITY,DATE_ADDED,DATE_UPDATED,CREATE_BY,ADDRESS,BANK_CODE) 
				values('','','$branch_name','$branchcode','$country','$state','$city',NOW(),NOW(),'$create','$addr','$bankcode')";
	
			if ($result = $db->query($sql2)) 
			{
				//echo '<script type="text/javascript"> window.location.href = "manage-branch.html" </script>';
				//echo "success";
			}
			
			$sql3 = "insert into BRANCH_INTEGRATION (BANK_ID,BRANCH_ID,BANK_CODE,BRANCH_CODE,HEAD_OF_RETAIL,BANK_MANAGER) values('$bankcode','$branchcode','$bankcode','$branchcode','0','0')";
			$result = $db->query($sql3);
		}	
		
	}
	
	if($first_synchronise!=0){
		if($newbranch!=0){
		mailtoBankadmin($admin_email);
		}
	}	
    return $output;
	 
}

function mailtoBankadmin($admin_email){
	  require_once('../PHPMailer/PHPMailerAutoload.php');
	  $mailSubject="Touchpoint - Branch Synchronize Reminder";
	  $email=$admin_email;
	  $password="arunbutp@gmail.com";		  
	  $mailBody = '
		<html>
  <head>
    <title></title>
		<meta charset="iso-8859-1" />


	 <meta name="format-detection" content="telephone=no;address=no">


  </head>
  <body vlink="#0000ff" link="#0000ff" alink="#0000ff" style="padding-bottom: 0px; margin: 0px; padding-left: 0px; width: 100% !important; padding-right: 0px; padding-top: 0px; -webkit-font-smoothing: antialiased; -webkit-text-size-adjust: none">
    <img src="http://links.mkt030.com/open/log/11260781/OTk1ODg1NjYxMjAS1/3/NDY0MjI1NDQzS0/1/0"><!-- TRACKING AMPSCRIPT -->
    <div style="text-align: center; width: 100%; font-family: Arial, Helvetica, sans-serif; margin-left: 0px; font-size: 11px; margin-right: 0px">
      <table cellspacing="0" cellpadding="0" width="100%" border="0" align="center" class="wrapper">
        <tbody>
          <tr>
            <td width="100%" align="center" style="border-collapse: collapse">
              <table cellspacing="0" cellpadding="0" width="100%" border="0" align="center" class="wrapper">
                <tbody>
                  <tr>
                    <td align="center" style="border-collapse: collapse">
                      <span style="line-height: 0px; font-size: 0px"><img width="20" height="1" src="http://contentz.mkt030.com/ra/2014/517/10/11260781/usbank-010814_images_spacer.gif" style="display: block" alt=""></span>
                      <table cellspacing="0" cellpadding="0" width="667" border="0" class="wrapper" style="border-collapse: collapse">
                        <tbody style="border:1px solid #333">
                          <tr>
                            <td style="line-height: 0px; border-collapse: collapse; font-size: 0px">
                              <table cellspacing="0" cellpadding="0" width="667" border="0" class="wrapper" style="border-collapse: collapse">
                                <tbody>                                
                                  
                                  
                                </tbody>
                              </table>
                            </td>
                          </tr>
                          <!-- ////////////////////////////////////////////////////////// HIDDEN IN DESKTOP - EDIT THIS \\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\ --><!-- ////////////////////////////////////////////////////////// END OF EDITABLE AREA \\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\ -->
                          <tr>
                            <td style="border-collapse: collapse">
                              <table cellspacing="0" cellpadding="0" width="667" border="0" class="wrapper" style="border-collapse: collapse">
                                <tbody>
                                  <tr>
                                    <td width="1" bgcolor="#666666" class="noColor" style="line-height: 0px; font-size: 0px"><img width="1" height="1" src="http://contentz.mkt030.com/ra/2014/517/10/11260781/usbank-010814_images_spacer.gif" style="display: block" alt=""></td>
                                    <td height="20" bgcolor="#ffffff" class="h10" style="line-height: 0px; font-size: 0px"><img width="1" height="20" src="http://contentz.mkt030.com/ra/2014/517/10/11260781/usbank-010814_images_spacer.gif" style="display: block" alt="" class="h10"></td>
                                    <td width="1" bgcolor="#666666" class="noColor" style="line-height: 0px; font-size: 0px"><img width="1" height="1" src="http://contentz.mkt030.com/ra/2014/517/10/11260781/usbank-010814_images_spacer.gif" style="display: block" alt=""></td>
                                  </tr>
                                </tbody>
                              </table>
                            </td>
                          </tr>
                          <tr>
                            <td style="border-collapse: collapse">
                              <table cellspacing="0" cellpadding="0" width="667" border="0" class="wrapper" style="border-collapse: collapse">
                                <tbody>
                                  <tr>
                                    <td width="1" bgcolor="#666666" class="noColor" style="line-height: 0px; font-size: 0px"><img width="1" height="1" src="http://contentz.mkt030.com/ra/2014/517/10/11260781/usbank-010814_images_spacer.gif" style="display: block" alt=""></td>
                                    <td width="22" class="w10" style="line-height: 0px; font-size: 0px"><img width="22" height="1" src="http://contentz.mkt030.com/ra/2014/517/10/11260781/usbank-010814_images_spacer.gif" style="display: block" alt="" class="w10"></td>
                                    <td align="left">
                                      <table cellspacing="0" cellpadding="0" width="100%" border="0" style="border-collapse: collapse">
                                        <tbody>
                                          <tr>                                                                 </tr>
                                          <tr>
                                            <td align="left" style="font-family: Arial, Helvetica, sans-serif; color: #666766; font-size: 15px; line-height:19px; -webkit-text-size-adjust: none" colspan="4" class="leading"> Dear Bank Admin, </td>
                                          </tr>
                                          <tr>
                                            <td height="10" colspan="4" style="line-height: 0px; font-size: 0px"><img width="1" height="10" src="http://contentz.mkt030.com/ra/2014/517/10/11260781/usbank-010814_images_spacer.gif" style="display: block" alt=""></td>
                                          </tr>
                                          <tr>
                                            <td align="left" style="font-family: Arial, Helvetica, sans-serif; color: #666766; font-size: 15px; line-height: 19px; -webkit-text-size-adjust: none" colspan="4" class="leading"> </td>
                                          </tr>
										  <!--content area-->
										  <tr>
                                            <td align="left" style="font-family: Arial, Helvetica, sans-serif; color: #666766; font-size: 15px; line-height: 19px; -webkit-text-size-adjust: none" colspan="2" class="leading">
											New Branch details are detected,Kindly Assign The Branch to the Head Of Retail.
											</td> 
											<td align="left" style="font-family: Arial, Helvetica, sans-serif; color: #666766; font-size: 15px; line-height: 19px; -webkit-text-size-adjust: none" colspan="2" class="leading">
											
											</td>
                                          </tr>
										  <!--content area-->										  
										  
                                          <tr>
                                            <td height="10" colspan="4" style="line-height: 0px; font-size: 0px"><img width="1" height="10" src="http://contentz.mkt030.com/ra/2014/517/10/11260781/usbank-010814_images_spacer.gif" style="display: block" alt=""></td>
                                          </tr>
                                          
                                          <!-- ////////////////////////////////////////////////////////// HIDDEN IN DESKTOP - MOBILE BUTTON \\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\ --><!--[if !mso]><!-->                                                                 <!--<![endif]--><!-- ////////////////////////////////////////////////////////// END OF EDITABLE AREA \\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\ -->
                                          <tr>
                                            <td height="10" align="left" style="line-height: 18px; font-family: Arial, Helvetica, sans-serif; color: #666766; font-size: 14px; -webkit-text-size-adjust: none" colspan="4" class="leading"><img width="1" height="10" src="http://contentz.mkt030.com/ra/2014/517/10/11260781/usbank-010814_images_spacer.gif" style="display: block" alt=""></td>
                                          </tr>
                                        </tbody>
                                      </table>
                                    </td>
                                    <!------------- END LIST -->
                                    <td width="22" style="line-height: 0px; font-size: 0px" class="w10"><img width="22" height="1" src="http://contentz.mkt030.com/ra/2014/517/10/11260781/usbank-010814_images_spacer.gif" style="display: block" alt="" class="w10"></td>
                                    <td width="1" bgcolor="#666666" style="line-height: 0px; font-size: 0px" class="noColor"><img width="1" height="1" src="http://contentz.mkt030.com/ra/2014/517/10/11260781/usbank-010814_images_spacer.gif" style="display: block" alt=""></td>
                                  </tr>
                                </tbody>
                              </table>
                            </td>
                          </tr>
                          <tr>
                            <td style="border-collapse: collapse">
                              <table cellspacing="0" cellpadding="0" width="667" border="0" class="wrapper" style="border-collapse: collapse">
                                <tbody>
                                  <tr>
                                    <td width="1" bgcolor="#666666" class="noColor" style="line-height: 0px; font-size: 0px"><img width="1" height="1" src="http://contentz.mkt030.com/ra/2014/517/10/11260781/usbank-010814_images_spacer.gif" style="display: block" alt=""></td>
                                    <td width="22" class="w10" style="line-height: 0px; font-size: 0px"><img width="22" height="1" src="http://contentz.mkt030.com/ra/2014/517/10/11260781/usbank-010814_images_spacer.gif" style="display: block" alt="" class="w10"></td>
                                    <td align="left">
                                      <table cellspacing="0" cellpadding="0" width="100%" border="0" style="border-collapse: collapse">
                                        <tbody>
                                         
                                          <tr>
                                            <td align="left" style="font-family: Arial, Helvetica, sans-serif; color: #666766; font-size: 15px; line-height: 19px; -webkit-text-size-adjust: none" colspan="4" class="leading">Thanks, </td>
                                          </tr> 
										  <tr>
                                            <td align="left" style="font-family: Arial, Helvetica, sans-serif; color: #666766; font-size: 15px; line-height: 19px; -webkit-text-size-adjust: none" colspan="4" class="leading">Touchpoint - Team. </td>
                                          </tr>
                                          <!-- ////////////////////////////////////////////////////////// HIDDEN IN DESKTOP - MOBILE BUTTON \\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\ --><!--[if !mso]><!-->                                                                 <!--<![endif]--><!-- ////////////////////////////////////////////////////////// END OF EDITABLE AREA \\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\ -->
                                        </tbody>
                                      </table>
                                    </td>
                                    <!------------- END LIST -->
                                    <td width="22" style="line-height: 0px; font-size: 0px" class="w10"><img width="22" height="1" src="http://contentz.mkt030.com/ra/2014/517/10/11260781/usbank-010814_images_spacer.gif" style="display: block" alt="" class="w10"></td>
                                    <td width="1" bgcolor="#666666" style="line-height: 0px; font-size: 0px" class="noColor"><img width="1" height="1" src="http://contentz.mkt030.com/ra/2014/517/10/11260781/usbank-010814_images_spacer.gif" style="display: block" alt=""></td>
                                  </tr>
                                </tbody>
                              </table>
                            </td>
                          </tr>
                          <tr>
                            <td style="border-collapse: collapse">
                              <table cellspacing="0" cellpadding="0" width="667" border="0" class="wrapper" style="border-collapse: collapse">
                                <tbody>
                                  <tr>
                                    <td width="1" bgcolor="#666666" class="noColor" style="line-height: 0px; font-size: 0px"><img width="1" height="1" src="http://contentz.mkt030.com/ra/2014/517/10/11260781/usbank-010814_images_spacer.gif" style="display: block" alt=""></td>
                                    <td height="20" bgcolor="#ffffff" style="line-height: 0px; font-size: 0px"><img width="1" height="20" src="http://contentz.mkt030.com/ra/2014/517/10/11260781/usbank-010814_images_spacer.gif" style="display: block" alt=""></td>
                                    <td width="1" bgcolor="#666666" class="noColor" style="line-height: 0px; font-size: 0px"><img width="1" height="1" src="http://contentz.mkt030.com/ra/2014/517/10/11260781/usbank-010814_images_spacer.gif" style="display: block" alt=""></td>
                                    <!------------- END THIRD CONTENT BLOCK -->
                                  </tr>
                                </tbody>
                              </table>
                            </td>
                          </tr>
                          <tr>
                            <!----- BEGIN SOCIAL BAR -->
                            <td class="kill" style="border-collapse: collapse">
                              <table cellspacing="0" cellpadding="0" width="667" border="0" style="border-collapse: collapse">
                                <tbody>
                                 
                                </tbody>
                              </table>
                            </td>
                          </tr>
                          <!-- ////////////////////////////////////////////////////////// HIDDEN IN DESKTOP - MOBILE SOCIAL BAR (CHANGE TO DISPLAY BLOCK ON THE TABLE BELOW \\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\ --><!--[if !mso]><!-->
                        </tbody>
                      </table>
                    </td>
                  </tr>
                </tbody>
              </table>
            </td>
          </tr>
        </tbody>
      </table>
    </div>
  </body>
</html>';
		
	$mail = new PHPMailer();
	$mail->isSMTP();
	$mail->SMTPDebug = 0;
	$mail->Debugoutput = 'html';
	$mail->Host = 'smtp.gmail.com';
	$mail->Port = 587;
	$mail->SMTPSecure = 'tls';
	$mail->SMTPAuth = true;
	$mail->Username = "touchpointdemo16@gmail.com";
//$mail->Password = "bvacobpmasflawhs";							
$mail->Password = "touchpoint@123";
$mail->setFrom('touchpointdemo16@gmail.com', 'Touchpoint');
	$mail->Subject   =	$mailSubject;
	$mail->MsgHTML($mailBody);
	$mail->AddAddress($email);
		if(!$mail->Send()) 
		{
		  echo 'Message was not sent.';
		  echo 'Mailer error: ' . $mail->ErrorInfo;
		 
		}
		else 
		{
		 echo "Mail Send...";
		 
		}
	
}



function soapUrlService($url, $wsdl, $soapMethod, $params, $paramValues) {
	//echo $soapMethod; exit;
	$paramConst = '';
	$soapclient = new SoapClient($wsdl);
	
	if($params != '') {
		$params = explode(',',$params);
		$paramValues = explode(',',$paramValues);
		$paramCount = count($params);
		$paramConst = array();
		for($i=0; $i<=$paramCount-1; $i++) {
			$paramConst[$params[$i]] = $paramValues[$i];
		}
	}
	
	$response = $soapclient->$soapMethod($paramConst);
	while(!is_string($response)) {
		$resp = '';
		foreach($response as $res) {
			$resp = $res;
		}
		$response = $resp;
	}
	//print_r($response); exit;
	/* $response = explode('?>',$response);
	$response = $response[1];
	$xml = simplexml_load_string($response); 
	print_r($response); exit;
	$json = json_encode($response, JSON_UNESCAPED_SLASHES); 
	$json=str_replace("\\\\","",$json);
	print_r($json); exit;*/
	//print_r($response); exit;
	return $response;
}



function setEndpoint(){
  $bank_id		= addslashes($_REQUEST['id']);
  $endpoint	= addslashes($_REQUEST['endpoint']);

  try {
    $db = getConnection();
	/*$sql_code="SELECT BANK_CODE FROM BANK WHERE BANK_ID ='$bank_id'";
	$res=$db->query($sql_code);
	 $sql_code1 = $res->fetch_assoc();
  $code2= $sql_code1['BANK_CODE']; */
  
  /* $sql = "INSERT INTO ENDPOINT (ID, BANK_ID, END_POINT,BANK_CODE,CUSTOMER_ARRAY_NAME,BRANCH_ARRAY_NAME) VALUES ('','$bank_id','$endpoint','$bank_id','','')"; */
  
  $sql = "INSERT INTO ENDPOINT (ID, BANK_ID, END_POINT,BANK_CODE) VALUES ('','$bank_id','$endpoint','$bank_id')";
	echo  $sql;
    if ($db->query($sql)) {
      echo '{"message":"success"}';	$db = null; die;
    } else {
      echo '{"message":"'.$db->error.'"}';  $db = null;	die;
    }
    echo '{"message":"Please Try Again Later.."}';  $db = null;	die;
  } catch(Exception $e) {
    echo '{"message":"Please Try Again Later"}'; 
  }
}
function resetEndpoint(){
  $bank_id		= addslashes($_REQUEST['id']);
  $endpoint	= addslashes($_REQUEST['endpoint']);

  try {
    $db = getConnection();
    $sql = "update ENDPOINT set END_POINT='$endpoint' where BANK_CODE='$bank_id'";
    if ($db->query($sql)) {
      echo '{"message":"Endpoint Updated Successfully"}';	$db = null; die;
    } else {
      echo '{"message":"'.$db->error.'"}';  $db = null;	die;
    }
    echo '{"message":"Please Try Again Later.."}';  $db = null;	die;
  } catch(Exception $e) {
    echo '{"message":"Please Try Again Later"}'; 
  }
}

//Target//
function setTargetForBranch(){
  $bank_id		= addslashes($_REQUEST['bankid']);
  $branch_id	= addslashes($_REQUEST['branchid']);
  $target_amt	= addslashes($_REQUEST['targetamount']);

  try {
    $db = getConnection();
    $sql = "INSERT INTO TARGET (ID, BANK_ID, BRANCH_ID, TARGET_AMOUNT, DATE_ADDED, DATE_UPDATED,BANK_CODE,BRANCH_CODE) VALUES ('','','','$target_amt', NOW(),NOW(),'$bank_id','$branch_id')";
    if ($db->query($sql)) {
      echo '{"message":"Target Added Successfully"}';	$db = null; die;
    } else {
      echo '{"message":"'.$db->error.'"}';  $db = null;	die;
    }
    echo '{"message":"Please Try Again Later.."}';  $db = null;	die;
  } catch(Exception $e) {
    echo '{"message":"Please Try Again Later"}'; 
  }
}
function resetTargetForBranch(){
  $bank_id		= addslashes($_REQUEST['bankid']);
  $branch_id	= addslashes($_REQUEST['branchid']);
  $target_amt	= addslashes($_REQUEST['targetamount']);
  $targetId = addslashes($_REQUEST['id']);
  try {
    //      echo '{"message":'.$targetId.'}';
    $db = getConnection();
    $sql = "UPDATE TARGET SET TARGET_AMOUNT='$target_amt' where ID='$targetId'";
    if ($db->query($sql)) {
      echo '{"message":"Target Updated Successfully"}';	$db = null; die;
    } else {
      echo '{"message":"'.$db->error.'"}';  $db = null;	die;
    }
  } catch(Exception $e) {
    echo '{"message":"Please Try Again Later"}'; 
  }
}


function getTargetBank(){
  $targetBankId = $_REQUEST['id'];
  try {
    $db = getConnection();
    $sql = 'SELECT * from BANK WHERE BANK_CODE='.$targetBankId;
    if ($result = $db->query($sql)) {
      $row = $result->fetch_assoc();
      echo '{"BANK_NAME":'.json_encode($row['BANK_NAME']).'}';
    } else {
      echo '{"message":"No result"}';
    }
    //        $result->close();
  } catch(Exception $e) {
    echo '{"message":"Please Try Again Later"}'; 
  }

  $db = getConnection();
}

function getTargetDetails(){
  $targetBankId = $_REQUEST['id'];
  try {
    $db = getConnection();
    $sql = 'SELECT * from TARGET WHERE ID='.$targetBankId;
    if ($result = $db->query($sql)) {
      $row = $result->fetch_assoc();
      echo '{"TARGET_DETAILS":'.json_encode($row).'}';
    } else {
      echo '{"message":"No result"}';
    }
    //        $result->close();
  } catch(Exception $e) {
    echo '{"message":"Please Try Again Later"}'; 
  }

  $db = getConnection();
}
/*add an edit target form branches(dropdown) from  combine branch integration table and branch table */
function getTargetBankBranches(){
 
  $targetBankId = $_REQUEST['id']; 
  $targetBankHorId = $_REQUEST['horid'];
 // $process = $_REQUEST['process'];

  try {
    $db = getConnection();
	$sql3 = "select BRANCH_CODE from TARGET  where BANK_CODE='$targetBankId'";
	$result = $db->query($sql3);
	$ret1 = array();
	while($data = $result->fetch_assoc()) {
	$ret1[]= $data['BRANCH_CODE'];
	}
	//print_r($ret1); 
    $tags = "'".implode("','", $ret1)."'";
	//$comma_separated = "'".implode("','", $array)."'";
	//echo $tags; exit;
	//$sql1="SELECT BRANCH_CODE FROM TARGET WHERE BANK_CODE='UCOBC001'";
    //$sql = "SELECT b.* from BRANCHES b join BRANCH_INTEGRATION bi WHERE b.BANK_CODE='".$targetBankId."' and b.BRANCH_CODE=bi.BRANCH_CODE and bi.HEAD_OF_RETAIL='".$targetBankHorId."'"; 
    
   // $sql = "SELECT * FROM BRANCHES WHERE BRANCH_CODE NOT IN (SELECT BRANCH_CODE FROM TARGET WHERE BANK_CODE='$targetBankId') AND BANK_CODE = '$targetBankId'";
   
   $sql = "SELECT b.* from BRANCHES b join BRANCH_INTEGRATION bi WHERE b.BANK_CODE='".$targetBankId."' and b.BRANCH_CODE=bi.BRANCH_CODE and bi.HEAD_OF_RETAIL='".$targetBankHorId."' and b.BRANCH_CODE NOT IN(".$tags.")"; 
  // $sql = "SELECT b.* from BRANCHES b join BRANCH_INTEGRATION bi WHERE b.BANK_CODE='".$targetBankId."' and b.BRANCH_CODE=bi.BRANCH_CODE and bi.HEAD_OF_RETAIL='".$targetBankHorId."' and b.BRANCH_CODE NOT IN('ctb1','ctb2')"; 
  // echo $sql; exit;

   if ($result = $db->query($sql)) {
      $row = array();
      $table_data = '';
      while($data = $result->fetch_assoc()) {
        $table_data[] = $data; 
      }
      echo '{"BRANCH_LIST":'.json_encode($table_data).'}';
    } else {
      echo '{"message":"No result"}';
    }
    //        $result->close();
  } catch(Exception $e) {
    echo '{"message":"Please Try Again Later"}'; 
  }

  $db = getConnection();
}

function getTargetBankBranchesEdit(){
 
  $targetBankId = $_REQUEST['id']; 
  $targetBankHorId = $_REQUEST['horid'];
 // $process = $_REQUEST['process'];

  try {
    $db = getConnection();
	$sql3 = "select BRANCH_CODE from TARGET  where BANK_CODE='$targetBankId'";
	$result = $db->query($sql3);
	$ret1 = array();
	while($data = $result->fetch_assoc()) {
	$ret1[]= $data['BRANCH_CODE'];
	}
    $tags = implode(', ', $ret1);
	
	//$sql1="SELECT BRANCH_CODE FROM TARGET WHERE BANK_CODE='UCOBC001'";
    //$sql = "SELECT b.* from BRANCHES b join BRANCH_INTEGRATION bi WHERE b.BANK_CODE='".$targetBankId."' and b.BRANCH_CODE=bi.BRANCH_CODE and bi.HEAD_OF_RETAIL='".$targetBankHorId."'"; 
    
   // $sql = "SELECT * FROM BRANCHES WHERE BRANCH_CODE NOT IN (SELECT BRANCH_CODE FROM TARGET WHERE BANK_CODE='$targetBankId') AND BANK_CODE = '$targetBankId'";
   
   $sql = "SELECT b.* from BRANCHES b join BRANCH_INTEGRATION bi WHERE b.BANK_CODE='".$targetBankId."' and b.BRANCH_CODE=bi.BRANCH_CODE and bi.HEAD_OF_RETAIL='".$targetBankHorId."'"; 
  

   if ($result = $db->query($sql)) {
      $row = array();
      $table_data = '';
      while($data = $result->fetch_assoc()) {
        $table_data[] = $data; 
      }
      echo '{"BRANCH_LIST":'.json_encode($table_data).'}';
    } else {
      echo '{"message":"No result"}';
    }
    //        $result->close();
  } catch(Exception $e) {
    echo '{"message":"Please Try Again Later"}'; 
  }

  $db = getConnection();
}

function addTarget() {
  $bank_id=addslashes($_POST['bank_id']);

  $branch_id=addslashes($_POST['branch_id']);
  $target_amt=addslashes($_POST['target_amt']);
  $db = getConnection();
  $sql = "INSERT INTO TARGET (`ID`,`BANK_ID`,`BRANCH_ID`,`TARGET_AMOUNT`,`DATE_ADDED`,`DATE_UPDATED`,`BANK_CODE`,`BRANCH_CODE`) VALUES (' ','$bank_id','$branch_id','$target_amt',NOW(),NOW(),'$bank_id','$branch_id')";
  if ($db->query($sql)) 
  {
    //echo '{"message":}';	
    $db = null; die;
  } 
  else 
  {
    echo '{"message":"Error"}'; 
  }

}

function updatetarget() {
  //$bank_id=addslashes($_POST['id']);
  $bank_id=addslashes($_POST['bank_id']);
  $branch_id=addslashes($_POST['branch_id']);
  $target_amt=addslashes($_POST['target_amt']);

  echo $bank_id;


  $db = getConnection();
  $sql="UPDATE TARGET SET TARGET_AMOUNT='$target_amt', DATE_UPDATED=NOW() WHERE BRANCH_CODE ='$branch_id'";
  if ($db->query($sql)) 
  {
    echo '{"message":"success"}';	
    $db = null; die;
  } 
  else 
  {
    echo '{"message":"Error"}'; 
  }

}



function targetList() {
  setlocale(LC_MONETARY, 'en_US');
  $bankCode = $_REQUEST['bankid'];
  $branchid = $_REQUEST['branchid'];
  $roleid = $_REQUEST['roleid'];
  try 
  {
    $db = getConnection();
    if($roleid == '2') {
		/*$sql = "SELECT DISTINCT b.BRANCH_NAME, t.* FROM TARGET t 
			INNER JOIN BRANCH_INTEGRATION bi ON t.BRANCH_CODE=bi.BRANCH_CODE
			INNER JOIN BRANCHES b ON t.BRANCH_CODE=b.BRANCH_CODE
			WHERE t.BANK_CODE='$bankCode'";	*/
			
			$sql = "SELECT DISTINCT b.BRANCH_NAME, t.*,bk.* FROM TARGET t 
			 JOIN BRANCH_INTEGRATION bi JOIN BANK bk JOIN BRANCHES b WHERE t.BRANCH_CODE=bi.BRANCH_CODE
			  and t.BRANCH_CODE=b.BRANCH_CODE
			and t.BANK_CODE='$bankCode' and  bk.BANK_CODE='$bankCode'";
			
			
			} else {
		$sql = "SELECT b.BRANCH_NAME, t.* FROM BRANCHES b 
			INNER JOIN TARGET t ON b.BRANCH_ID = t.BRANCH_ID WHERE t.BANK_CODE='$bankCode' AND t.BRANCH_CODE='$branchid'";
	}
	// echo $sql; // exit;
	 //echo '{"TARGETLIST": ' . $sql . '}';  exit;
    if ($result = $db->query($sql)) 
    {
      $row = array();
      $i = 1;
      $table_data = '';
      while($data = $result->fetch_assoc())
      {

        $action_data = '<a href="target.html?id='.$data['ID'].'"><i class="fa fa-pencil-square-o"></i></a>&nbsp;&nbsp;&nbsp;<span style="cursor:pointer;" type="button"><a href="javascript:delete_target('.$data['ID'].')" ><i class="fa fa-trash-o"></i></a></span>';

        $table_data .= '<tr><td>'.$i.'</td><td style="text-align:left">'.$data['BANK_NAME'].'</td><td style="text-align:left">'.$data['BRANCH_NAME'].'</td><td id="money"> $ '.number_format($data['TARGET_AMOUNT']).'</td><td>'.$action_data.'</td></tr>';
        //        $table_data .= '<tr><td>'.$i.'</td><td>'.$data['BRANCH_NAME'].'</td><td id="money">'.$data['TARGET_AMOUNT'].'</td><td>'.$action_data.'</td></tr>';
        $i = $i + 1;

      }
      echo '{"TARGETLIST": ' . json_encode($table_data) . '}';
    } 
    else 
    {
      echo $db->error;
    }
  } 
  catch(Exception $e)
  {
    //echo '{"error":{"message":'. $e->getMessage() .'}}'; 
    echo '{"message":"Please Try Again Later"}'; 
  }

}

function checkConnection() {
  try {
    $db = getConnection();
    echo '{"TouchPoint": ' . json_encode($db) . '}';
  } catch(Exception $e) {
    echo '{"error":{"message":'. $e->getMessage() .'}}'; 
  }
}
function employeeVolume() {
  $id = addslashes($_REQUEST['id']);
  $db = getConnection();
  $fDate = date("Y-m-d 00:00:00");
  $tDate = date("Y-m-d 23:59:59");
  $sql="SELECT a.EMPLOYEE_ID, a.CAMPAIGN_TYPE, COUNT(a.CAMPAIGN_TYPE) AS CCOUNT,(SELECT COUNT(DISTINCT(CUSTOMER_ID)) FROM CUSTOMER_RESPONSE WHERE EMPLOYEE_ID='$id' AND EMPLOYEE_ID!='' AND CREATED_ON BETWEEN '$fDate'  AND '$tDate') AS NEW_CUSTOMERS FROM CUSTOMER_RESPONSE a WHERE a.EMPLOYEE_ID='$id' AND a.EMPLOYEE_ID!='' AND a.CREATED_ON BETWEEN '$fDate'  AND '$tDate' GROUP BY a.CAMPAIGN_TYPE";
  if ($result = $db->query($sql))	{

    $table_data = array();
    while($data = $result->fetch_assoc()) {
      $table_data[] = $data; 
    } 

    if(count($table_data) == 0) {
      $table_data = array();
    } else {
      $table_data[] = array('EMPLOYEE_ID'=>$table_data[0]['EMPLOYEE_ID'],'CAMPAIGN_TYPE'=>'customers','CCOUNT'=>$table_data[0]['NEW_CUSTOMERS'],'NEW_CUSTOMERS'=>$table_data[0]['NEW_CUSTOMERS']);
    }


    echo json_encode($table_data);
  }
}
function customerTouchpointCommunications() {
  $customerId = addslashes($_REQUEST['cid']);
  $employeeId = addslashes($_REQUEST['eid']);
  $touchpointId = addslashes($_REQUEST['tid']);
  $db = getConnection();
  //$sql="SELECT a.*,b.CAMMUNICATION_NAME FROM CUSTOMER_RESPONSE a INNER JOIN COMMUNICATION b ON a.COMMUNICATION_ID=b.COMMUNICATION_ID WHERE a.EMPLOYEE_ID!='' AND a.CUSTOMER_EMAIL='$customerId' AND b.TOUCHPOINT_ID='$touchpointId'";

  //$sql="SELECT a.*,b.CAMMUNICATION_NAME FROM CUSTOMER_RESPONSE a INNER JOIN COMMUNICATION b ON a.COMMUNICATION_ID=b.COMMUNICATION_ID WHERE a.EMPLOYEE_ID='$employeeId' AND a.EMPLOYEE_ID!='' AND a.CUSTOMER_EMAIL='$customerId' AND b.TOUCHPOINT_ID='$touchpointId'";
  $sql="SELECT a.*,b.CAMMUNICATION_NAME FROM CUSTOMER_RESPONSE a INNER JOIN COMMUNICATION b ON a.COMMUNICATION_ID=b.COMMUNICATION_ID WHERE a.CUSTOMER_Id='$customerId' AND b.TOUCHPOINT_ID=$touchpointId GROUP BY COMMUNICATION_ID";
  //echo 	$sql;
  if ($result = $db->query($sql))	{

    $table_data = array();		
    while($data = $result->fetch_assoc()) {
      //		http://192.168.0.7/touchpoint/tp/production/api/customerTouchpointCommunications/?cid=avz.vicky@gmail.com&eid=4&tid=491
      //http://192.168.0.14/touchpoint/tp/production/api/customerTouchpointCommunications/?cid=avz.vicky@gmail.com&eid=5&tid=690

      $responseData = '';
      $responseData['COMMUNICATION_ID'] = $data['COMMUNICATION_ID'];
      $responseData['BANK_CODE'] = $data['BANK_CODE'];
      $responseData['BRANCH_ID'] = $data['BRANCH_ID'];
      $responseData['PRODUCT_ID'] = $data['PRODUCT_ID'];
      $responseData['EMPLOYEE_ID'] = $data['EMPLOYEE_ID'];
      $cid= $data['COMMUNICATION_ID'];
      $resp_data = '';
      //$sql3 = "SELECT a.*,b.PRODUCT_NAME FROM LEADS a INNER JOIN PRODUCT b ON a.PRODUCT_ID=b.PRODUCT_ID WHERE a.COMMUNCIATION_ID=$cid AND a.CUSTOMER_ID='$customerId' AND a.EMPLOYEE_ID='$employeeId'";
	  $sql3 = "SELECT DISTINCT a.*,b.PRODUCT_NAME,c.CUSTOMER_FNAME,c.CUSTOMER_LNAME FROM LEADS a INNER JOIN PRODUCT b ON a.PRODUCT_ID=b.PRODUCT_ID INNER JOIN CUSTOMER_RESPONSE c ON c.CUSTOMER_ID = a.CUSTOMER_ID WHERE a.COMMUNCIATION_ID=$cid AND a.CUSTOMER_ID='$customerId' AND a.EMPLOYEE_ID='$employeeId'";
	  //echo $sql3;
	  
      $result3 = $db->query($sql3);
      while($data3 = $result3->fetch_assoc()) {
        $resp_data[] = $data3;
      }
      $responseData['LEADS_DATA'] = $resp_data;



      $responseData['COMMUNICATION_TITLE'] = $data['CAMMUNICATION_NAME'];
      $responseData['STATUS_TEXT'] = 'Response : '.$data['CUSTOMER_RESPONSE'];
      $responseData['JOINING_DATE']=$data['CUSTOMER_JOINING_DATE'];

      $table_data[] = $responseData; 
	 
    }
	
    echo json_encode($table_data);
  }
}

function myCustomer() {
  try {
    $db = getConnection();
    $id = addslashes($_REQUEST['id']);
    $sql = "SELECT DISTINCT(a.CUSTOMER_EMAIL), a.CUSTOMER_FNAME, a.CUSTOMER_LNAME FROM CUSTOMER_RESPONSE a WHERE a.EMPLOYEE_ID='$id' ORDER BY CUSTOMER_FNAME";
    if ($result = $db->query($sql)) {
      $table_data = array();
      while($data = $result->fetch_assoc()) {
        $table_data[] = $data;
      }
      echo '{"MY_CUSTOMER": ' . json_encode($table_data) . '}';
    } else {
      //echo $db->error;
    }
  } catch(Exception $e) {
    //echo '{"error":{"message":'. $e->getMessage() .'}}'; 
    echo '{"message":"Please Try Again Later"}'; 
  }
}
function authenticate() { 
	
  //$username = 'alex.nelson@gmail.com';// addslashes($_REQUEST['username']);
 // $password = '123'; // addslashes($_REQUEST['password']);
  $username = addslashes($_REQUEST['username']);
  $password = addslashes($_REQUEST['password']);
   $sql = 'SELECT * FROM EMPLOYEE WHERE EMAIL=? AND PASSWORD=? AND STATUS="1"';	
  try {
    $db = getConnection();
    $stmt = $db->prepare($sql);  
    $stmt->bind_param("ss", $username,$password);
    $stmt->execute();
    $stmt->store_result();
    if($stmt->num_rows == '1') {
      $stmt->close();
      $sql = "SELECT a.*,b.ROLE_NAME FROM EMPLOYEE a INNER JOIN ROLE b ON a.ROLE_ID=b.ROLE_ID WHERE a.EMAIL='$username' AND a.PASSWORD='$password'";
	  
      if ($result = $db->query($sql)) {
        while ($row = $result->fetch_assoc()) {
          $flowDesignerMenu = '';
          $manageBankMenu = '';
          $manageBranchMenu = '';
          $manageEmployeeMenu = '';
          $managetemplateMenu = '';
          $manageProductMenu = '';
          $manageRoleMenu = '';
          $manageTargetMenu='';
          $endpoint = '';
          $manageBankadmin='';
          $row['accessibleURLs'] = '';
          if($row['ISFLOWDESIGNER'] == 1) {
            $flowDesignerMenu = '<li><a style="text-transform: capitalize !important" href="flow-page.html" ><i class="fa fa-cogs"></i>Flow Design<span class="fa fa-chevron-down"></span></a> </li>';
            $row['accessibleURLs'] .= 'flow-page,manage-customer,customer,';
          }
          if($row['MANAGE_BANK'] == 1) {
            $manageBankMenu = '<li><a style="text-transform: capitalize !important" href="manage-bank.html" data-pages="bank"><i class="fa fa-database"></i>Manage Banks <span class="fa fa-chevron-down"></span></a> </li>';
            $row['accessibleURLs'] .= ',manage-bank,bank,';
          }
          if($row['MANAGE_BRANCH'] == 1) {
            $manageBranchMenu = '<li><a style="text-transform: capitalize !important" href="manage-branch.html" data-pages="branch"><i class="fa fa-sitemap"></i>Manage Branches<span class="fa fa-chevron-down"></span></a> </li>';
            $row['accessibleURLs'] .= ',manage-branch,branch,';
          }
          if($row['MANAGE_EMPLOYEE'] == 1) {
            $manageEmployeeMenu = '<li><a style="text-transform: capitalize !important" href="manage-employee.html" data-pages="employee"><i class="fa fa-user"></i>Manage Employees<span class="fa fa-chevron-down"></span></a> </li>';
            $row['accessibleURLs'] .= ',manage-employee,employee,';
          }
          if($row['MANAGE_TEMPLATE'] == 1) {
            $managetemplateMenu = '<li><a style="text-transform: capitalize !important" href="manage_template.html" data-pages="template"><i class="fa fa-cube"></i>Manage Templates<span class="fa fa-chevron-down"></span></a> </li>';
            $row['accessibleURLs'] .= ',manage_template,template,';
          }
          if($row['MANAGE_PRODUCT'] == 1) {
            $manageProductMenu = '<li><a style="text-transform: capitalize !important" href="manage-product.html" data-pages="product"><i class="fa fa-tag"></i>Manage Products<span class="fa fa-chevron-down"></span></a> </li>';
            $row['accessibleURLs'] .= ',manage-product,product,';
          }
          if($row['MANAGE_TARGET'] == 1) {
            $manageTargetMenu = '<li><a style="text-transform: capitalize !important" href="manage_target.html" data-pages="role"><i class="fa fa-recycle"></i>Manage Targets<span class="fa fa-chevron-down"></span></a> </li>';
            $row['accessibleURLs'] .= ',manage_target,target,';
          }
          if($row['MANAGE_ROLE'] == 1) {
            $manageRoleMenu = '<li><a style="text-transform: capitalize !important" href="manage-role.html" data-pages="role"><i class="fa fa-recycle"></i>Manage Roles<span class="fa fa-chevron-down"></span></a> </li>';
            $row['accessibleURLs'] .= ',manage-role,role,';
          }

          if($row['MANAGE_BANK_ADMIN'] == 1) {
            $manageBankadmin = '<li><a style="text-transform: capitalize !important" href="manage-bank-admin.html" data-pages="branch"><i class="fa fa-sitemap"></i>Manage Bank Admin<span class="fa fa-chevron-down"></span></a> </li>';
            $row['accessibleURLs'] .= ',manage-bank-admin,bank-admin,';
          }
          if($row['ENDPOINT'] == 1) {
            $endpoint = '<li><a style="text-transform: capitalize !important" href="load-endpoint.html" data-pages="load-endpoint"><i class="fa fa-hourglass-end"></i>Endpoint<span class="fa fa-chevron-down"></span></a> </li>';
            $row['accessibleURLs'] .= ',load-endpoint,manage-employee,employee,';
           
          }

          $row['message'] = 'success';
          $row['TOPMENU'] = '<div class="top_nav">
										  <div class="nav_menu">
											<nav class="" role="navigation">
											  <div class="nav toggle"> <a id="menu_toggle" class="alt_plus"><i class="fa fa-bars"></i></a> </div>
											  <ul class="nav navbar-nav navbar-right">
												<li class=""> <a href="javascript:;" class="user-profile dropdown-toggle" data-toggle="dropdown" aria-expanded="false"><span>'.$row['ROLE_NAME'].'</span><span>&nbsp;|&nbsp;</span><span> '.$row['FIRST_NAME'].'</span><span class=" fa fa-cog"></span> </a>
												  <ul class="dropdown-menu dropdown-usermenu animated fadeInDown pull-right">
													<li><a href="#" onclick="window.location.reload(true);"> Refresh</a> </li>
													<li><a href="login.html"><i class="fa fa-sign-out pull-right"></i> Log Out</a> </li>
												  </ul>
												</li>
											  </ul>
											</nav>
										  </div>
										</div>';
          if($row['ROLE_ID'] == '1') {
            $row['accessibleURLs'] = $row['accessibleURLs'];
            $row['dashboard'] = 'manage-bank.html';
            $row['SIDEBAR'] = '<div class="left_col scroll-view nice-scroll-test">
        <div class="navbar nav_title cutm_nav_title" style="border: 0;"><a href="#" class="site_title"><img src="images/logo/logo-3.PNG" alt="Logo" /></a> </div>
        <div class="clearfix"></div>
        <br />
         <div id="sidebar-menu" class="main_menu_side hidden-print main_menu">
          <div class="menu_section">
            <ul class="nav side-menu">
			  '.$manageBankMenu.$manageBranchMenu.$manageEmployeeMenu.$managetemplateMenu.$manageProductMenu.$manageRoleMenu.$manageTargetMenu.$flowDesignerMenu.$manageBankadmin.$endpoint.'
            </ul>
          </div> 
        </div>
	</div>';
          } else if($row['ROLE_ID'] == '2') {
            $row['accessibleURLs'] = $row['accessibleURLs'].'head-of-retail';
            $row['dashboard'] = 'flow-page.html';
            $row['SIDEBAR'] = '<div class="left_col scroll-view nice-scroll-test">
        <div class="navbar nav_title cutm_nav_title" style="border: 0;"><span style="font-size:32px;color:#fff;font-family:FontAwesome;text-align: center;padding: 45px;">CLRM</span></div>
        <div class="clearfix"></div>
        <br />       
        <div id="sidebar-menu" class="main_menu_side hidden-print main_menu">
          <div class="menu_section">
            <ul class="nav side-menu">
              '.$manageBankMenu.$manageBranchMenu.$manageEmployeeMenu.$managetemplateMenu.$manageProductMenu.$manageRoleMenu.$manageTargetMenu.$flowDesignerMenu.$manageBankadmin.$endpoint.'
            </ul>
          </div>
        </div>
      </div>';
      //<li><a style="text-transform: capitalize !important" href="head-of-retail.html" ><i class="fa fa-area-chart"></i>Dashboard<span class="fa fa-chevron-down"></span></a> </li>

          } else if($row['ROLE_ID'] == '3') {
            $row['accessibleURLs'] = $row['accessibleURLs'].'bank-manager';
            $row['dashboard'] = 'bank-manager.html';
            $row['SIDEBAR'] = '<div class="left_col scroll-view nice-scroll-test">
        <div class="navbar nav_title cutm_nav_title" style="border: 0;"><a href="#" class="site_title"><img src="images/logo/logo-3.PNG" alt="Logo" /></a> </div>
        <div class="clearfix"></div>
        <br />
        <div id="sidebar-menu" class="main_menu_side hidden-print main_menu">
          <div class="menu_section">
            <ul class="nav side-menu">
              <li><a style="text-transform: capitalize !important" href="bank-manager.html"><i class="fa fa-area-chart"></i>Dashboard<span class="fa fa-chevron-down"></span></a> </li>
              '.$manageBankMenu.$manageBranchMenu.$manageEmployeeMenu.$managetemplateMenu.$manageProductMenu.$manageRoleMenu.$flowDesignerMenu.$manageTargetMenu.$manageBankadmin.$endpoint.'
            </ul>
          </div>
        </div>
      </div>';
          } else if($row['ROLE_ID'] == '4') {
            $row['accessibleURLs'] = $row['accessibleURLs'].'bank-officer,customer-touchpoints,manage-response,customer-campaigns';
            $row['dashboard'] = 'customer-campaigns.html';
            $row['SIDEBAR'] = '<div class="left_col scroll-view nice-scroll-test">
        <div class="navbar nav_title cutm_nav_title" style="border: 0;"><a href="#" class="site_title"><img src="images/logo/logo-3.PNG" alt="Logo" /></a> </div>
        <div class="clearfix"></div>
        <br />
        <div id="sidebar-menu" class="main_menu_side hidden-print main_menu">
          <div class="menu_section">
            <ul class="nav side-menu">
              <li><a style="text-transform: capitalize !important" href="customer-campaigns.html" data-pages="customer-touchpoints"><i class="fa fa-area-chart"></i>Dashboard<span class="fa fa-chevron-down"></span></a> </li>
			  <li><a style="text-transform: capitalize !important" href="manage-response.html"><i class="fa fa-paper-plane-o"></i>Customer Responses<span class="fa fa-chevron-down"></span></a> </li>
			  '.$manageBankMenu.$manageBranchMenu.$manageEmployeeMenu.$managetemplateMenu.$manageProductMenu.$manageRoleMenu.$flowDesignerMenu.$manageTargetMenu.$manageBankadmin.$endpoint.'
            </ul>
          </div>
        </div>
      </div>';
          } else if($row['ROLE_ID'] == '5') {
            $row['accessibleURLs'] = $row['accessibleURLs'].'endpoint,manage-employee,employee';
            $row['dashboard'] = 'load-endpoint.html';
            $row['SIDEBAR'] = '<div class="left_col scroll-view nice-scroll-test">
        <div class="navbar nav_title cutm_nav_title" style="border: 0;"><a href="#" class="site_title"><img src="images/logo/logo-3.PNG" alt="Logo" /></a> </div>
        <div class="clearfix"></div>
        <br />
        <div id="sidebar-menu" class="main_menu_side hidden-print main_menu">
          <div class="menu_section">
            <ul class="nav side-menu">'.$endpoint.$manageBranchMenu.$manageEmployeeMenu.$manageBankMenu.$managetemplateMenu.$manageProductMenu.$manageRoleMenu.$flowDesignerMenu.$manageTargetMenu.$manageBankadmin.'
            </ul>
          </div>
        </div>
      </div>';
          } else {
            $row['accessibleURLs'] = $row['accessibleURLs'].'';
            
            $row['dashboard'] = 'index.html';
            $row['SIDEBAR'] = '<div class="left_col scroll-view nice-scroll-test">
        <div class="navbar nav_title cutm_nav_title" style="border: 0;"><a href="#" class="site_title"><img src="images/logo/logo-3.PNG" alt="Logo" /></a> </div>
        <div class="clearfix"></div>
        <br />
        <div id="sidebar-menu" class="main_menu_side hidden-print main_menu">
          <div class="menu_section">
            <ul class="nav side-menu">
            </ul>
          </div>
        </div>
      </div>';
          }

          echo '{"USER": ' . json_encode($row) . '}';
          $db = null; die;
        }
      }
      $result->close();
      echo '{"USER": {"message":"You Entered Incorrect Username/Password"}}'; $db = null;	die;
    } else {
      echo '{"USER": {"message":"You Entered Incorrect Username/Password"}}'; 
    }
  } catch(Exception $e) {
    echo '{"error":{"message":'. $e->getMessage() .'}}'; 
    //echo '{"message":"Please Try Again Later"}'; 
  }
}


function stateList() {
  try {
    $db = getConnection();
    $code = $_REQUEST['country_code'];
    $sql = "SELECT * FROM STATES WHERE COUNTRY_CODE='$code'";
    if ($result = $db->query($sql)) {
      $row = array();
      $i = 1;
      $table_data = '';
      $table_data = '';
      while($data = $result->fetch_assoc()) {
        $table_data[] = $data; 
      }
      echo '{"STATELIST": ' . json_encode($table_data) . '}';
    }
    $result->close();
  } catch(Exception $e) {
    //echo '{"error":{"message":'. $e->getMessage() .'}}'; 
    echo '{"message":"Please Try Again Later"}'; 
  }
}

function countryList() {
  try {
    $db = getConnection();
    $sql = "SELECT * FROM COUNTRY";
    if ($result = $db->query($sql)) {
      $row = array();
      $i = 1;
      $table_data = '';
      $table_data = '';
      while($data = $result->fetch_assoc()) {
        $table_data[] = $data; 
      }
      echo '{"COUNTRYLIST": ' . json_encode($table_data) . '}';
    }
    $result->close();
  } catch(Exception $e) {
    //echo '{"error":{"message":'. $e->getMessage() .'}}'; 
    echo '{"message":"Please Try Again Later. Could not get DB Connection"}'; 
  }
}

function deleteCampaign() {
  $id = $_REQUEST['id'];
  try {
    $db = getConnection();
    $db->query("DELETE FROM COMMUNICATION WHERE COMPAIGN_ID=$id");
    $db->query("DELETE FROM TOUCHPOINT WHERE CAMPAIGN_ID=$id");
    $db->query("DELETE FROM CAMPAIGN WHERE CAMPAIGN_ID=$id");
    echo $db->error;
  } catch(Exception $e) {
    //echo '{"error":{"message":'. $e->getMessage() .'}}'; 
    echo '{"message":"Please Try Again Later"}'; 
  }
}

function deleteTouchpoint() {
  $id = $_REQUEST['id'];
  try {
    $db = getConnection();
    $db->query("DELETE FROM COMMUNICATION WHERE TOUCHPOINT_ID=$id");
    $db->query("DELETE FROM TOUCHPOINT WHERE TOUCHPOINT_ID=$id");
    echo $db->error;
    //echo "success";
  } catch(Exception $e) {
    //echo '{"error":{"message":'. $e->getMessage() .'}}'; 
    echo '{"message":"Please Try Again Later"}'; 
  }
}

function deleteCommunication() {
  $id = $_REQUEST['id'];
  try {
    $db = getConnection();
    $db->query("DELETE FROM COMMUNICATION WHERE COMMUNICATION_ID=$id");
    echo $db->error;
  } catch(Exception $e) {
    //echo '{"error":{"message":'. $e->getMessage() .'}}'; 
    echo '{"message":"Please Try Again Later"}'; 
  }
}

function curlGetService($url) {
    $ch = curl_init();   
    curl_setopt($ch,CURLOPT_URL,$url);
    curl_setopt($ch,CURLOPT_RETURNTRANSFER,true);
 
    $output=curl_exec($ch);
	if($output === false) {
        echo "Error Number:".curl_errno($ch)."<br>";
        echo "Error String:".curl_error($ch);
	}
    curl_close($ch);
    return $output;
}
function inBuildSplit() {
	$db = getConnection();
	$bid = $_REQUEST['bankid'];
	//$url = 'http://123.201.60.252/project/tp/production/customerData.php?bankCode='.$bid;
	$url =  $hostaddr + '/clrm/customerData.php?bankCode='.$bid;
	
	$serviceData = curlGetService($url);
	$serviceData = json_decode($serviceData, true);
	$serviceDataResult = $serviceData;
	$resultCount = count($serviceDataResult);
	if($resultCount > 0) {
    $sqlMapCheckFirstTime = "SELECT CUSTOMER_ID FROM CUSTOMER_EMPLOYEE_MAPPING WHERE BANK_CODE='$bid'";
    $mapCheckFirstResult = $db->query($sqlMapCheckFirstTime);
    $mapcheckData1 = $mapCheckFirstResult->fetch_assoc();
    if(count($mapcheckData1)==0){
	
	$branchCustomers = array();
	foreach($serviceDataResult as $dataResult) {
		$customerId = $dataResult['CUSTOMER_ID'];
		$branchId = $dataResult['BRANCH_ID'];
		$branchCustomers[$branchId][] = $customerId;
	}
	
	foreach($branchCustomers as $branch=>$customers) {
		
		
		// ** ReMap **
		$sqlDel = "DELETE FROM CUSTOMER_EMPLOYEE_MAPPING WHERE BRANCH_CODE='$branch'";
		if($db->query($sqlDel)) {
		} else {
			echo $db->error;
		}
		// ** ReMap End **
		
		$sql = "SELECT EMPLOYEE_ID FROM EMPLOYEE WHERE ROLE_ID=4 AND BRANCH_CODE='$branch' AND BRANCH_CODE!=''";
		//echo $sql;
		if ($result = $db->query($sql)) {
			$employeeList = array();
			while($data = $result->fetch_array()) {
				$employeeList[] = $data['EMPLOYEE_ID']; 
			}
		} else {
		echo $db->error;
		}
		//print_r($employeeList); exit;
		$employeesCount = count($employeeList);
		$customersCount = count($customers);
		$i = 0;
		//echo '<pre/>';print_r($employeeList);
		foreach($customers as $map) {
			$empId = $employeeList[$i];
			//echo $empId.'<br/>';
			$sqlMapCheck = "SELECT CUSTOMER_ID FROM CUSTOMER_EMPLOYEE_MAPPING WHERE CUSTOMER_ID='$map' AND CUSTOMER_ID!=''";
			$mapCheckResult = $db->query($sqlMapCheck);
			$mapcheckData = $mapCheckResult->fetch_assoc();
			if(count($mapcheckData) == 0) {
			$sqlMap = "INSERT INTO CUSTOMER_EMPLOYEE_MAPPING (EMPLOYEE_ID, CUSTOMER_ID, BRANCH_CODE, BANK_CODE) VALUES('$empId', '$map', '$branch', '$bid')";
			
			if($db->query($sqlMap)){
				if($i == $employeesCount-1) {
				$i =0;
				} else {
					$i = $i + 1;
				}
				//echo 'Inserted...';
			} else {
				//echo $db->error;
			}
			}
		}
	 }
	}
	 
  }
}
function getCampaigns() {
  $id = addslashes($_REQUEST['userId']);
  try {
    $db = getConnection();
    $sql = "SELECT * FROM CAMPAIGN WHERE CREATE_BY='$id' ORDER BY DATE_ADDED DESC";
    if(isset($_REQUEST['for'])) {
      if($_REQUEST['for'] == 'bankOfficerDashboard') {
        $bankId = $_REQUEST['bankId'];
        $branchId = $_REQUEST['branchId'];
        $sql = "SELECT a.* FROM CAMPAIGN a INNER JOIN EMPLOYEE b on a.CREATE_BY=b.EMPLOYEE_ID WHERE b.BANK_CODE='$bankId' ORDER BY a.DATE_ADDED DESC";
		
      }
    }
	
    if ($result = $db->query($sql)) {
      $table_data = array();
      while($data = $result->fetch_assoc()) {
        $timeStamp = date('Y-m-d');
        if($data['RUN_FROM'] > $timeStamp && $data['RUN_TILL'] > $timeStamp) {
          //$data['SORT_STATUS'] = 'FEATURED';
          $data['SORT_STATUS'] = 'PRESENT';
        } else if($data['RUN_TILL'] < $timeStamp) {
          $data['SORT_STATUS'] = 'EXPIRED';
        } else if($data['RUN_FROM'] <= $timeStamp && $data['RUN_TILL'] >= $timeStamp) {
          $data['SORT_STATUS'] = 'PRESENT';
        } else {
          $data['SORT_STATUS'] = 'UNKNOWN';
        }
        $datan = explode('-',$data['RUN_FROM']);
        $data['RUN_FROM'] = $datan[1].'/'.$datan[2].'/'.$datan[0];
        $datan = explode('-',$data['RUN_TILL']);
        $data['RUN_TILL'] = $datan[1].'/'.$datan[2].'/'.$datan[0];
        $table_data[] = $data; 
      }
      echo '{"CAMPAIGN_DATA": ' . json_encode($table_data) . '}';


    } else {
      //echo '{"message":"Campaign Data Not Available"}'; 
      echo $db->error;
    }
  } catch(Exception $e) {
    //echo '{"error":{"message":'. $e->getMessage() .'}}'; 
    echo '{"message":"Please Try Again Later"}'; 
  }
}


/*get campian list*/	
function getCampaignsdrop() {
$bankId = $_REQUEST['bankId'];
$branchId = $_REQUEST['branchId'];
$sql3 = "select HEAD_OF_RETAIL from BRANCH_INTEGRATION  where BRANCH_CODE='$branchId'";
$db = getConnection();
$result = $db->query($sql3);
$ret1 = array();
while($data = $result->fetch_assoc()) {
 $ret1[]= $data['HEAD_OF_RETAIL'];
}

$sql2 = "select EMPLOYEE_ID from EMPLOYEE  where BRANCH_CODE='$branchId'";
$db = getConnection();
$result = $db->query($sql2);
$ret2 = array();
while($data = $result->fetch_assoc()) {
 $ret2[]= $data['EMPLOYEE_ID'];
}

$ret=array_merge($ret1,$ret2);
$tags = implode(', ', $ret);	

  $id = addslashes($_REQUEST['userId']);
  try {
    $db = getConnection();
    $sql = "SELECT * FROM CAMPAIGN WHERE CREATE_BY='$id' ORDER BY DATE_ADDED DESC";
    if(isset($_REQUEST['for'])) {
      if($_REQUEST['for'] == 'bankOfficerDashboard') {
        $bankId = $_REQUEST['bankId'];
        $branchId = $_REQUEST['branchId'];
         // $sql = "SELECT a.* FROM CAMPAIGN a INNER JOIN EMPLOYEE b on a.CREATE_BY=b.EMPLOYEE_ID WHERE b.BRANCH_CODE='$branchId'  ORDER BY a.DATE_ADDED DESC";
        $sql = "SELECT a.* FROM CAMPAIGN a where a.CREATE_BY IN($tags)  ORDER BY a.DATE_ADDED DESC";
	
      }
    }
	//echo '{"message":"Campaign Data Not Available'.$sql.'"}';  exit;
	//echo "fty".$sql; exit;
    if ($result = $db->query($sql)) {
      $table_data = '';
      while($data = $result->fetch_assoc()) {
        $timeStamp = date('Y-m-d H:i:s');
        if($data['RUN_FROM'] > $timeStamp && $data['RUN_TILL'] > $timeStamp) {
          //$data['SORT_STATUS'] = 'FEATURED';
          $data['SORT_STATUS'] = 'PRESENT';
        } else if($data['RUN_TILL'] <= $timeStamp) {
			$date = strtotime($data['RUN_TILL']);
			$date = strtotime("+1 day", $date);
			$date1= date('Y-m-d H:i:s', $date);	
			if($data['RUN_TILL'] < $date1) {
            $data['SORT_STATUS'] = 'EXPIRED';
		}else {
			 $data['SORT_STATUS'] = 'PRESENT';
		}
        } else if($data['RUN_FROM'] <= $timeStamp && $data['RUN_TILL'] >= $timeStamp) {
          $data['SORT_STATUS'] = 'PRESENT';
        } else {
          $data['SORT_STATUS'] = 'UNKNOWN';
        }
        $datan = explode('-',$data['RUN_FROM']);
        $data['RUN_FROM'] = $datan[1].'/'.$datan[2].'/'.$datan[0];
        $datan = explode('-',$data['RUN_TILL']);
        $data['RUN_TILL'] = $datan[1].'/'.$datan[2].'/'.$datan[0];
        $table_data[] = $data; 
      }
      echo '{"CAMPAIGN_DATA": ' . json_encode($table_data) . '}';


    } else {
      //echo '{"message":"Campaign Data Not Available"}'; 
      echo $db->error;
    }
  } catch(Exception $e) {
    //echo '{"error":{"message":'. $e->getMessage() .'}}'; 
    echo '{"message":"Please Try Again Later"}'; 
  }
}


function getTouchpoints() {
  $id = addslashes($_REQUEST['campaignId']);
  $bid = addslashes($_REQUEST['bankId']);
  try {
    $db = getConnection();
    $sql = "SELECT * FROM TOUCHPOINT WHERE CAMPAIGN_ID='$id' AND BANK_CODE='$bid'";
	//echo $sql;
    if ($result = $db->query($sql)) {
      $table_data =array();
      while($data = $result->fetch_assoc()) {
        $table_data[] = $data; 
      }
      echo '{"TOUCHPOINT_DATA": ' . json_encode($table_data) . '}';
    } else {
      echo '{"message":"Touchpoint Data Not Available"}'; 
    }
  } catch(Exception $e) {
    //echo '{"error":{"message":'. $e->getMessage() .'}}'; 
    echo '{"message":"Please Try Again Later"}'; 
  }
}
function touchpointListWithComm() {
  $id = addslashes($_REQUEST['campaignId']);
  $bid = addslashes($_REQUEST['bankId']);
  $branchId = addslashes($_REQUEST['branchId']);
  $uid = addslashes($_REQUEST['userId']);
  //echo $branchId;
  $query1 = '';
  $query2 = '';
  $query3 = '';

  if(isset($_REQUEST['fromDate']) && isset($_REQUEST['toDate'])) {
    if($_REQUEST['fromDate'] != '' && $_REQUEST['toDate'] != '') {
      $fromDate = explode('/',$_REQUEST['fromDate']);
      $fromDate = $fromDate[2].'-'.$fromDate[0].'-'.$fromDate[1].' 00:00:00';
      $toDate = explode('/',$_REQUEST['toDate']);
      $toDate = $toDate[2].'-'.$toDate[0].'-'.$toDate[1].' 23:59:59';
      $query1 = "AND DATE_ADDED BETWEEN '$fromDate' AND '$toDate'";
      $query2 = "AND CREATED_ON BETWEEN '$fromDate' AND '$toDate'";
      $query3 = "AND a.CREATED_ON BETWEEN '$fromDate' AND '$toDate'";
    } else if($_REQUEST['fromDate'] == '' && $_REQUEST['toDate'] != '') {
      die;
    } else if($_REQUEST['fromDate'] != '' && $_REQUEST['toDate'] == '') {
      die;
    }
  }
  try {
    $db = getConnection();
    $sql = "SELECT * FROM TOUCHPOINT WHERE CAMPAIGN_ID=$id AND BANK_CODE='$bid' $query1";
	//echo $sql;
	
    if ($result = $db->query($sql)) {
      $table_data = '';
      $commu_data = '';
      $resp_data = '';
      while($data = $result->fetch_assoc()) {
        $commu_data = '';
        $resp_data = '';
        $tid = $data['TOUCHPOINT_ID'];
        $sql2 = "SELECT a.COMMUNICATION_ID,a.TOUCHPOINT_ID,a.CAMMUNICATION_NAME,a.TEMPLATE_TYPE FROM COMMUNICATION a WHERE a.TOUCHPOINT_ID=$tid $query1";
		//echo $sql2;
		
        $result2 = $db->query($sql2);
        while($data2 = $result2->fetch_assoc()) {
          $resp_data = '';
          $cid = $data2['COMMUNICATION_ID'];
          $sql3 = "SELECT * FROM CUSTOMER_RESPONSE a left outer join "
            . " CUSTOMER_EMPLOYEE_MAPPING cem ON a.CUSTOMER_ID = cem.CUSTOMER_ID  "
            . " WHERE cem.EMPLOYEE_ID = $uid and COMMUNICATION_ID=$cid $query2";

         // echo $sql3; exit;

          $result3 = $db->query($sql3);
          while($data3 = $result3->fetch_assoc()) {
            $resp_data[] = $data3;
          }
		  //print_r($resp_data);die;
          $data2['RESPONSE_DATA'] = $resp_data;
          //$commu_data[] = $data2;

          $leads_data = '';
          //$sql3 = "SELECT a.*,b.PRODUCT_NAME FROM LEADS a INNER JOIN PRODUCT b  ON a.PRODUCT_ID=b.PRODUCT_ID WHERE a.COMMUNCIATION_ID=$cid $query1";
		  $sql3 = "SELECT DISTINCT a.*,b.PRODUCT_NAME,cr.CUSTOMER_FNAME,cr.CUSTOMER_LNAME FROM LEADS a INNER JOIN PRODUCT b  ON a.PRODUCT_ID=b.PRODUCT_ID INNER JOIN CUSTOMER_RESPONSE cr ON a.CUSTOMER_ID = cr.CUSTOMER_ID LEFT OUTER JOIN  CUSTOMER_EMPLOYEE_MAPPING cem ON cr.CUSTOMER_ID = cem.CUSTOMER_ID WHERE cem.EMPLOYEE_ID = '$uid' and a.COMMUNCIATION_ID='$cid' $query1";
		 
		  //echo $sql3;
		   
          $result3 = $db->query($sql3);
          while($data3 = $result3->fetch_assoc()) {
            $data3['DATE_ADDED_FMTED'] = date("M d, Y h:i:s A",strtotime($data3['DATE_ADDED']));
            $leads_data[] = $data3;
          }
          $data2['LEADS_DATA'] = $leads_data;
          $commu_data[] = $data2;

          $resp_data = '';
          //$sql3 = "SELECT DISTINCT(a.CUSTOMER_EMAIL),a.CUSTOMER_FNAME FROM CUSTOMER_RESPONSE a INNER JOIN COMMUNICATION b ON a.COMMUNICATION_ID=b.COMMUNICATION_ID INNER JOIN CAMPAIGN c ON b.COMPAIGN_ID=c.CAMPAIGN_ID WHERE c.CAMPAIGN_ID=$id $query3";
          $sql2 = "SELECT DISTINCT(a.CUSTOMER_EMAIL),a.CUSTOMER_FNAME,DATEDIFF(NOW(), a.CUSTOMER_JOINING_DATE) AS CREATED_ON_DIFF FROM CUSTOMER_RESPONSE a WHERE a.BRANCH_ID='$branchId' ORDER BY CREATED_ON_DIFF";
		  
		  //echo $sql2;
		
		 /*  $sql_code="SELECT BRANCH_CODE FROM EMPLOYEE WHERE BRANCH_CODE='$branchId'";
  $resul = $db->query($sql_code);
  $code = $resul->fetch_assoc();
  $code1= $code['BRANCH_CODE']; */
          
		  $sql3 = " SELECT DISTINCT(a.CUSTOMER_ID),a.CUSTOMER_EMAIL,a.CUSTOMER_FNAME,a.CUSTOMER_LNAME,DATEDIFF(NOW(), a.CUSTOMER_JOINING_DATE) AS CREATED_ON_DIFF "
            . " FROM CUSTOMER_RESPONSE a left outer join "
            . " CUSTOMER_EMPLOYEE_MAPPING cem ON a.CUSTOMER_ID = cem.CUSTOMER_ID  "
            . " WHERE a.BRANCH_ID='$branchId' AND cem.EMPLOYEE_ID ='$uid' GROUP BY CUSTOMER_EMAIL ORDER BY CREATED_ON_DIFF"; 
			 //echo $sql3;

          //        $sql3 = " SELECT DISTINCT(a.CUSTOMER_EMAIL),a.CUSTOMER_FNAME,DATEDIFF(NOW(), a.CUSTOMER_JOINING_DATE) AS CREATED_ON_DIFF "
          //            . " FROM CUSTOMER_RESPONSE a left outer join "
          //            . " CUSTOMER_EMPLOYEE_MAPPING cem ON a.BRANCH_ID = cem.BRANCH_ID  "
          //            . " WHERE a.BRANCH_ID='$branchId' AND cem.EMPLOYEE_ID ='$uid' GROUP BY CUSTOMER_EMAIL ORDER BY CREATED_ON_DIFF";  
          //echo $sql3;

          /* $sql3 = " SELECT DISTINCT(a.CUSTOMER_EMAIL),a.CUSTOMER_FNAME,DATEDIFF(NOW(), a.CUSTOMER_JOINING_DATE) AS CREATED_ON_DIFF FROM CUSTOMER_RESPONSE a LEFT OUTER JOIN CUSTOMER_EMPLOYEE_MAPPING cem ON a.BRANCH_ID = cem.BRANCH_ID WHERE a.BRANCH_ID='$branchid' AND cem.EMPLOYEE_ID = '$uid' GROUP BY CUSTOMER_EMAIL ORDER BY CREATED_ON_DIFF";  */

          $result3 = $db->query($sql3);
          while($data3 = $result3->fetch_assoc()) {
            /* $datan = explode(' ',$data3['CREATED_ON']);
									$datan = explode('-',$datan[0]);
									$data3['CREATED_ON'] = $datan[1].'/'.$datan[2].'/'.$datan[0]; */
            $resp_data[] = $data3;
          }
          $data['MY_CUSTOMERS'] = $resp_data;

        }
        $data['COMMUNICATIN_DATA'] = $commu_data;
        $table_data[] = $data; 
      }
      $count=count($table_data);
      echo '{"TOUCHPOINT_DATA": ' . json_encode($table_data) . ',"TOUCHPOINT_COUNT": '.$count.'}';
      //echo $count;
    } else {
      echo '{"message":"Touchpoint Data Not Available"}'; 
    }
  } catch(Exception $e) {
    //echo '{"error":{"message":'. $e->getMessage() .'}}'; 
    echo '{"message":"Please Try Again Later"}'; 
  }
}
function AdminbankList() 
{
  $id = (int)$_REQUEST['id'];
  //echo $id;
  if($id == '') { echo '{"EMPLOYEE_DATA": {"EMPLOYEE_NAME":"","EMPLOYEE_ID":""}}'; die; }
  $sql = 'SELECT * FROM EMPLOYEE WHERE EMPLOYEE_ID=?';

  try {
    //echo $id;
    $db = getConnection();
    $stmt = $db->prepare($sql);  
    $stmt->bind_param("i", $id);
    $stmt->execute();
    $stmt->store_result();
    if($stmt->num_rows == '1') {
      $sql = "SELECT * FROM EMPLOYEE WHERE EMPLOYEE_ID='$id'" ;
      //echo $sql;
      if ($result = $db->query($sql)) {
        $data = $result->fetch_object();
        //$datan = explode('-',$data->DATE_OF_BIRTH);
        //$data->DATE_OF_BIRTH = $datan[1].'/'.$datan[2].'/'.$datan[0];
        echo '{"EMPLOYEE_DATA": ' . json_encode($data) . '}';
      } 
    } else {
      echo '{"message":"Employee Data Not Available"}'; 
    }
  } catch(Exception $e) {
    echo '{"error":{"message":'. $e->getMessage() .'}}'; 
    //echo '{"message":'.json_encode($db->error).'}';
    //echo '{"message":'.json_encode($db->error).'}';
  }

}	
function manageBanklist() {
  try {
    $db = getConnection();
    $sql = "SELECT DISTINCT a.FIRST_NAME,a.LAST_NAME,a.EMAIL,a.EMPLOYEE_ID,b.BANK_NAME FROM EMPLOYEE a INNER JOIN BANK b on a.BANK_CODE=b.BANK_CODE where role_id = 5 ORDER BY a.EMPLOYEE_ID DESC";
	
	/*$sql = "SELECT DISTINCT a.FIRST_NAME,a.LAST_NAME,a.EMAIL,a.EMPLOYEE_ID,b.BANK_NAME,c.BANK_NAME FROM EMPLOYEE a INNER JOIN BANK b on a.BANK_CODE=b.BANK_CODE INNER JOIN BANK c ON a.BANK_CODE=c.BANK_CODE where role_id = 5 ORDER BY a.EMPLOYEE_ID DESC";*/
	
    if ($result = $db->query($sql)) {
      $row = array();
      $i = 1;
      $table_data = '';
      while($data = $result->fetch_assoc()) {
        $action_data = '<a href="bank.html?id='.$data['EMPLOYEE_ID'].'"><i class="fa fa-pencil-square-o" title="Edit"></i></a>&nbsp;&nbsp;<a href="javascript:delete_id('.$data['EMPLOYEE_ID'].')"><i class="fa fa-trash-o" title="Delete"></i></a>';
        $table_data .= '<tr><td>'.$i.'</td><td  class="left-align" >'.$data['BANK_NAME'].'</td><td  class="left-align" style="text-transform: capitalize !important;">'.$data['LAST_NAME'].', '.$data['FIRST_NAME'].'</td><td  class="left-align" style="text-transform:none;" >'.$data['EMAIL'].'</td><td>'.$action_data.'</td></tr>';
        $i = $i + 1;
      }
      echo '{"BANKLIST": ' . json_encode($table_data) . '}';
    }
    $result->close();
  } catch(Exception $e) {
    //echo '{"error":{"message":'. $e->getMessage() .'}}'; 
    echo '{"message":"Please Try Again Later"}'; 
  }
}

//function updateBankadmin() {
//  $id = (int)$_REQUEST['id'];
//  if($id == '') { echo '{"EMPLOYEE_DATA": {"EMPLOYEE_NAME":"","EMPLOYEE_ID":""}}'; die; }
//  $sql = 'SELECT * FROM EMPLOYEE WHERE EMPLOYEE_ID=?';
//  try {
//    $db = getConnection();
//    $stmt = $db->prepare($sql);  
//    $stmt->bind_param("i", $id);
//    $stmt->execute();
//    $stmt->store_result();
//    if($stmt->num_rows == '1') {
//      echo "HAIII";
//      $sql = "SELECT * FROM EMPLOYEE WHERE EMPLOYEE_ID='$id'";
//      if ($result = $db->query($sql)) {
//        $data = $result->fetch_object();
//        $datan = explode('-',$data->DATE_OF_BIRTH);
//        $data->DATE_OF_BIRTH = $datan[1].'/'.$datan[2].'/'.$datan[0];
//        echo '{"EMPLOYEE_DATA": ' . json_encode($data) . '}';
//      } 
//    } else {
//      echo '{"message":"Employee Data Not Available"}'; 
//    }
//  } catch(Exception $e) {
//    //echo '{"error":{"message":'. $e->getMessage() .'}}'; 
//    echo '{"message":"Please Try Again Later"}'; 
//  }
//}
function updateAdmin() {
  $db = getConnection();
  $id		= $_REQUEST['id'];
  $bank_name = $_REQUEST['bankname']; 
  $bank_code = $_REQUEST['bankcode'];  
 // echo $bank_name;

  //$bankid="SELECT BANK_CODE FROM BANK WHERE BANK_NAME='$bank_name'";
  //$max= $db->query($bankid);
//echo $max;
  //$data = $max->fetch_assoc();
 // $bid=$data['BANK_CODE']; 
  //echo "Hai".$bid;


  $email			= addslashes($_REQUEST['email']);
  $password			= addslashes($_REQUEST['password']);
  $first_name		= addslashes($_REQUEST['firstname']);
  $last_name		= addslashes($_REQUEST['lastname']);
  // $dob			= addslashes($_REQUEST['dob']);
  // $phno			= addslashes($_REQUEST['phno']);
  //$mono			= addslashes($_REQUEST['mono']);
  // $gender			= addslashes($_REQUEST['gender']);
  // $country		= addslashes($_REQUEST['country']);
  /// $state			= addslashes($_REQUEST['state']);
  // $city			= addslashes($_REQUEST['city']);
  // $address		= addslashes($_REQUEST['address']);
  //$zip			= (int)addslashes($_REQUEST['zip']);

  //$dob = explode('/',$dob);
  // $dob = $dob[2].'-'.$dob[0].'-'.$dob[1];
  //$sql = 'SELECT * FROM EMPLOYEE WHERE EMAIL=?';

  try {
    //$db = getConnection();
    $sql_bank_name="update BANK set BANK_NAME='$bank_name' WHERE BANK_CODE='$bank_code'";
	//echo $sql_bank_name;
    $bankname_update=$db->query($sql_bank_name);
    $sql="update EMPLOYEE set FIRST_NAME='$first_name',LAST_NAME='$last_name',EMAIL='$email',PASSWORD='$password',DATE_ADDED=NOW(),DATE_UPDATED=NOW() where EMPLOYEE_ID='$id' ";

    //echo $sql;

    if ($db->query($sql)) {
      echo '{"message":"success"}';	
      $db = null; die;
    } else {
      echo '{"message":"'.json_encode($db->error).'"}'; 
    }
  } catch(Exception $e) {
    //echo '{"error":{"message":'. $e->getMessage() .'}}'; 
    echo '{"message":"'.json_encode($e->getMessage()).'"}'; 
  }	 
}



function forgetPassword() {
  $username = addslashes($_REQUEST['username']);
  $sql = 'SELECT * FROM EMPLOYEE WHERE EMAIL=? AND STATUS="1"';	
//  echo $username;
  try {
    $db = getConnection();
    $stmt = $db->prepare($sql);  
    $stmt->bind_param("s", $username);
    $stmt->execute();
    $stmt->store_result();
    if($stmt->num_rows == '1') {
      $stmt->close();
      $sql = "SELECT * FROM EMPLOYEE WHERE EMAIL='$username'";
      if ($result = $db->query($sql)) {
        while ($row = $result->fetch_assoc()) {          
		    require_once('../PHPMailer/PHPMailerAutoload.php');			
			 $mailSubject="Touchpoint - Password Request";
			$mailBody = '<html>
<head>
	<title></title>
	<meta charset="iso-8859-1" />


 <meta name="format-detection" content="telephone=no;address=no">


</head>
<body>
<img src="http://link.p0.com/1x1.dyn?0aEVvhe6ioIQmh2zqotPdeg2V=0" width="1" height="1" border="0">
<table cellpadding="0" cellspacing="0" border="0" width="100%" align="center">
	<tr>
		<td align="center" valign="top">
		<table class="width320" style="z-index: 10; font-family: Arial, Helvetica, sans-serif; color: #666666; font-size: 14px; line-height: 17px; min-width:640px;" width="640" border="0" cellspacing="0" cellpadding="0" align="center" valign="top">



<!-- MASTHEAD SECTION ======-->
<!--MOBILE MASTHEAD=-->
			<tr class="show" style="font-size: 0px; line-height: 0px; max-height: 0px; display: none;">
				<td class="showMast" style="font-size:0px; line-height: 0px; display:none; max-height:0px" align="left"><a href="http://link.alerts.usbank.com/u.d?T4GjfhjzoIyr9L8DPcr=21" target="_blank"><img class="showMast" border="0" src="http://image.email.usbank.com/lib/fe911570726d007475/m/2/Mobile_Masthead.jpg" style="font-size:0px;line-height:0px;display:none;max-height:0px" alt="U.S.&nbsp;Bank - All of US serving you&reg;" title="U.S.&nbsp;Bank - All of US serving you&reg;"></a></td>
			</tr>
<!--DESKTOP MASTHEAD-->
			<tr>
				<td class="hide" style="padding-bottom: 0px; margin-bottom: 0px;" width="640" align="left" valign="bottom">
				<div class="hide">
					<img class="hide" style="display:block" border="0" src="http://localhost:8080/tpv3/demo_new/images/header.jpg" width="640" height="65" alt="U.S.&nbsp;Bank - All of US serving you&reg;" title="">
				</div>
				</td>
			</tr>

<!--HERO AND BODY SECTION ==========-->
			<tr>
				<td class="width320" style="" align="left">
				<table class="width320" width="640" border="0" cellspacing="0" cellpadding="0">
					<tr>
						<td class="hide" style="line-height:0px; font-size:0px;" width="1" bgcolor="#696969" align="left"><img class="hide" height="1" src="http://image.email.usbank.com/lib/fe911570726d007475/m/1/spacer.gif" style="display:block" width="1"></td>
						<td align="left">
							<table class="bodyWidth320" width="638" border="0" cellspacing="0" cellpadding="0">
								<tr>
								<td bgcolor="#FFFFFF" width="638" align="left" valign="top" style="padding:20px;font-size: 14px; line-height: 17px; font-family: Arial, Helvetica, sans-serif; color:#666666;">
								Dear '.$row['LAST_NAME'].', '.$row['FIRST_NAME'].'<br><br>
								Kindly find your login details!</td>
								<br><br>
								</tr>
							</table>
						</td>
						<td class="hide" style="line-height:0px; font-size:0px;" width="1" bgcolor="#696969" align="left"><img class="hide" height="1" src="http://image.email.usbank.com/lib/fe911570726d007475/m/1/spacer.gif" style="display:block" width="1"></td>
					</tr>
					
					<tr>
						<td class="hide" style="line-height:0px; font-size:0px;" width="1" bgcolor="#696969" align="left"><img class="hide" height="1" src="http://image.email.usbank.com/lib/fe911570726d007475/m/1/spacer.gif" style="display:block" width="1"></td>
						<td align="left">
							<table class="bodyWidth320" width="100px" border="0" cellspacing="1" cellpadding="0">
								<tr>
								<td bgcolor="#FFFFFF"  width="25px" align="left" valign="top" style="padding:0 0 0 18px;font-size: 14px;  font-family: Arial, Helvetica, sans-serif; color:#666666;">
								Username</td><td width="2px">:</td><td width="25px">'.$username.'</td></tr>								
								<tr><td width="25px" bgcolor="#FFFFFF"  align="left" valign="top" style="padding:0 0 0 18px;font-size: 14px;  font-family: Arial, Helvetica, sans-serif; color:#666666;">
								Password</td><td width="2px">:</td><td width="25px">'.$row['PASSWORD'].'</td></tr>								
							</table>
						</td>
						<td class="hide" style="line-height:0px; font-size:0px;" width="1" bgcolor="#696969" align="left"><img class="hide" height="1" src="http://image.email.usbank.com/lib/fe911570726d007475/m/1/spacer.gif" style="display:block" width="1"></td>
					</tr>

					<tr>
						<td class="hide" style="line-height:0px; font-size:0px;" width="1" bgcolor="#696969" align="left"><img class="hide" height="1" src="http://image.email.usbank.com/lib/fe911570726d007475/m/1/spacer.gif" style="display:block" width="1"></td>
						<td align="left">
							<table class="bodyWidth320"  border="0" cellspacing="1" cellpadding="0">
								<tr>
								<td bgcolor="#FFFFFF"   align="left" valign="top" style="padding:0 0 0 18px;font-size: 14px;  font-family: Arial, Helvetica, sans-serif; color:#666666;">
								Login URL</td><td>:</td><td>http://localhost:8080/tpv3/demo_new/login.html</td></tr>
							</table>
						</td>
						<td class="hide" style="line-height:0px; font-size:0px;" width="1" bgcolor="#696969" align="left"><img class="hide" height="1" src="http://image.email.usbank.com/lib/fe911570726d007475/m/1/spacer.gif" style="display:block" width="1"></td>
					</tr>					
				</table>               
			
				
				<table class="width320" width="640" border="0" cellspacing="0" cellpadding="0">
					<tr>
						<td class="hide" style="line-height:0px; font-size:0px;" width="1" bgcolor="#696969" align="left"><img class="hide" height="1" src="http://image.email.usbank.com/lib/fe911570726d007475/m/1/spacer.gif" style="display:block" width="1"></td>
						<td align="left">
							<table class="bodyWidth320" width="638" border="0" cellspacing="0" cellpadding="0">
								<tr>
								<td bgcolor="#FFFFFF" width="638" align="left" valign="top" style="padding:20px;font-size: 14px; line-height: 17px; font-family: Arial, Helvetica, sans-serif; color:#666666;">
								Thanks,<br>
								Touchpoint - Team.
								</td>
								<br><br>
								</tr>
							</table>
						</td>
						<td class="hide" style="line-height:0px; font-size:0px;" width="1" bgcolor="#696969" align="left"><img class="hide" height="1" src="http://image.email.usbank.com/lib/fe911570726d007475/m/1/spacer.gif" style="display:block" width="1"></td>
					</tr>
				</table>
<!-- END HERO AND BODY SECTION ==========-->

<!--SOCIAL BAR SECTION =====-->
				<table class="width320" width="640" border="0" cellspacing="0" cellpadding="0">
					<tr>
						<td class="hide" width="640" style="line-height: 5px; line-height: 5px;" align="left">
						<table class="hide" width="640" cellpadding="0" cellspacing="0" border="0" bgcolor="#d9edf8">
							<tr>
								<td width="79" align="left"><img border="0" style="display:block" src="http://localhost:8080/tpv3/demo_new/images/footer.jpg" alt="Find us on:" title="Find us on:"></td>
							</tr>
						</table>
						</td>
					</tr>

<!--MOBILE SOCIAL BAR =========-->
					<tr class="show" style="font-size:0px; line-height: 0px; display:none; visibility:hidden; border-collapse:collapse; max-height: 0px">
						<td class="show" style="font-size:0px; line-height: 0px; display:none; visibility:hidden; border-collapse:collapse; max-height: 0px" align="left">
						<div class="show" style="max-height: 0px; font-size:0px; line-height: 0px; display:none; visibility:hidden; border-collapse:collapse;">
							<table class="show" style="font-size:0px; line-height: 0px; display:none; visibility:hidden; border-collapse:collapse; max-height: 0px" cellpadding="0" cellspacing="0" border="0">

<!--FIRST ROW =========-->
								<tr class="show" style="font-size:0px; line-height: 0px; display:none; visibility:hidden; border-collapse:collapse; max-height: 0px">
									<td align="left">
									<div class="show" style="max-height: 0px; font-size:0px; line-height: 0px; display:none; visibility:hidden; border-collapse:collapse;">
										<table class="show" style="font-size:0px; line-height: 0px; display:none; visibility:hidden; border-collapse:collapse; max-height: 0px" cellpadding="0" cellspacing="0" border="0">
											<tr>
												<td align="left">
												<div class="show" style="max-height: 0px; font-size:0px; line-height: 0px; display:none; visibility:hidden; border-collapse:collapse;">
													<img class="show" style="font-size:0px;line-height:0px;display:none;max-height:0px" src="http://image.email.usbank.com/lib/fe911570726d007475/m/2/social_footer_mobile_01.jpg" border="0">
												</div>
												</td>
												<td align="left">
												<div class="show" style="max-height: 0px; font-size:0px; line-height: 0px; display:none; visibility:hidden; border-collapse:collapse;">
													<a href="http://link.alerts.usbank.com/u.d?Y4GjfhjzoIyr9Lv91=63"><img class="show" style="font-size:0px;line-height:0px;display:none;max-height:0px" src="http://image.email.usbank.com/lib/fe911570726d007475/m/2/social_footer_mobile_02.jpg" border="0"></a>
												</div>
												</td>
												<td align="left">
												<div class="show" style="max-height: 0px; font-size:0px; line-height: 0px; display:none; visibility:hidden; border-collapse:collapse;">
													<a href="http://link.alerts.usbank.com/u.d?Y4GjfhjzoIyr9Lv9O=73"><img class="show" style="font-size:0px;line-height:0px;display:none;max-height:0px" src="http://image.email.usbank.com/lib/fe911570726d007475/m/2/social_footer_mobile_03.jpg" border="0"></a>
												</div>
												</td>
												<td align="left">
												<div class="show" style="max-height: 0px; font-size:0px; line-height: 0px; display:none; visibility:hidden; border-collapse:collapse;">
													<a href="http://link.alerts.usbank.com/u.d?C4GjfhjzoIyr9Lv9D=83"><img class="show" style="font-size:0px;line-height:0px;display:none;max-height:0px" src="http://image.email.usbank.com/lib/fe911570726d007475/m/2/social_footer_mobile_04.jpg" border="0" alt="YouTube" title="YouTube"></a>
												</div>
												</td>
											</tr>
										</table>
									</div>
									</td>

<!--SECOND ROW =========-->
								<tr class="show" style="font-size:0px; line-height: 0px; display:none; visibility:hidden; border-collapse:collapse; max-height: 0px">
									<td align="left">
									<div class="show" style="max-height: 0px; font-size:0px; line-height: 0px; display:none; visibility:hidden; border-collapse:collapse;">
										<table class="show" style="font-size:0px; line-height: 0px; display:none; visibility:hidden; border-collapse:collapse; max-height: 0px" cellpadding="0" cellspacing="0" border="0">
											<tr>
												<td align="left">
												<div class="show" style="max-height: 0px; font-size:0px; line-height: 0px; display:none; visibility:hidden; border-collapse:collapse;">
													<a href="http://link.alerts.usbank.com/u.d?H4GjfhjzoIyr9Lv9E=93"><img class="show" style="font-size:0px;line-height:0px;display:none;max-height:0px" src="http://image.email.usbank.com/lib/fe911570726d007475/m/2/social_footer_mobile_05.jpg" border="0" alt="Branch Locator" title="Branch Locator"></a>
												</div>
												</td>
												<td align="left">
												<div class="show" style="max-height: 0px; font-size:0px; line-height: 0px; display:none; visibility:hidden; border-collapse:collapse;">
													<a href="http://link.alerts.usbank.com/u.d?H4GjfhjzoIyr9Lv9Z=103"><img class="show" style="font-size:0px;line-height:0px;display:none;max-height:0px" src="http://image.email.usbank.com/lib/fe911570726d007475/m/2/social_footer_mobile_06.jpg" border="0" alt="usbank.com" title="usbank.com"></a>
												</div>
												</td>
											</tr>
										</table>
									</div>
									</td>
								</tr>

<!--THIRD ROW =========-->
								<tr class="show" style="font-size:0px; line-height: 0px; display:none; visibility:hidden; border-collapse:collapse; max-height: 0px">
									<td class="show" style="font-size:0px; line-height: 0px; display:none;" align="left">
									<div class="show" style="max-height: 0px; font-size:0px; line-height: 0px; display:none; visibility:hidden; border-collapse:collapse; max-height: 0px">
										<table class="show" style="font-size:0px; line-height: 0px; display:none;" cellpadding="0" cellspacing="0" border="0">
											<tr>
												<td align="left">
												<div class="show" style="max-height: 0px; font-size:0px; line-height: 0px; display:none; visibility:hidden; border-collapse:collapse; max-height: 0px">
													<img class="show" style="font-size:0px;line-height:0px;display:none;max-height:0px" src="http://image.email.usbank.com/lib/fe911570726d007475/m/2/social_footer_mobile_07.jpg" border="0" alt="800-US-BANKS (800-872-2657)" title="800-US-BANKS (800-872-2657)">
												</div>
												</td>
												<td class="show" style="max-height: 0px; font-size:0px; line-height: 0px; display:none; visibility:hidden; border-collapse:collapse; max-height: 0px" align="left"><a href="http://link.alerts.usbank.com/u.d?QYGjfhjzoIyr9Lv9S=113" target="_blank"><img class="show" style="font-size:0px;line-height:0px;display:none;max-height:0px" src="http://image.email.usbank.com/lib/fe911570726d007475/m/2/social_footer_mobile_08.jpg" border="0" alt="U.S.&nbsp;Bank Mobile" title="U.S.&nbsp;Bank Mobile"></a></td>
											</tr>
										</table>
									</div>
									</td>
								</tr>
							</table>
						</div>
						</td>
					</tr>
				</table>
<!-- End Social Footer -->


				</td>
			</tr>
		</table>
		</td>
	</tr>
</table>
</body>
</html>
';
			$mail = new PHPMailer();         
			$mail->Subject   =	$mailSubject;   		 
			$mail->isSMTP();
			$mail->SMTPDebug = 0;
			$mail->Debugoutput = 'html';
			$mail->Host = 'smtp.gmail.com';
			$mail->Port = 587;
			$mail->SMTPSecure = 'tls';
			$mail->SMTPAuth = true;
			$mail->Username = "touchpointdemo16@gmail.com";
//$mail->Password = "bvacobpmasflawhs";							
$mail->Password = "touchpoint@123";
$mail->setFrom('touchpointdemo16@gmail.com', 'Touchpoint');
			$mail->Subject   =	$mailSubject;
			$mail->MsgHTML($mailBody);
			$mail->AddAddress($row['EMAIL']);
		  
          if(!$mail->Send()) {
            $res['message'] = $mail->ErrorInfo;
          } else {
            $res['message'] = 'success';
          }	
		 // $res['message'] = 'success';
          // print_r($res); exit;

          echo '{"USER": ' . json_encode($res) . '}';
          $db = null; die;
        }
      }
      $result->close();
      echo '{"USER": {"message":"Username Not Exist"}}'; $db = null;	die;
    } else {
      echo '{"USER": {"message":"Username Not Exist"}}'; 
    }
  } catch(Exception $e) {
    //echo '{"error":{"message":'. $e->getMessage() .'}}'; 
    echo '{"message":"Please Try Again Later"}'; 
  }
}



function addAdmin() {

  $db = getConnection();
  $bank_name = $_REQUEST['bankname'];
  $bank_code = $_REQUEST['bankcode'];
  //echo $bank_name;
  $sql1="INSERT INTO BANK(BANK_NAME,BANK_CODE)VALUES('$bank_name','$bank_code')";
 
  $reslut=$db->query($sql1);
 

  $role_name		= "5";
  $email			= addslashes($_REQUEST['email']);
  $password		= addslashes($_REQUEST['password']);
  $first_name		= addslashes($_REQUEST['firstname']);
  $last_name		= addslashes($_REQUEST['lastname']);
  // $dob			= addslashes($_REQUEST['dob']);
  // $phno			= addslashes($_REQUEST['phno']);
  //$mono			= addslashes($_REQUEST['mono']);
  // $gender			= addslashes($_REQUEST['gender']);
  // $country		= addslashes($_REQUEST['country']);
  /// $state			= addslashes($_REQUEST['state']);
  // $city			= addslashes($_REQUEST['city']);
  // $address		= addslashes($_REQUEST['address']);
  //$zip			= (int)addslashes($_REQUEST['zip']);

  //$dob = explode('/',$dob);
  // $dob = $dob[2].'-'.$dob[0].'-'.$dob[1];
  //$sql = 'SELECT * FROM EMPLOYEE WHERE EMAIL=?';

  try {
   $db = getConnection();
    $sql = "INSERT INTO EMPLOYEE (BANK_ID,ROLE_ID,FIRST_NAME,LAST_NAME,EMAIL,PASSWORD,DATE_ADDED,DATE_UPDATED,STATUS,MANAGE_BRANCH,MANAGE_EMPLOYEE,ENDPOINT,MANAGE_PRODUCT,BANK_CODE) 
	VALUES ('','$role_name','$first_name','$last_name','$email','$password',NOW(),NOW(), '1','1','1','1','1','$bank_code')";
   // echo $sql;
    if ($db->query($sql)) {
      echo '{"message":"success"}';	
	 
	   require_once('../PHPMailer/PHPMailerAutoload.php');
	  $mailSubject="Touchpoint - Login Credentials";
	  $customerEmail=$email;
	  $mailBody = '<html>
<head>
	<title></title>
	<meta charset="iso-8859-1" />


 <meta name="format-detection" content="telephone=no;address=no">


</head>
<body>
<img src="http://link.p0.com/1x1.dyn?0aEVvhe6ioIQmh2zqotPdeg2V=0" width="1" height="1" border="0">
<table cellpadding="0" cellspacing="0" border="0" width="100%" align="center">
	<tr>
		<td align="center" valign="top">
		<table class="width320" style="z-index: 10; font-family: Arial, Helvetica, sans-serif; color: #666666; font-size: 14px; line-height: 17px; min-width:640px;" width="640" border="0" cellspacing="0" cellpadding="0" align="center" valign="top">



<!-- MASTHEAD SECTION ======-->
<!--MOBILE MASTHEAD=-->
			<tr class="show" style="font-size: 0px; line-height: 0px; max-height: 0px; display: none;">
				<td class="showMast" style="font-size:0px; line-height: 0px; display:none; max-height:0px" align="left"><a href="http://link.alerts.usbank.com/u.d?T4GjfhjzoIyr9L8DPcr=21" target="_blank"><img class="showMast" border="0" src="http://image.email.usbank.com/lib/fe911570726d007475/m/2/Mobile_Masthead.jpg" style="font-size:0px;line-height:0px;display:none;max-height:0px" alt="U.S.&nbsp;Bank - All of US serving you&reg;" title="U.S.&nbsp;Bank - All of US serving you&reg;"></a></td>
			</tr>
<!--DESKTOP MASTHEAD-->
			<tr>
				<td class="hide" style="padding-bottom: 0px; margin-bottom: 0px;" width="640" align="left" valign="bottom">
				<div class="hide">
					<img class="hide" style="display:block" border="0" src="http://localhost:8080/tpv3/demo_new/images/header.jpg" width="640" height="65" alt="U.S.&nbsp;Bank - All of US serving you&reg;" title="">
				</div>
				</td>
			</tr>

<!--HERO AND BODY SECTION ==========-->
			<tr>
				<td class="width320" style="" align="left">
				<table class="width320" width="640" border="0" cellspacing="0" cellpadding="0">
					<tr>
						<td class="hide" style="line-height:0px; font-size:0px;" width="1" bgcolor="#696969" align="left"><img class="hide" height="1" src="http://image.email.usbank.com/lib/fe911570726d007475/m/1/spacer.gif" style="display:block" width="1"></td>
						<td align="left">
							<table class="bodyWidth320" width="638" border="0" cellspacing="0" cellpadding="0">
								<tr>
								<td bgcolor="#FFFFFF" width="638" align="left" valign="top" style="padding:20px;font-size: 14px; line-height: 17px; font-family: Arial, Helvetica, sans-serif; color:#666666;">
								Dear '.$last_name.', '.$first_name.'<br><br>
								Kindly find your login details!</td>
								<br><br>
								</tr>
							</table>
						</td>
						<td class="hide" style="line-height:0px; font-size:0px;" width="1" bgcolor="#696969" align="left"><img class="hide" height="1" src="http://image.email.usbank.com/lib/fe911570726d007475/m/1/spacer.gif" style="display:block" width="1"></td>
					</tr>
					
					<tr>
						<td class="hide" style="line-height:0px; font-size:0px;" width="1" bgcolor="#696969" align="left"><img class="hide" height="1" src="http://image.email.usbank.com/lib/fe911570726d007475/m/1/spacer.gif" style="display:block" width="1"></td>
						<td align="left">
							<table class="bodyWidth320" width="100px" border="0" cellspacing="1" cellpadding="0">
								<tr>
								<td bgcolor="#FFFFFF"  width="25px" align="left" valign="top" style="padding:0 0 0 18px;font-size: 14px;  font-family: Arial, Helvetica, sans-serif; color:#666666;">
								Username</td><td width="2px">:</td><td width="25px">'.$email.'</td></tr>								
								<tr><td width="25px" bgcolor="#FFFFFF"  align="left" valign="top" style="padding:0 0 0 18px;font-size: 14px;  font-family: Arial, Helvetica, sans-serif; color:#666666;">
								Password</td><td width="2px">:</td><td width="25px">'.$password.'</td></tr>								
							</table>
						</td>
						<td class="hide" style="line-height:0px; font-size:0px;" width="1" bgcolor="#696969" align="left"><img class="hide" height="1" src="http://image.email.usbank.com/lib/fe911570726d007475/m/1/spacer.gif" style="display:block" width="1"></td>
					</tr>

					<tr>
						<td class="hide" style="line-height:0px; font-size:0px;" width="1" bgcolor="#696969" align="left"><img class="hide" height="1" src="http://image.email.usbank.com/lib/fe911570726d007475/m/1/spacer.gif" style="display:block" width="1"></td>
						<td align="left">
							<table class="bodyWidth320"  border="0" cellspacing="1" cellpadding="0">
								<tr>
								<td bgcolor="#FFFFFF"   align="left" valign="top" style="padding:0 0 0 18px;font-size: 14px;  font-family: Arial, Helvetica, sans-serif; color:#666666;">
								Login URL</td><td>:</td><td>http://localhost:8080/tpv3/demo_new/login.html</td></tr>
							</table>
						</td>
						<td class="hide" style="line-height:0px; font-size:0px;" width="1" bgcolor="#696969" align="left"><img class="hide" height="1" src="http://image.email.usbank.com/lib/fe911570726d007475/m/1/spacer.gif" style="display:block" width="1"></td>
					</tr>					
				</table>               
			
				
				<table class="width320" width="640" border="0" cellspacing="0" cellpadding="0">
					<tr>
						<td class="hide" style="line-height:0px; font-size:0px;" width="1" bgcolor="#696969" align="left"><img class="hide" height="1" src="http://image.email.usbank.com/lib/fe911570726d007475/m/1/spacer.gif" style="display:block" width="1"></td>
						<td align="left">
							<table class="bodyWidth320" width="638" border="0" cellspacing="0" cellpadding="0">
								<tr>
								<td bgcolor="#FFFFFF" width="638" align="left" valign="top" style="padding:20px;font-size: 14px; line-height: 17px; font-family: Arial, Helvetica, sans-serif; color:#666666;">
								Thanks,<br>
								Touchpoint - Team.
								</td>
								<br><br>
								</tr>
							</table>
						</td>
						<td class="hide" style="line-height:0px; font-size:0px;" width="1" bgcolor="#696969" align="left"><img class="hide" height="1" src="http://image.email.usbank.com/lib/fe911570726d007475/m/1/spacer.gif" style="display:block" width="1"></td>
					</tr>
				</table>
<!-- END HERO AND BODY SECTION ==========-->

<!--SOCIAL BAR SECTION =====-->
				<table class="width320" width="640" border="0" cellspacing="0" cellpadding="0">
					<tr>
						<td class="hide" width="640" style="line-height: 5px; line-height: 5px;" align="left">
						<table class="hide" width="640" cellpadding="0" cellspacing="0" border="0" bgcolor="#d9edf8">
							<tr>
								<td width="79" align="left"><img border="0" style="display:block" src="http://localhost:8080/tpv3/demo_new/images/footer.jpg" alt="Find us on:" title="Find us on:"></td>
							</tr>
						</table>
						</td>
					</tr>

<!--MOBILE SOCIAL BAR =========-->
					<tr class="show" style="font-size:0px; line-height: 0px; display:none; visibility:hidden; border-collapse:collapse; max-height: 0px">
						<td class="show" style="font-size:0px; line-height: 0px; display:none; visibility:hidden; border-collapse:collapse; max-height: 0px" align="left">
						<div class="show" style="max-height: 0px; font-size:0px; line-height: 0px; display:none; visibility:hidden; border-collapse:collapse;">
							<table class="show" style="font-size:0px; line-height: 0px; display:none; visibility:hidden; border-collapse:collapse; max-height: 0px" cellpadding="0" cellspacing="0" border="0">

<!--FIRST ROW =========-->
								<tr class="show" style="font-size:0px; line-height: 0px; display:none; visibility:hidden; border-collapse:collapse; max-height: 0px">
									<td align="left">
									<div class="show" style="max-height: 0px; font-size:0px; line-height: 0px; display:none; visibility:hidden; border-collapse:collapse;">
										<table class="show" style="font-size:0px; line-height: 0px; display:none; visibility:hidden; border-collapse:collapse; max-height: 0px" cellpadding="0" cellspacing="0" border="0">
											<tr>
												<td align="left">
												<div class="show" style="max-height: 0px; font-size:0px; line-height: 0px; display:none; visibility:hidden; border-collapse:collapse;">
													<img class="show" style="font-size:0px;line-height:0px;display:none;max-height:0px" src="http://image.email.usbank.com/lib/fe911570726d007475/m/2/social_footer_mobile_01.jpg" border="0">
												</div>
												</td>
												<td align="left">
												<div class="show" style="max-height: 0px; font-size:0px; line-height: 0px; display:none; visibility:hidden; border-collapse:collapse;">
													<a href="http://link.alerts.usbank.com/u.d?Y4GjfhjzoIyr9Lv91=63"><img class="show" style="font-size:0px;line-height:0px;display:none;max-height:0px" src="http://image.email.usbank.com/lib/fe911570726d007475/m/2/social_footer_mobile_02.jpg" border="0"></a>
												</div>
												</td>
												<td align="left">
												<div class="show" style="max-height: 0px; font-size:0px; line-height: 0px; display:none; visibility:hidden; border-collapse:collapse;">
													<a href="http://link.alerts.usbank.com/u.d?Y4GjfhjzoIyr9Lv9O=73"><img class="show" style="font-size:0px;line-height:0px;display:none;max-height:0px" src="http://image.email.usbank.com/lib/fe911570726d007475/m/2/social_footer_mobile_03.jpg" border="0"></a>
												</div>
												</td>
												<td align="left">
												<div class="show" style="max-height: 0px; font-size:0px; line-height: 0px; display:none; visibility:hidden; border-collapse:collapse;">
													<a href="http://link.alerts.usbank.com/u.d?C4GjfhjzoIyr9Lv9D=83"><img class="show" style="font-size:0px;line-height:0px;display:none;max-height:0px" src="http://image.email.usbank.com/lib/fe911570726d007475/m/2/social_footer_mobile_04.jpg" border="0" alt="YouTube" title="YouTube"></a>
												</div>
												</td>
											</tr>
										</table>
									</div>
									</td>

<!--SECOND ROW =========-->
								<tr class="show" style="font-size:0px; line-height: 0px; display:none; visibility:hidden; border-collapse:collapse; max-height: 0px">
									<td align="left">
									<div class="show" style="max-height: 0px; font-size:0px; line-height: 0px; display:none; visibility:hidden; border-collapse:collapse;">
										<table class="show" style="font-size:0px; line-height: 0px; display:none; visibility:hidden; border-collapse:collapse; max-height: 0px" cellpadding="0" cellspacing="0" border="0">
											<tr>
												<td align="left">
												<div class="show" style="max-height: 0px; font-size:0px; line-height: 0px; display:none; visibility:hidden; border-collapse:collapse;">
													<a href="http://link.alerts.usbank.com/u.d?H4GjfhjzoIyr9Lv9E=93"><img class="show" style="font-size:0px;line-height:0px;display:none;max-height:0px" src="http://image.email.usbank.com/lib/fe911570726d007475/m/2/social_footer_mobile_05.jpg" border="0" alt="Branch Locator" title="Branch Locator"></a>
												</div>
												</td>
												<td align="left">
												<div class="show" style="max-height: 0px; font-size:0px; line-height: 0px; display:none; visibility:hidden; border-collapse:collapse;">
													<a href="http://link.alerts.usbank.com/u.d?H4GjfhjzoIyr9Lv9Z=103"><img class="show" style="font-size:0px;line-height:0px;display:none;max-height:0px" src="http://image.email.usbank.com/lib/fe911570726d007475/m/2/social_footer_mobile_06.jpg" border="0" alt="usbank.com" title="usbank.com"></a>
												</div>
												</td>
											</tr>
										</table>
									</div>
									</td>
								</tr>

<!--THIRD ROW =========-->
								<tr class="show" style="font-size:0px; line-height: 0px; display:none; visibility:hidden; border-collapse:collapse; max-height: 0px">
									<td class="show" style="font-size:0px; line-height: 0px; display:none;" align="left">
									<div class="show" style="max-height: 0px; font-size:0px; line-height: 0px; display:none; visibility:hidden; border-collapse:collapse; max-height: 0px">
										<table class="show" style="font-size:0px; line-height: 0px; display:none;" cellpadding="0" cellspacing="0" border="0">
											<tr>
												<td align="left">
												<div class="show" style="max-height: 0px; font-size:0px; line-height: 0px; display:none; visibility:hidden; border-collapse:collapse; max-height: 0px">
													<img class="show" style="font-size:0px;line-height:0px;display:none;max-height:0px" src="http://image.email.usbank.com/lib/fe911570726d007475/m/2/social_footer_mobile_07.jpg" border="0" alt="800-US-BANKS (800-872-2657)" title="800-US-BANKS (800-872-2657)">
												</div>
												</td>
												<td class="show" style="max-height: 0px; font-size:0px; line-height: 0px; display:none; visibility:hidden; border-collapse:collapse; max-height: 0px" align="left"><a href="http://link.alerts.usbank.com/u.d?QYGjfhjzoIyr9Lv9S=113" target="_blank"><img class="show" style="font-size:0px;line-height:0px;display:none;max-height:0px" src="http://image.email.usbank.com/lib/fe911570726d007475/m/2/social_footer_mobile_08.jpg" border="0" alt="U.S.&nbsp;Bank Mobile" title="U.S.&nbsp;Bank Mobile"></a></td>
											</tr>
										</table>
									</div>
									</td>
								</tr>
							</table>
						</div>
						</td>
					</tr>
				</table>
<!-- End Social Footer -->


				</td>
			</tr>
		</table>
		</td>
	</tr>
</table>
</body>
</html>';		
	$mail = new PHPMailer();
	$mail->isSMTP();
	$mail->SMTPDebug = 0;
	$mail->Debugoutput = 'html';
	$mail->Host = 'smtp.gmail.com';
	$mail->Port = 587;
	$mail->SMTPSecure = 'tls';
	$mail->SMTPAuth = true;
	$mail->Username = "touchpointdemo16@gmail.com";
	//$mail->Password = "bvacobpmasflawhs";							
	$mail->Password = "touchpoint@123";
	$mail->setFrom('touchpointdemo16@gmail.com', 'Touchpoint');
	$mail->Subject   =	$mailSubject;
	$mail->MsgHTML($mailBody);
	$mail->AddAddress($customerEmail);
		if(!$mail->Send()) 
		{
		  echo 'Message was not sent.';
		  echo 'Mailer error: ' . $mail->ErrorInfo;
		 
		}
		else 
		{
		 echo "Mail Send...";
		 
		}
	   
      $db = null; die;
    } else {
      echo '{"message":"'.json_encode($db->error).'"}'; 
    }
  } catch(Exception $e) {
    //echo '{"error":{"message":'. $e->getMessage() .'}}'; 
    echo '{"message":"'.json_encode($e->getMessage()).'"}'; 
  }	 

}

function customerTouchpoints() {
  $employeeId = addslashes($_REQUEST['eid']);
  $customerId = addslashes($_REQUEST['cid']);
  $bankId = addslashes($_REQUEST['bankId']);
  $branchId = addslashes($_REQUEST['branchId']);
  try {
    $db = getConnection();
	/* $sql_code="SELECT BRANCH_CODE, BANK_CODE FROM EMPLOYEE WHERE BRANCH_CODE='$branchId'";
  $resul = $db->query($sql_code);
  $code = $resul->fetch_assoc();
  $code1= $code['BRANCH_CODE']; 
  $code2= $code['BANK_CODE']; */
  
    $sql = "SELECT DISTINCT(c.TOUCHPOINT_ID),c.TOUCHPOINT_NAME FROM CUSTOMER_RESPONSE a INNER JOIN COMMUNICATION b ON a.COMMUNICATION_ID=b.COMMUNICATION_ID INNER JOIN TOUCHPOINT c ON c.TOUCHPOINT_ID=b.TOUCHPOINT_ID WHERE a.CUSTOMER_ID='$customerId' AND a.BRANCH_ID='$branchId' AND a.BANK_ID='$bankId'";
	//echo $sql;

    if ($result = $db->query($sql)) {
      $table_data = '';
      while($data = $result->fetch_assoc()) {

        $table_data[] = $data; 
      }
      $count= count($table_data);
      echo '{"TOUCHPOINTS": ' . json_encode($table_data) . ',"TOUCHPOINT_COUNT": ' .$count.'}';

      //echo count($table_data);
      //Print_r($table_data[0]);

      /* $arrays = (array) $table_data['TOUCHPOINTS'];
		$eee=count($arrays);
		echo $eee; */


    } else {
      echo '{"TOUCHPOINTS":"Touchpoint Data Not Available"}'; 
    }
  } catch(Exception $e) {
    //echo '{"error":{"message":'. $e->getMessage() .'}}'; 
    echo '{"message":"Please Try Again Later"}'; 
  }
}

function getCommunication() {
  $cid = (int)addslashes($_REQUEST['cid']);
  try {
    $db = getConnection();
     $sql = "SELECT a.*,b.TEMPLATE_NAME FROM COMMUNICATION a INNER JOIN TEMPLATE b on a.TEMPLATE_ID=b.TEMPLATE_ID WHERE a.COMMUNICATION_ID=$cid";
    if ($result = $db->query($sql)) {
      $table_data = array();
      while($data = $result->fetch_assoc()) {
        $data['USER_ACTIONS'] = json_decode($data['USER_ACTIONS']);
        $table_data[] = $data; 
       // print_r($table_data);
      }
      echo '{"COMMUNICATION_DATA": ' . json_encode($table_data) . '}';
    } else {
      echo '{"message":"Communication Data Not Available"}'; 
    }
  } catch(Exception $e) {
    //echo '{"error":{"message":'. $e->getMessage() .'}}'; 
    echo '{"message":"Please Try Again Later"}'; 
  }
}
function communicationList() {

  $cid = (int)addslashes($_REQUEST['campaignId']);
  $tid = (int)addslashes($_REQUEST['touchpointId']);
 
  try {
    $db = getConnection();
    $sql = "SELECT * FROM COMMUNICATION WHERE COMPAIGN_ID='$cid' AND TOUCHPOINT_ID='$tid'";
	//echo $sql;
    if ($result = $db->query($sql)) {
      $table_data = array();
      while($data = $result->fetch_assoc()) {
        $table_data[] = $data; 
      }
      echo '{"COMMUNICATION_DATA": ' . json_encode($table_data) . '}';
    } else {
      echo '{"message":"Communication Data Not Available"}'; 
    }
  } catch(Exception $e) {
    //echo '{"error":{"message":'. $e->getMessage() .'}}'; 
    echo '{"message":"Please Try Again Later"}'; 
  }
}
function employeeActivityList() {
  $id = (int)addslashes($_REQUEST['id']);
  //$group = addslashes($_REQUEST['group']);
  try {
    $db = getConnection();
    $groupQuery = '';
    $date = date('Y-m-d');

    if($_REQUEST['groupBy'] == 'w') {
      $groupQuery = " AND WEEK(z.CREATED_ON) = WEEK('$date') AND YEAR(z.CREATED_ON) = YEAR('$date')";
    } else if($_REQUEST['groupBy'] == 'm') {
      $groupQuery = " AND MONTH(z.CREATED_ON) = MONTH('$date') AND YEAR(z.CREATED_ON) = YEAR('$date')";
    } else if($_REQUEST['groupBy'] == 'y') {
      $groupQuery = " AND YEAR(z.CREATED_ON) = YEAR('$date')";
    } 
    $sql = "SELECT DISTINCT(a.ACTIVITY_NAME) AS ACTIVITY_NAME, b.CAMMUNICATION_NAME, (SELECT COUNT(z.ACTIVITY_NAME)  FROM EMPLOYEE_ACTIVITIES z WHERE z.EMPLOYEE_ID='$id' AND z.ACTIVITY_NAME=a.ACTIVITY_NAME $groupQuery AND z.COMMUNICATION_ID=a.COMMUNICATION_ID) AS ACTIVITY_COUNT FROM EMPLOYEE_ACTIVITIES a INNER JOIN COMMUNICATION b ON a.COMMUNICATION_ID=b.COMMUNICATION_ID WHERE a.EMPLOYEE_ID='$id'";
    if ($result = $db->query($sql)) {
      $table_data = array();
      $cdata = '';
      while($data = $result->fetch_assoc()) {	
        $table_data[] = $data; 
      }
      echo '{"COMMUNICATION_DATA": ' . json_encode($table_data) . '}';
    } else {
      echo '{"message":"Communication Data Not Available"}'; 
      echo $db->error;
    }
  } catch(Exception $e) {
    echo '{"error":{"message":'. $e->getMessage() .'}}'; 
    //echo '{"message":"Please Try Again Later"}'; 
  }
}
function templateList() {

  $type = addslashes($_REQUEST['tempType']);
  $bankId = $_REQUEST['bankId'];

  $inQuery = '';
  $searchQuery = '';
  if(isset($_REQUEST['fromDate']) && isset($_REQUEST['toDate'])) {
    if($_REQUEST['fromDate'] != '' && $_REQUEST['toDate'] != '') {
      $fromDate = addslashes($_REQUEST['fromDate']);
      $toDate = addslashes($_REQUEST['toDate']);
      $fromDate = explode('/',$fromDate);
      $fromDate = $fromDate[2].'-'.$fromDate[0].'-'.$fromDate[1].' 00:00:00';
      $toDate = explode('/',$toDate);
      $toDate = $toDate[2].'-'.$toDate[0].'-'.$toDate[1].' 23:59:59';	
      $inQuery = " AND DATE_ADDED BETWEEN '".$fromDate."' AND '".$toDate."'";
    }
  }
  if(isset($_REQUEST['searchText'])) {
    if($_REQUEST['searchText'] != '') {
      $searchText = addslashes($_REQUEST['searchText']);
      $searchQuery = " AND TEMPLATE_NAME LIKE '%".$searchText."%'";
    }
  }
  try {
    $db = getConnection();
    //$sql = "SELECT * FROM TEMPLATE WHERE BANK_ID='$bankId' AND TEMPLATE_TYPE='$type'";


    $sql = "SELECT * FROM TEMPLATE WHERE BANK_CODE='$bankId' AND TEMPLATE_TYPE='".$type."' $inQuery $searchQuery";
    if ($result = $db->query($sql)) {

      $table_data = array();
      while($data = $result->fetch_assoc()) {

        //$data['TEMPLATE_CONTENT'] = $data['TEMPLATE_CONTENT'];
        $table_data[] = $data; 
      }
      echo '{"TEMPLATE_DATA": ' . json_encode($table_data) . '}';
    } else {
      echo '{"message":"Template Data Not Available"}'; 
    }
  } catch(Exception $e) {
    //echo '{"error":{"message":'. $e->getMessage() .'}}'; 
    echo '{"message":"Please Try Again Later"}'; 
  }
}
function addCampaign() { 
  $userId = addslashes($_REQUEST['userId']);
  $bankId = addslashes($_REQUEST['bankId']);
  $campaignName = addslashes($_REQUEST['campaignName']);
  $fromDate = addslashes($_REQUEST['fromDate']);
  $toDate = addslashes($_REQUEST['toDate']);

  $fromDate = explode('/',$fromDate);
  $fromDate = $fromDate[2].'-'.$fromDate[0].'-'.$fromDate[1];
  $toDate = explode('/',$toDate);
  $toDate = $toDate[2].'-'.$toDate[0].'-'.$toDate[1];	

   $sql = 'SELECT * FROM CAMPAIGN WHERE CAMPAIGN_NAME=?';
  try {
    $db = getConnection();
    $stmt = $db->prepare($sql);  
    $stmt->bind_param("s", $campaignName);
    $stmt->execute();
    $stmt->store_result();
    $stmt->num_rows;
    if($stmt->num_rows == '0') {
      $stmt->close();
      //$sql = "INSERT INTO CAMPAIGN (CAMPAIGN_NAME,BANK_ID,BANK_CODE,DATE_ADDED,DATE_UPDATED,CREATE_BY,UPDATED_BY,RUN_FROM,RUN_TILL) VALUES ('$campaignName','','',NOW(),NOW(),$userId,$userId,'$fromDate','$toDate')";
	  $sql = "INSERT INTO CAMPAIGN (CAMPAIGN_NAME,BANK_ID,BANK_CODE,DATE_ADDED,DATE_UPDATED,CREATE_BY,UPDATED_BY,RUN_FROM,RUN_TILL) VALUES ('$campaignName','$bankId','$bankId',NOW(),NOW(),$userId,$userId,'$fromDate','$toDate')";
      if ($db->query($sql)) {
        echo '{"message":"success"}';	$db = null; die;
      }
      echo '{"message":"Please Try Again Later.."}';  $db = null;	die;
    } else {
      echo '{"message":"Exist"}'; 
    }
  } catch(Exception $e) {
    //echo '{"error":{"message":'. $e->getMessage() .'}}'; 
    echo '{"message":"Please Try Again Later"}'; 
  }
}
function addCommunication() {
  $userId = (int)addslashes($_REQUEST['userId']);
  $bankId = (int)addslashes($_REQUEST['bankId']);
  $campaingId = (int)addslashes($_REQUEST['campaingId']);
  $touchpointId = (int)addslashes($_REQUEST['touchpointId']);
  $communicationName = addslashes($_REQUEST['communicationName']);
  $targetCutomers = (int)addslashes($_REQUEST['targetCutomers']);
  //$targetCutomers2 = (int)addslashes($_REQUEST['targetCutomers2']);
  $templateId = (int)addslashes($_REQUEST['templateId']);
  $templateType = addslashes($_REQUEST['templateType']);
  $userActions = json_encode($_REQUEST['userActionList']);
  $templateContent = html_entity_decode(addslashes($_REQUEST['templateContent']));
  //$templateContent = $_REQUEST['templateContent'];
  try {
    $db = getConnection();
   $sql = "INSERT INTO COMMUNICATION (COMPAIGN_ID,TOUCHPOINT_ID,BANK_ID,CAMMUNICATION_NAME,TARGET_CUSTOMERS,TEMPLATE_ID,TEMPLATE_TYPE,DATE_ADDED,DATE_UPDATED,CREATED_BY,UPDATED_BY,STATUS,PRODUCT_ID,TEMPLATE_CONTENT,USER_ACTIONS,BANK_CODE) VALUES ($campaingId,$touchpointId,'$bankId','$communicationName',$targetCutomers,$templateId,'$templateType',NOW(),NOW(),$userId,$userId,'progress',14,'$templateContent','$userActions','$bankId')";
   echo $sql;
    if ($db->query($sql)) {
      echo '{"message":"success"}';	$db = null; die;
    } else {
      //echo '{"message":"Error"}'; 
      echo $db->error;
    }
  } catch(Exception $e) {
    //echo '{"error":{"message":'. $e->getMessage() .'}}'; 
    echo '{"message":"Please Try Again Later"}'; 
  }
}

function updateCommunication() {
  $userId = (int)addslashes($_REQUEST['userId']); 
  $communicationId = (int)addslashes($_REQUEST['communicationId']);
  $communicationName = addslashes($_REQUEST['communicationName']);
  $targetCutomers = (int)addslashes($_REQUEST['targetCutomers']);
  //$targetCutomers2 = (int)addslashes($_REQUEST['targetCutomers2']);
  $templateId = (int)addslashes($_REQUEST['templateId']);
  $templateType = addslashes($_REQUEST['templateType']);
  $userActions = json_encode($_REQUEST['userActionList']);
  $templateContent = html_entity_decode(addslashes($_REQUEST['templateContent']));
  //$templateContent = $_REQUEST['templateContent'];
  try {
    $db = getConnection();
    $sql = "UPDATE COMMUNICATION SET CAMMUNICATION_NAME='$communicationName', TARGET_CUSTOMERS='$targetCutomers', TEMPLATE_ID='$templateId', TEMPLATE_TYPE='$templateType', DATE_UPDATED=NOW(), UPDATED_BY=$userId, TEMPLATE_CONTENT='$templateContent', USER_ACTIONS='$userActions' WHERE COMMUNICATION_ID=$communicationId";
    if ($db->query($sql)) {
      echo '{"message":"success"}';	$db = null; die;
    } else {
      echo '{"message":"Error"}'; 
    }
  } catch(Exception $e) {
    echo '{"error":{"message":'. $e->getMessage() .'}}'; 
    //echo '{"message":"Please Try Again Later"}'; 
  }
}

function addTouchpoint() {
  $userId = addslashes($_REQUEST['userId']);
  $bankId = addslashes($_REQUEST['bankId']);
  $campaignId = addslashes($_REQUEST['campaingId']);
  $touchpointName = addslashes($_REQUEST['touchpointName']);
  try {
    $db = getConnection();
    $sql = "INSERT INTO TOUCHPOINT (CAMPAIGN_ID,TOUCHPOINT_NAME,BANK_ID,DATE_ADDED,DATE_UPDATED,CREATED_BY,UPDATED_BY,BANK_CODE) VALUES ('$campaignId','$touchpointName','$bankId',NOW(),NOW(),$userId,$userId,'$bankId')";
	//echo $sql;
    if ($db->query($sql)) {
      echo '{"message":"success"}';	$db = null; die;
    }
    echo '{"message":"Please Try Again Later.."}';  $db = null;	die;
  } catch(Exception $e) {
    //echo '{"error":{"message":'. $e->getMessage() .'}}'; 
    echo '{"message":"Please Try Again Later"}'; 
  }
}
function bankList() {
  try {
    $db = getConnection();
    $sql = "SELECT a.*,(SELECT COUNT(c.BRANCH_CODE) FROM BRANCHES c WHERE a.BANK_CODE=c.BANK_CODE) AS branch_count FROM BANK a ORDER BY a.BANK_CODE DESC";
	
    if ($result = $db->query($sql)) {
      $row = array();
      $i = 1;
      $table_data = array();
      if(isset($_REQUEST['type'])) { 
        $table_data = array();
        while($data = $result->fetch_assoc()) {
          $table_data[] = $data; 
        }
        echo '{"BANKLIST": ' . json_encode($table_data) . '}';
      } else {
        while($data = $result->fetch_assoc()) {
          $action_data = '<a href="bank.html?id='.$data['BANK_CODE'].'"><i class="fa fa-pencil-square-o" title="Edit"></i></a>&nbsp;&nbsp;<a href=delete_bank.php?id='.$data['BANK_CODE'].'><i class="fa fa-trash-o" title="Delete"></i></a>';
          $table_data .= '<tr><td>'.$i.'</td><td>'.$data['BANK_CODE'].'</td><td  class="left-align" >'.$data['BANK_NAME'].'</td><td>'.$data['branch_count'].'</td><td>'.$action_data.'</td></tr>';
          $i = $i + 1;
        }
        echo '{"BANKLIST": ' . json_encode($table_data) . '}';
      }
    }
    $result->close();
  } catch(Exception $e) {
    //echo '{"error":{"message":'. $e->getMessage() .'}}'; 
    echo '{"message":"Please Try Again Later"}'; 
  }
}
function branchList() {
	
  $bank_code = $_REQUEST['bankId'];
  // echo $bank_code;
  try {
    $db = getConnection();
    $sql = "SELECT a.BRANCH_CODE,a.BRANCH_NAME,a.BRANCH_CODE,b.BANK_NAME FROM BRANCHES a INNER JOIN BANK b on a.BANK_CODE=b.BANK_CODE where b.BANK_CODE = '$bank_code' ORDER BY a.BRANCH_ID DESC";
   
 if ($result = $db->query($sql)) {
      $row = array();
      $i = 1;
      $table_data = '';
      if(isset($_REQUEST['type'])) { 
        $table_data = '';
        while($data = $result->fetch_assoc()) {
          $table_data[] = $data; 
        }
        echo '{"BRANCHLIST": ' . json_encode($table_data) . '}';
      } else {
        while($data = $result->fetch_assoc()) {
          $action_data = '<a href="branch.html?id='.$data['BRANCH_CODE'].'"><i class="fa fa-pencil-square-o" title="Edit"></i></a>&nbsp;&nbsp;<a href="delete_branch.php?id='.$data['BRANCH_CODE'].'"><i class="fa fa-trash-o" title="Delete"></i></a>';
          $table_data .= '<tr><td>'.$i.'</td><td  class="left-align" >'.$data['BANK_NAME'].'</td><td  class="left-align" >'.$data['BRANCH_NAME'].'</td><td style="text-transform: none;">'.$data['BRANCH_CODE'].'</td><td>'.$action_data.'</td></tr>';
          $i = $i + 1;
        }
        echo '{"BRANCHLIST": ' . json_encode($table_data) . '}';
      }
    }
    // $result->close();
  } catch(Exception $e) {
    //echo '{"error":{"message":'. $e->getMessage() .'}}'; 
    echo '{"message":"Please Try Again Later"}'; 
  }
}


function bankadminList() {
  try {
    $db = getConnection();
    $sql = "SELECT a.FIRST_NAME,a.LAST_NAME,a.EMAIL,a.EMPLOYEE_ID,b.BANK_NAME,c.BRANCH_NAME FROM EMPLOYEE a INNER JOIN BANK b on a.BANK_CODE=b.BANK_CODE INNER JOIN BRANCHES c ON a.BRANCH_CODE=c.BRANCH_CODE where role_id > 2 ORDER BY a.EMPLOYEE_ID DESC";

    if ($result = $db->query($sql)) {
      $row = array();
      $i = 1;
      $table_data = '';
      while($data = $result->fetch_assoc()) {
        $action_data = '<a href="bank-admin.html?id='.$data['EMPLOYEE_ID'].'"><i class="fa fa-pencil-square-o" title="Edit"></i></a>&nbsp;&nbsp;<a href="#"><i class="fa fa-trash-o" title="Delete"></i></a>';
        $table_data .= '<tr><td>'.$i.'</td><td  class="left-align" >'.$data['LAST_NAME'].', '.$data['FIRST_NAME'].'</td><td  class="left-align">'.$data['EMAIL'].'</td><td  class="left-align" >'.$data['BANK_NAME'].'</td><td class="left-align">'.$data['BRANCH_NAME'].'</td><td>'.$action_data.'</td></tr>';
        $i = $i + 1;
      }
      echo '{"BANKLIST": ' . json_encode($table_data) . '}';
    }
    $result->close();
  } catch(Exception $e) {
    //echo '{"error":{"message":'. $e->getMessage() .'}}'; 
    echo '{"message":"Please Try Again Later"}'; 
  }
}

function currentBankName()
{
  $id = $_REQUEST['id'];
  $db = getConnection();
  $sql= "select BANK_NAME from BANK where BANK_CODE = '$id'";
  if ($result = $db->query($sql)) 
  {
    while($data = $result->fetch_assoc())
    {
      echo $data['BANK_NAME'];
    }
  }
}
/*function bankBranchList() {
  $bank_id = (int)$_REQUEST['id'];
  try {
    $db = getConnection();
    $sql = "SELECT a.BRANCH_NAME,a.BRANCH_CODE,a.BRANCH_ID FROM BRANCHES a WHERE a.BANK_ID=$bank_id ORDER BY a.BRANCH_ID DESC";
    if ($result = $db->query($sql)) {
      $table_data = '';
      while($data = $result->fetch_assoc()) {
        $table_data[] = $data; 
      }
      echo '{"BRANCH_LIST": ' . json_encode($table_data) . '}';
    }
    $result->close();
  } catch(Exception $e) {
    //echo '{"error":{"message":'. $e->getMessage() .'}}'; 
    echo '{"message":"Please Try Again Later"}'; 
  }
}*/
function bankBranchList() {
  $bank_id = $_REQUEST['id'];
  $currentUserId = $_REQUEST['currentUserId']; 
  $currentRoleId = (int)$_REQUEST['currentRoleId'];
  try {
    $db = getConnection();
    $sql = "SELECT * FROM BRANCHES WHERE BANK_CODE='$bank_id'";
    $sqlselected = "SELECT BRANCH_CODE FROM BRANCH_INTEGRATION WHERE BANK_CODE='$bank_id' and HEAD_OF_RETAIL='$currentUserId' ";
	
    if ($resultselected = $db->query($sqlselected)) {
      $table_data1 = '';
      while($data1 = $resultselected->fetch_array()) {
        $table_data1[] = $data1['BRANCH_CODE']; 
      }
	}
   if ($result = $db->query($sql)) {
      $table_data = '';
      while($data = $result->fetch_assoc()) {
        $table_data[] = $data; 
      }
      echo '{"BRANCH_LIST": ' . json_encode($table_data) . ',"BRANCH_SEL_LIST": ' . json_encode($table_data1) . '}';
      //print_r(json_encode($table_data));
    }
    $result->close();
  } catch(Exception $e) {
    //echo '{"error":{"message":'. $e->getMessage() .'}}'; 
    echo '{"message":"'.json_encode($e->getMessage()).'"}'; 
  }
}

function branchRoleList() {
  $bank_id = (int)$_REQUEST['id'];
  $currentRoleId = (int)$_REQUEST['currentRoleId'];

  try {
    $db = getConnection();
    $sql = "SELECT ROLE_NAME,ROLE_ID FROM ROLE WHERE ROLE_ID!=$currentRoleId AND ROLE_ID!=1 AND ROLE_ID!=5 ORDER BY ROLE_ID DESC";
    if ($result = $db->query($sql)) {
      $table_data = '';
      while($data = $result->fetch_assoc()) {
        $table_data[] = $data; 
      }
      echo '{"ROLE_LIST": ' . json_encode($table_data) . '}';
    }
    $result->close();
  } catch(Exception $e) {
    //echo '{"error":{"message":'. $e->getMessage() .'}}'; 
    echo '{"message":"Please Try Again Later"}'; 
  }
}
/*
function branchRoleList() {
  $bank_id = (int)$_REQUEST['id'];
  try {
    $db = getConnection();
    $sql = "SELECT a.ROLE_NAME,a.ROLE_ID FROM ROLE a WHERE a.ROLE_ID!=1 ORDER BY a.ROLE_ID DESC";
    if ($result = $db->query($sql)) {
      $table_data = '';
      while($data = $result->fetch_assoc()) {
        $table_data[] = $data; 
      }
      echo '{"ROLE_LIST": ' . json_encode($table_data) . '}';
    }
    $result->close();
  } catch(Exception $e) {
    //echo '{"error":{"message":'. $e->getMessage() .'}}'; 
    echo '{"message":"Please Try Again Later"}'; 
  }
}*/

function branchEmployeeList() {
  $branch_id = (int)$_REQUEST['id'];
  try {
    $db = getConnection();
    $sql = "SELECT a.FIRST_NAME AS EMPLOYEE_NAME,a.EMPLOYEE_ID FROM EMPLOYEE a WHERE a.BRANCH_CODE=$branch_id ORDER BY a.EMPLOYEE_ID ASC";
    if ($result = $db->query($sql)) {
      $table_data = '';
      while($data = $result->fetch_assoc()) {
        $table_data[] = $data; 
      }
      echo '{"EMPLOYEE_LIST": ' . json_encode($table_data) . '}';
    }
    $result->close();
  } catch(Exception $e) {
    //echo '{"error":{"message":'. $e->getMessage() .'}}'; 
    echo '{"message":"Please Try Again Later"}'; 
  }
}

function roleList() {
  try {
    $db = getConnection();
    $sql = "SELECT a.ROLE_NAME,a.ROLE_ID,b.BANK_NAME FROM ROLE a INNER JOIN BANK b on a.BANK_CODE=b.BANK_CODE ORDER BY a.ROLE_ID DESC";
    if ($result = $db->query($sql)) {
      $row = array();
      $i = 1;
      $table_data = '';
      while($data = $result->fetch_assoc()) {
        $action_data = '<a href="role.html?id='.$data['ROLE_ID'].'"><i class="fa fa-pencil-square-o" title="Edit"></i></a>&nbsp;&nbsp;&nbsp;<span style="cursor:pointer;" type="button"><i class="fa fa-trash-o" title="Delete"></i></span>';
        $table_data .= '<tr><td>'.$i.'</td><td>'.$data['ROLE_NAME'].'</td><td>'.$data['BANK_NAME'].'</td><td>'.$action_data.'</td></tr>';
        $i = $i + 1;
      }
      echo '{"ROLELIST": ' . json_encode($table_data) . '}';
    }
    $result->close();
  } catch(Exception $e) {
    echo '{"error":{"message":'. $e->getMessage() .'}}'; 
    //echo '{"message":"Please Try Again Later"}'; 
  }
}

function employeeList() {
  $currentRoleId = addslashes($_REQUEST['currentRoleId']);
  $currentBankId = addslashes($_REQUEST['currentBankId']);
  //echo $currentBankId;
  try {
    $db = getConnection();
 $sql = "SELECT a.FIRST_NAME,a.LAST_NAME,a.EMAIL,a.EMPLOYEE_ID,a.ROLE_ID,b.BANK_NAME,b.BANK_CODE,c.BRANCH_NAME FROM EMPLOYEE a INNER JOIN BANK b on ( a.BANK_CODE = '$currentBankId' && b.BANK_CODE = '$currentBankId' ) INNER JOIN BRANCHES c ON a.BRANCH_CODE=c.BRANCH_CODE where ROLE_ID != 5 AND ROLE_ID!=1 AND ROLE_ID!='$currentRoleId'
UNION
SELECT e.FIRST_NAME,e.LAST_NAME,e.EMAIL,e.EMPLOYEE_ID,e.ROLE_ID,b.BANK_NAME,b.BANK_CODE,c.BRANCH_NAME 
FROM BRANCH_INTEGRATION a 
INNER JOIN EMPLOYEE e ON e.EMPLOYEE_ID = a.HEAD_OF_RETAIL
INNER JOIN BANK b ON e.BANK_CODE=b.BANK_CODE
INNER JOIN BRANCHES c ON a.BRANCH_CODE=c.BRANCH_CODE 
WHERE e.ROLE_ID != 5 AND e.ROLE_ID!=1 AND e.ROLE_ID!='5' AND e.BANK_CODE='$currentBankId'"; 
	
	
    
    if ($result = $db->query($sql)) {
      $strBranchName = '';
      $row = array();
      $i = 1;
      $table_data = '';
      while($data = $result->fetch_assoc()) {
         if($data['ROLE_ID']==1){
			 $role="Admin"; 
		 } else if($data['ROLE_ID']==2) {
			  $role="Head Of Retail";
		 } elseif($data['ROLE_ID']==3) {
			 $role="Bank Manager"; 
		 } elseif($data['ROLE_ID']==4) {
			 $role="Bank Officer"; 
		 } else {			 
			$role="Bank Admin";  
		 }
        if($data['BRANCH_NAME'] == 'UNKNOWN'){
          $data['BRANCH_NAME'] = '';
          $arg = $data['EMPLOYEE_ID'];
          $selectAllBranch = "select * from BRANCH_INTEGRATION where HEAD_OF_RETAIL = '$arg'";
          if($allBranch = $db->query($selectAllBranch)){
            $rowBranch = array();
            $i = 1;
            while($dataAllBranch = $allBranch->fetch_assoc()){
              $arg = $dataAllBranch['BRANCH_CODE'];
              $selectBranchName = "select BRANCH_NAME from BRANCHES where BRANCH_CODE='$arg'";
              if($allBranchName = $db->query($selectBranchName)){
                $rowBranchName = array();
                $i = 1;
                while($dataAllBranchName = $allBranchName->fetch_assoc()){
                  $strBranchName = $strBranchName.", ".$dataAllBranchName['BRANCH_NAME'];
                }
              }
            }
          }
          $data['BRANCH_NAME'] = substr($strBranchName,2);
		  
        }
        
        
        $action_data = '<a href="employee.html?id='.$data['EMPLOYEE_ID'].'"><i class="fa fa-pencil-square-o" title="Edit"></i></a>&nbsp;&nbsp;<a href="delete_employee.php?id='.$data['EMPLOYEE_ID'].'"><i class="fa fa-trash-o" title="Delete"></i></a>';
        $table_data .= '<tr><td>'.$i.'</td><td  class="left-align" >'.$data['BANK_NAME'].'</td><td  class="left-align" style="text-transform: capitalize !important;">'.$data['BRANCH_NAME'].'</td><td  class="left-align">'.$role.'</td><td  class="left-align" >'.$data['LAST_NAME'].', '.$data['FIRST_NAME'].'</td><td class="left-align" style="text-transform:none;">'.$data['EMAIL'].'</td><td>'.$action_data.'</td></tr>';
//        $action_data = '<a href="employee.html?id='.$data['EMPLOYEE_ID'].'"><i class="fa fa-pencil-square-o" title="Edit"></i></a>&nbsp;&nbsp;<a href="delete_employee.php?id='.$data['EMPLOYEE_ID'].'"><i class="fa fa-trash-o" title="Delete"></i></a>';
//        $table_data .= '<tr><td>'.$i.'</td><td  class="left-align" >'.$data['FIRST_NAME'].' '.$data['LAST_NAME'].'</td><td  class="left-align">'.$data['EMAIL'].'</td><td  class="left-align" >'.$data['BANK_NAME'].'</td><td class="left-align">'.$strBranchName.'</td><td>'.$action_data.'</td></tr>';
        $i = $i + 1;
      }
	  
	  
	  
	  
	  
//      echo $strBranchName;
      echo '{"BANKLIST": ' . json_encode($table_data) . '}';
    } else {
		//echo $db->error;
	}
    //    $result->close();
  } catch(Exception $e) {
    //echo '{"error":{"message":'. $e->getMessage() .'}}'; 
    echo '{"message":"'.json_encode($e->getMessage()).'"}'; 
  }
}
function customerList() {
  try {
    $db = getConnection();
    $sql = "SELECT a.CUSTOMER_ID,a.FIRST_NAME,a.LAST_NAME,a.EMAIL,b.BANK_NAME,c.BRANCH_NAME,d.FIRST_NAME AS EMPLOYEE_NAME FROM CUSTOMER a INNER JOIN BANK b on a.BANK_ID=b.BANK_ID INNER JOIN BRANCHES c ON a.BRANCH_ID=c.BRANCH_ID INNER JOIN EMPLOYEE d ON a.EMPLOYEE_ID=d.EMPLOYEE_ID ORDER BY a.CUSTOMER_ID DESC";
    if ($result = $db->query($sql)) {
      $row = array();
      $i = 1;
      $table_data = '';
      if(isset($_REQUEST['type'])) { 
        while($data = $result->fetch_assoc()) {
          $table_data[] = $data; 
        }
        echo '{"CUSTOMER_LIST": ' . json_encode($table_data) . '}';die;
      } else {
        while($data = $result->fetch_assoc()) {
          $action_data = '<a href="customer.html?id='.$data['CUSTOMER_ID'].'"><i class="fa fa-pencil-square-o" title="Edit"></i></a>&nbsp;&nbsp;<a href="#"><i class="fa fa-trash-o" title="Delete"></i></a>';
          $table_data .= '<tr><td>'.$i.'</td><td>'.$data['LAST_NAME'].', '.$data['FIRST_NAME'].'</td><td>'.$data['EMAIL'].'</td><td>'.$data['EMPLOYEE_NAME'].'</td><td>'.$data['BRANCH_NAME'].'</td><td>'.$data['BANK_NAME'].'</td><td>'.$action_data.'</td></tr>';
          $i = $i + 1;
        }
      }
      echo '{"BANKLIST": ' . json_encode($table_data) . '}';
    } else {
      echo $db->error;
    }
  } catch(Exception $e) {
    //echo '{"error":{"message":'. $e->getMessage() .'}}'; 
    echo '{"message":"Please Try Again Later"}'; 
  }
}
function customersResponseList() {
  $userId = $_REQUEST['userId'];
  try {
    $db = getConnection();
    $sql = "SELECT a.*,b.CAMMUNICATION_NAME FROM CUSTOMER_RESPONSE a 
			INNER JOIN COMMUNICATION b ON a.COMMUNICATION_ID=b.COMMUNICATION_ID
			INNER JOIN CUSTOMER_EMPLOYEE_MAPPING c ON a.CUSTOMER_ID=c.CUSTOMER_ID
			WHERE c.EMPLOYEE_ID='$userId'";
			//echo $sql;
    if ($result = $db->query($sql)) {
      $table_data = '';
      if(isset($_REQUEST['type'])) { 
        while($data = $result->fetch_assoc()) {
          $table_data[] = $data; 
        }
        echo '{"RESPONSE_LIST": ' . json_encode($table_data) . '}';die;
      } else {
        while($data = $result->fetch_assoc()) {
          $table_data .= '<tr><td>'.$data['CUSTOMER_ID'].'</td><td class="left-align">'.$data['CUSTOMER_LNAME'].', '.$data['CUSTOMER_FNAME'].'</td><td class="left-align">'.$data['CUSTOMER_EMAIL'].'</td><td class="left-align">'.$data['CAMMUNICATION_NAME'].'</td><td style="display:none;">'.$data['CUSTOMER_MOBILE_NO'].'</td><td>'.$data['CAMPAIGN_TYPE'].'</td><td>'.$data['CUSTOMER_RESPONSE'].'</td></tr>';
        }
      }
      echo '{"RESPONSE_LIST": ' . json_encode($table_data) . '}';
    } else {
      echo $db->error;
    }
  } catch(Exception $e) {
    //echo '{"error":{"message":'. $e->getMessage() .'}}'; 
    echo '{"message":"Please Try Again Later"}'; 
  }
}
function productList() {
  try {

    $db = getConnection();
	
	$bid = $_REQUEST['bankcode'];
	
    $sql = "SELECT a.* FROM PRODUCT a WHERE a.BANK_ID='$bid' ORDER BY a.PRODUCT_ID DESC";


    if ($result = $db->query($sql)) {
      $row = array();
      $i = 1;
      $table_data = '';
      if(isset($_REQUEST['type'])) { 
        while($data = $result->fetch_assoc()) {
          $table_data[] = $data; 
        }
        echo '{"PRODUCT_LIST": ' . json_encode($table_data) . '}';die;
      } else {
        while($data = $result->fetch_assoc()) {
          $action_data = '<a href="product.html?id='.$data['PRODUCT_ID'].'"><i class="fa fa-pencil-square-o" title="Edit"></i></a>&nbsp;&nbsp;<a href="delete_product.php?id='.$data['PRODUCT_ID'].'"><i class="fa fa-trash-o" title="Delete"></i></a>';
          $table_data .= '<tr><td>'.$i.'</td><td style="display:none;">'.$data['PRODUCT_ID'].'</td><td class="left-align" >'.$data['PRODUCT_NAME'].'</td><td>'.$action_data.'</td></tr>';
          $i = $i + 1;
        }
        echo '{"BANKLIST": ' . json_encode($table_data) . '}';
      }
    }
    $result->close();
  } catch(Exception $e) {
    //echo '{"error":{"message":'. $e->getMessage() .'}}'; 
    echo '{"message":"Please Try Again Later"}'; 
  }
}
function addBank() {

  $bank_name = addslashes($_REQUEST['bankname']);
  $bankid = $_REQUEST['bankid'];
  $sql = 'SELECT * FROM BANK WHERE BANK_NAME=?';
  try {
    $db = getConnection();
    $stmt = $db->prepare($sql);  
    $stmt->bind_param("s", $bank_name);
    $stmt->execute();
    $stmt->store_result();
    if($stmt->num_rows == '0') {
      $stmt->close();
      $sql = "INSERT INTO BANK (BANK_NAME,BANK_CODE) VALUES ('$bank_name','$bankid')";
      if ($db->query($sql)) {
        echo '{"message":"success"}';						
			$db = null; die;
      }
      echo '{"message":"Please Try Again Later.."}';  $db = null;	die;
    } else {
      echo '{"message":"Bank Name Already Exist"}'; 
    }
  } catch(Exception $e) {
    //echo '{"error":{"message":'. $e->getMessage() .'}}'; 
    echo '{"message":"Please Try Again Later"}'; 
  }
}
function addProduct() {
  $product_name = addslashes($_REQUEST['productname']);
  $bank_code = addslashes($_REQUEST['bankcode']);
  $sql = 'SELECT * FROM PRODUCT WHERE PRODUCT_NAME=? AND BANK_ID=?';
  try {
    $db = getConnection();
    $stmt = $db->prepare($sql);  
    $stmt->bind_param("ss", $product_name, $bank_code);
    $stmt->execute();
    $stmt->store_result();
    if($stmt->num_rows == '0') {
      $stmt->close();
      $sql = "INSERT INTO PRODUCT (PRODUCT_NAME, BANK_ID) VALUES ('$product_name', '$bank_code')";
      if ($db->query($sql)) {
        echo '{"message":"success"}';	$db = null; die;
      }
      echo '{"message":"Please Try Again Later.."}';  $db = null;	die;
    } else {
      echo '{"message":"Product Name Already Exist"}'; 
    }
  } catch(Exception $e) {
    //echo '{"error":{"message":'. $e->getMessage() .'}}'; 
    echo '{"message":"Please Try Again Later"}'; 
  }
}
function getBank() {
  $bank_id = addslashes($_REQUEST['id']);
  $sql = 'SELECT * FROM BANK WHERE BANK_CODE=?';
  try {
    $db = getConnection();
    $stmt = $db->prepare($sql);  
    $stmt->bind_param("s", $bank_id);
    $stmt->execute();
    $stmt->store_result();
    if($stmt->num_rows == '1') {
      $sql = "SELECT * FROM BANK WHERE BANK_CODE='$bank_id'";
      if ($result = $db->query($sql)) {
        $row = array();
        $data = $result->fetch_object();
        echo '{"BANK_DATA": ' . json_encode($data) . '}';
      } 
    } else {
      echo '{"message":"Bank Data Not Available"}'; 
    }
  } catch(Exception $e) {
    //echo '{"error":{"message":'. $e->getMessage() .'}}'; 
    echo '{"message":"Please Try Again Later"}'; 
  }
}

function getCampaign() {
  $id = (int)addslashes($_REQUEST['id']);
  $sql = 'SELECT * FROM CAMPAIGN WHERE CAMPAIGN_ID=?';
  try {
    $db = getConnection();
    $stmt = $db->prepare($sql);  
    $stmt->bind_param("i", $id);
    $stmt->execute();
    $stmt->store_result();
    if($stmt->num_rows == '1') {
      $sql = "SELECT * FROM CAMPAIGN WHERE CAMPAIGN_ID='$id'";
      if ($result = $db->query($sql)) {
        $row = array();
        $data = $result->fetch_object();
        echo '{"CAMPAIGN_DATA": ' . json_encode($data) . '}'; die;
      } 
    } else {
      echo '{"message":"Campaign Data Not Available"}'; 
    }
  } catch(Exception $e) {
    //echo '{"error":{"message":'. $e->getMessage() .'}}'; 
    echo '{"message":"Please Try Again Later"}'; 
  }
}
function getRole() {
  $role_id = (int)$_REQUEST['id'];
  if($role_id == '') { echo '{"ROLE_DATA": {"ROLE_NAME":"","BANK_CODE":""}}'; die; }
  $sql = 'SELECT * FROM ROLE WHERE ROLE_ID=?';
  try {
    $db = getConnection();
    $stmt = $db->prepare($sql);  
    $stmt->bind_param("i", $role_id);
    $stmt->execute();
    $stmt->store_result();
    if($stmt->num_rows == '1') {
      $sql = "SELECT * FROM ROLE WHERE ROLE_ID='$role_id'";
      if ($result = $db->query($sql)) {
        $row = array();
        $data = $result->fetch_object();
        echo '{"ROLE_DATA": ' . json_encode($data) . '}';
      } 
    } else {
      echo '{"message":"Role Data Not Available"}'; 
    }
  } catch(Exception $e) {
    //echo '{"error":{"message":'. $e->getMessage() .'}}'; 
    echo '{"message":"Please Try Again Later"}'; 
  }
}

function getBranch() {
  $branch_id = $_REQUEST['id'];
  // if($branch_id == '') { echo '{"BRANCH_DATA": {"BRANCH_NAME":"","BRANCH_CODE":""}}'; die; }
  // $sql = 'SELECT * FROM BRANCHES WHERE BRANCH_CODE=?';
  try {
    $db = getConnection();
    // $stmt = $db->prepare($sql);  
    // $stmt->bind_param("i", $branch_id);
    // $stmt->execute();
    // $stmt->store_result();
    // //echo $stmt->num_rows;die;
    // if($stmt->num_rows == '1') {
      $sql = "SELECT * FROM BRANCHES WHERE BRANCH_CODE='$branch_id'";
      if ($result = $db->query($sql)) {
        // echo $result;die;
        $row = array();
        $data = $result->fetch_object();
        echo '{"BRANCH_DATA": ' . json_encode($data) . '}';
      } 
    // } else {
    //   echo '{"message":"Branch Data Not Available"}'; 
    // }
  } catch(Exception $e) {
    //echo '{"error":{"message":'. $e->getMessage() .'}}'; 
    echo '{"message":"Please Try Again Later"}'; 
  }
}

function getEmployee() {
  $id = (int)$_REQUEST['id'];
  if($id == '') { echo '{"EMPLOYEE_DATA": {"EMPLOYEE_NAME":"","EMPLOYEE_ID":""}}'; die; }
  $sql = 'SELECT * FROM EMPLOYEE WHERE EMPLOYEE_ID=?';
  try {
    $db = getConnection();
    $stmt = $db->prepare($sql);  
    $stmt->bind_param("i", $id);
    $stmt->execute();
    $stmt->store_result();
    if($stmt->num_rows == '1') {
      $sql = "SELECT * FROM EMPLOYEE WHERE EMPLOYEE_ID='$id'";
      if ($result = $db->query($sql)) {
        $data = $result->fetch_object();
        $datan = explode('-',$data->DATE_OF_BIRTH);
        $data->DATE_OF_BIRTH = $datan[1].'/'.$datan[2].'/'.$datan[0];
        echo '{"EMPLOYEE_DATA": ' . json_encode($data) . '}';
      } 
    } else {
      echo '{"message":"Employee Data Not Available"}'; 
    }
  } catch(Exception $e) {
    //echo '{"error":{"message":'. $e->getMessage() .'}}'; 
    echo '{"message":"Please Try Again Later"}'; 
  }
}

function templateData() {
  $id = (int)$_REQUEST['id'];
  if($id == '') { echo '{"EMPLOYEE_DATA": {"EMPLOYEE_NAME":"","EMPLOYEE_ID":""}}'; die; }
  $sql = 'SELECT * FROM TEMPLATE WHERE TEMPLATE_ID=?';
  try {
    $db = getConnection();
    $stmt = $db->prepare($sql);  
    $stmt->bind_param("i", $id);
    $stmt->execute();
    $stmt->store_result();
    if($stmt->num_rows == '1') {
      $sql = "SELECT * FROM TEMPLATE WHERE TEMPLATE_ID='$id'";
      if ($result = $db->query($sql)) {
        $row = array();
        $data = $result->fetch_object();
        echo '{"TEMPLATE_DATA": ' . json_encode($data) . '}';
      } 
    } else {
      echo '{"message":"TEMPLATE Data Not Available"}'; 
    }
  } catch(Exception $e) {
    //echo '{"error":{"message":'. $e->getMessage() .'}}'; 
    echo '{"message":"Please Try Again Later"}'; 
  }
}

function getCustomer() {
  $id = (int)$_REQUEST['id'];
  if($id == '') { echo '{"CUSTOMER_DATA": {"CUSTOMER_NAME":"","CUSTOMER_ID":""}}'; die; }
  $sql = 'SELECT * FROM CUSTOMER WHERE CUSTOMER_ID=?';
  try {
    $db = getConnection();
    $stmt = $db->prepare($sql);  
    $stmt->bind_param("i", $id);
    $stmt->execute();
    $stmt->store_result();
    if($stmt->num_rows == '1') {
      $sql = "SELECT * FROM CUSTOMER WHERE CUSTOMER_ID='$id'";
      if ($result = $db->query($sql)) {
        $row = array();
        $data = $result->fetch_object();
        $datan = explode('-',$data->DATE_OF_BIRTH);
        $data->DATE_OF_BIRTH = $datan[1].'/'.$datan[2].'/'.$datan[0];
        $datan = explode('-',$data->DATE_OF_JOIN);
        $data->DATE_OF_JOIN = $datan[1].'/'.$datan[2].'/'.$datan[0];
        echo '{"CUSTOMER_DATA": ' . json_encode($data) . '}';
      } 
    } else {
      echo '{"message":"Customer Data Not Available"}'; 
    }
  } catch(Exception $e) {
    //echo '{"error":{"message":'. $e->getMessage() .'}}'; 
    echo '{"message":"Please Try Again Later"}'; 
  }
}

function getCustomerByEmail() {
  $id = $_REQUEST['id'];
  if($id == '') { echo '{"CUSTOMER_DATA": {"CUSTOMER_NAME":"","CUSTOMER_ID":""}}'; die; }
  $sql = 'SELECT * FROM CUSTOMER_RESPONSE WHERE CUSTOMER_ID=?';
  try {
    $db = getConnection();
    $stmt = $db->prepare($sql);  
    $stmt->bind_param("s", $id);
    $stmt->execute();
    $stmt->store_result();
    if($stmt->num_rows >= '1') {
      $sql = "SELECT * FROM CUSTOMER_RESPONSE WHERE CUSTOMER_Id='$id'";
      if ($result = $db->query($sql)) {

        $data = $result->fetch_object();
        $data->CUSTOMER_JOINING_DATE = date("M d, Y",strtotime($data->CUSTOMER_JOINING_DATE));
        echo '{"CUSTOMER_DATA": ' . json_encode($data) . '}';

      } 
    } else {
      echo '{"CUSTOMER_DATA":"Customer Data Not Available"}'; 
    }
  } catch(Exception $e) {
    //echo '{"error":{"message":'. $e->getMessage() .'}}'; 
    echo '{"CUSTOMER_DATA":"Please Try Again Later"}'; 
  }
}

function getProduct() {
  $id = addslashes($_REQUEST['id']);
  $sql = 'SELECT * FROM PRODUCT WHERE PRODUCT_ID=?';
  try {
    $db = getConnection();
    $stmt = $db->prepare($sql);  
    $stmt->bind_param("s", $id);
    $stmt->execute();
    $stmt->store_result();
    if($stmt->num_rows == '1') {
      $sql = "SELECT * FROM PRODUCT WHERE PRODUCT_ID='$id'";
      if ($result = $db->query($sql)) {
        $row = array();
        $data = $result->fetch_object();
        echo '{"PRODUCT_DATA": ' . json_encode($data) . '}';
      } 
    } else {
      echo '{"message":"Product Data Not Available"}'; 
    }
  } catch(Exception $e) {
    //echo '{"error":{"message":'. $e->getMessage() .'}}'; 
    echo '{"message":"Please Try Again Later"}'; 
  }
}

function updateBank() {
  $bank_id = (int)addslashes($_REQUEST['id']);
  $bank_name = addslashes($_REQUEST['bankname']);
  $sql = 'UPDATE BANK SET BANK_NAME=? WHERE BANK_CODE=?';
  try {
    $db = getConnection();
    $stmt = $db->prepare($sql);  
    $stmt->bind_param("si", $bank_name,$bank_id);
    if($stmt->execute()) {
      echo '{"message":"Bank Data Updated"}'; 
    } else {
      echo '{"message":"error"}'; 
    }
  } catch(Exception $e) {
    //echo '{"error":{"message":'. $e->getMessage() .'}}'; 
    echo '{"message":"Please Try Again Later"}'; 
  }
}
function updateCampaign() {
  $cid = (int)addslashes($_REQUEST['cid']);
  $cname = addslashes($_REQUEST['campaignName']);
  $fromDate = addslashes($_REQUEST['fromDate']);
  $toDate = addslashes($_REQUEST['toDate']);
  $fromDate = explode('/',$fromDate);
  $fromDate = $fromDate[2].'-'.$fromDate[0].'-'.$fromDate[1];
  $toDate = explode('/',$toDate);
  $toDate = $toDate[2].'-'.$toDate[0].'-'.$toDate[1];
  $sql = 'UPDATE CAMPAIGN SET CAMPAIGN_NAME=?, RUN_FROM=?, RUN_TILL=? WHERE CAMPAIGN_ID=?';
  try {
    $db = getConnection();
    $stmt = $db->prepare($sql);  
    $stmt->bind_param("sssi", $cname,$fromDate,$toDate,$cid);
    if($stmt->execute()) {
      echo '{"message":"success"}'; 
    } else {
      echo '{"message":"error"}'; 
    }
  } catch(Exception $e) {
    //echo '{"error":{"message":'. $e->getMessage() .'}}'; 
    echo '{"message":"Please Try Again Later"}'; 
  }
}
function pauseCampaign() {
  $cid = (int)addslashes($_REQUEST['id']);
  $sql = 'UPDATE CAMPAIGN SET STATUS="INACTIVE" WHERE CAMPAIGN_ID=?';
  try {
    $db = getConnection();
    $stmt = $db->prepare($sql);  
    $stmt->bind_param("i", $cid);
    if($stmt->execute()) {
      echo '{"message":"success"}'; 
    } else {
      echo '{"message":"error"}'; 
    }
  } catch(Exception $e) {
    //echo '{"error":{"message":'. $e->getMessage() .'}}'; 
    echo '{"message":"Please Try Again Later"}'; 
  }
}
function updateTouchpoint() {
  $id = (int)addslashes($_REQUEST['touchpointId']);
  $name = addslashes($_REQUEST['touchpointName']);
  $sql = 'UPDATE TOUCHPOINT SET TOUCHPOINT_NAME=? WHERE TOUCHPOINT_ID=?';
  try {
    $db = getConnection();
    $stmt = $db->prepare($sql);  
    $stmt->bind_param("si", $name,$id);
    if($stmt->execute()) {
      echo '{"message":"success"}'; 
    } else {
      echo '{"message":"error"}'; 
    }
  } catch(Exception $e) {
    //echo '{"error":{"message":'. $e->getMessage() .'}}'; 
    echo '{"message":"Please Try Again Later"}'; 
  }
}

function updateRole() {
  $role_id = (int)addslashes($_REQUEST['id']);
  $bank_name = (int)$_REQUEST['bankname'];
  $role_name = addslashes($_REQUEST['role_name']);
  $sql = 'UPDATE ROLE SET ROLE_NAME=?, BANK_CODE=? WHERE ROLE_ID=?';
  try {
    $db = getConnection();
    $stmt = $db->prepare($sql);  
    $stmt->bind_param("sii", $role_name, $bank_name, $role_id);
    if($stmt->execute()) {
      echo '{"message":"Role Data Updated"}'; 
    } else {
      echo '{"message":"error"}'; 
      //echo '{"message":"'.$db->error.'"}'; 
    }
  } catch(Exception $e) {
    //echo '{"error":{"message":'. $e->getMessage() .'}}'; 
    echo '{"message":"Please Try Again Later"}'; 
  }
}

function updateBranch() {
  $id		= (int)$_REQUEST['id'];
  $updated_by = (int)1;
  $bank_name		= (int)$_REQUEST['bankname'];
  $branch_name	= addslashes($_REQUEST['branchname']);
  $branch_code	= addslashes($_REQUEST['branchcode']);
  $country		= addslashes($_REQUEST['country']);
  $state			= addslashes($_REQUEST['state']);
  $city			= addslashes($_REQUEST['city']);
  $address		= addslashes($_REQUEST['address']);
  $sql = 'UPDATE BRANCHES SET BANK_CODE=?, BRANCH_NAME=?, BRANCH_CODE=?, COUNTRY=?, STATE=?, CITY=?, ADDRESS=?, DATE_UPDATED=NOW(), UPDATED_BY=? WHERE BRANCH_CODE=?';
  try {
    $db = getConnection();
    $stmt = $db->prepare($sql);  
    $stmt->bind_param("issssssii", $bank_name, $branch_name, $branch_code, $country, $state, $city, $address, $updated_by, $id);
    if($stmt->execute()) {
      echo '{"message":"Branch Data Updated"}'; 
    } else {
      echo '{"message":"error"}'; 
      //echo '{"message":"'.$db->error.'"}'; 
    }
  } catch(Exception $e) {
    //echo '{"error":{"message":'. $e->getMessage() .'}}'; 
    echo '{"message":"Please Try Again Later"}'; 
  }
}


function updateBankadmin() {
  $id		= (int)$_REQUEST['id'];
  $updated_by = (int)1;
  $bank_name		= (int)$_REQUEST['bankname'];
  $branch_name	= addslashes($_REQUEST['branchname']);
  $branch_code	= addslashes($_REQUEST['branchcode']);
  $country		= addslashes($_REQUEST['country']);
  $state			= addslashes($_REQUEST['state']);
  $city			= addslashes($_REQUEST['city']);
  $address		= addslashes($_REQUEST['address']);
  $sql = 'UPDATE BRANCHES SET BANK_CODE=?, BRANCH_NAME=?, BRANCH_CODE=?, COUNTRY=?, STATE=?, CITY=?, ADDRESS=?, DATE_UPDATED=NOW(), UPDATED_BY=? WHERE BRANCH_CODE=?';
  try {
    $db = getConnection();
    $stmt = $db->prepare($sql);  
    $stmt->bind_param("issssssii", $bank_name, $branch_name, $branch_code, $country, $state, $city, $address, $updated_by, $id);
    if($stmt->execute()) {
      echo '{"message":"Branch Data Updated"}'; 
    } else {
      echo '{"message":"error"}'; 
      //echo '{"message":"'.$db->error.'"}'; 
    }
  } catch(Exception $e) {
    //echo '{"error":{"message":'. $e->getMessage() .'}}'; 
    echo '{"message":"Please Try Again Later"}'; 
  }
}
function updateEmployee() {
  $id		= (int)$_REQUEST['id'];
  $updated_by = (int)1;
  $bank_name		= addslashes($_REQUEST['bankname']);
  $branch_name	= addslashes($_REQUEST['branchname']);
  $role_name		= (int)$_REQUEST['role'];
  $email			= addslashes($_REQUEST['email']);
  $password		= addslashes($_REQUEST['password']);
  $first_name		= addslashes($_REQUEST['firstname']);
  $last_name		= addslashes($_REQUEST['lastname']);
  $dob			= addslashes($_REQUEST['dob']);
  $phno			= addslashes($_REQUEST['phno']);
  $mono			= addslashes($_REQUEST['mono']);
  $gender			= addslashes($_REQUEST['gender']);
  $country		= addslashes($_REQUEST['country']);
  $state			= addslashes($_REQUEST['state']);
  $city			= addslashes($_REQUEST['city']);
  $address		= addslashes($_REQUEST['address']);
  $zip			= addslashes($_REQUEST['zip']);
  $status			= addslashes($_REQUEST['status']);
  $isfd			= (int)$_REQUEST['isfd'];
  $dob = explode('/',$dob);
  $dob = $dob[2].'-'.$dob[0].'-'.$dob[1];
  $sql = 'UPDATE EMPLOYEE SET BANK_CODE=?, BRANCH_CODE=?, ROLE_ID=?, FIRST_NAME=?, LAST_NAME=?, GENDER=?, DATE_OF_BIRTH=?, EMAIL=?, PASSWORD=?, PHONE_NO=?, MOBILE_NO=?, ADDRESS=?, CITY=?, STATE=?, COUNTY=?, PIN_CODE=?, STATUS=?, DATE_UPDATED=NOW(), ISFLOWDESIGNER=? WHERE EMPLOYEE_ID=?';
  try {
    $db = getConnection();
	 if($role_name == 2) {
		$branch_name = 0;
		 
		//$db->query("UPDATE BRANCH_INTEGRATION SET HEAD_OF_RETAIL=0 WHERE HEAD_OF_RETAIL='$id'");
	}
	
    $stmt = $db->prepare($sql);  
    $stmt->bind_param("ssissssssssssssssii", $bank_name, $branch_name, $role_name, $first_name, $last_name, $gender, $dob, $email, $password, $phno, $mono, $address, $city, $state, $country, $zip, $status, $isfd, $id);
    if($stmt->execute()) {
      echo '{"message":"Employee Data Updated"}'; 
    } else {
      echo '{"message":'.json_encode($db->error).'}'; 
      //echo '{"message":"'.$db->error.'"}'; 
    }
  } catch(Exception $e) {
    //echo '{"error":{"message":'. $e->getMessage() .'}}'; 
    echo '{"message":'.json_encode($db->error).'}'; 
  }
}

function updateCustomer() {
  $id		= (int)$_REQUEST['id'];
  $updated_by = (int)1;
  $bank_name		= (int)$_REQUEST['bankname'];
  $branch_name	= (int)$_REQUEST['branchname'];
  $employee_name	= (int)$_REQUEST['employee'];
  $email			= addslashes($_REQUEST['email']);
  $first_name		= addslashes($_REQUEST['firstname']);
  $last_name		= addslashes($_REQUEST['lastname']);
  $doj			= addslashes($_REQUEST['doj']);
  $dob			= addslashes($_REQUEST['dob']);
  $phno			= addslashes($_REQUEST['phno']);
  $mono			= addslashes($_REQUEST['mono']);
  $gender			= addslashes($_REQUEST['gender']);
  $country		= addslashes($_REQUEST['country']);
  $state			= addslashes($_REQUEST['state']);
  $city			= addslashes($_REQUEST['city']);
  $address		= addslashes($_REQUEST['address']);
  $zip			= addslashes($_REQUEST['zip']);
  $account		= addslashes($_REQUEST['account']);
  $occupation		= addslashes($_REQUEST['occupation']);
  $dob = explode('/',$dob);
  $dob = $dob[2].'-'.$dob[0].'-'.$dob[1];
  $doj = explode('/',$doj);
  $doj = $doj[2].'-'.$doj[0].'-'.$doj[1];
  $sql = 'UPDATE CUSTOMER SET BANK_CODE=?, BRANCH_CODE=?, EMPLOYEE_ID=?, ACCOUNT_TYPE=?, EMAIL=?, FIRST_NAME=?, LAST_NAME=?, OCCUPATION=?, DATE_OF_JOIN=?, PHONE_NO=?, MOBILE_NO=?, GENDER=?, DATE_OF_BIRTH=?, ADDRESS=?, CITY=?, STATE=?, COUNTRY=?, PINCODE=?, DATE_UPDATED=NOW() WHERE CUSTOMER_ID=?';
  try {
    $db = getConnection();
    $stmt = $db->prepare($sql);  
    $stmt->bind_param("iiisssssssssssssssi", $bank_name, $branch_name, $employee_name, $account, $email, $first_name, $last_name, $occupation, $doj, $phno, $mono, $gender, $dob, $address, $city, $state, $country, $zip, $id);
    if($stmt->execute()) {
      echo '{"message":"Customer Data Updated"}'; 
    } else {
      echo '{"message":"error"}'; 
      //echo '{"message":"'.$db->error.'"}'; 
    }
  } catch(Exception $e) {
    //echo '{"error":{"message":'. $e->getMessage() .'}}'; 
    echo '{"message":"Please Try Again Later"}'; 
  }
}

function updateProduct() {
  $id = (int)$_REQUEST['id'];
  $name = addslashes($_REQUEST['productname']);
  $sql = 'UPDATE PRODUCT SET PRODUCT_NAME=? WHERE PRODUCT_ID=?';
  try {
    $db = getConnection();
    $stmt = $db->prepare($sql);  
    $stmt->bind_param("si", $name,$id);
    if($stmt->execute()) {
      echo '{"message":"Product Data Updated"}'; 
    } else {
      echo '{"message":"error"}'; 
    }
  } catch(Exception $e) {
    //echo '{"error":{"message":'. $e->getMessage() .'}}'; 
    echo '{"message":"Please Try Again Later"}'; 
  }
}
function addBranch() {
  // echo "this method called"; die;
	// $bankid = $_REQUEST['bankid'];
  $bank_code = addslashes($_REQUEST['bankcode']);
  // echo $bank_code; die;
  $branch_name	= addslashes($_REQUEST['branchname']);
  $branch_code	= addslashes($_REQUEST['branchcode']);
  $country		= addslashes($_REQUEST['country']);
  $state			= addslashes($_REQUEST['state']);
  $city			= addslashes($_REQUEST['city']);
  $address		= addslashes($_REQUEST['address']);
  // echo $address; die;
  $sql = 'SELECT * FROM BRANCHES WHERE BANK_CODE=? AND BRANCH_CODE=?';
  try {
    $db = getConnection();
    $stmt = $db->prepare($sql);  
    $stmt->bind_param("ss", $bank_name,$branch_code);
    $stmt->execute();
    $stmt->store_result();
    if($stmt->num_rows == '0') {
      $stmt->close();
      $sql = "INSERT INTO BRANCHES (BRANCH_NAME,BRANCH_CODE,COUNTRY,STATE,CITY,ADDRESS, DATE_ADDED,DATE_UPDATED,CREATE_BY,UPDATED_BY,BANK_CODE) VALUES ('$branch_name','$branch_code','$country','$state','$city','$address', NOW(),NOW(),'','','$bank_code')";
      if ($db->query($sql)) {
        echo '{"message":"success"}';	$db = null; die;
      } else {
        echo '{"message":"'.$db->error.'"}';  $db = null;	die;
      }
      echo '{"message":"Please Try Again Later.."}';  $db = null;	die;
    } else {
      echo '{"message":"Branch Code Already Exist in the Selected Bank"}'; 
    }
  } catch(Exception $e) {
    //echo '{"error":{"message":'. $e->getMessage() .'}}'; 
    echo '{"message":"Please Try Again Later"}'; 
  }
}
function addLead() {
  $employeeId		= addslashes($_REQUEST['employeeId']);
  $branchId	= addslashes($_REQUEST['branchId']);
  $bankId		= addslashes($_REQUEST['bankId']);
  $customerId	= addslashes($_REQUEST['customerId']);
  $productId	= addslashes($_REQUEST['productId']);
  $campaignId		= addslashes($_REQUEST['campaignId']);
  $touchpointId			= addslashes($_REQUEST['touchpointId']);
  $commId			= addslashes($_REQUEST['commId']);
  $leadText		= addslashes($_REQUEST['leadText']);
  try {
    $db = getConnection();
	/* $sql_bc="SELECT BANK_CODE FROM BANK WHERE BANK_ID='$bankId'";
	 $bankc=$db->query($sql_bc);
	 $bank_code = $bankc->fetch_assoc();
  $code1= $bank_code['BANK_CODE']; */
  
   /* $sql_brc="SELECT BRANCH_CODE FROM BRANCHES WHERE BRANCH_ID='$branchId'";
	 $branchc=$db->query($sql_brc);
	 $branch_code = $branchc->fetch_assoc();
  $code2= $branch_code['BRANCH_CODE']; */ 
  
  
    $sql = "INSERT INTO LEADS (BANK_ID, BRANCH_ID, EMPLOYEE_ID,CUSTOMER_ID,PRODUCT_ID,TOUCHPOINT_ID,COMMUNCIATION_ID,CAMPAIGN_ID,DATE_ADDED,LEAD_TEXT,BANK_CODE,BRANCH_CODE) VALUES ('$bankId', '$branchId', '$employeeId','$customerId','$productId','$touchpointId','$commId','$campaignId', NOW(), '$leadText','$bankId','$branchId')";
    if ($db->query($sql)) {
      echo '{"message":"success"}';	$db = null; die;
    } else {
      echo '{"message":"'.$db->error.'"}';  $db = null;	die;
    }
  } catch(Exception $e) {
    //echo '{"error":{"message":'. $e->getMessage() .'}}'; 
    echo '{"message":"Please Try Again Later"}'; 
  }
}

function addEmployee() {
  $bank_name		= $_REQUEST['bankname'];
  $branch_name	= $_REQUEST['branchname'];
  $role_name		= (int)$_REQUEST['role'];
  $email			= addslashes($_REQUEST['email']);
  $password		= addslashes($_REQUEST['password']);
  $first_name		= addslashes($_REQUEST['firstname']);
  $last_name		= addslashes($_REQUEST['lastname']);
  $dob			= addslashes($_REQUEST['dob']);
  $phno			= addslashes($_REQUEST['phno']);
  $mono			= addslashes($_REQUEST['mono']);
  $gender			= addslashes($_REQUEST['gender']);
  $country		= addslashes($_REQUEST['country']);
  $state			= addslashes($_REQUEST['state']);
  $city			= addslashes($_REQUEST['city']);
  $address		= addslashes($_REQUEST['address']);
  $zip			= (int)addslashes($_REQUEST['zip']);
  $status			= (int)$_REQUEST['status'];
  $isfd			= (int)$_REQUEST['isfd'];
  
  $selectedRole = $_REQUEST['selectedRole'];
  $dob = explode('/',$dob);
  $dob = $dob[2].'-'.$dob[0].'-'.$dob[1];
  $sql = 'SELECT * FROM EMPLOYEE WHERE EMAIL=?';
  try {
    $db = getConnection();
    $stmt = $db->prepare($sql);  
    $stmt->bind_param("s", $email);
    $stmt->execute();
    $stmt->store_result();
    if($stmt->num_rows == '0') {
      $stmt->close();
      $manageBranch = '0';
      $manageBank = '0';
      $manageEmployee = '0';
      $manageTemplate = '0';
      $manageProduct = '0';
      $manageRole = '0';
      $manageTarget = '0';
      $manageBankAdmin = '0';
      $endpoint = '0';
      if($selectedRole == "Head Of Retail"){
        $manageBranch = '0';
        $manageBank = '0';
        $manageTemplate = '1';
        $manageTarget = '1';
        $manageProduct = '0';
        $manageEmployee = '0';
      }
      else if ($selectedRole == "Bank Admin"){
        $endpoint = '1';
        $manageBranch = '1';
        $manageEmployee = '1';
		$manageProduct = '1';
      }
      elseif($selectedRole == "Bank Manager"){
        $isfd = 1;
      }
	  $db = getConnection();
	 /* $sql_bc="SELECT BANK_CODE FROM BANK WHERE BANK_ID='$bank_name'";
	 $bankc=$db->query($sql_bc);
	 $bank_code = $bankc->fetch_assoc();
  $code1= $bank_code['BANK_CODE'] */;
  
   /*$sql_brc="SELECT BRANCH_CODE FROM BRANCHES WHERE BANK_CODE='$bank_name'";
	 $branchc=$db->query($sql_brc);
	 $branch_code = $branchc->fetch_assoc();
  $code2= $branch_code['BRANCH_CODE']; */

	
      $sql = "INSERT INTO EMPLOYEE (ROLE_ID,FIRST_NAME,LAST_NAME,GENDER,DATE_OF_BIRTH,EMAIL,PASSWORD,PHONE_NO,MOBILE_NO,ADDRESS,CITY,STATE,COUNTY,PIN_CODE,STATUS,DATE_ADDED,DATE_UPDATED,ISFLOWDESIGNER, MANAGE_BANK, MANAGE_BRANCH, MANAGE_EMPLOYEE, MANAGE_TEMPLATE, MANAGE_PRODUCT, MANAGE_ROLE, MANAGE_TARGET, MANAGE_BANK_ADMIN, ENDPOINT,BRANCH_CODE,BANK_CODE,EMP_CAM_BRA_CODE) 
	VALUES ('$role_name','$first_name','$last_name','$gender','$dob','$email','$password','$phno','$mono','$address','$city','$state','$country','$zip','$status',NOW(),NOW(),$isfd, '$manageBank','$manageBranch','$manageEmployee','$manageTemplate', '$manageProduct', '$manageRole', '$manageTarget', '$manageBankAdmin', '$endpoint','$branch_name','$bank_name','$branch_name')";
	//echo $sql;
      if ($db->query($sql)) {
		  employEmailSend($first_name,$last_name,$email,$password);
        echo '{"message":"success", "last_insert_id":"'.$db->insert_id.'"}';	
		
		
		$db = null; die;
      } else {
        echo '{"message":"'.$db->error.'"}';  $db = null;	die;
      }
      echo '{"message":"Please Try Again Later.."}';  $db = null;	die;
    } else {
      echo '{"message":"Employee Email Already Exist"}'; 
    }
  } catch(Exception $e) {
    echo '{"error":{"message":'. $e->getMessage() .'}}'; 
    //echo '{"message":"Please Try Again Later"}'; 
  }
}

/*Employe registration email sending*/
function employEmailSend($first_name,$last_name,$email,$password) {
require_once('../PHPMailer/PHPMailerAutoload.php');
	  $mailSubject="Touchpoint - Login Credentials";
	  $customerEmail=$email;
	  //$customerEmail="arunbutp@gmail.com";
	  $mailBody = '<html>
<head>
	<title></title>
	<meta charset="iso-8859-1" />


 <meta name="format-detection" content="telephone=no;address=no">


</head>
<body>
<img src="http://link.p0.com/1x1.dyn?0aEVvhe6ioIQmh2zqotPdeg2V=0" width="1" height="1" border="0">
<table cellpadding="0" cellspacing="0" border="0" width="100%" align="center">
	<tr>
		<td align="center" valign="top">
		<table class="width320" style="z-index: 10; font-family: Arial, Helvetica, sans-serif; color: #666666; font-size: 14px; line-height: 17px; min-width:640px;" width="640" border="0" cellspacing="0" cellpadding="0" align="center" valign="top">



<!-- MASTHEAD SECTION ======-->
<!--MOBILE MASTHEAD=-->
			<tr class="show" style="font-size: 0px; line-height: 0px; max-height: 0px; display: none;">
				<td class="showMast" style="font-size:0px; line-height: 0px; display:none; max-height:0px" align="left"><a href="http://link.alerts.usbank.com/u.d?T4GjfhjzoIyr9L8DPcr=21" target="_blank"><img class="showMast" border="0" src="http://image.email.usbank.com/lib/fe911570726d007475/m/2/Mobile_Masthead.jpg" style="font-size:0px;line-height:0px;display:none;max-height:0px" alt="U.S.&nbsp;Bank - All of US serving you&reg;" title="U.S.&nbsp;Bank - All of US serving you&reg;"></a></td>
			</tr>
<!--DESKTOP MASTHEAD-->
			<tr>
				<td class="hide" style="padding-bottom: 0px; margin-bottom: 0px;" width="640" align="left" valign="bottom">
				<div class="hide">
					<img class="hide" style="display:block" border="0" src="http://localhost:8080/tpv3/demo_new/images/header.jpg" width="640" height="65" alt="U.S.&nbsp;Bank - All of US serving you&reg;" title="">
				</div>
				</td>
			</tr>

<!--HERO AND BODY SECTION ==========-->
			<tr>
				<td class="width320" style="" align="left">
				<table class="width320" width="640" border="0" cellspacing="0" cellpadding="0">
					<tr>
						<td class="hide" style="line-height:0px; font-size:0px;" width="1" bgcolor="#696969" align="left"><img class="hide" height="1" src="http://image.email.usbank.com/lib/fe911570726d007475/m/1/spacer.gif" style="display:block" width="1"></td>
						<td align="left">
							<table class="bodyWidth320" width="638" border="0" cellspacing="0" cellpadding="0">
								<tr>
								<td bgcolor="#FFFFFF" width="638" align="left" valign="top" style="padding:20px;font-size: 14px; line-height: 17px; font-family: Arial, Helvetica, sans-serif; color:#666666;">
								Dear '.$last_name.', '.$first_name.'<br><br>
								Kindly find your login details!</td>
								<br><br>
								</tr>
							</table>
						</td>
						<td class="hide" style="line-height:0px; font-size:0px;" width="1" bgcolor="#696969" align="left"><img class="hide" height="1" src="http://image.email.usbank.com/lib/fe911570726d007475/m/1/spacer.gif" style="display:block" width="1"></td>
					</tr>
					
					<tr>
						<td class="hide" style="line-height:0px; font-size:0px;" width="1" bgcolor="#696969" align="left"><img class="hide" height="1" src="http://image.email.usbank.com/lib/fe911570726d007475/m/1/spacer.gif" style="display:block" width="1"></td>
						<td align="left">
							<table class="bodyWidth320" width="100px" border="0" cellspacing="1" cellpadding="0">
								<tr>
								<td bgcolor="#FFFFFF"  width="25px" align="left" valign="top" style="padding:0 0 0 18px;font-size: 14px;  font-family: Arial, Helvetica, sans-serif; color:#666666;">
								Username</td><td width="2px">:</td><td width="25px">'.$email.'</td></tr>								
								<tr><td width="25px" bgcolor="#FFFFFF"  align="left" valign="top" style="padding:0 0 0 18px;font-size: 14px;  font-family: Arial, Helvetica, sans-serif; color:#666666;">
								Password</td><td width="2px">:</td><td width="25px">'.$password.'</td></tr>								
							</table>
						</td>
						<td class="hide" style="line-height:0px; font-size:0px;" width="1" bgcolor="#696969" align="left"><img class="hide" height="1" src="http://image.email.usbank.com/lib/fe911570726d007475/m/1/spacer.gif" style="display:block" width="1"></td>
					</tr>

					<tr>
						<td class="hide" style="line-height:0px; font-size:0px;" width="1" bgcolor="#696969" align="left"><img class="hide" height="1" src="http://image.email.usbank.com/lib/fe911570726d007475/m/1/spacer.gif" style="display:block" width="1"></td>
						<td align="left">
							<table class="bodyWidth320"  border="0" cellspacing="1" cellpadding="0">
								<tr>
								<td bgcolor="#FFFFFF"   align="left" valign="top" style="padding:0 0 0 18px;font-size: 14px;  font-family: Arial, Helvetica, sans-serif; color:#666666;">
								Login URL</td><td>:</td><td>http://localhost:8080/tpv3/demo_new/login.html</td></tr>
							</table>
						</td>
						<td class="hide" style="line-height:0px; font-size:0px;" width="1" bgcolor="#696969" align="left"><img class="hide" height="1" src="http://image.email.usbank.com/lib/fe911570726d007475/m/1/spacer.gif" style="display:block" width="1"></td>
					</tr>					
				</table>               
			
				
				<table class="width320" width="640" border="0" cellspacing="0" cellpadding="0">
					<tr>
						<td class="hide" style="line-height:0px; font-size:0px;" width="1" bgcolor="#696969" align="left"><img class="hide" height="1" src="http://image.email.usbank.com/lib/fe911570726d007475/m/1/spacer.gif" style="display:block" width="1"></td>
						<td align="left">
							<table class="bodyWidth320" width="638" border="0" cellspacing="0" cellpadding="0">
								<tr>
								<td bgcolor="#FFFFFF" width="638" align="left" valign="top" style="padding:20px;font-size: 14px; line-height: 17px; font-family: Arial, Helvetica, sans-serif; color:#666666;">
								Thanks,<br>
								Touchpoint - Team.
								</td>
								<br><br>
								</tr>
							</table>
						</td>
						<td class="hide" style="line-height:0px; font-size:0px;" width="1" bgcolor="#696969" align="left"><img class="hide" height="1" src="http://image.email.usbank.com/lib/fe911570726d007475/m/1/spacer.gif" style="display:block" width="1"></td>
					</tr>
				</table>
<!-- END HERO AND BODY SECTION ==========-->

<!--SOCIAL BAR SECTION =====-->
				<table class="width320" width="640" border="0" cellspacing="0" cellpadding="0">
					<tr>
						<td class="hide" width="640" style="line-height: 5px; line-height: 5px;" align="left">
						<table class="hide" width="640" cellpadding="0" cellspacing="0" border="0" bgcolor="#d9edf8">
							<tr>
								<td width="79" align="left"><img border="0" style="display:block" src="http://localhost:8080/tpv3/demo_new/images/footer.jpg" alt="Find us on:" title="Find us on:"></td>
							</tr>
						</table>
						</td>
					</tr>

<!--MOBILE SOCIAL BAR =========-->
					<tr class="show" style="font-size:0px; line-height: 0px; display:none; visibility:hidden; border-collapse:collapse; max-height: 0px">
						<td class="show" style="font-size:0px; line-height: 0px; display:none; visibility:hidden; border-collapse:collapse; max-height: 0px" align="left">
						<div class="show" style="max-height: 0px; font-size:0px; line-height: 0px; display:none; visibility:hidden; border-collapse:collapse;">
							<table class="show" style="font-size:0px; line-height: 0px; display:none; visibility:hidden; border-collapse:collapse; max-height: 0px" cellpadding="0" cellspacing="0" border="0">

<!--FIRST ROW =========-->
								<tr class="show" style="font-size:0px; line-height: 0px; display:none; visibility:hidden; border-collapse:collapse; max-height: 0px">
									<td align="left">
									<div class="show" style="max-height: 0px; font-size:0px; line-height: 0px; display:none; visibility:hidden; border-collapse:collapse;">
										<table class="show" style="font-size:0px; line-height: 0px; display:none; visibility:hidden; border-collapse:collapse; max-height: 0px" cellpadding="0" cellspacing="0" border="0">
											<tr>
												<td align="left">
												<div class="show" style="max-height: 0px; font-size:0px; line-height: 0px; display:none; visibility:hidden; border-collapse:collapse;">
													<img class="show" style="font-size:0px;line-height:0px;display:none;max-height:0px" src="http://image.email.usbank.com/lib/fe911570726d007475/m/2/social_footer_mobile_01.jpg" border="0">
												</div>
												</td>
												<td align="left">
												<div class="show" style="max-height: 0px; font-size:0px; line-height: 0px; display:none; visibility:hidden; border-collapse:collapse;">
													<a href="http://link.alerts.usbank.com/u.d?Y4GjfhjzoIyr9Lv91=63"><img class="show" style="font-size:0px;line-height:0px;display:none;max-height:0px" src="http://image.email.usbank.com/lib/fe911570726d007475/m/2/social_footer_mobile_02.jpg" border="0"></a>
												</div>
												</td>
												<td align="left">
												<div class="show" style="max-height: 0px; font-size:0px; line-height: 0px; display:none; visibility:hidden; border-collapse:collapse;">
													<a href="http://link.alerts.usbank.com/u.d?Y4GjfhjzoIyr9Lv9O=73"><img class="show" style="font-size:0px;line-height:0px;display:none;max-height:0px" src="http://image.email.usbank.com/lib/fe911570726d007475/m/2/social_footer_mobile_03.jpg" border="0"></a>
												</div>
												</td>
												<td align="left">
												<div class="show" style="max-height: 0px; font-size:0px; line-height: 0px; display:none; visibility:hidden; border-collapse:collapse;">
													<a href="http://link.alerts.usbank.com/u.d?C4GjfhjzoIyr9Lv9D=83"><img class="show" style="font-size:0px;line-height:0px;display:none;max-height:0px" src="http://image.email.usbank.com/lib/fe911570726d007475/m/2/social_footer_mobile_04.jpg" border="0" alt="YouTube" title="YouTube"></a>
												</div>
												</td>
											</tr>
										</table>
									</div>
									</td>

<!--SECOND ROW =========-->
								<tr class="show" style="font-size:0px; line-height: 0px; display:none; visibility:hidden; border-collapse:collapse; max-height: 0px">
									<td align="left">
									<div class="show" style="max-height: 0px; font-size:0px; line-height: 0px; display:none; visibility:hidden; border-collapse:collapse;">
										<table class="show" style="font-size:0px; line-height: 0px; display:none; visibility:hidden; border-collapse:collapse; max-height: 0px" cellpadding="0" cellspacing="0" border="0">
											<tr>
												<td align="left">
												<div class="show" style="max-height: 0px; font-size:0px; line-height: 0px; display:none; visibility:hidden; border-collapse:collapse;">
													<a href="http://link.alerts.usbank.com/u.d?H4GjfhjzoIyr9Lv9E=93"><img class="show" style="font-size:0px;line-height:0px;display:none;max-height:0px" src="http://image.email.usbank.com/lib/fe911570726d007475/m/2/social_footer_mobile_05.jpg" border="0" alt="Branch Locator" title="Branch Locator"></a>
												</div>
												</td>
												<td align="left">
												<div class="show" style="max-height: 0px; font-size:0px; line-height: 0px; display:none; visibility:hidden; border-collapse:collapse;">
													<a href="http://link.alerts.usbank.com/u.d?H4GjfhjzoIyr9Lv9Z=103"><img class="show" style="font-size:0px;line-height:0px;display:none;max-height:0px" src="http://image.email.usbank.com/lib/fe911570726d007475/m/2/social_footer_mobile_06.jpg" border="0" alt="usbank.com" title="usbank.com"></a>
												</div>
												</td>
											</tr>
										</table>
									</div>
									</td>
								</tr>

<!--THIRD ROW =========-->
								<tr class="show" style="font-size:0px; line-height: 0px; display:none; visibility:hidden; border-collapse:collapse; max-height: 0px">
									<td class="show" style="font-size:0px; line-height: 0px; display:none;" align="left">
									<div class="show" style="max-height: 0px; font-size:0px; line-height: 0px; display:none; visibility:hidden; border-collapse:collapse; max-height: 0px">
										<table class="show" style="font-size:0px; line-height: 0px; display:none;" cellpadding="0" cellspacing="0" border="0">
											<tr>
												<td align="left">
												<div class="show" style="max-height: 0px; font-size:0px; line-height: 0px; display:none; visibility:hidden; border-collapse:collapse; max-height: 0px">
													<img class="show" style="font-size:0px;line-height:0px;display:none;max-height:0px" src="http://image.email.usbank.com/lib/fe911570726d007475/m/2/social_footer_mobile_07.jpg" border="0" alt="800-US-BANKS (800-872-2657)" title="800-US-BANKS (800-872-2657)">
												</div>
												</td>
												<td class="show" style="max-height: 0px; font-size:0px; line-height: 0px; display:none; visibility:hidden; border-collapse:collapse; max-height: 0px" align="left"><a href="http://link.alerts.usbank.com/u.d?QYGjfhjzoIyr9Lv9S=113" target="_blank"><img class="show" style="font-size:0px;line-height:0px;display:none;max-height:0px" src="http://image.email.usbank.com/lib/fe911570726d007475/m/2/social_footer_mobile_08.jpg" border="0" alt="U.S.&nbsp;Bank Mobile" title="U.S.&nbsp;Bank Mobile"></a></td>
											</tr>
										</table>
									</div>
									</td>
								</tr>
							</table>
						</div>
						</td>
					</tr>
				</table>
<!-- End Social Footer -->


				</td>
			</tr>
		</table>
		</td>
	</tr>
</table>
</body>
</html>';		
	$mail = new PHPMailer();
	$mail->isSMTP();
	$mail->SMTPDebug = 0;
	$mail->Debugoutput = 'html';
	$mail->Host = 'smtp.gmail.com';
	$mail->Port = 587;
	$mail->SMTPSecure = 'tls';
	$mail->SMTPAuth = true;
	$mail->Username = "touchpointdemo16@gmail.com";
//$mail->Password = "bvacobpmasflawhs";							
$mail->Password = "touchpoint@123";
$mail->setFrom('touchpointdemo16@gmail.com', 'Touchpoint');
	$mail->Subject   =	$mailSubject;
	$mail->MsgHTML($mailBody);
	$mail->AddAddress($customerEmail);
    $mail->Send();
	
}
/*Employe registration email sending*/	


/*HOR Branch update reminder email sending*/
function horemployEmailSend($first_name,$last_name,$email,$password) {
require_once('../PHPMailer/PHPMailerAutoload.php');
	  $mailSubject="Touchpoint - Login Credentials";
	  $customerEmail=$email;
	  //$customerEmail="arunbutp@gmail.com";
	  $mailBody = '<html>
<head>
	<title></title>
	<meta charset="iso-8859-1" />


 <meta name="format-detection" content="telephone=no;address=no">


</head>
<body>
<img src="http://link.p0.com/1x1.dyn?0aEVvhe6ioIQmh2zqotPdeg2V=0" width="1" height="1" border="0">
<table cellpadding="0" cellspacing="0" border="0" width="100%" align="center">
	<tr>
		<td align="center" valign="top">
		<table class="width320" style="z-index: 10; font-family: Arial, Helvetica, sans-serif; color: #666666; font-size: 14px; line-height: 17px; min-width:640px;" width="640" border="0" cellspacing="0" cellpadding="0" align="center" valign="top">



<!-- MASTHEAD SECTION ======-->
<!--MOBILE MASTHEAD=-->
			<tr class="show" style="font-size: 0px; line-height: 0px; max-height: 0px; display: none;">
				<td class="showMast" style="font-size:0px; line-height: 0px; display:none; max-height:0px" align="left"><a href="http://link.alerts.usbank.com/u.d?T4GjfhjzoIyr9L8DPcr=21" target="_blank"><img class="showMast" border="0" src="http://image.email.usbank.com/lib/fe911570726d007475/m/2/Mobile_Masthead.jpg" style="font-size:0px;line-height:0px;display:none;max-height:0px" alt="U.S.&nbsp;Bank - All of US serving you&reg;" title="U.S.&nbsp;Bank - All of US serving you&reg;"></a></td>
			</tr>
<!--DESKTOP MASTHEAD-->
			<tr>
				<td class="hide" style="padding-bottom: 0px; margin-bottom: 0px;" width="640" align="left" valign="bottom">
				<div class="hide">
					<img class="hide" style="display:block" border="0" src="http://localhost:8080/tpv3/demo_new/images/header.jpg" width="640" height="65" alt="U.S.&nbsp;Bank - All of US serving you&reg;" title="">
				</div>
				</td>
			</tr>

<!--HERO AND BODY SECTION ==========-->
			<tr>
				<td class="width320" style="" align="left">
				<table class="width320" width="640" border="0" cellspacing="0" cellpadding="0">
					<tr>
						<td class="hide" style="line-height:0px; font-size:0px;" width="1" bgcolor="#696969" align="left"><img class="hide" height="1" src="http://image.email.usbank.com/lib/fe911570726d007475/m/1/spacer.gif" style="display:block" width="1"></td>
						<td align="left">
							<table class="bodyWidth320" width="638" border="0" cellspacing="0" cellpadding="0">
								<tr>
								<td bgcolor="#FFFFFF" width="638" align="left" valign="top" style="padding:20px;font-size: 14px; line-height: 17px; font-family: Arial, Helvetica, sans-serif; color:#666666;">
								Dear '.$last_name.', '.$first_name.'<br><br>
								Your Bank Admin added New Branch  in to your Account.Kindly view in your Account</td>
								<br><br>
								</tr>
							</table>
						</td>
						<td class="hide" style="line-height:0px; font-size:0px;" width="1" bgcolor="#696969" align="left"><img class="hide" height="1" src="http://image.email.usbank.com/lib/fe911570726d007475/m/1/spacer.gif" style="display:block" width="1"></td>
					</tr>	
				</table>               
			
				
				<table class="width320" width="640" border="0" cellspacing="0" cellpadding="0">
					<tr>
						<td class="hide" style="line-height:0px; font-size:0px;" width="1" bgcolor="#696969" align="left"><img class="hide" height="1" src="http://image.email.usbank.com/lib/fe911570726d007475/m/1/spacer.gif" style="display:block" width="1"></td>
						<td align="left">
							<table class="bodyWidth320" width="638" border="0" cellspacing="0" cellpadding="0">
								<tr>
								<td bgcolor="#FFFFFF" width="638" align="left" valign="top" style="padding:20px;font-size: 14px; line-height: 17px; font-family: Arial, Helvetica, sans-serif; color:#666666;">
								Thanks,<br>
								Touchpoint - Team.
								</td>
								<br><br>
								</tr>
							</table>
						</td>
						<td class="hide" style="line-height:0px; font-size:0px;" width="1" bgcolor="#696969" align="left"><img class="hide" height="1" src="http://image.email.usbank.com/lib/fe911570726d007475/m/1/spacer.gif" style="display:block" width="1"></td>
					</tr>
				</table>
<!-- END HERO AND BODY SECTION ==========-->

<!--SOCIAL BAR SECTION =====-->
				<table class="width320" width="640" border="0" cellspacing="0" cellpadding="0">
					<tr>
						<td class="hide" width="640" style="line-height: 5px; line-height: 5px;" align="left">
						<table class="hide" width="640" cellpadding="0" cellspacing="0" border="0" bgcolor="#d9edf8">
							<tr>
								<td width="79" align="left"><img border="0" style="display:block" src="http://localhost:8080/tpv3/demo_new/images/footer.jpg" alt="Find us on:" title="Find us on:"></td>
							</tr>
						</table>
						</td>
					</tr>

<!--MOBILE SOCIAL BAR =========-->
					<tr class="show" style="font-size:0px; line-height: 0px; display:none; visibility:hidden; border-collapse:collapse; max-height: 0px">
						<td class="show" style="font-size:0px; line-height: 0px; display:none; visibility:hidden; border-collapse:collapse; max-height: 0px" align="left">
						<div class="show" style="max-height: 0px; font-size:0px; line-height: 0px; display:none; visibility:hidden; border-collapse:collapse;">
							<table class="show" style="font-size:0px; line-height: 0px; display:none; visibility:hidden; border-collapse:collapse; max-height: 0px" cellpadding="0" cellspacing="0" border="0">

<!--FIRST ROW =========-->
								<tr class="show" style="font-size:0px; line-height: 0px; display:none; visibility:hidden; border-collapse:collapse; max-height: 0px">
									<td align="left">
									<div class="show" style="max-height: 0px; font-size:0px; line-height: 0px; display:none; visibility:hidden; border-collapse:collapse;">
										<table class="show" style="font-size:0px; line-height: 0px; display:none; visibility:hidden; border-collapse:collapse; max-height: 0px" cellpadding="0" cellspacing="0" border="0">
											<tr>
												<td align="left">
												<div class="show" style="max-height: 0px; font-size:0px; line-height: 0px; display:none; visibility:hidden; border-collapse:collapse;">
													<img class="show" style="font-size:0px;line-height:0px;display:none;max-height:0px" src="http://image.email.usbank.com/lib/fe911570726d007475/m/2/social_footer_mobile_01.jpg" border="0">
												</div>
												</td>
												<td align="left">
												<div class="show" style="max-height: 0px; font-size:0px; line-height: 0px; display:none; visibility:hidden; border-collapse:collapse;">
													<a href="http://link.alerts.usbank.com/u.d?Y4GjfhjzoIyr9Lv91=63"><img class="show" style="font-size:0px;line-height:0px;display:none;max-height:0px" src="http://image.email.usbank.com/lib/fe911570726d007475/m/2/social_footer_mobile_02.jpg" border="0"></a>
												</div>
												</td>
												<td align="left">
												<div class="show" style="max-height: 0px; font-size:0px; line-height: 0px; display:none; visibility:hidden; border-collapse:collapse;">
													<a href="http://link.alerts.usbank.com/u.d?Y4GjfhjzoIyr9Lv9O=73"><img class="show" style="font-size:0px;line-height:0px;display:none;max-height:0px" src="http://image.email.usbank.com/lib/fe911570726d007475/m/2/social_footer_mobile_03.jpg" border="0"></a>
												</div>
												</td>
												<td align="left">
												<div class="show" style="max-height: 0px; font-size:0px; line-height: 0px; display:none; visibility:hidden; border-collapse:collapse;">
													<a href="http://link.alerts.usbank.com/u.d?C4GjfhjzoIyr9Lv9D=83"><img class="show" style="font-size:0px;line-height:0px;display:none;max-height:0px" src="http://image.email.usbank.com/lib/fe911570726d007475/m/2/social_footer_mobile_04.jpg" border="0" alt="YouTube" title="YouTube"></a>
												</div>
												</td>
											</tr>
										</table>
									</div>
									</td>

<!--SECOND ROW =========-->
								<tr class="show" style="font-size:0px; line-height: 0px; display:none; visibility:hidden; border-collapse:collapse; max-height: 0px">
									<td align="left">
									<div class="show" style="max-height: 0px; font-size:0px; line-height: 0px; display:none; visibility:hidden; border-collapse:collapse;">
										<table class="show" style="font-size:0px; line-height: 0px; display:none; visibility:hidden; border-collapse:collapse; max-height: 0px" cellpadding="0" cellspacing="0" border="0">
											<tr>
												<td align="left">
												<div class="show" style="max-height: 0px; font-size:0px; line-height: 0px; display:none; visibility:hidden; border-collapse:collapse;">
													<a href="http://link.alerts.usbank.com/u.d?H4GjfhjzoIyr9Lv9E=93"><img class="show" style="font-size:0px;line-height:0px;display:none;max-height:0px" src="http://image.email.usbank.com/lib/fe911570726d007475/m/2/social_footer_mobile_05.jpg" border="0" alt="Branch Locator" title="Branch Locator"></a>
												</div>
												</td>
												<td align="left">
												<div class="show" style="max-height: 0px; font-size:0px; line-height: 0px; display:none; visibility:hidden; border-collapse:collapse;">
													<a href="http://link.alerts.usbank.com/u.d?H4GjfhjzoIyr9Lv9Z=103"><img class="show" style="font-size:0px;line-height:0px;display:none;max-height:0px" src="http://image.email.usbank.com/lib/fe911570726d007475/m/2/social_footer_mobile_06.jpg" border="0" alt="usbank.com" title="usbank.com"></a>
												</div>
												</td>
											</tr>
										</table>
									</div>
									</td>
								</tr>

<!--THIRD ROW =========-->
								<tr class="show" style="font-size:0px; line-height: 0px; display:none; visibility:hidden; border-collapse:collapse; max-height: 0px">
									<td class="show" style="font-size:0px; line-height: 0px; display:none;" align="left">
									<div class="show" style="max-height: 0px; font-size:0px; line-height: 0px; display:none; visibility:hidden; border-collapse:collapse; max-height: 0px">
										<table class="show" style="font-size:0px; line-height: 0px; display:none;" cellpadding="0" cellspacing="0" border="0">
											<tr>
												<td align="left">
												<div class="show" style="max-height: 0px; font-size:0px; line-height: 0px; display:none; visibility:hidden; border-collapse:collapse; max-height: 0px">
													<img class="show" style="font-size:0px;line-height:0px;display:none;max-height:0px" src="http://image.email.usbank.com/lib/fe911570726d007475/m/2/social_footer_mobile_07.jpg" border="0" alt="800-US-BANKS (800-872-2657)" title="800-US-BANKS (800-872-2657)">
												</div>
												</td>
												<td class="show" style="max-height: 0px; font-size:0px; line-height: 0px; display:none; visibility:hidden; border-collapse:collapse; max-height: 0px" align="left"><a href="http://link.alerts.usbank.com/u.d?QYGjfhjzoIyr9Lv9S=113" target="_blank"><img class="show" style="font-size:0px;line-height:0px;display:none;max-height:0px" src="http://image.email.usbank.com/lib/fe911570726d007475/m/2/social_footer_mobile_08.jpg" border="0" alt="U.S.&nbsp;Bank Mobile" title="U.S.&nbsp;Bank Mobile"></a></td>
											</tr>
										</table>
									</div>
									</td>
								</tr>
							</table>
						</div>
						</td>
					</tr>
				</table>
<!-- End Social Footer -->


				</td>
			</tr>
		</table>
		</td>
	</tr>
</table>
</body>
</html>';		
	$mail = new PHPMailer();
	$mail->isSMTP();
	$mail->SMTPDebug = 0;
	$mail->Debugoutput = 'html';
	$mail->Host = 'smtp.gmail.com';
	$mail->Port = 587;
	$mail->SMTPSecure = 'tls';
	$mail->SMTPAuth = true;
	$mail->Username = "touchpointdemo16@gmail.com";
//$mail->Password = "bvacobpmasflawhs";							
$mail->Password = "touchpoint@123";
$mail->setFrom('touchpointdemo16@gmail.com', 'Touchpoint');
	$mail->Subject   =	$mailSubject;
	$mail->MsgHTML($mailBody);
	$mail->AddAddress($customerEmail);
    $mail->Send();
	
}
/*HOR Branch update reminder email sending*/	
	
	
function addCustomer() {
  $bank_name		= (int)$_REQUEST['bankname'];
  $branch_name	= (int)$_REQUEST['branchname'];
  $employee_name	= (int)$_REQUEST['employee'];
  $email			= addslashes($_REQUEST['email']);
  $first_name		= addslashes($_REQUEST['firstname']);
  $last_name		= addslashes($_REQUEST['lastname']);
  $doj			= addslashes($_REQUEST['doj']);
  $dob			= addslashes($_REQUEST['dob']);
  $phno			= addslashes($_REQUEST['phno']);
  $mono			= addslashes($_REQUEST['mono']);
  $gender			= addslashes($_REQUEST['gender']);
  $country		= addslashes($_REQUEST['country']);
  $state			= addslashes($_REQUEST['state']);
  $city			= addslashes($_REQUEST['city']);
  $address		= addslashes($_REQUEST['address']);
  $zip			= (int)addslashes($_REQUEST['zip']);
  $account		= addslashes($_REQUEST['account']);
  $occupation		= addslashes($_REQUEST['occupation']);
  $dob = explode('/',$dob);
  $dob = $dob[2].'-'.$dob[0].'-'.$dob[1];
  $doj = explode('/',$doj);
  $doj = $doj[2].'-'.$doj[0].'-'.$doj[1];
  $sql = 'SELECT * FROM CUSTOMER WHERE EMAIL=?';
  try {
    $db = getConnection();
    $stmt = $db->prepare($sql);  
    $stmt->bind_param("s", $email);
    $stmt->execute();
    $stmt->store_result();
    if($stmt->num_rows == '0') {
      $stmt->close();
      $sql = "INSERT INTO CUSTOMER (BANK_ID,BRANCH_ID,EMPLOYEE_ID,ACCOUNT_TYPE,EMAIL,FIRST_NAME,LAST_NAME,OCCUPATION,DATE_OF_JOIN,PHONE_NO,MOBILE_NO,GENDER,DATE_OF_BIRTH,ADDRESS,CITY,STATE,COUNTRY,PINCODE,DATE_ADDED,DATE_UPDATED) 
	VALUES ($bank_name,$branch_name,$employee_name,'$account','$email','$first_name','$last_name','$occupation','$doj','$phno','$mono','$gender','$dob','$address','$city','$state','$country','$zip',NOW(),NOW())";
      if ($db->query($sql)) {
        echo '{"message":"success"}';	$db = null; die;
      } else {
        echo '{"message":"'.$db->error.'"}';  $db = null;	die;
      }
      echo '{"message":"Please Try Again Later.."}';  $db = null;	die;
    } else {
      echo '{"message":"Customer Email Already Exist"}'; 
    }
  } catch(Exception $e) {
    echo '{"error":{"message":'. $e->getMessage() .'}}'; 
    //echo '{"message":"Please Try Again Later"}'; 
  }
}
function addRole() {
  $bank_name = addslashes($_REQUEST['bankname']);
  $role_name = addslashes($_REQUEST['role_name']);
  $sql = 'SELECT * FROM ROLE WHERE BANK_CODE=? AND ROLE_NAME=?';
  try {
    $db = getConnection();
    $stmt = $db->prepare($sql);  
    $stmt->bind_param("ss", $bank_name,$role_name);
    $stmt->execute();
    $stmt->store_result();
    if($stmt->num_rows == '0') {
      $stmt->close();
      $sql = "INSERT INTO ROLE (BANK_ID,ROLE_NAME,BANK_CODE) VALUES ('$bank_name','$role_name','$bank_name')";
      if ($db->query($sql)) {
        echo '{"message":"success"}';	$db = null; die;
      }
      echo '{"message":"Please Try Again Later.."}';  $db = null;	die;
    } else {
      echo '{"message":"Bank Name Already Exist"}'; 
    }
  } catch(Exception $e) {
    //echo '{"error":{"message":'. $e->getMessage() .'}}'; 
    echo '{"message":"Please Try Again Later"}'; 
  }
}

function addTemplate() {
	//$bankid = $_POST['bankid'];
  $tmp_bank_id=addslashes($_POST['bankname']);
  echo "bank code".$tmp_bank_id;
 // exit();
  $tmp_name=addslashes($_POST['tmp_name']);
  $tmp_content=addslashes($_POST['tmp_content']);
  //$tmp_contents=htmlspecialchars($tmp_content);
  $tmp_contents=html_entity_decode($tmp_content);
  $tmp_type=addslashes($_POST['tmp_type']);
  $screenShot = addslashes($_POST['screenShot']);
  $db = getConnection();
  try {
   // $db = getConnection();
    $sql = "INSERT INTO TEMPLATE (`BANK_ID`,`TEMPLATE_TYPE`,`TEMPLATE_NAME`,`TEMPLATE_CONTENT`,`SCREENSHOT`,`DATE_ADDED`,`DATE_UPDATED`,`BANK_CODE`) VALUES ('1','$tmp_type','$tmp_name','$tmp_contents','$screenShot',NOW(),NOW(),'$tmp_bank_id')";
	//echo $sql;

    if ($db->query($sql)) {
      echo '{"message":"success"}';	
      $db = null; die;
    } else {
      echo '{"message":"'.json_encode($db->error).'"}'; 
    }
  } catch(Exception $e) {
    //echo '{"error":{"message":'. $e->getMessage() .'}}'; 
    echo '{"message":"Please Try Again Later"}'; 
  }	

}


function editTmpForm() 
{
  try 
  {
    $bankCode = $_REQUEST['bid'];
	$db = getConnection();
    $sql = "SELECT e.TEMPLATE_TYPE,e.TEMPLATE_NAME,e.TEMPLATE_ID,r.BANK_CODE,r.BANK_NAME FROM TEMPLATE e JOIN BANK r ON r.BANK_CODE=e.BANK_CODE WHERE e.BANK_CODE='$bankCode'";
	
    if ($result = $db->query($sql)) 
    {
      $row = array();
      $i = 1;
      $table_data = '';
      while($data = $result->fetch_assoc())
      {
        /* $action_data = '<a href="template.html?id='.$data['TEMPLATE_ID'].'"><i class="fa fa-pencil-square-o" title="Edit"></i><i class="fa fa-trash-o" title="Delete"></i></a>';
          $table_data .= '<tr><td class="a-center">'.$i.'</td><td  class="left-align" >'.$data['BANK_NAME'].'</td><td>'.$data['TEMPLATE_TYPE'].'</td><td  class="left-align" >'.$data['TEMPLATE_NAME'].'</td><td class="last">'.$action_data.'</td></tr>';
					$i = $i + 1; */

        $action_data = '<a href="template.html?id='.$data['TEMPLATE_ID'].'"><i class="fa fa-pencil-square-o" title="Edit"></i></a>&nbsp;&nbsp;<a href="javascript:delete_template('.$data['TEMPLATE_ID'].')" ><i class="fa fa-trash-o" title="Delete"></i></a>';
        $table_data .= '<tr><td class="a-center">'.$i.'</td><td  class="left-align" >'.$data['BANK_NAME'].'</td><td>'.$data['TEMPLATE_TYPE'].'</td><td  class="left-align" >'.$data['TEMPLATE_NAME'].'</td><td class="last">'.$action_data.'</td></tr>';
        $i = $i + 1;
      }
      echo '{"TEMPLATE": ' . json_encode($table_data) . '}';
    } 
    else 
    {
      echo $db->error;
    }
  } 
  catch(Exception $e)
  {
    //echo '{"error":{"message":'. $e->getMessage() .'}}'; 
    echo '{"message":"Please Try Again Later"}'; 
  }

}





function updateTmpForm() 
{
  $id=addslashes($_POST['id']);
  $tmp_bank_id=addslashes($_POST['bankname']);
  $tmp_name=addslashes($_POST['tmp_name']);
  $tmp_content=addslashes($_POST['tmp_content']);
  $tmp_type=addslashes($_POST['tmp_type']);
  //echo $tmp_bank_id;
  $db = getConnection();
  $sql = "UPDATE TEMPLATE SET BANK_CODE='$tmp_bank_id',TEMPLATE_TYPE='$tmp_type',TEMPLATE_NAME='$tmp_name', TEMPLATE_CONTENT='$tmp_content' WHERE TEMPLATE_ID='$id'";


  if ($result = $db->query($sql)) 
  {
    echo "Updated";	
    //$db = null; 
  } 

}

function getTemplateId() 
{
  try 
  {
    $id=addslashes($_REQUEST['id']);
    $db = getConnection();
    $sql="SELECT BANK_CODE FROM TEMPLATE WHERE TEMPLATE_ID='$id'";
    $bid=$db->query($sql);
    $res1=mysqli_fetch_array($bid);
    $b_id=$res1['BANK_CODE'];
    $sqls = "SELECT BANK_NAME FROM BANK WHERE BANK_CODE='$b_id'";
    if ($result = $db->query($sqls)) 
    {
      while($res2=mysqli_fetch_array($result))
      {
        $emparray = $res2['BANK_NAME'];
        //print_r($emparray);

        $json= json_encode($emparray);

      }    

      echo '{"BANK_NAME": ' . $json . '}';
    } 
    else 
    {
      echo $db->error;
    }
  } 
  catch(Exception $e)
  {
    //echo '{"error":{"message":'. $e->getMessage() .'}}'; 
    echo '{"message":"Please Try Again Later"}'; 
  }

}

function GetTemplatenameList() 
{

  try {
    $id=addslashes($_REQUEST['id']);
    $db = getConnection();
    $sql = "SELECT TEMPLATE_NAME FROM TEMPLATE WHERE TEMPLATE_ID='$id'";
    if ($result = $db->query($sql)) {
      $table_data = '';
      while($data = $result->fetch_assoc()) {
        $table_data = $data; 
      }
      echo '{"TEMPLATE_NAME": ' . json_encode($table_data) . '}';
    } else {
      echo '{"message":"Template Data Not Available"}'; 
    }
  } catch(Exception $e) {
    //echo '{"error":{"message":'. $e->getMessage() .'}}'; 
    echo '{"message":"Please Try Again Later"}'; 
  }
}


function GetTemplateType() 
{

  $id=addslashes($_REQUEST['id']);
  $db = getConnection();
  $sql = "SELECT TEMPLATE_TYPE FROM TEMPLATE WHERE TEMPLATE_ID='$id'";
  $result = $db->query($sql);	
  $data = mysqli_fetch_assoc($result);
  echo $data['TEMPLATE_TYPE'];
}

function GetTemplatecontentList() 
{

  $id=addslashes($_REQUEST['id']);
  $db = getConnection();
  $sql = "SELECT TEMPLATE_CONTENT FROM TEMPLATE WHERE TEMPLATE_ID='$id'";
  $result = $db->query($sql);	
  $data = mysqli_fetch_assoc($result);
  echo htmlspecialchars_decode($data['TEMPLATE_CONTENT']);

}

function GetTemplatetypeList() 
{

  $db = getConnection();
  $sql="SELECT DISTINCT TEMPLATE_TYPE FROM TEMPLATE";
  if ($result = $db->query($sql))
  {
    $row = array();

    $table_data = '';

    while($data = $result->fetch_assoc()) 
    {
      $table_data[] = $data; 
    }
    echo '{"TEMPLATE_TYPE_LIST": ' . json_encode($table_data) . '}';
  }

}



function customerResponse() {
  $bankId = '';
  $branchId = '';
  $productId = '';
  $employeeId = '';
  $customerId = '';
  $fDate = '';
  $tDate = '';
  if(isset($_REQUEST['bankId'])) {
    $bankId = $_REQUEST['bankId'];
  }
  if(isset($_REQUEST['branchId'])) {
    $branchId = $_REQUEST['branchId'];
  }
  if(isset($_REQUEST['productId'])) {
    $productId = $_REQUEST['productId'];
  }
  if(isset($_REQUEST['employeeId'])) {
    $employeeId = $_REQUEST['employeeId'];
  }
  if(isset($_REQUEST['fromDate']) && isset($_REQUEST['toDate'])) {
    if($_REQUEST['fromDate'] != '' && $_REQUEST['toDate'] != '') {
      $fDate = explode("/",$_REQUEST['fromDate']);
      $fDate = $fDate[2].'-'.$fDate[0].'-'.$fDate[1].' 00:00:00';
      $tDate = explode("/",$_REQUEST['toDate']);
      $tDate = $tDate[2].'-'.$tDate[0].'-'.$tDate[1].' 23:59:59';
    }
  }
  if(isset($_REQUEST['listing'])) {
    $listing = $_REQUEST['listing'];
    if($listing == 'product') {
		//echo $listing;
      $db = getConnection();
	  

      $sql = "SELECT a.* FROM PRODUCT a WHERE a.BANK_ID='$bankId' ORDER BY a.PRODUCT_ID ASC";
    //  echo $sql; exit;
	    /*$sql_emp="SELECT BANK_CODE FROM BANK WHERE BANK_ID='$bankId'";
			
			 $empid=$db->query($sql_emp);
	 $employee_ids = $empid->fetch_assoc();
  $code2= $employee_ids['BANK_CODE']; */
  
  
      if ($result = $db->query($sql)) {
        $table_data = array();
        while($data = $result->fetch_assoc()) {
       //$table_data[] = array(str_replace(" ","_",$data['PRODUCT_NAME'])=>sendCountJson($bankId,$branchId,$data['PRODUCT_NAME'], $employeeId, $customerId, $fDate, $tDate));
       $table_data[] = array(str_replace(" ","_",$data['PRODUCT_NAME'])=>sendCountJson($bankId,$branchId, $data['PRODUCT_ID'] , $employeeId, $customerId, $fDate, $tDate));
        }
        echo json_encode($table_data);
      }
      $result->close();
    } else if($listing == 'date' && ($fDate == '' || $tDate == '')) {
      $todayDate = date('m/d/Y');
      $table_data = array();
      for($i=10;$i>=0;$i--) {
        $userFmt = date('m/d/Y',strtotime($todayDate . "-".$i." days"));
        $plusNDate = date('Y-m-d',strtotime($todayDate . "-".$i." days"));
        $plusNDateStart = $plusNDate.' 00:00:00';
        $plusNDateEnd = $plusNDate.' 23:59:59';
        $table_data[] = array($userFmt=>sendCountJson($bankId, $branchId, $productId, $employeeId, $customerId, $plusNDateStart, $plusNDateEnd));
      }
      echo json_encode($table_data);
    }  else if($listing == 'date' && $fDate != '' && $tDate != '') {
      $table_data = array();
      //Hyphen added in between two dates
      $table_data[] = array($_REQUEST['fromDate'].' - '.$_REQUEST['toDate']=>sendCountJson($bankId, $branchId, $productId, $employeeId, $customerId, $fDate, $tDate));
      echo json_encode($table_data);
    } else if($listing == 'branch') {
      $db = getConnection();
	  $sql = "SELECT a.* FROM PRODUCT a ORDER BY a.PRODUCT_ID ASC";
      //echo $sql;
	   /* $sql_emp="SELECT BANK_CODE FROM BANK WHERE BANK_CODE='$bankId'";
			
			 $empid=$db->query($sql_emp);
	 $employee_ids = $empid->fetch_assoc();
  $code2= $employee_ids['BANK_CODE' ];*/
	 if ($_REQUEST['logRoleId'] == 3) {
	  $brID = $_REQUEST['logBranchId'];
      $sql = "SELECT a.* FROM BRANCHES a WHERE a.BANK_CODE='$bankId' AND a.BRANCH_CODE='$brId' ORDER BY a.BRANCH_CODE ASC";
	 } else if ($_REQUEST['logRoleId'] == 2) {
	  $loggedUserId = $_REQUEST['logid'];
	  $sql = "SELECT b.BRANCH_CODE,b.BRANCH_NAME 
				FROM BRANCH_INTEGRATION a 
				INNER JOIN BRANCHES b ON a.BRANCH_CODE=b.BRANCH_CODE 
				WHERE a.HEAD_OF_RETAIL='$loggedUserId'";
			
	 } else {
	  $sql = "SELECT a.* FROM BRANCHES a WHERE a.BANK_CODE='$bankId' AND a.BRANCH_CODE='$brId' ORDER BY a.BRANCH_CODE ASC";
	 }
	 //echo $sql;
	  
	  
      if ($result = $db->query($sql)) {
        $table_data = array();
        while($data = $result->fetch_assoc()) {
          $table_data[] = array(str_replace(" ","_",$data['BRANCH_NAME'])=>sendCountJson($bankId, $data['BRANCH_CODE'], $productId, $employeeId, $customerId, $fDate, $tDate));
        }
        echo json_encode($table_data);
      } else {
		echo $db->error;
	  }
      $result->close();
    }  else if($listing == 'officer') {
      $db = getConnection();
	  
	 /*  $sql_emp="SELECT BANK_CODE FROM BANK WHERE BANK_ID='$bankId'";
			
			 $empid=$db->query($sql_emp);
	 $employee_ids = $empid->fetch_assoc();
  $code2= $employee_ids['BANK_CODE']; */
	  
      $sql = "SELECT a.* FROM EMPLOYEE a WHERE a.BANK_CODE='$bankId' AND a.BRANCH_CODE='$branchId' AND a.BRANCH_CODE != '' AND a.ROLE_ID=4 ORDER BY a.FIRST_NAME ASC";
	 //echo $sql;
      if ($result = $db->query($sql)) {
        $table_data = array();
        while($data = $result->fetch_assoc()) {
          $table_data[] = array("".$data['LAST_NAME'].", ".$data['FIRST_NAME'].""=>sendCountJson($bankId, $branchId, $productId, $data['EMPLOYEE_ID'], $customerId, $fDate, $tDate));
        }
        echo json_encode($table_data);
      } else {
        //echo $db->error;
      }
    }  else if($listing == 'customer') {
		
      $db = getConnection();
      $newQuery = '';
      if($employeeId != '') {
        $newQuery = 'AND a.EMPLOYEE_ID ='.$employeeId;
      }
	  
	  /* $sql_emp="SELECT BANK_CODE FROM BANK WHERE BANK_ID='$bankId'";
			
			 $empid=$db->query($sql_emp);
	 $employee_ids = $empid->fetch_assoc();
  $code2= $employee_ids['BANK_CODE']; */
	//echo $code2;
  
  $sql = "SELECT DISTINCT(a.CUSTOMER_EMAIL),a.CUSTOMER_FNAME,a.CUSTOMER_LNAME,a.CUSTOMER_ID FROM CUSTOMER_RESPONSE a WHERE a.BANK_CODE='$bankId' $newQuery ORDER BY a.CUSTOMER_FNAME ASC";
      
	 // echo $sql;
      if ($result = $db->query($sql)) {
        $table_data = array();
        while($data = $result->fetch_assoc()) {
          $table_data[] = array(str_replace(" ","_",$data['CUSTOMER_LNAME'].", ".$data['CUSTOMER_FNAME'])=>sendCountJson($bankId, $branchId, $productId, $employeeId, $data['CUSTOMER_ID'], $fDate, $tDate));
		  
        }
        echo json_encode($table_data);
      } else {
        //echo $db->error;
      }
    } else {
      echo json_encode(sendCountJson($bankId, $branchId, $productId, $employeeId, $customerId, $fDate, $tDate));
    }
  }

}

function sendCountJson($bankId, $branchId, $productId, $employeeId, $customerId, $fDate, $tDate) {
  //echo '-<br/>'.$branchId.'-<br/>';
  //echo $bankId.' - '.$branchId.' - '.$productId.' - '.$employeeId.' - '.$customerId.' - '.$fDate.' - '.$tDate; exit;
 //$customerId='638';
 try {
    $db = getConnection();
	
    $topDateQuery = '';
    $leadDateQuery =  "";
    $betweenDateQuery = '';
    $betweenTypeQuery = '';
    $topTypeQuery = "a.BANK_CODE='$bankId'";
    $betweenTypeQuery = " AND z.BANK_CODE='$bankId'";
    $betweenleadQuery = " AND zn.BANK_CODE='$bankId'";
    if($bankId == '') {
      die;  
    }
    if($branchId != '') {
      $topTypeQuery .=   "AND a.BRANCH_ID='$branchId'";
      $betweenTypeQuery .=  " AND z.BRANCH_ID='$branchId'";
      $betweenleadQuery .=  " AND zn.BRANCH_ID='$branchId'";
    }
    if($productId != '') {
      $topTypeQuery .=  " AND a.PRODUCT_ID='$productId'";
      $betweenTypeQuery .=  " AND z.PRODUCT_ID='$productId'";
      $betweenleadQuery .=  " AND zn.PRODUCT_ID='$productId'";
    }
    if($employeeId != '') {
      $topTypeQuery .=  " AND a.EMPLOYEE_ID='$employeeId' ";
      $betweenTypeQuery .=  " AND z.EMPLOYEE_ID='$employeeId' ";
      $betweenleadQuery .=  " AND zn.EMPLOYEE_ID=$employeeId' ";
    }

    if($customerId != '') {
      $topTypeQuery .=  " AND a.CUSTOMER_EMAIL='$customerId'";
      $betweenTypeQuery .=  " AND z.CUSTOMER_ID='$customerId'";
      $betweenleadQuery .=  " AND zn.CUSTOMER_ID='$customerId'";
    }
    if($fDate != '') {
      $topDateQuery =  " AND a.CREATED_ON BETWEEN '".$fDate." 00:00:00'  AND '".$tDate." 23:59:59'";
      $betweenDateQuery =  " AND z.CREATED_ON BETWEEN '".$fDate." 00:00:00'  AND '".$tDate." 		23:59:59'";
      $dateRange =  " AND CREATED_ON BETWEEN '".$fDate." 00:00:00'  AND '".$tDate." 		23:59:59'";
      $leadDateQuery =  " AND zn.DATE_ADDED BETWEEN '".$fDate." 00:00:00'  AND '".$tDate." 		23:59:59'";
    }
	
	/* $sql_emp="SELECT BANK_CODE FROM BANK WHERE BANK_CODE='$bankId'";
			
			 $empid=$db->query($sql_emp);
	 $employee_ids = $empid->fetch_assoc();
  $code2= $employee_ids['BANK_CODE']; */
	

    $sql = "SELECT  
						(SELECT COUNT(z.BANK_CODE) AS COUNT_LEADS FROM CUSTOMER_RESPONSE z WHERE z.IS_LEAD='true' $betweenTypeQuery $betweenDateQuery) AS COUNT_LEADS,
						(SELECT SUM(zn.LEAD_TEXT) FROM LEADS zn WHERE zn.BANK_CODE!='' $betweenleadQuery $leadDateQuery) AS LEADS_AMOUNT,
						(SELECT COUNT(z.BANK_CODE) FROM CUSTOMER_RESPONSE z WHERE z.CAMPAIGN_TYPE='email' $betweenTypeQuery $betweenDateQuery) AS SEND_COUNT_EMAIL ,
						(SELECT COUNT(z.BANK_CODE) FROM CUSTOMER_RESPONSE z WHERE z.CAMPAIGN_TYPE='sms' $betweenTypeQuery $betweenDateQuery) AS SEND_COUNT_SMS ,
						(SELECT COUNT(z.BANK_CODE) FROM CUSTOMER_RESPONSE z WHERE z.CAMPAIGN_TYPE='voice' $betweenTypeQuery $betweenDateQuery) AS SEND_COUNT_VOICE,
						(SELECT COUNT(zn.BANK_CODE) FROM LEADS zn WHERE zn.BANK_CODE!='' $betweenleadQuery $leadDateQuery) AS LOAN_COUNT,
            (SELECT SUM(TARGET_AMOUNT) FROM TARGET WHERE BANK_CODE='$bankId' AND BRANCH_CODE='$branchId') AS TARGET_VALUE,
            (SELECT COUNT(BANK_CODE) FROM CUSTOMER_RESPONSE WHERE BANK_CODE='$bankId' AND CAMPAIGN_TYPE='email' $dateRange) AS TOTAL_VALUE_EMAIL,
            (SELECT COUNT(BANK_CODE) FROM CUSTOMER_RESPONSE WHERE BANK_CODE='$bankId' AND CAMPAIGN_TYPE='sms' $dateRange) AS TOTAL_VALUE_SMS,
            (SELECT COUNT(BANK_CODE) FROM CUSTOMER_RESPONSE WHERE BANK_CODE='$bankId' AND CAMPAIGN_TYPE='voice' $dateRange) AS TOTAL_VALUE_VOICE
						FROM CUSTOMER_RESPONSE a LIMIT 0,1";
						
						//echo $sql; echo "</br>"; exit;


    if ($result = $db->query($sql)) {
      $row = array();
      $i = 1;
      $table_data = array(0=>array("CAMPAIGN_TYPE" => "EMAIL",
                                   "CUSTOMER_RESPONSE" => "EMAIL",
                                   "SEND_COUNT" => 0.01),
                          1=>array("CAMPAIGN_TYPE" => "SMS",
                                   "CUSTOMER_RESPONSE" => "SMS",
                                   "SEND_COUNT" => 0.01),
                          2=>array("CAMPAIGN_TYPE" => "VOICE",
                                   "CUSTOMER_RESPONSE" => "VOICE",
                                   "SEND_COUNT" => 0.01),
                          3=>array("CAMPAIGN_TYPE" => "LEADS",
                                   "CUSTOMER_RESPONSE" => "LEADS",
                                   "SEND_COUNT" => 0.01),
                          4=>array("CAMPAIGN_TYPE" => "LOAN",
                                   "CUSTOMER_RESPONSE" => "LOAN",
                                   "SEND_COUNT" => 0.01),
                          5=>array("CAMPAIGN_TYPE" => "DEPOSITS",
                                   "CUSTOMER_RESPONSE" => "DEPOSITS",
                                   "SEND_COUNT" => 0.01),
                          6=>array("CAMPAIGN_TYPE" => "LOANAMT",
                                   "CUSTOMER_RESPONSE" => "LOANAMT",
                                   "SEND_COUNT" => 0),
                          7=>array("CAMPAIGN_TYPE" => "TARGETAMT",
                                   "CUSTOMER_RESPONSE" => "TARGETAMT",
                                   "SEND_COUNT" => 0),
                          8=>array("CAMPAIGN_TYPE" => "TOTALAMT",
                                   "CUSTOMER_RESPONSE" => "TOTALAMT",
                                   "SEND_COUNT" => 0),
                          9=>array("CAMPAIGN_TYPE" => "TOTALAMT",
                                   "CUSTOMER_RESPONSE" => "TOTALAMT",
                                   "SEND_COUNT" => 0),
                          10=>array("CAMPAIGN_TYPE" => "TOTALAMT",
                                    "CUSTOMER_RESPONSE" => "TOTALAMT",
                                    "SEND_COUNT" => 0)); 
      while($data = $result->fetch_assoc()) {
        if($data['SEND_COUNT_EMAIL'] == 0) $data['SEND_COUNT_EMAIL'] = 0.01;
        if($data['SEND_COUNT_SMS'] == 0) $data['SEND_COUNT_SMS'] = 0.01;
        if($data['SEND_COUNT_VOICE'] == 0) $data['SEND_COUNT_VOICE'] = 0.01; 
        if($data['COUNT_LEADS'] == 0) $data['COUNT_LEADS'] = 0.01; 
        if($data['LOAN_COUNT'] == 0) $data['LOAN_COUNT'] = 0.01; 
        if($data['TARGET_VALUE'] == 0) $data['TARGET_VALUE'] = 0.01;   
        if($data['LEADS_AMOUNT'] == null) $data['LEADS_AMOUNT'] = 0.01; 
        if($data['TOTAL_VALUE_EMAIL'] == null) $data['TOTAL_VALUE_EMAIL'] = 0.01; 
        if($data['TOTAL_VALUE_SMS'] == null) $data['TOTAL_VALUE_SMS'] = 0.01; 
        if($data['TOTAL_VALUE_VOICE'] == null) $data['TOTAL_VALUE_VOICE'] = 0.01; 

        $table_data = array(0=>array("CAMPAIGN_TYPE" => "EMAIL",
                                     "CUSTOMER_RESPONSE" => "EMAIL",
                                     "SEND_COUNT" => $data['SEND_COUNT_EMAIL']),
                            1=>array("CAMPAIGN_TYPE" => "SMS",
                                     "CUSTOMER_RESPONSE" => "SMS",
                                     "SEND_COUNT" => $data['SEND_COUNT_SMS']),
                            2=>array("CAMPAIGN_TYPE" => "VOICE",
                                     "CUSTOMER_RESPONSE" => "VOICE",
                                     "SEND_COUNT" => $data['SEND_COUNT_VOICE']),
                            3=>array("CAMPAIGN_TYPE" => "LEADS",
                                     "CUSTOMER_RESPONSE" => "LEADS",
                                     "SEND_COUNT" =>  $data['COUNT_LEADS']),
                            4=>array("CAMPAIGN_TYPE" => "LOAN",
                                     "CUSTOMER_RESPONSE" => "LOAN",
                                     "SEND_COUNT" => $data['LOAN_COUNT']),
                            5=>array("CAMPAIGN_TYPE" => "DEPOSITS",
                                     "CUSTOMER_RESPONSE" => "DEPOSITS",
                                     "SEND_COUNT" => 0.01),
                            6=>array("CAMPAIGN_TYPE" => "LOANAMT",
                                     "CUSTOMER_RESPONSE" => "LOANAMT",
                                     "SEND_COUNT" => $data['LEADS_AMOUNT']),
                            7=>array("CAMPAIGN_TYPE" => "TARGETAMT",
                                     "CUSTOMER_RESPONSE" => "TARGETAMT",
                                     "SEND_COUNT" => $data['TARGET_VALUE']),
                            8=>array("CAMPAIGN_TYPE" => "TOTALAMT",
                                     "CUSTOMER_RESPONSE" => "TOTALAMT",
                                     "SEND_COUNT" => $data['TOTAL_VALUE_EMAIL']),
                            9=>array("CAMPAIGN_TYPE" => "TOTALAMT",
                                     "CUSTOMER_RESPONSE" => "TOTALAMT",
                                     "SEND_COUNT" => $data['TOTAL_VALUE_SMS']),
                            10=>array("CAMPAIGN_TYPE" => "TOTALAMT",
                                      "CUSTOMER_RESPONSE" => "TOTALAMT",
                                      "SEND_COUNT" => $data['TOTAL_VALUE_VOICE'])); 
      }
      return $table_data;

    } else {
      echo $db->error;
    }

  } catch(Exception $e) {
    echo '{"error":{"message":'. $e->getMessage() .'}}'; 
    echo '{"message":"Please Try Again Later"}'; 
  } 
}

/*dummy*/

function customerResponseBm() {
  $bankId = '';
  $branchId = '';
  $productId = '';
  $employeeId = '';
  $customerId = '';
  $fDate = '';
  $tDate = '';
  if(isset($_REQUEST['bankId'])) {
    $bankId = $_REQUEST['bankId'];
  }
  if(isset($_REQUEST['branchId'])) {
    $branchId = $_REQUEST['branchId'];
  }
  if(isset($_REQUEST['productId'])) {
    $productId = $_REQUEST['productId'];
  }
  if(isset($_REQUEST['employeeId'])) {
    $employeeId = $_REQUEST['employeeId'];
  }
  if(isset($_REQUEST['fromDate']) && isset($_REQUEST['toDate'])) {
    if($_REQUEST['fromDate'] != '' && $_REQUEST['toDate'] != '') {
      $fDate = explode("/",$_REQUEST['fromDate']);
      $fDate = $fDate[2].'-'.$fDate[0].'-'.$fDate[1].' 00:00:00';
      $tDate = explode("/",$_REQUEST['toDate']);
      $tDate = $tDate[2].'-'.$tDate[0].'-'.$tDate[1].' 23:59:59';
    }
  }
  if(isset($_REQUEST['listing'])) {
    $listing = $_REQUEST['listing'];
    if($listing == 'product') {
      $db = getConnection();
	  

      $sql = "SELECT a.* FROM PRODUCT a WHERE a.BANK_ID='$bankId' ORDER BY a.PRODUCT_ID ASC";
      //echo $sql;
	    /*$sql_emp="SELECT BANK_CODE FROM BANK WHERE BANK_ID='$bankId'";
			
			 $empid=$db->query($sql_emp);
	 $employee_ids = $empid->fetch_assoc();
  $code2= $employee_ids['BANK_CODE']; */
  
  
      if ($result = $db->query($sql)) {
        $table_data = array();
        while($data = $result->fetch_assoc()) {
       $table_data[] = array(str_replace(" "," ",$data['PRODUCT_NAME'])=>sendCountJsonbm($bankId,$branchId,$data['PRODUCT_ID'], $employeeId, $customerId, $fDate, $tDate));
        }
        echo json_encode($table_data);
      }
      $result->close();
    } else if($listing == 'date' && ($fDate == '' || $tDate == '')) {
      $todayDate = date('m/d/Y');
      $table_data = array();
      for($i=10;$i>=0;$i--) {
        $userFmt = date('m/d/Y',strtotime($todayDate . "-".$i." days"));
        $plusNDate = date('Y-m-d',strtotime($todayDate . "-".$i." days"));
        $plusNDateStart = $plusNDate.' 00:00:00';
        $plusNDateEnd = $plusNDate.' 23:59:59';
        $table_data[] = array($userFmt=>sendCountJsonbm($bankId, $branchId, $productId, $employeeId, $customerId, $plusNDateStart, $plusNDateEnd));
      }
      echo json_encode($table_data);
    }  else if($listing == 'date' && $fDate != '' && $tDate != '') {
      $table_data = array();
      //Hyphen added in between two dates
      $table_data[] = array($_REQUEST['fromDate'].' - '.$_REQUEST['toDate']=>sendCountJsonbm($bankId, $branchId, $productId, $employeeId, $customerId, $fDate, $tDate));
      echo json_encode($table_data);
    } else if($listing == 'branch') {
      $db = getConnection();
	  $sql = "SELECT a.* FROM PRODUCT a ORDER BY a.PRODUCT_ID ASC";
      //echo $sql;
	   /* $sql_emp="SELECT BANK_CODE FROM BANK WHERE BANK_CODE='$bankId'";
			
			 $empid=$db->query($sql_emp);
	 $employee_ids = $empid->fetch_assoc();
  $code2= $employee_ids['BANK_CODE' ];*/
	 if ($_REQUEST['logRoleId'] == 3) {
	  $brID = $_REQUEST['logBranchId'];
      $sql = "SELECT a.* FROM BRANCHES a WHERE a.BANK_CODE='$bankId' AND a.BRANCH_CODE='$brId' ORDER BY a.BRANCH_CODE ASC";
	 } else if ($_REQUEST['logRoleId'] == 2) {
	  $loggedUserId = $_REQUEST['logid'];
	  $sql = "SELECT b.BRANCH_CODE,b.BRANCH_NAME 
				FROM BRANCH_INTEGRATION a 
				INNER JOIN BRANCHES b ON a.BRANCH_CODE=b.BRANCH_CODE 
				WHERE a.HEAD_OF_RETAIL='$loggedUserId'";
			
	 } else {
	  $sql = "SELECT a.* FROM BRANCHES a WHERE a.BANK_CODE='$bankId' AND a.BRANCH_CODE='$brId' ORDER BY a.BRANCH_CODE ASC";
	 }
	 //echo $sql;
	  
	  
      if ($result = $db->query($sql)) {
        $table_data = array();
        while($data = $result->fetch_assoc()) {
          $table_data[] = array(str_replace(" ","_",$data['BRANCH_NAME'])=>sendCountJsonbm($bankId, $data['BRANCH_CODE'], $productId, $employeeId, $customerId, $fDate, $tDate));
        }
        echo json_encode($table_data);
      } else {
		echo $db->error;
	  }
      $result->close();
    }  else if($listing == 'officer') {
      $db = getConnection();
	  
	 /*  $sql_emp="SELECT BANK_CODE FROM BANK WHERE BANK_ID='$bankId'";
			
			 $empid=$db->query($sql_emp);
	 $employee_ids = $empid->fetch_assoc();
  $code2= $employee_ids['BANK_CODE']; */
	  
      $sql = "SELECT a.* FROM EMPLOYEE a WHERE a.BANK_CODE='$bankId' AND a.BRANCH_CODE='$branchId' AND a.BRANCH_CODE != '' AND a.ROLE_ID=4 ORDER BY a.FIRST_NAME ASC";
	 //echo $sql;
      if ($result = $db->query($sql)) {
        $table_data = array();
        while($data = $result->fetch_assoc()) {
          $table_data[] = array("".$data['LAST_NAME'].", ".$data['FIRST_NAME'].""=>sendCountJsonbm($bankId, $branchId, $productId, $data['EMPLOYEE_ID'], $customerId, $fDate, $tDate));
        }
        echo json_encode($table_data);
      } else {
        //echo $db->error;
      }
    }  else if($listing == 'customer') {
		
      $db = getConnection();
      $newQuery = '';
      if($employeeId != '') {
        $newQuery = 'AND a.EMPLOYEE_ID ='.$employeeId;
      }
	  
	  /* $sql_emp="SELECT BANK_CODE FROM BANK WHERE BANK_ID='$bankId'";
			
			 $empid=$db->query($sql_emp);
	 $employee_ids = $empid->fetch_assoc();
  $code2= $employee_ids['BANK_CODE']; */
	//echo $code2;
  
  $sql = "SELECT DISTINCT(a.CUSTOMER_EMAIL),a.CUSTOMER_FNAME,a.CUSTOMER_ID FROM CUSTOMER_RESPONSE a WHERE a.BANK_CODE='$bankId' $newQuery ORDER BY a.CUSTOMER_FNAME ASC";
      //echo $sql;
      if ($result = $db->query($sql)) {
        $table_data = array();
        while($data = $result->fetch_assoc()) {
          $table_data[] = array(str_replace(" ","_",$data['CUSTOMER_FNAME'])=>sendCountJsonbm($bankId, $branchId, $productId, $employeeId, $data['CUSTOMER_EMAIL'], $fDate, $tDate));
		  
        }
        echo json_encode($table_data);
      } else {
        //echo $db->error;
      }
    } else {
      echo json_encode(sendCountJsonbm($bankId, $branchId, $productId, $employeeId, $customerId, $fDate, $tDate));
    }
  }

}

function sendCountJsonbm($bankId, $branchId, $productId, $employeeId, $customerId, $fDate, $tDate) {
  //echo '-<br/>'.$branchId.'-<br/>';
  //echo $bankId.' - '.$branchId.' - '.$productId.' - '.$employeeId.' - '.$customerId.' - '.$fDate.' - '.$tDate;
  try {
    $db = getConnection();
	
    $topDateQuery = '';
    $leadDateQuery =  "";
    $betweenDateQuery = '';
    $betweenTypeQuery = '';
    $topTypeQuery = "a.BANK_CODE='$bankId'";
    $betweenTypeQuery = " AND z.BANK_CODE='$bankId'";
    $betweenleadQuery = " AND zn.BANK_CODE='$bankId'";
    if($bankId == '') {
      die;  
    }
    if($branchId != '') {
      $topTypeQuery .=   "AND a.BRANCH_ID='$branchId'";
      $betweenTypeQuery .=  " AND z.BRANCH_ID='$branchId'";
      $betweenleadQuery .=  " AND zn.BRANCH_ID='$branchId'";
    }
    if($productId != '') {
      $topTypeQuery .=  " AND a.PRODUCT_ID='".$productId."'";
      $betweenTypeQuery .=  " AND z.PRODUCT_ID='".$productId."'";
      $betweenleadQuery .=  " AND zn.PRODUCT_ID='".$productId."'";
    }
    if($employeeId != '') {
      $topTypeQuery .=  " AND a.EMPLOYEE_ID=".$employeeId;
      $betweenTypeQuery .=  " AND z.EMPLOYEE_ID=".$employeeId;
      $betweenleadQuery .=  " AND zn.EMPLOYEE_ID=".$employeeId;
    }

    if($customerId != '') {
      $topTypeQuery .=  " AND a.CUSTOMER_EMAIL='".$customerId."'";
      $betweenTypeQuery .=  " AND z.CUSTOMER_EMAIL='".$customerId."'";
      $betweenleadQuery .=  " AND zn.CUSTOMER_ID='".$customerId."'";
    }
    if($fDate != '') {
      $topDateQuery =  " AND a.CREATED_ON BETWEEN '".$fDate." 00:00:00'  AND '".$tDate." 23:59:59'";
      $betweenDateQuery =  " AND z.CREATED_ON BETWEEN '".$fDate." 00:00:00'  AND '".$tDate." 		23:59:59'";
      $dateRange =  " AND CREATED_ON BETWEEN '".$fDate." 00:00:00'  AND '".$tDate." 		23:59:59'";
      $leadDateQuery =  " AND zn.DATE_ADDED BETWEEN '".$fDate." 00:00:00'  AND '".$tDate." 		23:59:59'";
    }
	
	/* $sql_emp="SELECT BANK_CODE FROM BANK WHERE BANK_CODE='$bankId'";
			
			 $empid=$db->query($sql_emp);
	 $employee_ids = $empid->fetch_assoc();
  $code2= $employee_ids['BANK_CODE']; */
	

    $sql = "SELECT  
						(SELECT COUNT(z.BANK_CODE) AS COUNT_LEADS FROM CUSTOMER_RESPONSE z WHERE z.IS_LEAD='true' $betweenTypeQuery $betweenDateQuery) AS COUNT_LEADS,
						(SELECT SUM(zn.LEAD_TEXT) FROM LEADS zn WHERE zn.BANK_CODE!='' $betweenleadQuery $leadDateQuery) AS LEADS_AMOUNT,
						(SELECT COUNT(z.BANK_CODE) FROM CUSTOMER_RESPONSE z WHERE z.CAMPAIGN_TYPE='email' $betweenTypeQuery $betweenDateQuery) AS SEND_COUNT_EMAIL ,
						(SELECT COUNT(z.BANK_CODE) FROM CUSTOMER_RESPONSE z WHERE z.CAMPAIGN_TYPE='sms' $betweenTypeQuery $betweenDateQuery) AS SEND_COUNT_SMS ,
						(SELECT COUNT(z.BANK_CODE) FROM CUSTOMER_RESPONSE z WHERE z.CAMPAIGN_TYPE='voice' $betweenTypeQuery $betweenDateQuery) AS SEND_COUNT_VOICE,
						(SELECT COUNT(zn.BANK_CODE) FROM LEADS zn WHERE zn.BANK_CODE!='' $betweenleadQuery $leadDateQuery) AS LOAN_COUNT,
            (SELECT SUM(TARGET_AMOUNT) FROM TARGET WHERE BANK_CODE='$bankId' AND BRANCH_ID='$branchId') AS TARGET_VALUE,
            (SELECT COUNT(BANK_CODE) FROM CUSTOMER_RESPONSE WHERE BANK_CODE='$bankId' AND CAMPAIGN_TYPE='email' $dateRange) AS TOTAL_VALUE_EMAIL,
            (SELECT COUNT(BANK_CODE) FROM CUSTOMER_RESPONSE WHERE BANK_CODE='$bankId' AND CAMPAIGN_TYPE='sms' $dateRange) AS TOTAL_VALUE_SMS,
            (SELECT COUNT(BANK_CODE) FROM CUSTOMER_RESPONSE WHERE BANK_CODE='$bankId' AND CAMPAIGN_TYPE='voice' $dateRange) AS TOTAL_VALUE_VOICE
						FROM CUSTOMER_RESPONSE a LIMIT 0,1";
						
						//echo $sql;


    if ($result = $db->query($sql)) {
      $row = array();
      $i = 1;
      $table_data = array(0=>array("CAMPAIGN_TYPE" => "EMAIL",
                                   "CUSTOMER_RESPONSE" => "EMAIL",
                                   "SEND_COUNT" => 0.01),
                          1=>array("CAMPAIGN_TYPE" => "SMS",
                                   "CUSTOMER_RESPONSE" => "SMS",
                                   "SEND_COUNT" => 0.01),
                          2=>array("CAMPAIGN_TYPE" => "VOICE",
                                   "CUSTOMER_RESPONSE" => "VOICE",
                                   "SEND_COUNT" => 0.01),
                          3=>array("CAMPAIGN_TYPE" => "LEADS",
                                   "CUSTOMER_RESPONSE" => "LEADS",
                                   "SEND_COUNT" => 0.01),
                          4=>array("CAMPAIGN_TYPE" => "LOAN",
                                   "CUSTOMER_RESPONSE" => "LOAN",
                                   "SEND_COUNT" => 0.01),
                          5=>array("CAMPAIGN_TYPE" => "DEPOSITS",
                                   "CUSTOMER_RESPONSE" => "DEPOSITS",
                                   "SEND_COUNT" => 0.01),
                          6=>array("CAMPAIGN_TYPE" => "LOANAMT",
                                   "CUSTOMER_RESPONSE" => "LOANAMT",
                                   "SEND_COUNT" => 0),
                          7=>array("CAMPAIGN_TYPE" => "TARGETAMT",
                                   "CUSTOMER_RESPONSE" => "TARGETAMT",
                                   "SEND_COUNT" => 0),
                          8=>array("CAMPAIGN_TYPE" => "TOTALAMT",
                                   "CUSTOMER_RESPONSE" => "TOTALAMT",
                                   "SEND_COUNT" => 0),
                          9=>array("CAMPAIGN_TYPE" => "TOTALAMT",
                                   "CUSTOMER_RESPONSE" => "TOTALAMT",
                                   "SEND_COUNT" => 0),
                          10=>array("CAMPAIGN_TYPE" => "TOTALAMT",
                                    "CUSTOMER_RESPONSE" => "TOTALAMT",
                                    "SEND_COUNT" => 0)); 
      while($data = $result->fetch_assoc()) {
        if($data['SEND_COUNT_EMAIL'] == 0) $data['SEND_COUNT_EMAIL'] = 0.01;
        if($data['SEND_COUNT_SMS'] == 0) $data['SEND_COUNT_SMS'] = 0.01;
        if($data['SEND_COUNT_VOICE'] == 0) $data['SEND_COUNT_VOICE'] = 0.01; 
        if($data['COUNT_LEADS'] == 0) $data['COUNT_LEADS'] = 0.01; 
        if($data['LOAN_COUNT'] == 0) $data['LOAN_COUNT'] = 0.01; 
        if($data['TARGET_VALUE'] == 0) $data['TARGET_VALUE'] = 0.01;   
        if($data['LEADS_AMOUNT'] == null) $data['LEADS_AMOUNT'] = 0.01; 
        if($data['TOTAL_VALUE_EMAIL'] == null) $data['TOTAL_VALUE_EMAIL'] = 0.01; 
        if($data['TOTAL_VALUE_SMS'] == null) $data['TOTAL_VALUE_SMS'] = 0.01; 
        if($data['TOTAL_VALUE_VOICE'] == null) $data['TOTAL_VALUE_VOICE'] = 0.01; 

        $table_data = array(0=>array("CAMPAIGN_TYPE" => "EMAIL",
                                     "CUSTOMER_RESPONSE" => "EMAIL",
                                     "SEND_COUNT" => $data['SEND_COUNT_EMAIL']),
                            1=>array("CAMPAIGN_TYPE" => "SMS",
                                     "CUSTOMER_RESPONSE" => "SMS",
                                     "SEND_COUNT" => $data['SEND_COUNT_SMS']),
                            2=>array("CAMPAIGN_TYPE" => "VOICE",
                                     "CUSTOMER_RESPONSE" => "VOICE",
                                     "SEND_COUNT" => $data['SEND_COUNT_VOICE']),
                            3=>array("CAMPAIGN_TYPE" => "LEADS",
                                     "CUSTOMER_RESPONSE" => "LEADS",
                                     "SEND_COUNT" =>  $data['COUNT_LEADS']),
                            4=>array("CAMPAIGN_TYPE" => "LOAN",
                                     "CUSTOMER_RESPONSE" => "LOAN",
                                     "SEND_COUNT" => $data['LOAN_COUNT']),
                            5=>array("CAMPAIGN_TYPE" => "DEPOSITS",
                                     "CUSTOMER_RESPONSE" => "DEPOSITS",
                                     "SEND_COUNT" => 0.01),
                            6=>array("CAMPAIGN_TYPE" => "LOANAMT",
                                     "CUSTOMER_RESPONSE" => "LOANAMT",
                                     "SEND_COUNT" => $data['LEADS_AMOUNT']),
                            7=>array("CAMPAIGN_TYPE" => "TARGETAMT",
                                     "CUSTOMER_RESPONSE" => "TARGETAMT",
                                     "SEND_COUNT" => $data['TARGET_VALUE']),
                            8=>array("CAMPAIGN_TYPE" => "TOTALAMT",
                                     "CUSTOMER_RESPONSE" => "TOTALAMT",
                                     "SEND_COUNT" => $data['TOTAL_VALUE_EMAIL']),
                            9=>array("CAMPAIGN_TYPE" => "TOTALAMT",
                                     "CUSTOMER_RESPONSE" => "TOTALAMT",
                                     "SEND_COUNT" => $data['TOTAL_VALUE_SMS']),
                            10=>array("CAMPAIGN_TYPE" => "TOTALAMT",
                                      "CUSTOMER_RESPONSE" => "TOTALAMT",
                                      "SEND_COUNT" => $data['TOTAL_VALUE_VOICE'])); 
      }
      return $table_data;

    } else {
      echo $db->error;
    }

  } catch(Exception $e) {
    echo '{"error":{"message":'. $e->getMessage() .'}}'; 
    echo '{"message":"Please Try Again Later"}'; 
  } 
}
/*dummy*/




function responseRate() {
  try {
    $db = getConnection();			
    $id = $_REQUEST['id'];
    $searchOn = $_REQUEST['searchOn'];
    $table_data = array();
    for($i=5;$i>=0;$i--) {
      $searchDate = date("Y-m-d 00:00:00", strtotime("-".$i." months"));
      //$searchMonth = date("M", strtotime("-".$i." months"));
      $searchMonth = date("M Y", strtotime("-".$i." months"));
      $sql = "SELECT DISTINCT(a.CUSTOMER_RESPONSE),(SELECT COUNT(z.CUSTOMER_RESPONSE) FROM CUSTOMER_RESPONSE z WHERE z.CUSTOMER_RESPONSE=a.CUSTOMER_RESPONSE AND MONTH(z.CREATED_ON)=MONTH('$searchDate') AND YEAR(z.CREATED_ON)=YEAR('$searchDate') AND z.EMPLOYEE_ID='$id' AND z.CAMPAIGN_TYPE='$searchOn') AS RESPONSE_COUNT FROM CUSTOMER_RESPONSE a WHERE a.CAMPAIGN_TYPE='$searchOn'";

      if ($result = $db->query($sql)) {
        while($data = $result->fetch_assoc()) {
          $data['RESPONSE_MONTH'] = $searchMonth;
          $data['RESPONSE_COUNT'] = (int)$data['RESPONSE_COUNT'];
          $table_data[] = $data;
        }

      } else {
        //echo $db->error;
      }
    }
    echo json_encode($table_data);
  } catch(Exception $e) {
    echo '{"error":{"message":'. $e->getMessage() .'}}'; 
    echo '{"message":"Please Try Again Later"}'; 
  }
}


function getConnection() {
  $dbhost='localhost';
    $dbuser ='sedev';
    $dbpass = 'p@$$w0rdSEDONA';
  $dbname='asic_antelope_dev';
  $mysqli_conn = new mysqli($dbhost, $dbuser, $dbpass, $dbname);
  if ($mysqli_conn->connect_errno) {
    die("Failed to connect to MySQL: (" . $mysqli_conn->connect_errno . ") " . $mysqli_conn->connect_error);
  }
  return $mysqli_conn;
}

function sampleapi() {

  echo '{"message":"Test"}'; 
 
}

function sso_authenticate() { 
  
  //$username = 'alex.nelson@gmail.com';// addslashes($_REQUEST['username']);
 // $password = '123'; // addslashes($_REQUEST['password']);
   $username = addslashes($_REQUEST['username']);
  //$password = addslashes($_REQUEST['password']);
   $sql = 'SELECT * FROM EMPLOYEE WHERE EMAIL=? AND STATUS="1"'; 
  try {
    $db = getConnection();
    $stmt = $db->prepare($sql);  
    $stmt->bind_param("s", $username);
    $stmt->execute();
    $stmt->store_result();
    if($stmt->num_rows == '1') {
      $stmt->close();
      $sql = "SELECT a.*,b.ROLE_NAME FROM EMPLOYEE a INNER JOIN ROLE b ON a.ROLE_ID=b.ROLE_ID WHERE a.EMAIL='$username'";
    
      if ($result = $db->query($sql)) {
        while ($row = $result->fetch_assoc()) {
          $flowDesignerMenu = '';
          $manageBankMenu = '';
          $manageBranchMenu = '';
          $manageEmployeeMenu = '';
          $managetemplateMenu = '';
          $manageProductMenu = '';
          $manageRoleMenu = '';
          $manageTargetMenu='';
          $endpoint = '';
          $manageBankadmin='';
          $row['accessibleURLs'] = '';
          if($row['ISFLOWDESIGNER'] == 1) {
            $flowDesignerMenu = '<li><a style="text-transform: capitalize !important" href="flow-page.html" ><i class="fa fa-cogs"></i>Flow Design<span class="fa fa-chevron-down"></span></a> </li>';
            $row['accessibleURLs'] .= 'flow-page,manage-customer,customer,';
          }
          if($row['MANAGE_BANK'] == 1) {
            $manageBankMenu = '<li><a style="text-transform: capitalize !important" href="manage-bank.html" data-pages="bank"><i class="fa fa-database"></i>Manage Banks <span class="fa fa-chevron-down"></span></a> </li>';
            $row['accessibleURLs'] .= ',manage-bank,bank,';
          }
          if($row['MANAGE_BRANCH'] == 1) {
            $manageBranchMenu = '<li><a style="text-transform: capitalize !important" href="manage-branch.html" data-pages="branch"><i class="fa fa-sitemap"></i>Manage Branches<span class="fa fa-chevron-down"></span></a> </li>';
            $row['accessibleURLs'] .= ',manage-branch,branch,';
          }
          if($row['MANAGE_EMPLOYEE'] == 1) {
            $manageEmployeeMenu = '<li><a style="text-transform: capitalize !important" href="manage-employee.html" data-pages="employee"><i class="fa fa-user"></i>Manage Employees<span class="fa fa-chevron-down"></span></a> </li>';
            $row['accessibleURLs'] .= ',manage-employee,employee,';
          }
          if($row['MANAGE_TEMPLATE'] == 1) {
            $managetemplateMenu = '<li><a style="text-transform: capitalize !important" href="manage_template.html" data-pages="template"><i class="fa fa-cube"></i>Manage Templates<span class="fa fa-chevron-down"></span></a> </li>';
            $row['accessibleURLs'] .= ',manage_template,template,';
          }
          if($row['MANAGE_PRODUCT'] == 1) {
            $manageProductMenu = '<li><a style="text-transform: capitalize !important" href="manage-product.html" data-pages="product"><i class="fa fa-tag"></i>Manage Products<span class="fa fa-chevron-down"></span></a> </li>';
            $row['accessibleURLs'] .= ',manage-product,product,';
          }
          if($row['MANAGE_TARGET'] == 1) {
            $manageTargetMenu = '<li><a style="text-transform: capitalize !important" href="manage_target.html" data-pages="role"><i class="fa fa-recycle"></i>Manage Targets<span class="fa fa-chevron-down"></span></a> </li>';
            $row['accessibleURLs'] .= ',manage_target,target,';
          }
          if($row['MANAGE_ROLE'] == 1) {
            $manageRoleMenu = '<li><a style="text-transform: capitalize !important" href="manage-role.html" data-pages="role"><i class="fa fa-recycle"></i>Manage Roles<span class="fa fa-chevron-down"></span></a> </li>';
            $row['accessibleURLs'] .= ',manage-role,role,';
          }

          if($row['MANAGE_BANK_ADMIN'] == 1) {
            $manageBankadmin = '<li><a style="text-transform: capitalize !important" href="manage-bank-admin.html" data-pages="branch"><i class="fa fa-sitemap"></i>Manage Bank Admin<span class="fa fa-chevron-down"></span></a> </li>';
            $row['accessibleURLs'] .= ',manage-bank-admin,bank-admin,';
          }
          if($row['ENDPOINT'] == 1) {
            $endpoint = '<li><a style="text-transform: capitalize !important" href="load-endpoint.html" data-pages="load-endpoint"><i class="fa fa-hourglass-end"></i>Endpoint<span class="fa fa-chevron-down"></span></a> </li>';
            $row['accessibleURLs'] .= ',load-endpoint,manage-employee,employee,';
           
          }

          $row['message'] = 'success';
          $row['TOPMENU'] = '<div class="top_nav">
                      <div class="nav_menu">
                      <nav class="" role="navigation">
                        <div class="nav toggle"> <a id="menu_toggle" class="alt_plus"><i class="fa fa-bars"></i></a> </div>
                        <ul class="nav navbar-nav navbar-right">
                        <li class=""> <a href="javascript:;" class="user-profile dropdown-toggle" data-toggle="dropdown" aria-expanded="false"><span>'.$row['ROLE_NAME'].'</span><span>&nbsp;|&nbsp;</span><span> '.$row['FIRST_NAME'].'</span><span class=" fa fa-cog"></span> </a>
                          <ul class="dropdown-menu dropdown-usermenu animated fadeInDown pull-right">
                          <li><a href="#" onclick="window.location.reload(true);"> Refresh</a> </li>
                          <li><a href="login.html"><i class="fa fa-sign-out pull-right"></i> Log Out</a> </li>
                          </ul>
                        </li>
                        </ul>
                      </nav>
                      </div>
                    </div>';
          if($row['ROLE_ID'] == '1') {
            $row['accessibleURLs'] = $row['accessibleURLs'];
            $row['dashboard'] = 'manage-bank.html';
            $row['SIDEBAR'] = '<div class="left_col scroll-view nice-scroll-test">
        <div class="navbar nav_title cutm_nav_title" style="border: 0;"><a href="#" class="site_title"><img src="images/logo/logo-3.PNG" alt="Logo" /></a> </div>
        <div class="clearfix"></div>
        <br />
         <div id="sidebar-menu" class="main_menu_side hidden-print main_menu">
          <div class="menu_section">
            <ul class="nav side-menu">
        '.$manageBankMenu.$manageBranchMenu.$manageEmployeeMenu.$managetemplateMenu.$manageProductMenu.$manageRoleMenu.$manageTargetMenu.$flowDesignerMenu.$manageBankadmin.$endpoint.'
            </ul>
          </div> 
        </div>
  </div>';
          } else if($row['ROLE_ID'] == '2') {
            $row['accessibleURLs'] = $row['accessibleURLs'].'head-of-retail';
            $row['dashboard'] = 'flow-page.html';
            $row['SIDEBAR'] = '<div class="left_col scroll-view nice-scroll-test">
        <div class="navbar nav_title cutm_nav_title" style="border: 0;"><span style="font-size:32px;color:#fff;text-align: center;padding-left: 62px;font-family: apple-system, BlinkMacSystemFont, "Segoe UI", Roboto, "Helvetica Neue", Arial, sans-serif, "Apple Color Emoji", "Segoe UI Emoji", "Segoe UI Symbol";">CLRM</span></div>
        <div class="clearfix"></div>
        <br />       
        <div id="sidebar-menu" class="main_menu_side hidden-print main_menu">
          <div class="menu_section">
            <ul class="nav side-menu">
              '.$manageBankMenu.$manageBranchMenu.$manageEmployeeMenu.$managetemplateMenu.$manageProductMenu.$manageRoleMenu.$manageTargetMenu.$flowDesignerMenu.$manageBankadmin.$endpoint.'
            </ul>
          </div>
        </div>
      </div>';
      //<li><a style="text-transform: capitalize !important" href="head-of-retail.html" ><i class="fa fa-area-chart"></i>Dashboard<span class="fa fa-chevron-down"></span></a> </li>

          } else if($row['ROLE_ID'] == '3') {
            $row['accessibleURLs'] = $row['accessibleURLs'].'bank-manager';
            $row['dashboard'] = 'bank-manager.html';
            $row['SIDEBAR'] = '<div class="left_col scroll-view nice-scroll-test">
        <div class="navbar nav_title cutm_nav_title" style="border: 0;"><a href="#" class="site_title"><img src="images/logo/logo-3.PNG" alt="Logo" /></a> </div>
        <div class="clearfix"></div>
        <br />
        <div id="sidebar-menu" class="main_menu_side hidden-print main_menu">
          <div class="menu_section">
            <ul class="nav side-menu">
              <li><a style="text-transform: capitalize !important" href="bank-manager.html"><i class="fa fa-area-chart"></i>Dashboard<span class="fa fa-chevron-down"></span></a> </li>
              '.$manageBankMenu.$manageBranchMenu.$manageEmployeeMenu.$managetemplateMenu.$manageProductMenu.$manageRoleMenu.$flowDesignerMenu.$manageTargetMenu.$manageBankadmin.$endpoint.'
            </ul>
          </div>
        </div>
      </div>';
          } else if($row['ROLE_ID'] == '4') {
            $row['accessibleURLs'] = $row['accessibleURLs'].'bank-officer,customer-touchpoints,manage-response,customer-campaigns';
            $row['dashboard'] = 'customer-campaigns.html';
            $row['SIDEBAR'] = '<div class="left_col scroll-view nice-scroll-test">
        <div class="navbar nav_title cutm_nav_title" style="border: 0;"><a href="#" class="site_title"><img src="images/logo/logo-3.PNG" alt="Logo" /></a> </div>
        <div class="clearfix"></div>
        <br />
        <div id="sidebar-menu" class="main_menu_side hidden-print main_menu">
          <div class="menu_section">
            <ul class="nav side-menu">
              <li><a style="text-transform: capitalize !important" href="customer-campaigns.html" data-pages="customer-touchpoints"><i class="fa fa-area-chart"></i>Dashboard<span class="fa fa-chevron-down"></span></a> </li>
        <li><a style="text-transform: capitalize !important" href="manage-response.html"><i class="fa fa-paper-plane-o"></i>Customer Responses<span class="fa fa-chevron-down"></span></a> </li>
        '.$manageBankMenu.$manageBranchMenu.$manageEmployeeMenu.$managetemplateMenu.$manageProductMenu.$manageRoleMenu.$flowDesignerMenu.$manageTargetMenu.$manageBankadmin.$endpoint.'
            </ul>
          </div>
        </div>
      </div>';
          } else if($row['ROLE_ID'] == '5') {
            $row['accessibleURLs'] = $row['accessibleURLs'].'endpoint,manage-employee,employee';
            $row['dashboard'] = 'load-endpoint.html';
            $row['SIDEBAR'] = '<div class="left_col scroll-view nice-scroll-test">
        <div class="navbar nav_title cutm_nav_title" style="border: 0;"><a href="#" class="site_title"><img src="images/logo/logo-3.PNG" alt="Logo" /></a> </div>
        <div class="clearfix"></div>
        <br />
        <div id="sidebar-menu" class="main_menu_side hidden-print main_menu">
          <div class="menu_section">
            <ul class="nav side-menu">'.$endpoint.$manageBranchMenu.$manageEmployeeMenu.$manageBankMenu.$managetemplateMenu.$manageProductMenu.$manageRoleMenu.$flowDesignerMenu.$manageTargetMenu.$manageBankadmin.'
            </ul>
          </div>
        </div>
      </div>';
          } else {
            $row['accessibleURLs'] = $row['accessibleURLs'].'';
            
            $row['dashboard'] = 'index.html';
            $row['SIDEBAR'] = '<div class="left_col scroll-view nice-scroll-test">
        <div class="navbar nav_title cutm_nav_title" style="border: 0;"><a href="#" class="site_title"><img src="images/logo/logo-3.PNG" alt="Logo" /></a> </div>
        <div class="clearfix"></div>
        <br />
        <div id="sidebar-menu" class="main_menu_side hidden-print main_menu">
          <div class="menu_section">
            <ul class="nav side-menu">
            </ul>
          </div>
        </div>
      </div>';
          }

          echo '{"USER": ' . json_encode($row) . '}';
          $db = null; die;
        }
      }
      $result->close();
      echo '{"USER": {"message":"You Entered Incorrect Username/Password"}}'; $db = null; die;
    } else {
      echo '{"USER": {"message":"You Entered Incorrect Username/PasswordLLLL"}}'; 
    }
  } catch(Exception $e) {
    echo '{"error":{"message":'. $e->getMessage() .'}}'; 
    //echo '{"message":"Please Try Again Later"}'; 
  }
}

?>