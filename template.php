<?php 
session_start();
$_SESSION['workbench']='uwb';

include ("views/header.php"); ?>
  <!-- Meta, title, CSS, favicons, etc. -->
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <title>Touch Point | Manage Template</title>
  <link rel="icon" href="images/logo/fprints_title.png">
  <script src="js/Angular/angular.min.js"></script>
  <script src="js/jquery.min.js"></script>
  <link href="css/bootstrap.min.css" rel="stylesheet">
  <link href="fonts/css/font-awesome.min.css" rel="stylesheet">
  <link href="css/animate.min.css" rel="stylesheet">
  <link href="css/test.css" rel="stylesheet">
<!--   <link href="css/custom.css" rel="stylesheet"> -->
  <link href="css/custom-2.css" rel="stylesheet">
  <link href="css/rwd.css" rel="stylesheet">  
  <link href="css/validation.css" rel="stylesheet">
  <!--[if lt IE 9]>
        <script src="../assets/js/ie8-responsive-file-warning.js"></script>
        <![endif]-->
  <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
  <!--[if lt IE 9]>
          <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
          <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
<![endif]-->
  <script src="js/angular-sanitize.js"></script>
  <link href="css/editor.css" rel="stylesheet">
  <!-- Template editor fontstyles -->
  <link href="css/editor/font/fontstyle.css" rel="stylesheet">
  <script src="js/pages/editor.js"></script>
  <script type="text/javascript" src="screenshot/html2canvas.js"></script>
</head>

<body class="nav-md" ng-app="touchpoint" ng-controller="tpCtrl">
  <div id="loader"></div>
  <div class="container body">
    <div class="main_container">
      <div class="col-md-3 left_col">
        <div id="sideMenu"></div>
      </div>

      <!-- top navigation -->
      <!-- <div id="topMenu"></div> -->
      <!-- /top navigation -->

      <!-- page content -->
      <div class="right_col" role="main">
        <div class="">
          <div class="x_panel">
            <div class="page-title">
              <div class="title_left">
                <!--<h3>Manage Template</h3>-->
              </div>

            </div>
            <div class="clearfix"></div>
            <div class="row">
              <div class="col-md-12 col-sm-12 col-xs-12">
                <div class="x_title">
                  <h2>{{title}}</h2>
                  <div class="clearfix"></div>
                </div>
                <div class="x_content">
                  <br />
                  <form id="demo-form2" class="form-horizontal form-label-left" novalidate name="branch">
                    <div class="form-group" style="display:none">
                      <label class="control-label col-md-3 col-sm-3 col-xs-12" for="bank_name">Bank<span class="required">*</span> </label>
                      <div class="col-md-6 col-sm-6 col-xs-12">
                        <input type="text" class="form-control" ng-model="banklist" disabled  name="banklist" style="border-color:#ddd !important; background:none; box-shadow:0 0 3px #ddd !important;" />

                        <span class="errormess"></span>
                      </div>
                    </div>
                    <div class="form-group">
                      <label class="control-label col-md-3 col-sm-3 col-xs-12" for="tmp_type">Template Type<span class="required">*</span> </label>
                      <div class="col-md-6 col-sm-6 col-xs-12">
                        <select class="form-control" ng-options="t.TEMPLATE_TYPE for t in tmptypelistoptions track by t.TEMPLATE_TYPE" ng-model="tmp_type" required name="tmp_type" style="border-color:#ddd !important; box-shadow:0 0 3px #ddd !important;text-transform:capitalize">
                          <option value="" disabled>Select Template Type</option>
                        </select>

                      </div>
                    </div>
                    <div class="form-group">
                      <label class="control-label col-md-3 col-sm-3 col-xs-12" for="tmp_name">Template Name<span class="required">*</span> </label>
                      <div class="col-md-6 col-sm-6 col-xs-12">
                        <input type="text" id="tmp_name" placeholder="Template Name" required class="form-control col-md-7 col-xs-12" ng-model="tmp_name" name="tmp_name">
                        <span class="errormess"></span>
                      </div>
                    </div>

                    <div class="form-group">
                      <label class="control-label col-md-3 col-sm-3 col-xs-12 template-contet">Template Content</label>
                      <div class="col-md-6 col-sm-6 col-xs-12">
                        <div class="x_content">
                          <div class="container-fluid">
                            <div class="row">
                              <div class="container">
                                <div class="row">
                                  <div class="col-lg-12 nopadding">
                                    <textarea id="txtEditor" ng-model="tmp_content" placeholder="Template c ontent goes here">
                                    </textarea>
                                  </div>
                                </div>
                              </div>
                            </div>
                          </div>

                          <br />

                        </div>
                      </div>

                      <div class="form-group">
                        <div class="col-md-6 col-sm-6 col-xs-12 col-md-offset-3 move-center">
                          <button type="submit" class="btn btn-success" ng-click="validateForm()" ng-disabled="branch.tmp_name.$invalid">Submit</button>
                        <button type="submit" class="btn btn-primary" ng-click="manageTemplate()">Cancel</button>
						</div>
                      </div>
                  </form>
                  </div>
                </div>
              </div>
            </div>
          </div>
          <!-- /page content -->
        </div>
      </div>
      <!-- footer content -->
 <!--      <footer class="manualCapitalize">
        <a class="foot-link" href="#">
          <img src="images/logo/logo-3.PNG" alt="footer_logo">
        </a>
        &copy;&nbsp; Copyright <span id="dynamicYear"></span> All rights reserved.
      </footer> -->
      <!-- /footer content -->
    </div>
  </div>
  <!-- Modal -->
  <div id="alertModelBox" class="modal fade" role="dialog">
    <div class="modal-dialog">
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal">&times;</button>
          <h4 class="modal-title">Touchpoint</h4>
        </div>
        <div class="modal-body">
          <div class="form-group">
            <div class="col-md-12 col-sm-12 col-xs-12">
              <div class="x_content">
                <div class="container-fluid">
                  <div class="row">
                    <div class="container">
                      <div class="row">
                        <div class="col-lg-12 nopadding">
                          <p>{{alertMessage}}</p>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-default" data-dismiss="modal" id="closeTempEditor">Close</button>
        </div>
      </div>
    </div>
  </div>
  <div data-toggle="modal" data-target="#alertModelBox" id="alertModelBoxBtn"></div>
  <div id="screenshot" style="visibility:hidden;display:none"></div>
  <script src="js/config.js" type="text/javascript"></script>
  <script src="js/bootstrap.min.js"></script>
  <script src="js/nicescroll/jquery.nicescroll.min.js"></script>
  <script src="js/custom.js"></script>
  <script src="js/pages/template.js" type="text/javascript"></script>
<?php include ("views/footer.php");?>